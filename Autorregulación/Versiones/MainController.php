<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\{Entrada,Establecimiento,Newsletter,Noticia,Tipos_de_establecimiento};
use App\User;
use Illuminate\Support\Facades\DB;

class MainController extends Controller
{
    function home(Request $request){
        $faq=Entrada::where('tipo','1')->where('estado','1')->get();
        $tip=Entrada::where('tipo','2')->where('estado','1')->get();
        $te=Tipos_de_establecimiento::all();
        
        $barrios=DB::table('barrios')->orderBy('NombreBarrio','asc')->get();
        $localidades=DB::table('localidades')->orderBy('NombreLocalidad','asc')->get();
        $upzs=DB::table('upz')->orderBy('NombreUPZ','asc')->get();

        $msg=$request->session()->pull('msg',null);
        $access=$request->session()->get('access',null);

        $menu=Entrada::where('tipo','15')->where('estado','1')->get();

        $slider=Entrada::where('tipo','12')->where('estado','1')->get();

        $bloque1=Entrada::find(96);
        $bloque2=Entrada::find(97);
        $bloque3=Entrada::find(98);

        $logos1= Entrada::find(143);
        $logos2= Entrada::find(144);
        return view('home',['faq'=>$faq,'te'=>$te,'tip'=>$tip,'msg'=>$msg,'menu'=>$menu,'sliders'=>$slider,'access'=>$access,
        'barrios'=>$barrios,'localidades'=>$localidades,'upzs'=>$upzs,'bloque1'=>$bloque1,'bloque2'=>$bloque2,'bloque3'=>$bloque3,'logo1'=>$logos1,'logo2'=>$logos2]);
    }
    
    function accesible(Request $request){
        $access=$request->session()->pull('access',null);
        if($access==null){
            $request->session()->put('access','active');
        }
        return redirect("/"); 
    }

    function register(Request $request){
        $faq=Entrada::where('tipo','1')->where('estado','1')->get();
        $tip=Entrada::where('tipo','2')->where('estado','1')->get();
        $te=Tipos_de_establecimiento::all();

        $tipo=$request->input('tipo');
        $nombre=$request->input('nombre');
        $correo=$request->input('correo');
        $telefono=$request->input('telefono');

        //dd('registrando');
        $news=new Newsletter;
        
        $news->id_tipo_establecimiento=$tipo;
        $news->name=$nombre;
        $news->correo_e=$correo;
        $news->telefono=$telefono;
        $news->save();
        $msg="";
        if($news->id){
             $msg="Te has inscrito correctamente a nuestro newsletter, espera actualizaciones y más noticias en tu bandeja de correo!";
        }else{
             $msg="Error, intente más tarde.";
        }
        $request->session()->put('msg',$msg);
        return redirect("/"); 
    }

    function busqueda(Request $request){
        $term=$request->input('term',"");
        $faq=Entrada::where('tipo','1')->where('estado','1')->get();
        $tip=Entrada::where('tipo','2')->where('estado','1')->get();
        $te=Tipos_de_establecimiento::all();
        $access=$request->session()->get('access',null);
        $noticias=Noticia::where('estado', 1)->take(10)->get();

        $entradas=Entrada::where('enunciado','like',"%$term%")->get();
        $noticiasres=Noticia::where('titulo','like',"%$term%")->get();

        $menu=Entrada::where('tipo','15')->where('estado','1')->get();
        $slider=Entrada::where('tipo','13')->where('estado','1')->get();
        $logos1= Entrada::find(143);
        $logos2= Entrada::find(144);
        return view('results',['faq'=>$faq,'te'=>$te,'tip'=>$tip,'menu'=>$menu,'sliders'=>$slider,'access'=>$access,'noticias'=>$noticias
        ,'noticiasres'=>$noticiasres,'entradas'=>$entradas,'logo1'=>$logos1,'logo2'=>$logos2]);
    }
    

    function autorregulacion(Request $request){
        $faq=Entrada::where('tipo','1')->where('estado','1')->get();
        $tip=Entrada::where('tipo','2')->where('estado','1')->get();
        $te=Tipos_de_establecimiento::all();
        $msg=$request->session()->pull('msg',null);
        $access=$request->session()->get('access',null);
        
        $menu=Entrada::where('tipo','15')->where('estado','1')->get();
        $slider=Entrada::where('tipo','13')->where('estado','1')->get();
        $bloque1=Entrada::find(99);
        $bloque2=Entrada::find(100);
        $bloque3=Entrada::find(101);
        $bloque4=Entrada::find(102);
        $pasosbotones=Entrada::where('tipo','20')->get();
        $pasosbloques=Entrada::where('tipo','22')->get();
        $logos1= Entrada::find(143);
        $logos2= Entrada::find(144);
        return view('autorregulacion',['faq'=>$faq,'te'=>$te,'tip'=>$tip,'menu'=>$menu,'sliders'=>$slider,'access'=>$access,'bloque1'=>$bloque1,'bloque2'=>$bloque2,'bloque3'=>$bloque3,'bloque4'=>$bloque4,'pasosbotones'=>$pasosbotones,'pasosbloques'=>$pasosbloques,'logo1'=>$logos1,'logo2'=>$logos2]);
    }

    function registrate(Request $request){
        $faq=Entrada::where('tipo','1')->where('estado','1')->get();
        $tip=Entrada::where('tipo','2')->where('estado','1')->get();
        $te=Tipos_de_establecimiento::all();
        $access=$request->session()->get('access',null);
        $listas=Entrada::where('tipo','3')->where('estado','1')->get();
        $slider=Entrada::where('tipo','13')->where('estado','1')->get();
        $menu=Entrada::where('tipo','15')->where('estado','1')->get();

        $bloque1=Entrada::find(103);
        $bloque2=Entrada::find(104);
        $bloque3=Entrada::find(105);
        $bloque4=Entrada::find(106);
        $bloque5=Entrada::find(107);
        $bloque6=Entrada::find(108);

        $logos1= Entrada::find(143);
        $logos2= Entrada::find(144);

        $triada= Entrada::where('tipo','24')->get();

        return view('registrate',['triada'=>$triada,'listas'=>$listas,'faq'=>$faq,'te'=>$te,'tip'=>$tip,'menu'=>$menu,'sliders'=>$slider,'access'=>$access,'bloque1'=>$bloque1,'bloque2'=>$bloque2,'bloque3'=>$bloque3,'bloque4'=>$bloque4,'bloque5'=>$bloque5,'bloque6'=>$bloque6,'logo1'=>$logos1,'logo2'=>$logos2]);
    }

    function buenpropietario(Request $request){
        $faq=Entrada::where('tipo','1')->where('estado','1')->get();
        $tip=Entrada::where('tipo','2')->where('estado','1')->get();
        $te=Tipos_de_establecimiento::all();
        $access=$request->session()->get('access',null);
        $buenos=Entrada::where('tipo','4')->where('estado','1')->get();
        $ejemplos=Entrada::where('tipo','5')->where('estado','1')->get();
        $stra=Entrada::where('tipo','6')->where('estado','1')->get();
        $docs=Entrada::where('tipo','7')->where('estado','1')->get();
        $slider=Entrada::where('tipo','13')->where('estado','1')->get();
        $menu=Entrada::where('tipo','15')->where('estado','1')->get();

        $bloque1=Entrada::find(109);
        $bloque2=Entrada::find(110);
        $bloque3=Entrada::find(111);

        $logos1= Entrada::find(143);
        $logos2= Entrada::find(144);

        return view('buenpropietario',['docs'=>$docs,'stra'=>$stra,'ejemplos'=>$ejemplos,'buenos'=>$buenos,'faq'=>$faq,'te'=>$te,'tip'=>$tip,'menu'=>$menu,'sliders'=>$slider,'access'=>$access,'bloque1'=>$bloque1,'bloque2'=>$bloque2,'bloque3'=>$bloque3,'logo1'=>$logos1,'logo2'=>$logos2]);
    }

    function novedades(Request $request){
        $faq=Entrada::where('tipo','1')->where('estado','1')->get();
        $tip=Entrada::where('tipo','2')->where('estado','1')->get();
        $te=Tipos_de_establecimiento::all();
        $barrios=DB::table('barrios')->orderBy('NombreBarrio','asc')->get();
        $localidades=DB::table('localidades')->orderBy('NombreLocalidad','asc')->get();
        $upzs=DB::table('upz')->orderBy('NombreUPZ','asc')->get();
        $access=$request->session()->get('access',null);
        $noticias=Noticia::where('estado', 1)->take(10)->get();
        $slider=Entrada::where('tipo','13')->where('estado','1')->get();
        $menu=Entrada::where('tipo','15')->where('estado','1')->get();

        $bloque1=Entrada::find(112);
        $bloque2=Entrada::find(113);
        $bloque3=Entrada::find(114);

        $logos1= Entrada::find(143);
        $logos2= Entrada::find(144);

        return view('novedades',['faq'=>$faq,'te'=>$te,'tip'=>$tip,'menu'=>$menu,'sliders'=>$slider,'access'=>$access,'noticias'=>$noticias,'barrios'=>$barrios,'localidades'=>$localidades,'upzs'=>$upzs,'bloque1'=>$bloque1,'bloque2'=>$bloque2,'bloque3'=>$bloque3,'logo1'=>$logos1,'logo2'=>$logos2]);
    }
    function noticia($id, Request $request){
        $faq=Entrada::where('tipo','1')->where('estado','1')->get();
        $tip=Entrada::where('tipo','2')->where('estado','1')->get();
        $te=Tipos_de_establecimiento::all();
        $barrios=DB::table('barrios')->orderBy('NombreBarrio','asc')->get();
        $localidades=DB::table('localidades')->orderBy('NombreLocalidad','asc')->get();
        $upzs=DB::table('upz')->orderBy('NombreUPZ','asc')->get();
        $access=$request->session()->get('access',null);
        $not=Noticia::find($id);
        $slider=Entrada::where('tipo','13')->where('estado','1')->get();
        $menu=Entrada::where('tipo','15')->where('estado','1')->get();

        $logos1= Entrada::find(143);
        $logos2= Entrada::find(144);

        return view('noticia',['faq'=>$faq,'te'=>$te,'tip'=>$tip,'menu'=>$menu,'sliders'=>$slider,'access'=>$access,'not'=>$not,'barrios'=>$barrios,'localidades'=>$localidades,'upzs'=>$upzs,'logo1'=>$logos1,'logo2'=>$logos2]);
   
    }

    function informe(Request $request){
        $faq=Entrada::where('tipo','1')->where('estado','1')->get();
        $tip=Entrada::where('tipo','2')->where('estado','1')->get();
        $te=Tipos_de_establecimiento::all();
        $access=$request->session()->get('access',null);

        $menu=Entrada::where('tipo','15')->where('estado','1')->get();
        $slider=Entrada::where('tipo','13')->where('estado','1')->get();

        $bloque1=Entrada::find(115);
        $bloque2=Entrada::find(116);
        $bloque3=Entrada::find(117);
        $bloque4=Entrada::find(118);
        $bloque5=Entrada::find(119);
        $bloque6=Entrada::find(120);
        $bloque7=Entrada::find(121);

        $logos1= Entrada::find(143);
        $logos2= Entrada::find(144);

        return view('informe',['faq'=>$faq,'te'=>$te,'tip'=>$tip,'menu'=>$menu,'sliders'=>$slider,'access'=>$access,'bloque1'=>$bloque1,'bloque2'=>$bloque2,'bloque3'=>$bloque3,'bloque4'=>$bloque4,'bloque5'=>$bloque5,'bloque6'=>$bloque6,'bloque7'=>$bloque7,'logo1'=>$logos1,'logo2'=>$logos2]);
    }


    function search_for_establecimiento(Request $request){
        $term=$request->input('term',null);

        $tf1=$request->input('tf1',null);
        $tf2=$request->input('tf2',null);
        $tf3=$request->input('tf3',null);
        $tf4=$request->input('tf4',null);

        
        //$est=DB::connection('sivigila')->table('establecimientos');
        if(empty($term)&&empty($tf1)&&empty($tf2)&&empty($tf3)&&empty($tf4)  ){
            echo "error";
        }else{
			$serverNAME="172.16.0.227\VDBSQLA, 46782";
			$connectionInfo=array("Database"=>"SIVIGILA","UID"=>"usrApp_Autorregulacion","PWD"=>"Autorregulacion2019$$");
			$con=sqlsrv_connect($serverNAME,$connectionInfo);
		
			
            // usar la conección con sqlsrv_connect que ya sirve.!!!
            // $con = sqlsrv_connect($serverName,$connectionInfo);
            
            if($con=== false){
                echo "error";
            }else{
                // set charset solo para mysql
                $con->set_charset("utf8");
                // ---------------------------

                $query="SELECT * FROM dbo.View_EstabConcepFavorable";
                $first=true;
                if($term){
                    if($first){
                        $first=false;
                        $query.=" WHERE";
                    }
                    $query.=" NombreComercial like '%$term%'";
                }
                if($tf1){
                    if($first){
                        $first=false;
                        $query.=" WHERE ";
                    }else{  $query.=" AND ";}
                    $query.="CodTipoEstablecimiento = $tf1";
                }
                if($tf2){
                    if($first){
                        $first=false;
                        $query.=" WHERE ";
                    }else{  $query.=" AND ";}
                    $query.=" CodLocalidad = $term";
                }
                if($tf3){
                    if($first){
                        $first=false;
                        $query.=" WHERE ";
                    }else{  $query.=" AND ";}
                    $query.=" CodBarrio like '%$tf3%'";
                }
                if($tf4){
                    if($first){
                        $first=false;
                        $query.=" WHERE ";
                    }else{  $query.=" AND ";}
                    $query.=" codUPZ = $tf4";
                }
                // ejecución solo para mysql******************************
                //die($query);
                if($result=$con->query($query)){
                    $array = array();
                    while($row = $result->fetch_assoc() ){
                        $array[] = ($row);
                    }
                    //var_dump($array);
                    $result->close();
                    $con->close();
                    echo json_encode($array);
                }else{
                    echo "error:".$con->error;
                } 
                /* Ejecución usando sqlSRV ... No probada aun ******************************
                    $stmt = sqlsrv_query($con,$query);
                    if($stmt===false){
                        dd(print_r(sqlsrv_errors(),true) );
                    }else{
                        $array = array();
                        while($row = sqlsrv_fetch_array($stmt) ){
                            $array[] = ($row);
                        }
                        //var_dump($array);
                        $result->close();
                        $con->close();
                        echo json_encode($array);
                    }
                    *******************************************************************+
                */
                
            }

            /* Ejecución usando modelos de Laravel y Eloquent
            return $est->when($term,function($query,$term){
                return $query->where('NombreComercial','like',"%$term%");
            })->when($tf1,function($query,$tf1){
                return $query->where('CodTipoEstablecimiento',$tf1);
            })->when($tf2,function($query,$tf2){
                return $query->where('CodLocalidad',$tf2);
            })->when($tf3,function($query,$tf3){
                return $query->where('CodBarrio','like',"%$tf3%");
            })->when($tf4,function($query,$tf4){
                return $query->where('codUPZ',$tf4);
            })->take(20)->get();*/   
        }
    }


    
    
    function restaurantes(Request $request){
        $faq=Entrada::where('tipo','1')->where('estado','1')->get();
        $tip=Entrada::where('tipo','2')->where('estado','1')->get();
        $te=Tipos_de_establecimiento::all();
        $access=$request->session()->get('access',null);

        $normativas=Entrada::where('tipo','8')->where('estado','1')->get();
        $requerimientos=Entrada::where('tipo','16')->where('estado','1')->get();
        $menu=Entrada::where('tipo','15')->where('estado','1')->get();
        $slider=Entrada::where('tipo','13')->where('estado','1')->get();

        $bloque1=Entrada::find(122);
        $bloque2=Entrada::find(123);

        $pasosbotones=Entrada::where('tipo','20')->get();
        $pasosmodales=Entrada::where('tipo','25')->get();

        $logos1= Entrada::find(143);
        $logos2= Entrada::find(144);
        $triada= Entrada::where('tipo','24')->get();
        return view('trestaurantes',['triada'=>$triada,'docs'=>$normativas,'requerimientos'=>$requerimientos,'faq'=>$faq,'te'=>$te,'tip'=>$tip,'menu'=>$menu,'sliders'=>$slider,'access'=>$access,'bloque1'=>$bloque1,'bloque2'=>$bloque2,'pasosbotones'=>$pasosbotones,'pasosmodales'=>$pasosmodales,'logo1'=>$logos1,'logo2'=>$logos2]);
    }
    function carnicerias(Request $request){
        $faq=Entrada::where('tipo','1')->where('estado','1')->get();
        $tip=Entrada::where('tipo','2')->where('estado','1')->get();
        $te=Tipos_de_establecimiento::all();
        $access=$request->session()->get('access',null);

        $normativas=Entrada::where('tipo','10')->where('estado','1')->get();
        $requerimientos=Entrada::where('tipo','19')->where('estado','1')->get();
        $menu=Entrada::where('tipo','15')->where('estado','1')->get();
        $slider=Entrada::where('tipo','13')->where('estado','1')->get();

        $bloque1=Entrada::find(124);

        $pasosbotones=Entrada::where('tipo','20')->get();
        $pasosmodales=Entrada::where('tipo','26')->get();

        $logos1= Entrada::find(143);
        $logos2= Entrada::find(144);
        $triada= Entrada::where('tipo','24')->get();
        return view('tcarnicerias',['triada'=>$triada,'docs'=>$normativas,'requerimientos'=>$requerimientos,'faq'=>$faq,'te'=>$te,'tip'=>$tip,'menu'=>$menu,'sliders'=>$slider,'access'=>$access,'bloque1'=>$bloque1,'pasosbotones'=>$pasosbotones,'pasosmodales'=>$pasosmodales,'logo1'=>$logos1,'logo2'=>$logos2]);
    }
    function droguerias(Request $request){
        $faq=Entrada::where('tipo','1')->where('estado','1')->get();
        $tip=Entrada::where('tipo','2')->where('estado','1')->get();
        $te=Tipos_de_establecimiento::all();
        $access=$request->session()->get('access',null);

        $normativas=Entrada::where('tipo','9')->where('estado','1')->get();
        $requerimientos=Entrada::where('tipo','17')->where('estado','1')->get();
        $menu=Entrada::where('tipo','15')->where('estado','1')->get();
        $slider=Entrada::where('tipo','13')->where('estado','1')->get();
        $bloque1=Entrada::find(125);

        $pasosbotones=Entrada::where('tipo','20')->get();
        $pasosmodales=Entrada::where('tipo','27')->get();

        $logos1= Entrada::find(143);
        $logos2= Entrada::find(144);
        $triada= Entrada::where('tipo','24')->get();
        return view('tdroguerias',['triada'=>$triada,'docs'=>$normativas,'requerimientos'=>$requerimientos,'faq'=>$faq,'te'=>$te,'tip'=>$tip,'menu'=>$menu,'sliders'=>$slider,'access'=>$access,'bloque1'=>$bloque1,'pasosbotones'=>$pasosbotones,'pasosmodales'=>$pasosmodales,'logo1'=>$logos1,'logo2'=>$logos2]);
    }
    function salonesbelleza(Request $request){
        $faq=Entrada::where('tipo','1')->where('estado','1')->get();
        $tip=Entrada::where('tipo','2')->where('estado','1')->get();
        $te=Tipos_de_establecimiento::all();
        $access=$request->session()->get('access',null);

        $normativas=Entrada::where('tipo','11')->where('estado','1')->get();
        $requerimientos=Entrada::where('tipo','18')->where('estado','1')->get();
        $menu=Entrada::where('tipo','15')->where('estado','1')->get();
        $slider=Entrada::where('tipo','13')->where('estado','1')->get();

        $bloque1=Entrada::find(126);
        $bloque2=Entrada::find(127);

        $pasosbotones=Entrada::where('tipo','20')->get();
        $pasosmodales=Entrada::where('tipo','28')->get();

        $logos1= Entrada::find(143);
        $logos2= Entrada::find(144);
        $triada= Entrada::where('tipo','24')->get();
        return view('tbelleza',['triada'=>$triada,'docs'=>$normativas,'requerimientos'=>$requerimientos,'faq'=>$faq,'te'=>$te,'tip'=>$tip,'menu'=>$menu,'sliders'=>$slider,'access'=>$access,'bloque1'=>$bloque1,'bloque2'=>$bloque2,'pasosbotones'=>$pasosbotones,'pasosmodales'=>$pasosmodales,'logo1'=>$logos1,'logo2'=>$logos2]);
    }
    
}
