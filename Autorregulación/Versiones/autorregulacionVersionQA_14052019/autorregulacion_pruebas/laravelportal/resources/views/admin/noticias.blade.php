@extends('admin.layout',['logged'=>$logged,'tipos'=>$tipos])

@section('sectionTitle') Noticias @endsection
@section('content')
    {{--<form action="{{url()->current()}}" method="POST" >
        @csrf
        <div class="row">
            <div class="col-12 col-sm-6 col-lg-2">Buscar por Título</div>
        </div>
        <div class="row mt-2">
            <div class="col-12 col-sm-6 col-lg-2">
                <input name="term" type="text" class="form-control" placeholder="Término de búsqueda">
            </div>
            <div class="col-12 col-sm-6 col-lg-2">
                <button type="submit" class="btn btn-primary">Buscar</button>
            </div>
        </div>
    </form>--}} 
    <div class="my-4">
        <a href="{{url('/admin/nuevanoticia')}}"><button>Nueva Noticia</button></a>
    </div>
    <table class="table mt-4">
        <tr>
            <th>Título de Noticia</th>
            <th>Resumen</th>
            <th>Editar</th>
        </tr>
        @forelse( $results as $result)
        <tr>
            <td>{{$result->titulo}}</td>
            <td>{{$result->resumen}}</td>
            <td>
                <a href="{{url('/admin/editnoticia/'.$result->id)}}"><button><i class="fa fa-pencil-alt"></i></button></a>
            </td>
        </tr>
        @empty
        <tr>
            <td>-</td>
            <td>-</td>
            <td>-</td>
            <td>-</td>
        </tr>
        @endforelse    
    </table>
    @if(!empty($results))
    <div class="row mt-4">
        <div class="col-12">
            {{ $results->links()}}
        </div>
    </div>
    @endif
@endsection