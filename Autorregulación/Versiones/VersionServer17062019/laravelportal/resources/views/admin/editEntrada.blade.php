@extends('admin.layout',['logged'=>$logged,'tipos'=>$tipos])

@section('sectionTitle') Entrada @endsection
@section('content')
    <script src="{{url('/')}}/js/tinymce/tinymce.min.js"></script>
	<script type="text/javascript">
		var editor_config = {
        	path_absolute:"{{ URL::to('/') }}/",
        	selector:"textarea",
        	relative_urls: false,
            plugins: 'code'
        };
        tinymce.init(editor_config);
	</script>


<form action="{{url('/admin/saveentrada')}}" method="POST" enctype="multipart/form-data">
    @csrf
    <input name="ide" type="hidden" value="{{$result->id}}">
    <div class="form-group mt-2">
        <label for="">Enunciado</label>
        <input name="enunciado" type="text" class="form-control" value="{{$result->enunciado}}">
    </div>
    <div class="form-group mt-2">
        <label for="">Contenido</label>
        <textarea name="contenido" class="form-control" rows="3">{{$result->contenido}}</textarea>
    </div>
    <div class="row">
        <div class="col-12 col-md-6">
            <div class="form-group mt-2">
                <label for="">Imágen o archivo pdf</label>
                <input name="archivo" type="file">
            </div>
        </div>
        <div class="col-12 col-md-6">
            @if( !empty($result->archivo) )
                @php 
                    $extension=pathinfo($result->archivo,PATHINFO_EXTENSION);
                @endphp
                @if( $extension=="jpg"||$extension=="jpeg"||$extension=="png"||$extension=="svg" )
                    <img class="w-100" src="{{url('/'.$result->archivo) }}" alt="">
                @else
                    <a href="{{url('/'.$result->archivo)}}">ver</a>
                @endif
            @endif
        </div>
    </div>
    
    <div class="form-group mt-2">
        <label for="">Tipo de Entrada</label>
        <select class="form-control" name="tipo" id="tipo">
        @php 
            $selected=array(count($tipos));
            $i=0;
            for($i=0;$i<count($tipos);$i++){
                $selected[$i] = ' ';
            }
            if(!empty($result->tipo) ){
                $selected[$result->tipo-1]='selected';
            }
        @endphp
            @foreach($tipos as $t)
                <option value="{{$t->id}}" {{$selected[ $t->id-1 ]}}>{{$t->nombre}}</option>
            @endforeach
        </select>
    </div>
    <div class="row">
        <div class="col-3">
            <button type="submit">Guardar</button>
        </div>
        <div class="col-3">
                @if(!empty($result->id))
                    <a href="{{url('admin/toogleentrada/'.$result->id)}}">
                    @if($result->estado==0)
                        <button type="button">Activar</button>
                    @else
                        <button type="button">Desactivar</button>
                    @endif
                    </a>
                @endif
            </div>
    </div>
</form>

@endsection