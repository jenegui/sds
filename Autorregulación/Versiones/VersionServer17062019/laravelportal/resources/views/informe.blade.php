@extends('layout',['title' => 'Informes','faqs' => $faq,'te' => $te,'tips' => $tip,'access' => $access,'logos'=>$logos ])
@section('content')
        <div class="breadcrumb">
            <ul>
                <li><a href="{{url('/')}}">Inicio</a></li>
                <li><a href="{{url('/informe')}}">Envía tu informe</a></li>
            </ul>      
        </div>
        <div class="home_slider">
            <div id="subsideSlider" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    @php
                        $activo="active";
                    @endphp
                    @foreach($sliders as $slide)
                        <div class="carousel-item {{$activo}}">
                            <div class="container1">
                                <a href="{{url('/'.$slide->contenido)}}">
                                    <img class="w-100" src="{{$slide->archivo}}" alt="">
                                </a>
                            </div>
                        </div>
                        @php
                            $activo="";
                        @endphp
                    @endforeach
                </div>
                <ol class="carousel-indicators">
                    @php
                        $activo="active";
                        $i=0;
                    @endphp
                    @foreach($sliders as $slide)
                        <li data-target="#subsideSlider" data-slide-to="{{$i}}" class="{{$activo}}">
                            <span></span>                        
                        </li>
                        @php
                            $activo="";
                            $i++;
                        @endphp
                    @endforeach
                </ol>
            </div>
        </div>

        <div class="row block right">
            <div class="col-12 col-md-5 text-center">
                <img class="w-60 mx-auto my-5" src="{{url('/'.$bloque1->archivo)}}" alt="alternative text">
            </div>
            <div class="col-12 col-md-7 pl-4">
                <div class="subtitle">
                   <h2>{!! $bloque1->enunciado !!}</h2>
                </div>
                <div class="paragraph">
                    {!! $bloque1->contenido !!}
                </div>
            </div>
        </div>


        <div class="row block right">
            <div class="col-12 col-md-7">
                <div class="subtitle">
                   <h2>{!! $bloque2->enunciado !!}</h2>
                </div>
                <div class="paragraph">
                    {!! $bloque2->contenido !!}
                </div>
                <div class="text-left">
                    <a target="_blank" href="{{ $bloque3->contenido }}"><button class="btn yellow">{!! $bloque3->enunciado !!}</button></a>
                </div>
            </div>
            <div class="col-12 col-md-5 text-center">
                <img class="w-80 mx-auto my-5" src="{{url('/'.$bloque2->archivo)}}" alt="alternative text">
            </div>
        </div>
        

        <div class="row block right">
            <div class="col-12 col-md-5 text-center">
                <img class="w-50 mx-auto my-5" src="{{url('/'.$bloque4->archivo)}}" alt="alternative text">
            </div>
            <div class="col-12 col-md-7 pl-4">
                <div class="subtitle">
                    <h2>{!! $bloque4->enunciado !!}</h2>
                </div>
                <div class="paragraph">
                    {!! $bloque4->contenido !!}
                </div>
                <div class="text-left">
                    <a target="_blank" href="{{$bloque5->contenido}}"><button class="btn yellow">{!! $bloque5->enunciado !!}</button></a>
                </div>
            </div>
        </div>

        <div class="row block right">
            <div class="col-12 col-md-7">
                <div class="subtitle">
                    <h2>{!! $bloque6->enunciado !!}</h2>
                </div>
                <div class="paragraph">
                    {!! $bloque6->contenido !!}
                </div>
                <div class="text-left">
                    <a target="_blank" href="{{$bloque7->contenido}}"><button class="btn yellow">{!! $bloque7->enunciado !!}</button></a>
                </div>
            </div>
            <div class="col-12 col-md-5 text-center">
                <img class="w-70 mx-auto my-5" src="{{url('/'.$bloque6->archivo)}}" alt="alternative text">
            </div>
        </div>



        <div class="row subsections my-5">
            <ol>
                <li >
                    <div class="logo logo_rest"></div>
                    <div class="text">Restaurantes</div>
                    <div class="text-right mt-2">
                        <div class="btn yellow"><a href="{{url('/restaurantes')}}" >Ver requisitos</a></div>
                    </div>
                </li>
                <li>
                    <div class="logo logo_carne"></div>
                    <div class="text">Expendios de Carne</div>
                    <div class="text-right mt-2">
                        <div class="btn yellow"><a href="{{url('/carnicerias')}}" >Ver requisitos</a></div>
                    </div>
                </li>
                <li>
                    <div class="logo logo_drog"></div>
                    <div class="text">Droguerías</div>
                    <div class="text-right mt-2">
                        <div class="btn yellow"><a href="{{url('/droguerias')}}" >Ver requisitos</a></div>
                    </div>
                </li>
                <li>
                    <div class="logo logo_salon"></div>
                    <div class="text">Salones de belleza</div>
                    <div class="text-right mt-2">
                        <div class="btn yellow"><a href="{{url('/salonesbelleza')}}" >Ver requisitos</a></div>
                    </div>
                </li>
            </ol>
        </div>
@endsection
