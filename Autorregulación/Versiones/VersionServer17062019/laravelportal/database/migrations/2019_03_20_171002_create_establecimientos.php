<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEstablecimientos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('establecimientos', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('IdTBLEstablecimientos');
            $table->string('RazonSocial');
            $table->string('NombreComercial');
            $table->string('DireccionComercial');
            $table->string('CodLocalidad');
            $table->string('Localidad');
            $table->unsignedInteger('codUPZ');
            $table->string('NombreUPZ');
            $table->string('CodBarrio');
            $table->string('Barrio');
            $table->unsignedBigInteger('CodTipoEstablecimiento');
            $table->string('TipoEstablecimiento');
            $table->string('FechaVisita');
            $table->string('Concepto');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('establecimientos');
    }
}
