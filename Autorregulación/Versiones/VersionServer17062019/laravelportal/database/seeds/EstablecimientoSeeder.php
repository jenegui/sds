<?php

use Illuminate\Database\Seeder;
use App\Models\Establecimiento;

class EstablecimientoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Establecimiento::create([
            'IdTBLEstablecimientos'=>'399080',
            'RazonSocial'=>'8888',
            'NombreComercial'=>'SALA DE BELLEZA MARIA JO',
            'DireccionComercial'=>'CL 43 SUR 12F 56',
            'CodLocalidad'=>'18',
            'Localidad'=>'RAFAEL URIBE',
            'codUPZ'=>'53',
            'NombreUPZ'=>'MARCO FIDEL SUAREZ',
            'CodBarrio'=>'18144BD ',
            'Barrio'=>'S.C. SAN JORGE SUR',
            'CodTipoEstablecimiento'=>'420',
            'TipoEstablecimiento'=>'PELUQUERÍAS',
            'FechaVisita'=>'2018-06-28 00:00:00',
            'Concepto'=>'Concepto Favorable'
        ]);

        Establecimiento::create([
            'IdTBLEstablecimientos'=>'399081',
            'RazonSocial'=>'8888',
            'NombreComercial'=>'SALA DE BELLEZA MARIA JO',
            'DireccionComercial'=>'CL 43 SUR 12F 56',
            'CodLocalidad'=>'18',
            'Localidad'=>'RAFAEL URIBE',
            'codUPZ'=>'53',
            'NombreUPZ'=>'MARCO FIDEL SUAREZ',
            'CodBarrio'=>'18144BD ',
            'Barrio'=>'S.C. SAN JORGE SUR',
            'CodTipoEstablecimiento'=>'420',
            'TipoEstablecimiento'=>'PELUQUERÍAS',
            'FechaVisita'=>'2018-06-28 00:00:00',
            'Concepto'=>'Concepto Favorable'
        ]);

        Establecimiento::create([
            'IdTBLEstablecimientos'=>'399082',
            'RazonSocial'=>'8888',
            'NombreComercial'=>'SALA DE BELLEZA MARIA JO',
            'DireccionComercial'=>'CL 43 SUR 12F 56',
            'CodLocalidad'=>'18',
            'Localidad'=>'RAFAEL URIBE',
            'codUPZ'=>'53',
            'NombreUPZ'=>'MARCO FIDEL SUAREZ',
            'CodBarrio'=>'18144BD ',
            'Barrio'=>'S.C. SAN JORGE SUR',
            'CodTipoEstablecimiento'=>'420',
            'TipoEstablecimiento'=>'PELUQUERÍAS',
            'FechaVisita'=>'2018-06-28 00:00:00',
            'Concepto'=>'Concepto Favorable'
        ]);

        Establecimiento::create([
            'IdTBLEstablecimientos'=>'399083',
            'RazonSocial'=>'8888',
            'NombreComercial'=>'SALA DE BELLEZA MARIA JO',
            'DireccionComercial'=>'CL 43 SUR 12F 56',
            'CodLocalidad'=>'18',
            'Localidad'=>'RAFAEL URIBE',
            'codUPZ'=>'53',
            'NombreUPZ'=>'MARCO FIDEL SUAREZ',
            'CodBarrio'=>'18144BD ',
            'Barrio'=>'S.C. SAN JORGE SUR',
            'CodTipoEstablecimiento'=>'420',
            'TipoEstablecimiento'=>'PELUQUERÍAS',
            'FechaVisita'=>'2018-06-28 00:00:00',
            'Concepto'=>'Concepto Favorable'
        ]);

        Establecimiento::create([
            'IdTBLEstablecimientos'=>'399084',
            'RazonSocial'=>'8888',
            'NombreComercial'=>'SALA DE BELLEZA MARIA JO',
            'DireccionComercial'=>'CL 43 SUR 12F 56',
            'CodLocalidad'=>'18',
            'Localidad'=>'RAFAEL URIBE',
            'codUPZ'=>'53',
            'NombreUPZ'=>'MARCO FIDEL SUAREZ',
            'CodBarrio'=>'18144BD ',
            'Barrio'=>'S.C. SAN JORGE SUR',
            'CodTipoEstablecimiento'=>'420',
            'TipoEstablecimiento'=>'PELUQUERÍAS',
            'FechaVisita'=>'2018-06-28 00:00:00',
            'Concepto'=>'Concepto Favorable'
        ]);

        Establecimiento::create([
            'IdTBLEstablecimientos'=>'399085',
            'RazonSocial'=>'8888',
            'NombreComercial'=>'SALA DE BELLEZA MARIA JO',
            'DireccionComercial'=>'CL 43 SUR 12F 56',
            'CodLocalidad'=>'18',
            'Localidad'=>'RAFAEL URIBE',
            'codUPZ'=>'53',
            'NombreUPZ'=>'MARCO FIDEL SUAREZ',
            'CodBarrio'=>'18144BD ',
            'Barrio'=>'S.C. SAN JORGE SUR',
            'CodTipoEstablecimiento'=>'420',
            'TipoEstablecimiento'=>'PELUQUERÍAS',
            'FechaVisita'=>'2018-06-28 00:00:00',
            'Concepto'=>'Concepto Favorable'
        ]);

        Establecimiento::create([
            'IdTBLEstablecimientos'=>'399086',
            'RazonSocial'=>'8888',
            'NombreComercial'=>'SALA DE BELLEZA MARIA JO',
            'DireccionComercial'=>'CL 43 SUR 12F 56',
            'CodLocalidad'=>'18',
            'Localidad'=>'RAFAEL URIBE',
            'codUPZ'=>'53',
            'NombreUPZ'=>'MARCO FIDEL SUAREZ',
            'CodBarrio'=>'18144BD ',
            'Barrio'=>'S.C. SAN JORGE SUR',
            'CodTipoEstablecimiento'=>'420',
            'TipoEstablecimiento'=>'PELUQUERÍAS',
            'FechaVisita'=>'2018-06-28 00:00:00',
            'Concepto'=>'Concepto Favorable'
        ]);

        Establecimiento::create([
            'IdTBLEstablecimientos'=>'399087',
            'RazonSocial'=>'8888',
            'NombreComercial'=>'SALA DE BELLEZA MARIA JO',
            'DireccionComercial'=>'CL 43 SUR 12F 56',
            'CodLocalidad'=>'18',
            'Localidad'=>'RAFAEL URIBE',
            'codUPZ'=>'53',
            'NombreUPZ'=>'MARCO FIDEL SUAREZ',
            'CodBarrio'=>'18144BD ',
            'Barrio'=>'S.C. SAN JORGE SUR',
            'CodTipoEstablecimiento'=>'420',
            'TipoEstablecimiento'=>'PELUQUERÍAS',
            'FechaVisita'=>'2018-06-28 00:00:00',
            'Concepto'=>'Concepto Favorable'
        ]);

        Establecimiento::create([
            'IdTBLEstablecimientos'=>'399088',
            'RazonSocial'=>'8888',
            'NombreComercial'=>'SALA DE BELLEZA MARIA JO',
            'DireccionComercial'=>'CL 43 SUR 12F 56',
            'CodLocalidad'=>'18',
            'Localidad'=>'RAFAEL URIBE',
            'codUPZ'=>'53',
            'NombreUPZ'=>'MARCO FIDEL SUAREZ',
            'CodBarrio'=>'18144BD ',
            'Barrio'=>'S.C. SAN JORGE SUR',
            'CodTipoEstablecimiento'=>'420',
            'TipoEstablecimiento'=>'PELUQUERÍAS',
            'FechaVisita'=>'2018-06-28 00:00:00',
            'Concepto'=>'Concepto Favorable'
        ]);

        Establecimiento::create([
            'IdTBLEstablecimientos'=>'399089',
            'RazonSocial'=>'8888',
            'NombreComercial'=>'SALA DE BELLEZA MARIA JO',
            'DireccionComercial'=>'CL 43 SUR 12F 56',
            'CodLocalidad'=>'18',
            'Localidad'=>'RAFAEL URIBE',
            'codUPZ'=>'53',
            'NombreUPZ'=>'MARCO FIDEL SUAREZ',
            'CodBarrio'=>'18144BD ',
            'Barrio'=>'S.C. SAN JORGE SUR',
            'CodTipoEstablecimiento'=>'420',
            'TipoEstablecimiento'=>'PELUQUERÍAS',
            'FechaVisita'=>'2018-06-28 00:00:00',
            'Concepto'=>'Concepto Favorable'
        ]);

        Establecimiento::create([
            'IdTBLEstablecimientos'=>'399090',
            'RazonSocial'=>'8888',
            'NombreComercial'=>'SALA DE BELLEZA MARIA JO',
            'DireccionComercial'=>'CL 43 SUR 12F 56',
            'CodLocalidad'=>'18',
            'Localidad'=>'RAFAEL URIBE',
            'codUPZ'=>'53',
            'NombreUPZ'=>'MARCO FIDEL SUAREZ',
            'CodBarrio'=>'18144BD ',
            'Barrio'=>'S.C. SAN JORGE SUR',
            'CodTipoEstablecimiento'=>'420',
            'TipoEstablecimiento'=>'PELUQUERÍAS',
            'FechaVisita'=>'2018-06-28 00:00:00',
            'Concepto'=>'Concepto Favorable'
        ]);

        Establecimiento::create([
            'IdTBLEstablecimientos'=>'399091',
            'RazonSocial'=>'8888',
            'NombreComercial'=>'SALA DE BELLEZA MARIA JO',
            'DireccionComercial'=>'CL 43 SUR 12F 56',
            'CodLocalidad'=>'18',
            'Localidad'=>'RAFAEL URIBE',
            'codUPZ'=>'53',
            'NombreUPZ'=>'MARCO FIDEL SUAREZ',
            'CodBarrio'=>'18144BD ',
            'Barrio'=>'S.C. SAN JORGE SUR',
            'CodTipoEstablecimiento'=>'420',
            'TipoEstablecimiento'=>'PELUQUERÍAS',
            'FechaVisita'=>'2018-06-28 00:00:00',
            'Concepto'=>'Concepto Favorable'
        ]);
    }
}
