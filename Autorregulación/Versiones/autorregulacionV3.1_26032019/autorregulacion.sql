-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3308
-- Generation Time: Mar 26, 2019 at 09:39 PM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `microsssalud`
--

-- --------------------------------------------------------

--
-- Table structure for table `entradas`
--

CREATE TABLE `entradas` (
  `id` int(10) UNSIGNED NOT NULL,
  `enunciado` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contenido` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `tipo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `archivo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `estado` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `entradas`
--

INSERT INTO `entradas` (`id`, `enunciado`, `contenido`, `tipo`, `archivo`, `estado`, `created_at`, `updated_at`) VALUES
(1, '�Qu� requiero para que me visite la Secretar�a Distrital de Salud?', 'La Secretar�a Distrital de Salud expide el concepto sanitario cuando el establecimiento est� funcionando, a trav�s las Subredes Integradas de Servicios de Salud (SISS), seg�n la localidad donde est� ubicado. Para lo cual, debes haber hecho previamente la Inscripci�n del establecimiento <a href=\"./registrate\">[Registra tu negocio aqu�]</a>, luego verificar que cumples con la norma <a href=\"./registrate\">[Verifica tu lista de chequeo]</a> y despu�s, solicitar una Visita de inspecci�n, vigilancia y control <a href=\"./registrate\">[Solicita una visita]</a> Como resultado de esta vigilancia, se emite un concepto sanitario al establecimiento, acorde a las condiciones sanitarias evidenciadas en el momento de la visita.<br><a href=\"./registrate\">M�s informaci�n</a>', '1', NULL, 1, '2019-03-27 00:57:29', '2019-03-27 00:57:29'),
(2, '�Qu� reglamentaci�n sanitaria aplica para un restaurante?', 'La normatividad sanitaria vigente aplicable para la preparaci�n, expendio, almacenamiento, comercializaci�n de alimentos es: Ley 9 de 1979, Resoluci�n 2674 de 2013 espec�ficamente en el cap�tulo VIII las condiciones sanitarias que deben cumplir los restaurantes y establecimientos gastron�micos, Resoluci�n 5109 de 2005 y dem�s normas que rigen sobre la materia.<br><a href=\"./restaurantes\">M�s informaci�n</a>', '1', NULL, 1, '2019-03-27 00:57:29', '2019-03-27 00:57:29'),
(3, '�Qu� debo saber si tengo o tendr� un restaurante?', 'Si tienes un restaurante debes cumplir, entre otros, con aspectos como documentaci�n (siempre deber�n estar disponibles en el establecimiento el Plan de Saneamiento, el Plan de Capacitaci�n Continuo y Permanente, la Certificaci�n m�dica del personal); Buenas pr�cticas de manufactura, infraestructura adecuada (pisos construidos en material sanitario no poroso ni absorbente, de f�cil lavado, paredes de tonos claros, con materiales impermeables, techos que eviten la acumulaci�n de suciedad). Los equipos y utensilios deben estar hechos con materiales que no contaminen los alimentos, con acabado liso y sin grietas.<br><a href=\"./restaurantes\">M�s informaci�n</a>', '1', NULL, 1, '2019-03-27 00:57:29', '2019-03-27 00:57:29'),
(4, '�Qu� requisitos debo cumplir para ser manipulador de alimentos?', 'El manipulador de alimentos es toda persona que interviene directamente o en forma ocasional en actividades de fabricaci�n, procesamiento, preparaci�n, envase, almacenamiento, transporte y expendio de alimentos. Debe tener formaci�n en educaci�n sanitaria.<br>Todo manipulador de alimentos debe pasar por un reconocimiento m�dico antes de desempe�ar esta funci�n o cada vez que se considere necesario por razones cl�nicas y epidemiol�gicas. Por lo menos una vez al a�o.<br><a href=\"./restaurantes\">M�s informaci�n</a>', '1', NULL, 1, '2019-03-27 00:57:29', '2019-03-27 00:57:29'),
(5, '�Qu� son los restaurantes 1A?', 'Restaurantes 1A es un programa dise�ado para que los restaurantes populares reciban una distinci�n (v�lida por un a�o). El enfoque en calidad e inocuidad de este programa busca brindar a la comunidad alimentos nutritivos, sanos y seguros, con lo cual se contribuye a la salud de la poblaci�n. Para vincularte a este programa, puedes enviar la solicitud al correo: <a href=\"mailto:restaurantes1a@saludcapital.gov.co\">restaurantes1a@saludcapital.gov.co</a><br><a href=\"./buenpropietario\">M�s informaci�n</a>', '1', NULL, 1, '2019-03-27 00:57:29', '2019-03-27 00:57:29'),
(6, '�Qu� requisitos existen para la apertura y funcionamiento de un expendio de carnes?', 'Los expendios de carnes �conocidos popularmente como carnicer�as o famas� son todos los establecimientos que expenden, almacenan y comercializan carne y productos c�rnicos comestibles para consumo humano. Algunos de los requisitos sanitarios que deben cumplir los establecimientos son: la carne y los productos c�rnicos no deben estar expuestos al medio ambiente; deben contar con indicadores y sistema de registro de temperaturas, y con un sistema de refrigeraci�n con la capacidad de almacenar el volumen de carne que comercializan.<br><a href=\"./carnicerias\">M�s informaci�n</a>', '1', NULL, 1, '2019-03-27 00:57:29', '2019-03-27 00:57:29'),
(7, '�Qu� permisos requiere un veh�culo que transporta carne o derivados c�rnicos?', 'Los carros o veh�culos que transporten carne o productos c�rnicos comestibles deben estar autorizados por la Secretar�a Distrital de Salud. Debe realizarse una inscripci�n <a href=\"http://dev.saludcapital.gov.co/microsivigiladcPruebas/ServiciosComuni2.aspx\">[Registra tu veh�culo]</a> y, adem�s, solicitar la autorizaci�n sanitaria <a href=\"./registrate\">[Solic�tala aqu�]</a>. <br><br>Todo veh�culo debe cumplir con requisitos sanitarios. Para consultarlos, da clic en M�s informaci�n.<br> <a href=\"./carnicerias\">M�s informaci�n</a>', '1', NULL, 1, '2019-03-27 00:57:29', '2019-03-27 00:57:29'),
(8, '�Qu� requisitos existen para la apertura y funcionamiento de una droguer�a?', 'De acuerdo con la norma, una droguer�a debe cumplir, entre otros, con aspectos como instalaciones adecuadas, recurso humano, dotaci�n. Adem�s, debe contar con un sistema de gesti�n de calidad por procesos.<br><a href=\"./droguerias\">M�s informaci�n</a>', '1', NULL, 1, '2019-03-27 00:57:29', '2019-03-27 00:57:29'),
(9, '�Qu� requisitos existen para la apertura y funcionamiento de un sal�n de belleza?', 'Las peluquer�as, salones de belleza o barber�as deben cumplir, entre otros, con requisitos de documentaci�n (diplomas de estudio de cada trabajador, certificaci�n de capacitaci�n en bioseguridad, protocolo/manual de bioseguridad, plan de gesti�n integral de residuos, as� como manuales de instalaci�n, funcionamiento y registro de mantenimiento peri�dico de equipos). En el establecimiento, los pisos, paredes, techos, escaleras, rampas y divisiones deben estar construidos o recubiertos con pinturas o materiales sanitarios de tonos claros. El mobiliario construido debe estar recubierto o tapizado en material sanitario.<br><a href=\"./salonesbelleza\">M�s informaci�n</a>', '1', NULL, 1, '2019-03-27 00:57:29', '2019-03-27 00:57:29'),
(10, '�Qu� requisitos se exigen para abrir un hogar geri�trico en Bogot� D.C.?', 'Para su correcto funcionamiento, los hogares geri�tricos, gerontol�gicos y centros d�a deben cumplir con los siguientes requisitos: contar con profesionales de salud id�neos, calificados, preparados y amables para la atenci�n de la persona mayor, lo cual se debe garantizar permanentemente en las instalaciones; garantizar una alimentaci�n adecuada y saludable, acorde con las necesidades de salud de la persona institucionalizada; mantener buenas pr�cticas de manejo de alimentos, y contar con una infraestructura adecuada para su desplazamiento.<br><a href=\"./registrate\">M�s informaci�n</a>', '1', NULL, 1, '2019-03-27 00:57:29', '2019-03-27 00:57:29'),
(11, '', 'Recuerda: el registro o inscripci�n de establecimientos es obligatorio para todo tipo de negocio.', '2', 'img/icons/tipsauto1.svg', 1, '2019-03-27 00:57:29', '2019-03-27 00:57:29'),
(12, '', 'El cumplimiento de los requisitos sanitarios vigentes brinda mayor confianza a tus clientes, lo cual conlleva a n incremento de tus ingresos por los servicios brindados en el establecimiento.', '2', 'img/icons/tipsauto2.svg', 1, '2019-03-27 00:57:29', '2019-03-27 00:57:29'),
(13, '', 'Si realizas la autoevaluaci�n en el proceso de autorregulaci�n, conocer�s requisitos para el funcionamiento de los establecimientos y apuntar�s a contar con un concepto sanitario favorable.', '2', 'img/icons/tipsauto3.svg', 1, '2019-03-27 00:57:29', '2019-03-27 00:57:29'),
(14, '', 'Algunas de las normas que regulan el proceso de registro de establecimientos son: Resoluci�n 1229 de 2013, Resoluci�n 5194 de 2010, Resoluci�n 2263 de 2004, Resoluci�n No. 2016041871 de 2016.', '2', 'img/icons/tipsauto4.svg', 1, '2019-03-27 00:57:29', '2019-03-27 00:57:29');

-- --------------------------------------------------------

--
-- Table structure for table `establecimientos`
--

CREATE TABLE `establecimientos` (
  `id` int(10) UNSIGNED NOT NULL,
  `IdTBLEstablecimientos` bigint(20) UNSIGNED NOT NULL,
  `RazonSocial` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `NombreComercial` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `DireccionComercial` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `CodLocalidad` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Localidad` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `codUPZ` int(10) UNSIGNED NOT NULL,
  `NombreUPZ` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `CodBarrio` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Barrio` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `CodTipoEstablecimiento` bigint(20) UNSIGNED NOT NULL,
  `TipoEstablecimiento` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `FechaVisita` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Concepto` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `establecimientos`
--

INSERT INTO `establecimientos` (`id`, `IdTBLEstablecimientos`, `RazonSocial`, `NombreComercial`, `DireccionComercial`, `CodLocalidad`, `Localidad`, `codUPZ`, `NombreUPZ`, `CodBarrio`, `Barrio`, `CodTipoEstablecimiento`, `TipoEstablecimiento`, `FechaVisita`, `Concepto`, `created_at`, `updated_at`) VALUES
(1, 399080, '8888', 'SALA DE BELLEZA MARIA JO', 'CL 43 SUR 12F 56', '18', 'RAFAEL URIBE', 53, 'MARCO FIDEL SUAREZ', '18144BD ', 'S.C. SAN JORGE SUR', 420, 'PELUQUER�AS', '2018-06-28 00:00:00', 'Concepto Favorable', '2019-03-27 00:57:29', '2019-03-27 00:57:29'),
(2, 399081, '8888', 'SALA DE BELLEZA MARIA JO', 'CL 43 SUR 12F 56', '18', 'RAFAEL URIBE', 53, 'MARCO FIDEL SUAREZ', '18144BD ', 'S.C. SAN JORGE SUR', 420, 'PELUQUER�AS', '2018-06-28 00:00:00', 'Concepto Favorable', '2019-03-27 00:57:29', '2019-03-27 00:57:29'),
(3, 399082, '8888', 'SALA DE BELLEZA MARIA JO', 'CL 43 SUR 12F 56', '18', 'RAFAEL URIBE', 53, 'MARCO FIDEL SUAREZ', '18144BD ', 'S.C. SAN JORGE SUR', 420, 'PELUQUER�AS', '2018-06-28 00:00:00', 'Concepto Favorable', '2019-03-27 00:57:29', '2019-03-27 00:57:29'),
(4, 399083, '8888', 'SALA DE BELLEZA MARIA JO', 'CL 43 SUR 12F 56', '18', 'RAFAEL URIBE', 53, 'MARCO FIDEL SUAREZ', '18144BD ', 'S.C. SAN JORGE SUR', 420, 'PELUQUER�AS', '2018-06-28 00:00:00', 'Concepto Favorable', '2019-03-27 00:57:29', '2019-03-27 00:57:29'),
(5, 399084, '8888', 'SALA DE BELLEZA MARIA JO', 'CL 43 SUR 12F 56', '18', 'RAFAEL URIBE', 53, 'MARCO FIDEL SUAREZ', '18144BD ', 'S.C. SAN JORGE SUR', 420, 'PELUQUER�AS', '2018-06-28 00:00:00', 'Concepto Favorable', '2019-03-27 00:57:29', '2019-03-27 00:57:29'),
(6, 399085, '8888', 'SALA DE BELLEZA MARIA JO', 'CL 43 SUR 12F 56', '18', 'RAFAEL URIBE', 53, 'MARCO FIDEL SUAREZ', '18144BD ', 'S.C. SAN JORGE SUR', 420, 'PELUQUER�AS', '2018-06-28 00:00:00', 'Concepto Favorable', '2019-03-27 00:57:29', '2019-03-27 00:57:29'),
(7, 399086, '8888', 'SALA DE BELLEZA MARIA JO', 'CL 43 SUR 12F 56', '18', 'RAFAEL URIBE', 53, 'MARCO FIDEL SUAREZ', '18144BD ', 'S.C. SAN JORGE SUR', 420, 'PELUQUER�AS', '2018-06-28 00:00:00', 'Concepto Favorable', '2019-03-27 00:57:29', '2019-03-27 00:57:29'),
(8, 399087, '8888', 'SALA DE BELLEZA MARIA JO', 'CL 43 SUR 12F 56', '18', 'RAFAEL URIBE', 53, 'MARCO FIDEL SUAREZ', '18144BD ', 'S.C. SAN JORGE SUR', 420, 'PELUQUER�AS', '2018-06-28 00:00:00', 'Concepto Favorable', '2019-03-27 00:57:29', '2019-03-27 00:57:29'),
(9, 399088, '8888', 'SALA DE BELLEZA MARIA JO', 'CL 43 SUR 12F 56', '18', 'RAFAEL URIBE', 53, 'MARCO FIDEL SUAREZ', '18144BD ', 'S.C. SAN JORGE SUR', 420, 'PELUQUER�AS', '2018-06-28 00:00:00', 'Concepto Favorable', '2019-03-27 00:57:29', '2019-03-27 00:57:29'),
(10, 399089, '8888', 'SALA DE BELLEZA MARIA JO', 'CL 43 SUR 12F 56', '18', 'RAFAEL URIBE', 53, 'MARCO FIDEL SUAREZ', '18144BD ', 'S.C. SAN JORGE SUR', 420, 'PELUQUER�AS', '2018-06-28 00:00:00', 'Concepto Favorable', '2019-03-27 00:57:29', '2019-03-27 00:57:29'),
(11, 399090, '8888', 'SALA DE BELLEZA MARIA JO', 'CL 43 SUR 12F 56', '18', 'RAFAEL URIBE', 53, 'MARCO FIDEL SUAREZ', '18144BD ', 'S.C. SAN JORGE SUR', 420, 'PELUQUER�AS', '2018-06-28 00:00:00', 'Concepto Favorable', '2019-03-27 00:57:29', '2019-03-27 00:57:29'),
(12, 399091, '8888', 'SALA DE BELLEZA MARIA JO', 'CL 43 SUR 12F 56', '18', 'RAFAEL URIBE', 53, 'MARCO FIDEL SUAREZ', '18144BD ', 'S.C. SAN JORGE SUR', 420, 'PELUQUER�AS', '2018-06-28 00:00:00', 'Concepto Favorable', '2019-03-27 00:57:29', '2019-03-27 00:57:29');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_03_17_162922_create_entradas', 1),
(4, '2019_03_17_163036_create_tipos_de_establecimientos', 1),
(5, '2019_03_17_163203_create_newsletters', 1),
(6, '2019_03_17_163220_create_noticias', 1),
(7, '2019_03_20_171002_create_establecimientos', 1);

-- --------------------------------------------------------

--
-- Table structure for table `newsletters`
--

CREATE TABLE `newsletters` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_tipo_establecimiento` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `correo_e` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telefono` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `newsletters`
--

INSERT INTO `newsletters` (`id`, `id_tipo_establecimiento`, `name`, `correo_e`, `telefono`, `created_at`, `updated_at`) VALUES
(1, 540, 'Chris Collins', 'showell@example.org', '(924) 927-6927', '2019-03-27 00:57:30', '2019-03-27 00:57:30'),
(2, 540, 'Emmie Kertzmann', 'quitzon.naomie@example.net', '297-945-7753 x9002', '2019-03-27 00:57:30', '2019-03-27 00:57:30'),
(3, 540, 'Hermina Mohr', 'wrobel@example.com', '903.805.3762 x2435', '2019-03-27 00:57:30', '2019-03-27 00:57:30'),
(4, 540, 'Khalid Cormier', 'solon.medhurst@example.com', '+1-890-369-1600', '2019-03-27 00:57:30', '2019-03-27 00:57:30'),
(5, 540, 'Ms. Rosalee Stanton', 'marvin32@example.com', '485.788.3581 x16591', '2019-03-27 00:57:30', '2019-03-27 00:57:30'),
(6, 540, 'Robyn Upton', 'macejkovic.rhoda@example.net', '391-326-7609 x76128', '2019-03-27 00:57:30', '2019-03-27 00:57:30'),
(7, 540, 'Jettie O\'Reilly', 'alayna.schiller@example.net', '573.583.4774', '2019-03-27 00:57:30', '2019-03-27 00:57:30'),
(8, 540, 'Bradly Ankunding', 'luciano.nitzsche@example.net', '(825) 724-7020', '2019-03-27 00:57:30', '2019-03-27 00:57:30'),
(9, 540, 'Aliyah Buckridge DVM', 'kiley.abbott@example.com', '869.219.8218', '2019-03-27 00:57:30', '2019-03-27 00:57:30'),
(10, 540, 'Eva Wisozk', 'xframi@example.net', '+16062622938', '2019-03-27 00:57:30', '2019-03-27 00:57:30'),
(11, 540, 'Tillman Zulauf', 'koch.abigail@example.com', '1-746-264-2301', '2019-03-27 00:57:30', '2019-03-27 00:57:30'),
(12, 540, 'Annalise Sipes', 'lharris@example.net', '+1-364-264-2617', '2019-03-27 00:57:30', '2019-03-27 00:57:30'),
(13, 540, 'Lenora O\'Reilly Sr.', 'hamill.timothy@example.org', '759-388-8881 x153', '2019-03-27 00:57:30', '2019-03-27 00:57:30'),
(14, 540, 'Celestino Abernathy', 'acummings@example.net', '794.636.7283', '2019-03-27 00:57:30', '2019-03-27 00:57:30'),
(15, 540, 'Victor Jerde V', 'hudson.lauriane@example.net', '660.539.4009 x0996', '2019-03-27 00:57:30', '2019-03-27 00:57:30'),
(16, 540, 'Laron Franecki', 'isac.emmerich@example.com', '425-389-0670 x54246', '2019-03-27 00:57:30', '2019-03-27 00:57:30'),
(17, 540, 'Dr. Alexie Barrows MD', 'tianna10@example.org', '695-268-4767 x14714', '2019-03-27 00:57:30', '2019-03-27 00:57:30'),
(18, 540, 'Bridget Boyle', 'linnie45@example.org', '1-305-721-2806', '2019-03-27 00:57:30', '2019-03-27 00:57:30'),
(19, 540, 'Dr. Frank Mitchell', 'cgoyette@example.net', '1-496-842-9593 x49837', '2019-03-27 00:57:30', '2019-03-27 00:57:30'),
(20, 540, 'Elody Abshire', 'farrell.jacklyn@example.org', '218-218-3002 x43296', '2019-03-27 00:57:30', '2019-03-27 00:57:30'),
(21, 540, 'Finn Weimann', 'mallory48@example.net', '+1.791.874.3857', '2019-03-27 00:57:30', '2019-03-27 00:57:30'),
(22, 540, 'Dr. Sean Corkery', 'audra.grimes@example.net', '+14386138361', '2019-03-27 00:57:30', '2019-03-27 00:57:30'),
(23, 540, 'Prof. Michale Witting', 'nbailey@example.net', '589-788-2393', '2019-03-27 00:57:30', '2019-03-27 00:57:30'),
(24, 540, 'Faye Barrows I', 'moses99@example.net', '558.621.2692 x6224', '2019-03-27 00:57:30', '2019-03-27 00:57:30'),
(25, 540, 'Marcellus Reilly', 'mpagac@example.org', '1-693-653-6529 x307', '2019-03-27 00:57:30', '2019-03-27 00:57:30'),
(26, 540, 'Haskell Barrows', 'adeline.dubuque@example.org', '(853) 383-5246 x496', '2019-03-27 00:57:30', '2019-03-27 00:57:30'),
(27, 540, 'Felipa Dibbert', 'daniel.keara@example.net', '(364) 353-0316 x31971', '2019-03-27 00:57:30', '2019-03-27 00:57:30'),
(28, 540, 'Lenna Hodkiewicz', 'towne.jane@example.net', '478.517.1398', '2019-03-27 00:57:30', '2019-03-27 00:57:30'),
(29, 540, 'Prof. Raleigh Thompson', 'twaelchi@example.com', '862.219.7565 x677', '2019-03-27 00:57:30', '2019-03-27 00:57:30'),
(30, 540, 'Dr. Kamille Rempel', 'raoul90@example.org', '1-602-912-4597', '2019-03-27 00:57:30', '2019-03-27 00:57:30'),
(31, 540, 'Myah Monahan', 'samantha59@example.com', '1-665-343-9767', '2019-03-27 00:57:30', '2019-03-27 00:57:30'),
(32, 540, 'Calista Welch', 'auer.dario@example.net', '717.717.4689 x819', '2019-03-27 00:57:30', '2019-03-27 00:57:30'),
(33, 540, 'Guy Smitham IV', 'catharine.pfannerstill@example.net', '829-674-6733', '2019-03-27 00:57:30', '2019-03-27 00:57:30'),
(34, 540, 'Karina Osinski', 'gislason.marques@example.org', '382-761-4025', '2019-03-27 00:57:30', '2019-03-27 00:57:30'),
(35, 540, 'Ryan Wisozk', 'katrina.stark@example.org', '327-916-8579 x71781', '2019-03-27 00:57:30', '2019-03-27 00:57:30'),
(36, 540, 'Andre Herman', 'ida04@example.org', '(427) 742-1078 x936', '2019-03-27 00:57:30', '2019-03-27 00:57:30'),
(37, 540, 'Cassandre Corwin', 'larissa.schneider@example.com', '824.235.2222', '2019-03-27 00:57:30', '2019-03-27 00:57:30'),
(38, 540, 'Mr. Raul Bernhard I', 'sebastian.barrows@example.com', '1-582-772-0566 x17293', '2019-03-27 00:57:30', '2019-03-27 00:57:30'),
(39, 540, 'Audra Mohr', 'fannie.wiegand@example.net', '+18302922525', '2019-03-27 00:57:30', '2019-03-27 00:57:30'),
(40, 540, 'Henderson Cronin', 'tromp.gabriel@example.net', '(612) 985-5280', '2019-03-27 00:57:30', '2019-03-27 00:57:30'),
(41, 540, 'Walter Reilly', 'uoreilly@example.org', '1-601-499-7643 x96576', '2019-03-27 00:57:30', '2019-03-27 00:57:30'),
(42, 540, 'Mr. Reuben Gutmann MD', 'mhuel@example.org', '237-589-4755 x5314', '2019-03-27 00:57:30', '2019-03-27 00:57:30'),
(43, 540, 'Layne Dickens', 'rebeca06@example.org', '238-997-2732 x25456', '2019-03-27 00:57:30', '2019-03-27 00:57:30'),
(44, 540, 'Dulce Bode', 'nora67@example.org', '1-476-389-6944', '2019-03-27 00:57:30', '2019-03-27 00:57:30'),
(45, 540, 'Miguel Adams', 'lueilwitz.jody@example.net', '+1-643-574-0023', '2019-03-27 00:57:30', '2019-03-27 00:57:30'),
(46, 540, 'Alfonso Schamberger', 'wisozk.madie@example.com', '938-481-7170 x83827', '2019-03-27 00:57:30', '2019-03-27 00:57:30'),
(47, 540, 'Dr. Helen Dooley III', 'oleta.yundt@example.net', '1-357-451-0940 x7845', '2019-03-27 00:57:30', '2019-03-27 00:57:30'),
(48, 540, 'Esta Fisher', 'adavis@example.org', '(286) 987-3766 x36372', '2019-03-27 00:57:30', '2019-03-27 00:57:30');

-- --------------------------------------------------------

--
-- Table structure for table `noticias`
--

CREATE TABLE `noticias` (
  `id` int(10) UNSIGNED NOT NULL,
  `titulo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `resumen` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `contenido` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `imagen` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `estado` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `noticias`
--

INSERT INTO `noticias` (`id`, `titulo`, `resumen`, `contenido`, `imagen`, `estado`, `created_at`, `updated_at`) VALUES
(1, 'Secretar�a de Salud entrega reconocimiento a 25 restaurantes populares por su calidad y cumplimiento sanitario', 'La Secretar�a Distrital de Salud (SDS) entreg� la distinci�n \"Restaurantes 1A\" a 25 establecimientos populares de la ciudad que se destacan por cumplir con las normas sanitarias y contar con men�s balanceados y variados, la adecuada manipulaci�n de alimentos y buenos niveles de calidad y atenci�n.', 'La Secretar�a Distrital de Salud (SDS) entreg� la distinci�n \"Restaurantes 1A\" a 25 establecimientos populares de la ciudad que se destacan por cumplir con las normas sanitarias y contar con men�s balanceados y variados, la adecuada manipulaci�n de alimentos y buenos niveles de calidad y atenci�n.', 'noticias/noticia2.jpg', 1, '2019-03-27 00:57:29', '2019-03-27 00:57:29'),
(2, 'Titulo de la noticia', 'Este es un resumen', 'Esta es una noticia de prueba', 'noticias/noticia2.jpg', 1, '2019-03-27 00:57:29', '2019-03-27 00:57:29'),
(3, 'Titulo de la noticia', 'Este es un resumen', 'Esta es una noticia de prueba', 'noticias/noticia2.jpg', 1, '2019-03-27 00:57:29', '2019-03-27 00:57:29'),
(4, 'Titulo de la noticia', 'Este es un resumen', 'Esta es una noticia de prueba', 'noticias/noticia2.jpg', 1, '2019-03-27 00:57:29', '2019-03-27 00:57:29'),
(5, 'Titulo de la noticia', 'Este es un resumen', 'Esta es una noticia de prueba', 'noticias/noticia2.jpg', 1, '2019-03-27 00:57:29', '2019-03-27 00:57:29'),
(6, 'Titulo de la noticia', 'Este es un resumen', 'Esta es una noticia de prueba', 'noticias/noticia2.jpg', 1, '2019-03-27 00:57:29', '2019-03-27 00:57:29'),
(7, 'Titulo de la noticia', 'Este es un resumen', 'Esta es una noticia de prueba', 'noticias/noticia2.jpg', 1, '2019-03-27 00:57:29', '2019-03-27 00:57:29');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tipos_de_establecimientos`
--

CREATE TABLE `tipos_de_establecimientos` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre_tipo_de_establecimiento` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `intervencion` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_linea_ambiente` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tipos_de_establecimientos`
--

INSERT INTO `tipos_de_establecimientos` (`id`, `nombre_tipo_de_establecimiento`, `intervencion`, `id_linea_ambiente`, `created_at`, `updated_at`) VALUES
(540, 'Acabados de muebles met�licos', 'IVC A ESTABLECIMIENTOS QUE UTILICEN COMO MATERIA PRIMA EL METAL', 1000001, '2019-03-27 00:57:29', '2019-03-27 00:57:29');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rol` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `rol`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'ssadmin', 'admin@gmail.com', '1', NULL, '$2y$10$HA7kCU4GMAPQouC8FT30gOfCwCyG/syLyTI1d7FNnDqvJSAntwT/m', NULL, '2019-03-27 00:57:30', '2019-03-27 00:57:30');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `entradas`
--
ALTER TABLE `entradas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `establecimientos`
--
ALTER TABLE `establecimientos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `newsletters`
--
ALTER TABLE `newsletters`
  ADD PRIMARY KEY (`id`),
  ADD KEY `newsletters_id_tipo_establecimiento_foreign` (`id_tipo_establecimiento`);

--
-- Indexes for table `noticias`
--
ALTER TABLE `noticias`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `tipos_de_establecimientos`
--
ALTER TABLE `tipos_de_establecimientos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `entradas`
--
ALTER TABLE `entradas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `establecimientos`
--
ALTER TABLE `establecimientos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `newsletters`
--
ALTER TABLE `newsletters`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT for table `noticias`
--
ALTER TABLE `noticias`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tipos_de_establecimientos`
--
ALTER TABLE `tipos_de_establecimientos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=541;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `newsletters`
--
ALTER TABLE `newsletters`
  ADD CONSTRAINT `newsletters_id_tipo_establecimiento_foreign` FOREIGN KEY (`id_tipo_establecimiento`) REFERENCES `tipos_de_establecimientos` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
