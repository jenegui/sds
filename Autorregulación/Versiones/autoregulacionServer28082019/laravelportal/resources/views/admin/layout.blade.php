
<!doctype html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <title>Dashboard Autorregulación</title>
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
        integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

    <style>
      body{
        background-color:#FFEFD5;
      }
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
    <!-- Custom styles for this template -->
    <link href="{{asset('css/dashboard.css')}}" rel="stylesheet">
  </head>
  <body>
    <nav class="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
  <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">Autorregulación</a>
  @if( !empty($logged))
  {{--<input class="form-control form-control-dark w-100" type="text" placeholder="Search" aria-label="Search">--}}
  
  <ul class="navbar-nav px-3">
    <li class="nav-item text-nowrap">
      <a class="nav-link" href="{{url('/admin/logout')}}">Salir</a>
    </li>
  </ul>
  @endif
</nav>

<div class="container-fluid">

  <div class="row">
  @if( !empty($logged))
      <nav class="col-md-2 d-none d-md-block bg-light sidebar">
      <div class="sidebar-sticky pb-4">
        <ul class="nav flex-column">
          <li class="nav-item">
            <a class="nav-link active" href="{{url('/admin/stats')}}">
              <span data-feather="home"></span>
              Stats <span class="sr-only">(current)</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{url('/admin/noticias')}}">
              <span data-feather="file"></span>
              Noticias
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{url('/admin/newsletter')}}">
              <span data-feather="users"></span>
              Usuarios
            </a>
          </li>
          @if( !empty($tipos))
            @foreach($tipos as $t)
              <li class="nav-item">
                <a class="nav-link" href="{{url('/admin/entrada/'.$t->id)}}">
                  {{$t->nombre}}
                </a>
              </li>
            @endforeach
          @endif
        </ul>
      </div>
    </nav>
  @endif
    <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4 pb-4">
      <h2>@yield('sectionTitle')</h2>
      @yield('content')
    </main>
  </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
    <script src="{{asset('js/bootstrap.bundle.min.js')}}"></script>
    @yield("scripts")
</html>
