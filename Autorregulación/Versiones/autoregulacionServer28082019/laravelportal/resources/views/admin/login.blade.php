@extends('admin.layout',['logged'=>$logged])

@section('sectionTitle') Login @endsection
@section('content')
    <form action="{{url()->current()}}" method="POST" >
        @csrf
        <div class="form-group">
            <label for="">Nombre de usuario</label>
            <input name="nombre" type="text" class="form-control" require>
        </div>
        <div class="form-group">
            <label for="">Clave de ingreso</label>
            <input name="clave" type="password" class="form-control" require>
        </div>
        <div class="form-group">
            <button type="submit">Enviar</button>
        </div>
    </form>
   
@endsection