<?php

use Illuminate\Database\Seeder;
use App\Models\Tipos_de_establecimiento;
class TipoEstablecimientoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Tipos_de_establecimiento::create([
            'id'=>'540',
            'nombre_tipo_de_establecimiento'=>'Acabados de muebles metálicos',
            'intervencion'=>'IVC A ESTABLECIMIENTOS QUE UTILICEN COMO MATERIA PRIMA EL METAL',
            'id_linea_ambiente'=>'1000001'
        ]);
    }
}
