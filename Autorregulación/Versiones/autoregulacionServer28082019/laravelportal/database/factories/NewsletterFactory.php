<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Newsletter::class, function (Faker $faker) {
    return [
        'id_tipo_establecimiento' => '540',
        'correo_e' => $faker->unique()->safeEmail,
        'name' => $faker->name(),
        'telefono' => $faker->phonenumber(),
    ];
});
