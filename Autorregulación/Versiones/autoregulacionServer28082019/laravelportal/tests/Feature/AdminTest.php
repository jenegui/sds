<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AdminTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_root()
    {
        $response = $this->get('/admin');
        $response->assertRedirect('admin/stats');
    }

    public function test_stats()
    {
        $response = $this->get('/admin/stats');
        $response->assertStatus(200);
    }

    public function test_newsletter()
    {
        $response = $this->get('/admin/newsletter');
        $response->assertStatus(200);
    }

    public function test_entradas()
    {
        $response = $this->get('/admin/entradas');
        $response->assertStatus(200);
    }

    public function test_editEntrada()
    {
        $response = $this->get('/admin/editentrada');
        $response->assertStatus(200);
    }

    public function test_newEntrada()
    {
        $response = $this->get('/admin/nuevaentrada');
        $response->assertStatus(200);
    }

    public function test_noticias()
    {
        $response = $this->get('/admin/noticias');
        $response->assertStatus(200);
    }

    public function test_editNoticias()
    {
        $response = $this->get('/admin/editnoticia/1');
        $response->assertStatus(200);
    }

    public function test_newNoticias()
    {
        $response = $this->get('/admin/nuevanoticia');
        $response->assertStatus(200);
    }
}
