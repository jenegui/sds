<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0,shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Salud</title>
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet">
    <link href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
        integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link rel="stylesheet" href="{{asset('css/styles.css')}}">
    @isset($access)
    <link rel="stylesheet" href="{{asset('css/accesibilidad.css')}}">
    @endisset
</head>
<body>
    
    <header>
        <div class="container">
            <div class="logos">
                <div class="row">
                    <div class="col-12 col-md-6">
                        <img class="logo1" src="img/logos/home_alcaldia.svg" alt="">
                    </div>
                    <div class="col-12 col-md-6 text-right">
                        <img class="logo2" src="img/logos/home_negocios_rentables.svg" alt="">
                    </div>
                </div>
            </div>
            <nav class="navbar navbar-expand-md mt-2">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse"
                    aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarCollapse">
                    <div class="mr-2">
                        <div class="locator mb-2 {{$title}}">
                            <div class="point"><span></span></div>
                        </div>
                        <ul class="navbar-nav">
                            <li class="nav-item active">
                                <a class="nav-link" href="{{url('/')}}">Inicio <span class="sr-only">(current)</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{url('/autorregulacion')}}">Autorregulación</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{url('/registrate')}}">Regístrate</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{url('/buenpropietario')}}">Sea un buen propietario</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{url('/novedades')}}">Novedades</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{url('/informes')}}">Envía tu informe</a>
                            </li>
                        </ul>
                    </div>
                    <div class="right-content">
                        <form class="form-inline mt-2 mt-md-0">
                            <form action="{{url('/busqueda')}}" method="post">
                                <input class="form-control mr-sm-2" type="text" placeholder="Buscar" aria-label="Buscar">
                            </form>
                        </form>
                        <div class="dropdown">
                            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                                Accesibilidad
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="{{url('/accesibilidad')}}">Cambiar</a>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </nav>
        </div>
        
    </header>







    <main class="container">
        
        @yield('content')










        <div class="row mt-4">
            <div class="col-12 col-md-6">
                <div class="subtitle">
                    Preguntas Frecuentes
                </div>
                <div class="faqs">
                    <div class="faq_wrapper">
                        <form class="form-buscar mt-2 mt-md-0">
                            <input class="form-control mr-sm-2 buscar" type="text" placeholder="Buscar" aria-label="Buscar">
                        </form>
                    
                        <div id="faq_accordion">
                            @php 
                            $i=0;
                            @endphp 
                            @foreach($faqs as $f)
                                @php 
                                $i++;
                                @endphp
                                <div class="card">
                                    <div class="card-header" id="heading{{$i}}">
                                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapse{{$i}}" aria-expanded="false"
                                                aria-controls="collapse{{$i}}">
                                                {{$f->enunciado}}
                                            </button>
                                    </div>
                            
                                    <div id="collapse{{$i}}" class="collapse" aria-labelledby="heading{{$i}}" data-parent="#faq_accordion">
                                        <div class="card-body">
                                            {{$f->contenido}}
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>













            <div class="col-12 col-md-6">
                <div class="subtitle">
                    Tips de Autorregulación
                </div>
                <ul class="tips">
                    @foreach($tips as $tip)
                    <li>
                        <img src="{{$tip->imagen}}" alt="">
                        <p>{{$tip->contenido}}</p>
                    </li>
                    @endforeach
                    
                </ul>
            </div>


        </div>
    </main>
    <div class="gray-block">
        <main class="container">
            <div class="row newsletter">
                <div class="col-12 col-md-7 text-md-center px-4">
                    <img class="img_newsletter" src="img/icons/home_newsletter.svg" alt="">
                    <div class="text-left">
                        <h3 class="text-orange">Envíanos tus datos</h3>
                        <h3 class="text-blue">y recibe información de tu negocio</h3>
                        <p class="mt-4">Suscríbete en este espacio y recibe información de la Secretaría Distrital de Salud con los requerimientos sanitarios que rigen a tu establecimiento, así como la actualización de la norma.</p>
                    </div>
                </div>
                <div class="col-12 col-md-5">
                    <form action="{{url('/registro')}}" method="post">
                        @csrf
                        <div class="form-newsletter">
                            <div class="input-group">
                                <select name="tipo" id="tipo">
                                    @foreach ($te as $t)
                                        <option value="{{$t->id}}">{{$t->nombre_tipo_de_establecimiento}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="inputIconBox"><i class="fas fa-user"></i></span>
                                </div>
                                <input name="nombre" type="text" class="form-control" placeholder="Nombre">
                            </div>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="inputIconBox"><i class="fas fa-envelope"></i></span>
                                </div>
                                <input name="correo" type="text" class="form-control" placeholder="Correo electrónico">
                            </div>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="inputIconBox"><i class="fas fa-phone"></i></span>
                                </div>
                                <input name="telefono" type="text" class="form-control" placeholder="Teléfono">
                            </div>
                            <div class="captcha">

                            </div>
                            <div class="input-group form-check conditions">
                                <input name="checkbox" type="checkbox" class="" id="check1" required>
                                <p>
                                    Acepta los términos y condiciones: autorizo expresamente a la Secretaría Distrital de Salud y el Fondo Financiero
                                    Distrital de Salud, para hacer uso y tratamiento de mis datos personales de conformidad con lo previsto en el
                                    Decreto 1377 de 2013 que reglamenta la Ley 1581 de 2012. (Política de Protección de Datos Personales)
                                </p>
                            </div>
                            <button class="btn yellow w-100 py-2" type="submit">Suscribirme</button>
                        </div>
                    </form>
                </div>
            </div>
        </main>
    </div>
    @yield("redes")
    





    <div class="container enlaces">
        <div class="row">
            <div class="col-12 col-md-3">
                <div class="title">Entes de Control</div>
                <ul>
                    <li><a href="#">Personería de Bogotá</a></li>
                    <li><a href="#">Procuraduría General de la Nación</a></li>
                    <li><a href="#">Contraloría General de la Nación</a></li>
                    <li><a href="#">Concejo de Bogotá</a></li>
                    <li><a href="#">Veeduría Distrital</a></li>
                    <li><a href="#">Portal de Contratación a la Vista</a></li>
                    <li><a href="#">Portal de Contratación - SECOP</a></li>
                </ul>
            </div>
            <div class="col-12 col-md-3">
                <div class="title">Entes del Gobierno</div>
                <ul>
                    <li><a href="">Ministerio de Salud y Protección Social</a></li>
                    <li><a href="">Gobierno Digital</a></li>
                    <li><a href="">No más filas</a></li>
                </ul>
            </div>
            <div class="col-12 col-md-3">
                <div class="title">Menú princial</div>
                <ul>
                    <li><a href="">Inicio</a></li>
                    <li><a href="">La Entidad</a></li>
                    <li><a href="">Salud Ambiental</a></li>
                    <li><a href="">Bogotá Vital es Salud Urbana</a></li>
                    <li><a href="">Agilínea</a></li>
                </ul>
            </div>
            <div class="col-12 col-md-3">
                <div class="title">Secretaría Distrital de Salud</div>
                <ul>
                    <li><a href="">Cra 32# 12-81 Bogotá, Colombia</a></li>
                    <li><a href="">Teléfono: (571) 3649090</a></li>
                    <li><a href="">Código Postal: 0571</a></li>
                    <li><a href="">contactenos@saludcapital.gov.co</a></li>
                </ul>
            </div>
        </div>
    </div>

    <footer>
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-4"></div>
                <div class="col-12 col-md-4 text-md-center">
                    <p>2019. @ Todos los derechos reservados</p>
                    <p><a href="">*Habeas data</a></p>
                    <p><a href="">*Terminos y condiciones</a></p>
                </div>
                <div class="col-12 col-md-4 text-md-right">
                    <img src="img/logos/home_footer.svg" alt="">
                </div>
            </div>
        </div>
    </footer>


    <script src="http://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
    <script src="{{asset('js/bootstrap.bundle.min.js')}}"></script>
    <script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script src="{{asset('js/jquery.nicescroll.min.js')}}"></script>
    <script src="{{asset('js/code.js')}}"></script>
    
    @yield("scripts")
    
</body>
</html>