@extends('layout', ['title' => 'Autorregulacion','faqs' => $faq,'te' => $te,'tips' => $tip,'access' => $access ])
@section('content')
        <div class="breadcrumb">
            <ul>
                <li><a href="{{url('/')}}">Inicio</a></li>
                <li><a href="{{url('/autorregulacion')}}">Autorregulacion</a></li>
            </ul>      
        </div>
        <div class="home_slider">
            <div id="subsideSlider" class="carousel slide" data-ride="carousel">
                
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <div class="container">
                            <img class="w-100" src="img/slider/restaurante.png" alt="">
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="container">
                            <img class="w-100" src="img/slider/carniceria.png" alt="">
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="container">
                            <img class="w-100" src="img/slider/drogueria.png" alt="">
                        </div>
                    </div>
                </div>
                <ol class="carousel-indicators">
                    <li data-target="#subsideSlider" data-slide-to="0" class="active">
                        <span></span>                        
                    </li>
                    <li data-target="#subsideSlider" data-slide-to="1">
                        <span></span>
                    </li>
                    <li data-target="#subsideSlider" data-slide-to="2">
                        <span></span>
                    </li>
                </ol>
            </div>
        </div>

        <div class="row block right">
            <div class="col-12 col-md-5 text-center">
                <img class="w-80 m-auto" src="img/icons/autoregula_1.svg" alt="alternative text">
            </div>
            <div class="col-12 col-md-7 pl-4">
                <div class="subtitle">
                    ¿Que es la Autorregulación?
                </div>
                <div class="paragraph">
                    <p>
                        La Secretaría Distrital de Salud, como autoridad sanitaria en el Distrito Capital, debe verificar y vigilar las condiciones higiénicas y sanitarias de los establecimientos, con el fin de prevenir riesgos para la salud de la población.
                    </p>
                    <p>
                        Para esto, cuenta con una metodología que fomenta la conciencia sanitaria y la autorregulación en los dueños y encargados de los establecimientos. El proceso de autorregulación es muy sencillo, verifica los cinco pasos:
                    </p>
                    
                </div>
            </div>
        </div>

        <div class="row subsections pasos my-5">
            <ol class="w-100">
                <li class="active text-center">
                    <div class="text-big">Paso 1</div>
                    <div class="line"></div>
                    <img src="img/icons/AutorregulaPaso1Blanco.svg" alt="paso 1" title="paso 1">
                    <div>Conoce la norma sanitaria</div>
                </li>
                <li class="active text-center">
                    <div class="text-big">Paso 2</div>
                    <div class="line"></div>
                    <img src="img/icons/AutorregulaPaso2Blanco.svg" alt="paso 2" title="paso 2">
                    <div>Regístrate</div>
                </li>
                <li class="active text-center">
                    <div class="text-big">Paso 3</div>
                    <div class="line"></div>
                    <img src="img/icons/AutorregulaPaso3Blanco.svg" alt="paso 3" title="paso 3">
                    <div>Verifica</div>
                </li>
                <li class="active text-center">
                    <div class="text-big">Paso 4</div>
                    <div class="line"></div>
                    <img src="img/icons/AutorregulaPaso4Blanco.svg" alt="paso 4" title="paso 4">
                    <div>Solicita una visita</div>
                </li>
                <li class="active text-center">
                    <div class="text-big">Paso 5</div>
                    <div class="line"></div>
                    <img src="img/icons/AutorregulaPaso5Blanco.svg" alt="paso 5" title="paso 5">
                    <div>Autoregúlate</div>
                </li>
            </ol>
        </div>

        <div class="row block">
            <div class="col-12 col-lg-3 text-center">
                <h2 class="text-blue">Paso 1</h2>
                <img class="bluehand" src="img/icons/AutorregulaPaso1Azul.svg" alt="paso 1" title="paso 1">
            </div>
            <div class="col-12 col-lg-7">
                <div class="subtitle">
                    <h2>
                    Conoce la norma sanitaria
                    </h2>
                </div>
                <p>Ingresa a <a href=>www.saludcapital.gov.co</a>,da clic en el botón <b>‘Negocios Saludables, Negocios Rentables’</b> e infórmate sobre la regulación sanitaria para tu establecimiento. <b>Revisa la situación de tu local.</b></p>
                <div class="btn yellow"><a class="text-white" href="#" >Conócela</a></div>
            </div>
            <div class="col-12 col-lg-2">
                <img class="w-100" src="img/autorregula/a1.png" alt="">
            </div>
        </div>
        <div class="row block">
            <div class="col-12 col-lg-3 text-center">
                <h2 class="text-blue">Paso 2</h2>
                <img class="bluehand" src="img/icons/AutorregulaPaso1Azul.svg" alt="paso 1" title="paso 1">
            </div>
            <div class="col-12 col-lg-7">
                <div class="subtitle">
                    <h2>
                    Regístrate
                    </h2>
                </div>
                <p>Inscribe tu negocio de manera virtual, fácil y dinámica, <b>llena el formulario y recibe información</b> sobre tu actividad comercial. </p>
                <div class="btn yellow"><a class="text-white" href="#" >Regístrate</a></div>
            </div>
            <div class="col-12 col-lg-2">
                <img class="w-100" src="img/autorregula/a2.png" alt="">
            </div>
        </div>
        <div class="row block">
            <div class="col-12 col-lg-3 text-center">
                <h2 class="text-blue">Paso 3</h2>
                <img class="bluehand" src="img/icons/AutorregulaPaso1Azul.svg" alt="paso 1" title="paso 1">
            </div>
            <div class="col-12 col-lg-7">
                <div class="subtitle">
                    <h2>
                    Verifica
                    </h2>
                </div>
                <p>En tu correo electrónico recibirás una <b>lista de chequeo</b> para que puedas iniciar tu proceso de autorregulación y cumplir la norma sanitaria.</p>
                <div class="btn yellow"><a class="text-white" href="#" >Verifica</a></div>
            </div>
            <div class="col-12 col-lg-2">
                <img class="w-100" src="img/autorregula/a3.png" alt="">
            </div>
        </div>
        <div class="row block">
            <div class="col-12 col-lg-3 text-center">
                <h2 class="text-blue">Paso 4</h2>
                <img class="bluehand" src="img/icons/AutorregulaPaso1Azul.svg" alt="paso 1" title="paso 1">
            </div>
            <div class="col-12 col-lg-7">
                <div class="subtitle">
                    <h2>
                    Solicita una visita
                    </h2>
                </div>
                <p>Cuando estés listo para recibir una visita de Vigilancia y Control, pídela. Es gratuita. Un inspector hará la verificación y generará el concepto sanitario. <b>Recuerda:</b> todos los establecimientos serán visitados.</p>
                <div class="btn yellow"><a class="text-white" href="#" >Solicita visita</a></div>
            </div>
            <div class="col-12 col-lg-2">
                <img class="w-100" src="img/autorregula/a4.png" alt="">
            </div>
        </div>
        <div class="row block">
            <div class="col-12 col-lg-3 text-center">
                <h2 class="text-blue">Paso 5</h2>
                <img class="bluehand" src="img/icons/AutorregulaPaso1Azul.svg" alt="paso 1" title="paso 1">
            </div>
            <div class="col-12 col-lg-7">
                <div class="subtitle">
                    <h2>
                    Autorregúlate
                    </h2>
                </div>
                <p>Mantén el cumplimiento de la norma ¡y comienza a crecer de verdad!</p>
                <div class="btn yellow"><a class="text-white" href="#" >Autorregúlate</a></div>
            </div>
            <div class="col-12 col-lg-2">
                <img class="w-100" src="img/autorregula/a5.png" alt="">
            </div>
        </div> 




        <div class="row block">
            <div class="col-12 col-md-5 text-center">
                <img class="w-70 m-auto" src="img/autorregula/concepto.svg" alt="alternative text">
            </div>
            <div class="col-12 col-md-7 pl-4">
                <div class="subtitle">
                    <h2>Concepto sanitario</h2>
                </div>
                <div class="paragraph">
                    <p>
                        La vigilancia sanitaria es una responsabilidad estatal y ciudadana de protección de la salud, que consiste en el proceso sistemático de inspección, vigilancia y control del cumplimiento de normas y procesos. (Decreto 3518 de 2006)
                    </p>
                    <p>
                        La Secretaría Distrital de Salud, como autoridad sanitaria, realiza visitas de inspección para determinar si los establecimientos cumplen con la norma y protegen la salud de los ciudadanos. Como resultado de las visitas de inspección, se diligencia un acta y se emite un concepto sanitario, el cual puede ser: 
                    </p>
                    <ul class="concepto">
                        <li><b>Favorable:</b> cuando se cumple con todo lo evaluado y establecido en el acta. </li>
                        <li><b>Favorable con requerimientos o pendiente:</b> cuando existen aspectos pendientes por cumplir, aunque estos no generan riesgo a la salud.</li>
                        <li><b>Desfavorable:</b> cuando se incumple con lo establecido por la norma. </li>
                    </ul>
                </div>
            </div>
        </div>



        <div class="row block">
            <div class="col-12 col-md-7 pl-4">
                <div class="subtitle">
                    <h2>¿Qué debo saber acerca<br><b>de la visita de inspeccióna?</b></h2>
                </div>
                <div class="paragraph">
                    <ul class="concepto2">
                        <li>Solicita que el funcionario se identifique con el respectivo carné de la institución que representa.</li>
                        <li>Si tienes dudas respecto a la procedencia del funcionario, comunícate con la subred correspondiente en los números mencionados. </li>
                        <li>No permitas que te generen cobro por la visita ni por el concepto sanitario.</li>
                        <li>La visita deber ser atendida preferiblemente por el representante legal o propietario. En caso de no ser posible, se debe asignar una persona responsable, que sea mayor de edad.</li>
                        <li>Recuerda que la visita no se anuncia y se hará en cualquier momento.</li>
                        <li>Solicita siempre una copia del acta de visita, en la cual se indique el concepto sanitario otorgado y los requerimientos generados, si los hubo.</li>
                        <li>Solicita al funcionario que te lea el acta y que te explique los hallazgos generados en la visita, si hay lugar a ellos.</li>
                    </ul>
                </div>
            </div>
            <div class="col-12 col-md-5 text-center">
                <img class="w-70 m-auto" src="img/autorregula/inspeccion.svg" alt="alternative text">
            </div>
        </div>






        <div class="row block mapa">
            <div class="col-12">
                <div class="subtitle">
                    <h2><b>¿Quién me visita?</b></h2>
                </div>
            </div>
            <div class="col-12 col-md-7">
                <div id="mapa">
                    <img class="w-100" src="img/autorregula/mapa.png" alt="">
                </div>
                <div class="text-white mt-4">
                    <b>La Inspección, Vigilancia y Control es un proceso realizado a través de las cuatro Subredes Integradas de Servicios de Salud</b>, con las cuales se pueden realizar las solicitudes de visitas de inspección a establecimientos, teniendo en cuenta el sector donde están ubicados:
                </div>
            </div>
            <div class="col-12 col-md-5 text-white">
                <ul class="mapaExplain">
                    <li> 
                        <i><img src="img/autorregula/01.png" alt=""></i>
                        <div><b>Subred Norte:</b> Calle 66# 15-41. Teléfono:300 323 8661. Abarca las localidades de Usaquén, Teusaquillo, Chapinero, Barrios Unidos, Engativá y Suba.</div> 
                    </li>
                    <li>
                        <i><img src="img/autorregula/02.png" alt=""></i>
                        <div><b>Subred Sur:</b> Transversal 44 # 52B-16 Sbur. Teléfono: 730 0000, ext. 72415. Abarca las localidades de Ciudad Bolívar, Tunjuelito, Usme y Sumapaz.</div> 
                    </li>
                    <li>
                        <i><img src="img/autorregula/03.png" alt=""></i>
                        <div><b>Subred Centro Oriente:</b> Diagonal 34 # 5-43. Teléfono: 209 1480, ext. 9020. Abarca las localidad de Antonio Nariño, Rafael Uribe Uribe, La Candelaria, Santafé, San Cristóbal y Los Mártires.</div> 
                    </li>
                    <li>
                        <i><img src="img/autorregula/04.png" alt=""></i>
                        <div><b>Subred Sur Occidente:</b> Calle 9 # 39-46. Teléfono: 486 0033, ext. 10302. Abarca las localidades de Puente Aranda, Fontibón, Kenney y bosa.</div> 
                    </li>
                </ul>
            </div>
        </div>
        
        



        <div class="row block right">
            <div class="col-12 col-md-7">
                <div class="subtitle">
                    ¿A quién vigila<br><b>la Secretaría de Salud?</b>
                </div>
                <div class="paragraph">
                    <p>
                        La función de Inspección, Vigilancia y Control de las condiciones higiénico sanitarias, obedecen a un mandato legal contenido en la Ley 9 de 1979, por medio de la cual se dictan Medidas Sanitarias; así como al Decreto Único Reglamentario del Sector Salud No. 780 de 2016, que define a las autoridades sanitarias como entidades jurídicas de carácter público con atribuciones para ejercer funciones de rectoría, regulación, inspección, vigilancia y control de los sectores públicos y privados en salud y adoptar medidas de prevención y seguimiento que garanticen la protección de la salud pública
                    </p>
                    <p>
                        Tal definición y preceptos normativos permiten establecer sin lugar a dudas que todo establecimiento deberá tener concepto sanitario y su ejercicio obedece a una función y competencia de naturaleza reglada por parte del legislador. 
                    </p>
                </div>
            </div>
            <div class="col-12 col-md-5 text-center">
                <img class="w-80 m-auto" src="img/icons/autoregula_aquienvigila.svg" alt="alternative text">
            </div>
        </div>


        <div class="row subsections my-5">
            <ol>
                <li>
                    <div class="logo logo_rest"></div>
                    <div class="text">Restaurantes</div>
                    <div class="text-right mt-2">
                        <div class="btn yellow"><a href="{{url('/restaurantes')}}" >Ver requisitos</a></div>
                    </div>
                </li>
                <li>
                    <div class="logo logo_carne"></div>
                    <div class="text">Expendios de Carne</div>
                    <div class="text-right mt-2">
                        <div class="btn yellow"><a href="{{url('/carnicerias')}}" >Ver requisitos</a></div>
                    </div>
                </li>
                <li>
                    <div class="logo logo_drog"></div>
                    <div class="text">Droguerías</div>
                    <div class="text-right mt-2">
                        <div class="btn yellow"><a href="{{url('/droguerias')}}" >Ver requisitos</a></div>
                    </div>
                </li>
                <li>
                    <div class="logo logo_salon"></div>
                    <div class="text">Salones de belleza</div>
                    <div class="text-right mt-2">
                        <div class="btn yellow"><a href="{{url('/salonesbelleza')}}" >Ver requisitos</a></div>
                    </div>
                </li>
            </ol>
        </div>
@endsection
