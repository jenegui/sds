@extends('layout',['title' => 'BuenPropietario','faqs' => $faq,'te' => $te,'tips' => $tip,'access' => $access ])
@section('content')
        <div class="breadcrumb">
            <ul>
                <li><a href="{{url('/')}}">Inicio</a></li>
                <li><a href="{{url('/buenpropietario')}}">Sea un buen propietario</a></li>
            </ul>      
        </div>
        <div class="home_slider">
            <div id="subsideSlider" class="carousel slide" data-ride="carousel">
                
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <div class="container">
                            <img class="w-100" src="img/slider/restaurante.png" alt="">
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="container">
                            <img class="w-100" src="img/slider/carniceria.png" alt="">
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="container">
                            <img class="w-100" src="img/slider/drogueria.png" alt="">
                        </div>
                    </div>
                </div>
                <ol class="carousel-indicators">
                    <li data-target="#subsideSlider" data-slide-to="0" class="active">
                        <span></span>                        
                    </li>
                    <li data-target="#subsideSlider" data-slide-to="1">
                        <span></span>
                    </li>
                    <li data-target="#subsideSlider" data-slide-to="2">
                        <span></span>
                    </li>
                </ol>
            </div>
        </div>

        <div class="row block right">
            <div class="col-12 col-md-5 text-center">
                <img class="w-80 m-auto my-5" src="img/buenpro/01.svg" alt="alternative text">
            </div>
            <div class="col-12 col-md-7 pl-4">
                <div class="subtitle">
                    <b>¿Cómo ser</b> un buen propietario?
                </div>
                <div class="paragraph">
                    <p>
                        Previo a la apertura de su establecimiento, todo representante legal o propietario debe conocer la normatividad sanitaria aplicable, siendo parte de la responsabilidad que se adquiere al desarrollar una actividad económica, la cual puede llegar a generar riesgos sanitarios o ambientales a la comunidad que consume o adquiere sus productos o servicios; así como a quienes hace parte del entorno donde se desarrolle la actividad. <br>Este conocimiento será vital para evitar posibles sanciones o multas.
                    </p>
                </div>
            </div>
        </div>



        <div class="row block right my-5">
            <div class="col-12 py-4">
                <div class="subtitle">
                    <b>Sea un</b> buen propietario?
                </div>
                <div class="recomendaciones">
                    <div><div class="row">
                        <div class="col-12 col-md-2">
                            <img src="img/buenpro/recomendacion-01.svg" alt="">
                        </div>
                        <div class="col-12 col-md-10">
                            <h3 class="text-blue"><b>Recomendación</b></h3>
                            <p>
                                Ten siempre disponible en tu establecimiento toda la documentación que se requiera en los procesos de inspección, vigilancia y control sanitario. 
                            </p>
                        </div>
                    </div></div>

                    <div><div class="row">
                        <div class="col-12 col-md-2">
                            <img src="img/buenpro/recomendacion-02.svg" alt="">
                        </div>
                        <div class="col-12 col-md-10">
                            <h3 class="text-blue"><b>Recomendación</b></h3>
                            <p>
                                Recuerda que en cada visita de inspección, vigilancia y control sanitario el funcionario te entregará un acta de la misma, en la cual consta el respectivo concepto otorgado y, según el caso, las respectivas exigencias a cumplir. Este documento debe estar siempre disponible en el establecimiento.
                            </p>
                        </div>
                    </div></div>

                    <div><div class="row">
                        <div class="col-12 col-md-2">
                            <img src="img/buenpro/recomendacion-03.svg" alt="">
                        </div>
                        <div class="col-12 col-md-10">
                            <h3 class="text-blue"><b>Recomendación</b></h3>
                            <p>
                                No permitas que te generen cobro por la visita de inspección, vigilancia y control sanitario o la obtención del concepto sanitario, recuerda que esto es gratuito. 
                            </p>
                        </div>
                    </div></div>

                    <div><div class="row">
                        <div class="col-12 col-md-2">
                            <img src="img/buenpro/recomendacion-04.svg" alt="">
                        </div>
                        <div class="col-12 col-md-10">
                            <h3 class="text-blue"><b>Recomendación</b></h3>
                            <p>
                                Documenta e implementa los planes o programas necesarios para minimizar los riesgos asociados al desarrollo de la actividad comercial.
                            </p>
                        </div>
                    </div></div>

                    <div><div class="row">
                        <div class="col-12 col-md-2">
                            <img src="img/buenpro/recomendacion-05.svg" alt="">
                        </div>
                        <div class="col-12 col-md-10">
                            <h3 class="text-blue"><b>Recomendación</b></h3>
                            <p>
                                No esperes a ser visitado por la autoridad sanitaria para cumplir con los requisitos normativos, recuerda que siempre debes estar al día con estos.
                            </p>
                        </div>
                    </div></div>

                    <div><div class="row">
                        <div class="col-12 col-md-2">
                            <img src="img/buenpro/recomendacion-06.svg" alt="">
                        </div>
                        <div class="col-12 col-md-10">
                            <h3 class="text-blue"><b>Recomendación</b></h3>
                            <p>
                                Si el representante legal o propietario no puede estar disponible para atender la visita de inspección, vigilancia y control sanitario, debes asignar una persona responsable y con acceso a la información que se requiera durante la visita. Igualmente, debes mantenerte informado de los resultados de dicha visita. 
                            </p>
                        </div>
                    </div></div>

                </div>
            </div>
        </div>

        <div class="row justify-content-between">
            <div class="block col-12 col-md-5">
                <div class="subtitle">
                    <h2><b>¿Qué debo saber sobre</b> mi establecimiento?</h2>
                </div>
                <p>
                    A continuación, encontrarás algunas recomendaciones puntuales para abrir un negocio. Ten en cuenta el concepto sanitario puede ser requerido por otras entidades de control.
                </p>
            </div>
            <div class="block col-12 col-md-5 py-5">
                <div id="ejemploSlider" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img src="img/buenpro/ejemplo-01.svg" alt="">
                            <div class="text-blue mt-4"><h3><b>Ejemplo</b></h3></div>
                            <p>Documéntate de toda la normatividad aplicable a tu establecimiento y consulta con las demás entidades requisitos adicionales a cumplir. </p>
                        </div>
                        <div class="carousel-item">
                            <img src="img/buenpro/ejemplo-02.svg" alt="">
                            <div class="text-blue mt-4"><h3><b>Ejemplo</b></h3></div>
                            <p>Siempre que vayas a adquirir un nuevo negocio, solicita el historial sanitario del mismo. De esta manera conocerás si cumple con los requisitos sanitarios o si presenta sanciones por no cumplimiento.</p>
                        </div>
                        <div class="carousel-item">
                            <img src="img/buenpro/ejemplo-03.svg" alt="">
                            <div class="text-blue mt-4"><h3><b>Ejemplo</b></h3></div>
                            <p>Cerciórate que el predio cuente con los servicios públicos mínimos para el desarrollo de la actividad comercial. </p>
                        </div>
                    </div>
                    <ol class="carousel-indicators">
                        <li data-target="#ejemploSlider" data-slide-to="0" class="active">
                            <span></span>                        
                        </li>
                        <li data-target="#ejemploSlider" data-slide-to="1">
                            <span></span>
                        </li>
                        <li data-target="#ejemploSlider" data-slide-to="2">
                            <span></span>
                        </li>
                    </ol>
                </div>
            </div>
        </div>



        <div class="row my-5">
            <div class="subtitle">
                <h2><b>Estrategias para alcanzar</b> estándares de excelencia.</h2>
            </div>
            <p>La Secretaría Distrital de Salud desarrolla estrategias de certificación o acreditación a establecimientos e instituciones que realizan acciones en los diferentes espacios de la vida cotidiana y que cumplen estándares superiores a los exigidos por las normas sanitarias. Con este objetivo la SDS otorga:</p>
            <div class="estrategias">
                <div class="text-center">
                    <!-- <h3 class="text-blue">DISTINCIÓN RESTAURANTES</h3> -->
                    <img src="img/buenpro/estrategias-Restaurantes.svg" alt="DISTINCIÓN RESTAURANTES" title="DISTINCIÓN RESTAURANTES">
                    <p class="text-blue">Restaurantes 1A:</p>
                    <button class="btn yellow">Ver más</button>
                    <button class="btn yellow">Ver video</button>
                </div>
                <div class="text-center">
                    <!-- <h3 class="text-blue">SELLO</h3> -->
                    <!-- <p class="text-blue">DE BIOSEGURIDAD</p> -->
                    <img src="img/buenpro/estrategias-Cosmetologia.svg" alt="BIOSEGURIDAD" title="BIOSEGURIDAD">
                    <p class="text-blue">Acreditación de Centros de Cosmetología y Similares:</p>
                    <button class="btn yellow">Ver más</button>
                    <button class="btn yellow">Ver video</button>
                </div>
                <div class="text-center">
                    <!-- <h3 class="text-blue">TALLERES Y ÓPTICAS</h3> -->
                    <img src="img/buenpro/estrategias-Opticas.svg" alt="TALLERES Y ÓPTICAS" title="TALLERES Y ÓPTICAS">
                    <p class="text-blue">Certificación en adecuación y despensación:</p>
                    <button class="btn yellow">Ver más</button>
                    <button class="btn yellow">Ver video</button>
                </div>
                <div class="text-center">
                    <!-- <h3 class="text-blue">VETERINARIOS</h3> -->
                    <img src="img/buenpro/estrategias-Veterinarios.svg" alt="VETERINARIOS" title="VETERINARIOS">
                    <p class="text-blue">Certificación para veterinarios:</p>
                    <button class="btn yellow">Ver más</button>
                    <button class="btn yellow">Ver video</button>
                </div>
                <div class="text-center">
                    <!-- <h2 class="text-blue">PISA</h2> -->
                    <img src="img/buenpro/estrategias-Pisa.svg" alt="PISA" title="PISA">
                    <p class="text-blue">Certificación en Prevención Integral en Salud Ambiental (PISA)</p>
                    <button class="btn yellow">Ver más</button>
                    <button class="btn yellow">Ver video</button>
                </div>
                <div class="text-center">
                    <!-- <h3 class="text-blue">ADULTOS MAYORES</h3> -->
                    <img src="img/buenpro/estrategias-AdultosMayores.svg" alt="ADULTOS MAYORES" title="ADULTOS MAYORES">
                    <p class="text-blue">Hogar Servicio Dorado:</p>
                    <button class="btn yellow">Ver más</button>
                    <button class="btn yellow">Ver video</button>
                </div>
            </div>
        </div>



        <div class="row block my-5">
            <div class="col-12 col-md-12">
                <div class="subtitle">
                    <h2><b>Normativas sanitarias</b></h2>
                </div>
                <div class="normativas">
                    <div class="normativa">
                        <div class="icon"><img src="img/buenpro/ley.svg" alt="recomendacion" title="recomendacion"></div>
                        <div class="content">
                            <h3 class="text-blue">Ley 9 de 1979</h3>
                            <p>Por la cual se dictan Medidas Sanitarias.</p>
                            <a href="#"><button class="btn yellow">Descargar PDF</button></a>
                        </div>
                    </div>
                    <div class="normativa">
                        <div class="icon"><img src="img/buenpro/ley.svg" alt="recomendacion" title="recomendacion"></div>
                        <div class="content">
                            <h3 class="text-blue">Ley 9 de 1979</h3>
                            <p>Por la cual se dictan Medidas Sanitarias.</p>
                            <a href="#"><button class="btn yellow">Descargar PDF</button></a>
                        </div>
                    </div>
                    <div class="normativa">
                        <div class="icon"><img src="img/buenpro/ley.svg" alt="recomendacion" title="recomendacion"></div>
                        <div class="content">
                            <h3 class="text-blue">Ley 9 de 1979</h3>
                            <p>Por la cual se dictan Medidas Sanitarias.</p>
                            <a href="#"><button class="btn yellow">Descargar PDF</button></a>
                        </div>
                    </div>
                    <div class="normativa">
                        <div class="icon"><img src="img/buenpro/ley.svg" alt="recomendacion" title="recomendacion"></div>
                        <div class="content">
                            <h3 class="text-blue">Ley 9 de 1979</h3>
                            <p>Por la cual se dictan Medidas Sanitarias.</p>
                            <a href="#"><button class="btn yellow">Descargar PDF</button></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

@endsection
