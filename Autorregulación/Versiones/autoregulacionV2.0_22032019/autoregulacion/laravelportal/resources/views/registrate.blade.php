@extends('layout', ['title' => 'Registrate','faqs' => $faq,'te' => $te,'tips' => $tip,'access' => $access ])
@section('content')
        <div class="breadcrumb">
            <ul>
                <li><a href="{{url('/')}}">Inicio</a></li>
                <li><a href="{{url('/registrate')}}">Regístrate</a></li>
            </ul>      
        </div>
        <div class="home_slider">
            <div id="subsideSlider" class="carousel slide" data-ride="carousel">
                
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <div class="container">
                            <img class="w-100" src="img/slider/restaurante.png" alt="">
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="container">
                            <img class="w-100" src="img/slider/carniceria.png" alt="">
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="container">
                            <img class="w-100" src="img/slider/drogueria.png" alt="">
                        </div>
                    </div>
                </div>
                <ol class="carousel-indicators">
                    <li data-target="#subsideSlider" data-slide-to="0" class="active">
                        <span></span>                        
                    </li>
                    <li data-target="#subsideSlider" data-slide-to="1">
                        <span></span>
                    </li>
                    <li data-target="#subsideSlider" data-slide-to="2">
                        <span></span>
                    </li>
                </ol>
            </div>
        </div>

        <div class="row block right">
            <div class="col-12 col-md-7">
                <div class="subtitle">
                    <h2>Regístrate</h2>
                </div>
                <div class="paragraph">
                    <p>
                        La Secretaría Distrital de Salud pone a disposición de los propietarios y representantes legales de los establecimientos de Bogotá, una plataforma digital que cuenta con herramientas dirigidas a la información y educación sanitaria y que permite la inscripción, autoevaluación y solicitud de concepto sanitario.
                    </p>
                </div>
            </div>
            <div class="col-12 col-md-5 text-center">
                <img class="w-80 m-auto my-4" src="img/registro/01.svg" alt="registrate" title="registrate">
            </div>
        </div>

        <div class="row block right">
            <div class="col-12 col-md-4 text-center ">
                <img class="w-80 m-auto my-4" src="img/registro/02.svg" alt="establecimientos" title="establecimientos">
            </div>
            <div class="col-12 col-md-8 pl-4">
                <div class="subtitle">
                    Establecimientos
                </div>
                <div class="paragraph">
                    <p>
                        Todos los establecimientos productores o proveedores involucrados en cadenas productivas de bienes y servicios, deben registrar su existencia como sujeto de vigilancia y control sanitario (art. 13 de la Resolución 1229 de 2013).
                    </p>
                    
                </div>
                <div class="regitro_triada">
                    <a href=""><button class="btn yellow">1. Regístrate</button></a>
                    <i class="fa fa-chevron-right"></i>
                    <a href=""><button class="btn yellow">2. verifica</button></a>
                    <i class="fa fa-chevron-right"></i>
                    <a href=""><button class="btn yellow">3. Solicita una vista</button></a>
                </div>
            </div>
        </div>
        <div class="block">
            <div class="row">
                <h2 class="text-blue"><b>Otros tipos</b> de inscripción </h2>
            </div>
            <div class="row right mb-4">
                <div class="col-12 col-md-8">
                    <div class="subtitle">
                        <h3>Vehículos</h3>
                    </div>
                    <div class="paragraph">
                        <p>
                            Todos los propietarios de vehículos que transporten carne y productos cárnicos, agua potable, medicamentos y vehículos o carrozas fúnebres, deben realizar el respectivo registro por cada vehículo que desarrolle estas actividades.
                        </p>
                    </div>
                    <div class="text-left regitro_triada">
                        <a href=""><button class="btn yellow">1. Registra tu vehiculo</button></a>
                        <i class="fa fa-chevron-right"></i>
                        <a href=""><button class="btn yellow">2. Autorización Sanitaria</button></a>
                    </div>
                </div>
                <div class="col-12 col-md-4 text-center">
                    <img class="w-80 m-auto my-4" src="img/registro/vehiculos.svg" alt="vehiculos" title="vehiculos">
                </div>
            </div>
            <div class="row mt-4 pt-4">
                <div class="col-12 col-md-5 text-center">
                    <img class="w-50 m-auto" src="img/registro/tatuadores.svg" alt="tatuadores" title="tatuadores">
                </div>
                <div class="col-12 col-md-7">
                    <div class="subtitle">
                        <h3>Tatuadores</h3>
                    </div>
                    <div class="paragraph">
                        <p>
                            Los procedimientos de tatuaje y perforación corporal se constituyen en una técnica artística moderna, de carácter invasivo, que incurre en riesgos para la salud. El Acuerdo Distrital 103 de 2003 establece el registro de inscripción ante esta Secretaría para los tatuadores y/o perforadores corporales que presten sus servicios en el Distrito Capital.
                        </p>
                    </div>
                    <div class="text-left">
                        <a href="http://appb.saludcapital.gov.co/SivigilaDC/Login.aspx" target="_blank"><button class="btn yellow">Regístrate</button></a>
                    </div>
                </div>
            </div>
        </div>


        <div class="row block right">
            <div class="col-12 col-md-5">
                <img class="w-50 m-auto my-4" src="img/registro/visita.svg" alt="alternative text">
            </div>
            <div class="col-12 col-md-7 pl-4">
                <div class="subtitle">
                    Solicita una visita
                </div>
                <div class="paragraph">
                    <p>
                        Si ya conociste la norma sanitaria, registraste tu negocio, te autoevaluaste a través de una lista de chequeo y estás listo para recibir la Visita de Inspección, Vigilancia y Control, solicítala a través del siguiente formulario. Recuerda que todos los establecimientos serán visitados. 
                    </p>
                </div>
                <div class="text-right">
                    <a href=""><button class="btn yellow">Solicita una visita</button></a>
                </div>
            </div>
        </div>


        <div class="row subsections my-5">
            <ol>
                <li >
                    <div class="logo logo_rest"></div>
                    <div class="text">Restaurantes</div>
                    <div class="text-right mt-2">
                        <div class="btn yellow"><a href="{{url('/restaurantes')}}" >Ver requisitos</a></div>
                    </div>
                </li>
                <li>
                    <div class="logo logo_carne"></div>
                    <div class="text">Expendios de Carne</div>
                    <div class="text-right mt-2">
                        <div class="btn yellow"><a href="{{url('/carnicerias')}}" >Ver requisitos</a></div>
                    </div>
                </li>
                <li>
                    <div class="logo logo_drog"></div>
                    <div class="text">Droguerías</div>
                    <div class="text-right mt-2">
                        <div class="btn yellow"><a href="{{url('/droguerias')}}" >Ver requisitos</a></div>
                    </div>
                </li>
                <li>
                    <div class="logo logo_salon"></div>
                    <div class="text">Salones de belleza</div>
                    <div class="text-right mt-2">
                        <div class="btn yellow"><a href="{{url('/salonesbelleza')}}" >Ver requisitos</a></div>
                    </div>
                </li>
            </ol>
        </div>
@endsection
