@extends('layout',['title' => 'Novedades','faqs' => $faq,'te' => $te,'tips' => $tip,'access' => $access ])
@section('content')
    <div class="breadcrumb">
        <ul>
            <li><a href="{{url('/')}}">Inicio</a></li>
            <li><a href="{{url('/novedades')}}">Novedades</a></li>
        </ul>      
    </div>
    <div class="home_slider">
        <div id="subsideSlider" class="carousel slide" data-ride="carousel">
            
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <div class="container">
                        <img class="w-100" src="img/slider/restaurante.png" alt="">
                    </div>
                </div>
                <div class="carousel-item">
                    <div class="container">
                        <img class="w-100" src="img/slider/carniceria.png" alt="">
                    </div>
                </div>
                <div class="carousel-item">
                    <div class="container">
                        <img class="w-100" src="img/slider/drogueria.png" alt="">
                    </div>
                </div>
            </div>
            <ol class="carousel-indicators">
                <li data-target="#subsideSlider" data-slide-to="0" class="active">
                    <span></span>                        
                </li>
                <li data-target="#subsideSlider" data-slide-to="1">
                    <span></span>
                </li>
                <li data-target="#subsideSlider" data-slide-to="2">
                    <span></span>
                </li>
            </ol>
        </div>
    </div>

    <div class="row block noticias">
        <div class="col-12">
            <div class="row">
                <div class="col-12 col-lg-3 text-center">
                    <img class="w-80 m-auto my-4" src="img/novedades/noticias.svg" alt="alternative text">
                </div>
                <div class="col-12 col-lg-9">
                    <div class="subtitle">
                        <h1>Noticias</h1>
                    </div> 
                </div>
            </div>
        </div>
        <div class="articulos col-12 mt-4">
            <table id="newsTable" class="table w-100">
                <thead>
                    <tr><th> </th><th> </th><th> </th></tr>
                </thead>
                @foreach($noticias as $n)
                    <tr>
                        <td class="date">
                            {{$n->created_at}}
                        </td>
                        <td class="info">
                            <div class="text-blue">{{$n->resumen}}</div>
                        </td>
                        <td class="text-center">
                            <img class="w-80 m-auto my-4" src="{{$n->imagen}}" alt="{{$n->titulo}}"> 
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>


    <div class="row block right">
        
        <div class="col-12 col-md-7 pl-4">
            <div class="subtitle">
                Capacitaciones
            </div>
            <div class="paragraph">
                <p>
                    Lorem ipsum dolor, sit amet consectetur adipisicing elit. Aperiam, magni vitae eum adipisci est id quisquam placeat porro reiciendis ipsa voluptates quaerat, cumque minus qui ut dignissimos, nemo aliquid iste?Cupiditate, sequi nihil maiores possimus in fugiat quisquam necessitatibus dolorem quibusdam provident inventore atque qui consectetur aut officiis accusantium corrupti! Eligendi eum cum libero vitae eius suscipit iure dolorem voluptatem.
                </p>
            </div>
        </div>
        <div class="col-12 col-md-5 text-center">
            <img class="w-80 m-auto my-4" src="img/novedades/capacitaciones.svg" alt="Capacitaciones" title="Capacitaciones">
        </div>
    </div>


    <div class="row block right">
        <div class="col-12 col-md-5 text-center">
            <img class="w-70 m-auto my-4" src="img/novedades/legislaciones.svg" alt="Legislaciones" title="Legislaciones">
        </div>
        <div class="col-12 col-md-7">
            <div class="subtitle">
                Nueva <b>Legislación</b>
            </div>
            <div class="paragraph">
                <p>
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Temporibus totam tenetur possimus, consequuntur officia ea minima alias tempore aliquam vitae. Quos possimus molestiae ipsa laboriosam modi debitis, voluptatibus ea. Molestias? <br>
                    Itaque, consequatur quas obcaecati voluptatem iure aut recusandae dicta ex, nostrum laborum similique ab! Possimus hic cum vitae aperiam, in eius debitis eaque veritatis neque aspernatur nobis voluptatum quo commodi. <br>
                    Officia illum labore cupiditate rerum optio consequatur, eligendi voluptatibus adipisci ab libero pariatur ipsa similique earum. Placeat, expedita officiis a necessitatibus laboriosam reprehenderit odio, esse qui, quibusdam autem ipsam illo.
                </p>
            </div>
        </div>
        
    </div>
    <div class="row block right">
        <div class="col-12 col-md-7">
            <div class="subtitle">
                Consulte ahora los negocios <b>con Concepto Favorable</b>
            </div>
            <div class="paragraph">
                <p>
                    Encuentra aquí todos los establecimientos de la ciudad que cumplen 100 % con la norma sanitaria. La Secretaría Distrital de Salud vigila el cumplimiento de los requisitos legales, para prevenir riesgos en la salud. Si deseas aparecer en este listado, regístrate, verifica si cumple la norma y solicita una visita de inspección.
                </p>
            </div>
            <div class="text-left">
                <button class="btn yellow" data-toggle="modal" data-target="#consulta">Consulta aquí</button>
            </div>
        </div>
        <div class="col-12 col-md-5 text-center">
            <img class="w-80 m-auto my-4" src="img/icons/home_concepto_favorable.svg" alt="Concepto favorable" title="Concepto favorable">
        </div>
    </div>



    <!-- Modal -->
    <div class="modal fade" id="consulta" tabindex="-1" role="dialog" aria-labelledby="consultaLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="right-content w-100 my-2">
                <input id="term" class="form-control mr-sm-2" type="text" placeholder="Nombre Comercial" aria-label="Nombre Comercial">
            </div>
            <button id="btn_consultar" class="btn yellow mb-4">Consultar</button>
            <table id="modalTable" class="table table-striped table-bordered w-100">
               <thead><tr><th>Nombre Comercial</th><th>Razón Social</th></tr></thead>
            </table>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        </div>
        </div>
    </div>
    </div>

@endsection


@section ("scripts")
    <script languague="javascript">           
        $(document).ready(function () {
            $("#btn_consultar").click(function(){
                var theval=$("#term").val();
                $.post("{{url('/consultar')}}",{'term':theval,'_token':'{{ csrf_token() }}'},function(data){
                    if(data=="error"){
                        $("#modalTable").DataTable().clear().draw(false);
                    }else{
                        var tabla="";
                        jQuery.each(data,function(i,val){
                            tabla="<tr><td>"+val.NombreComercial+"</td><td>"+val.RazonSocial+"</td></tr>";
                            $("#modalTable").DataTable().row.add($(tabla));
                        });
                        $("#modalTable").DataTable().draw();
                    }
                });
            });
            $('#modalTable').DataTable( {
                searching: false,
                "pageLength": 5,
                "language": {
                    "lengthMenu": "Mostrar _MENU_ Registros por página",
                    "zeroRecords": "No hay resultados.",
                    "info": "Mostrando página _PAGE_ de _PAGES_",
                    "infoEmpty": "No hay registros disponibles",
                    "infoFiltered": "(filtered from _MAX_ total records)",
                    "paginate": {
                        "first":      "Primero",
                        "previous":   "Anterior",
                        "next":       "Siguiente",
                        "last":       "Último"
                    },
                }
            });


            $('#newsTable').DataTable( {
                searching: false,
                ordering:  false,
                "info": false,
                "lengthChange": false,
                "pageLength": 2,
                "language": {
                    "lengthMenu": "Mostrar _MENU_ Registros por página",
                    "zeroRecords": "No hay resultados.",
                    "info": "Mostrando página _PAGE_ de _PAGES_",
                    "infoEmpty": "No hay registros disponibles",
                    "infoFiltered": "(filtered from _MAX_ total records)",
                    "paginate": {
                        "first":      "Primero",
                        "previous":   "Anterior",
                        "next":       "Siguiente",
                        "last":       "Último"
                    },
                }
            });
            

        });
    </script>
@endsection