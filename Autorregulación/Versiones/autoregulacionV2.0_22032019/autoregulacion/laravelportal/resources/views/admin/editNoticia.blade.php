@extends('admin.layout')

@section('sectionTitle') Noticia @endsection
@section('content')
    
    <form action="{{url()->current()}}" method="POST" enctype="multipart/form-data">
        <input type="hidden" value="{{$id}}">
        <div class="form-group mt-2">
            <label for="">Título</label>
            <input name="titulo" type="text" class="form-control" value="{{$titulo}}">
        </div>
        <div class="form-group mt-2">
            <label for="">Resumen</label>
            <textarea name="resumen" class="form-control" rows="3">{{$resumen}}</textarea>
        </div>
        <div class="form-group mt-2">
            <label for="">Contenido</label>
            <textarea name="contenido" class="form-control" rows="3">{{$contenido}}</textarea>
        </div>
        <div class="row">
            <div class="col-12 col-md-6">
                <div class="form-group mt-2">
                    <label for="">Imágen</label>
                    <input type="file">
                </div>
            </div>
            <div class="col-12 col-md-6">
                @if( !empty($imagen) )
                    <img src="{{url('/files/').$imagen}}" alt="">
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-3">
                <button type="submit">Guardar</button>
            </div>
            <div class="col-3">
                @if(!empty($id))
                    @if($estado==0)
                        <button>Activar</button>
                    @else
                        <button>Desactivar</button>
                    @endif
                @endif
            </div>
        </div>
    </form>

@endsection