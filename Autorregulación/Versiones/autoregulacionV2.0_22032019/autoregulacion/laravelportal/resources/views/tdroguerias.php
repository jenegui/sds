@extends('layout', ['title' => 'Autorregulación','faqs' => $faq,'te' => $te,'tips' => $tip ])
@section('content')
    <div class="breadcrumb">
            <ul>
                <li><a href="{{url('/')}}">Inicio</a></li>
                <li><a href="{{url('/restaurantes')}}">Restaurantes</a></li>
            </ul>      
        </div>
        <div class="home_slider">
            <div id="subsideSlider" class="carousel slide" data-ride="carousel">
                
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <div class="container">
                            <img class="w-100" src="img/slider/restaurante.png" alt="">
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="container">
                            <img class="w-100" src="img/slider/carniceria.png" alt="">
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="container">
                            <img class="w-100" src="img/slider/drogueria.png" alt="">
                        </div>
                    </div>
                </div>
                <ol class="carousel-indicators">
                    <li data-target="#subsideSlider" data-slide-to="0" class="active">
                        <span></span>                        
                    </li>
                    <li data-target="#subsideSlider" data-slide-to="1">
                        <span></span>
                    </li>
                    <li data-target="#subsideSlider" data-slide-to="2">
                        <span></span>
                    </li>
                </ol>
            </div>
        </div>

        <div class="row block right">
            <div class="col-12 col-md-5">
                <img class="w-100" src="img/icons/home_1.svg" alt="alternative text">
            </div>
            <div class="col-12 col-md-7 pl-4">
                <div class="subtitle">
                    <h2><b>¿Qué debo saber</b> si tengo o tendré un restaurante?</h2>
                </div>
                <div class="paragraph">
                    <p>
                    Los restaurantes son establecimientos dedicados a la preparación y consumo de alimentos. Se encuentran regulados por la Ley 09 de 1979 “Código Sanitario Nacional” y por la Resolución 2674 de 2013, entre otras normas. Dicha normativa puede ser consultada en la página <a href="www.saludcapital.gov.co" target="_new">www.saludcapital.gov.co.</a> 
                    </p>
                </div>
                <div class="subtitle">
                   <h2><b>¿Qué servicios</b> se pueden ofrecer?</h2>
                </div>
                <div class="paragraph">
                    <p>
                    Preparación de alimentos para consumo en el mismo sitio (desayuno, almuerzo o comida). 
                    </p>
                </div>
            </div>
        </div>
        <div class="row block">
            <div class="col-12 col-md-10">
                <div class="subtitle">
                    <h2><b>¿Qué requerimientos</b> se deben cumplir?</h2>
                </div>
            </div>
        </div>

        <div class="row subsections my-5">
            <ol>
                <li>
                    <div class="logo logo_rest"></div>
                    <div class="text">Restaurantes</div>
                    <div class="text-right mt-2">
                        <div class="btn yellow"><a href="{{url('/restaurantes')}}" >Ver requisitos</a></div>
                    </div>
                </li>
                <li>
                    <div class="logo logo_carne"></div>
                    <div class="text">Expendios de Carne</div>
                    <div class="text-right mt-2">
                        <div class="btn yellow"><a href="{{url('/carnicerias')}}" >Ver requisitos</a></div>
                    </div>
                </li>
                <li class="active">
                    <div class="logo logo_drog"></div>
                    <div class="text">Droguerías</div>
                    <div class="text-right mt-2">
                        <div class="btn yellow"><a href="{{url('/droguerias')}}" >Ver requisitos</a></div>
                    </div>
                </li>
                <li>
                    <div class="logo logo_salon"></div>
                    <div class="text">Salones de belleza</div>
                    <div class="text-right mt-2">
                        <div class="btn yellow"><a href="{{url('/salonesbelleza')}}" >Ver requisitos</a></div>
                    </div>
                </li>
            </ol>
        </div>
@endsection