
@extends('layout', ['title' => 'Home','faqs' => $faq,'te' => $te,'tips' => $tip,'access' => $access ])
@section('content')
        <div class="breadcrumb">
            <ul>
                <li><a href="{{url('/')}}">Inicio</a></li>
            </ul>      
        </div>
        @isset($msg)
        <div class="alert alert-primary" role="alert">
            {{$msg}}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        @endisset
        <div class="main_slider">
            <div id="homeSlider" class="carousel slide" data-ride="carousel">
                
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <div class="container">
                            <img class="w-100" src="img/slider/restaurante.png" alt="">
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="container">
                            <img class="w-100" src="img/slider/carniceria.png" alt="">
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="container">
                            <img class="w-100" src="img/slider/drogueria.png" alt="">
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="container">
                            <img class="w-100" src="img/slider/peluqueria.png" alt="">
                        </div>
                    </div>
                </div>
                <ol class="carousel-indicators">
                    <li class="active">
                        <div data-target="#homeSlider" data-slide-to="0" >
                            <div class="logo logo_rest"></div>
                            <div class="text">Restaurantes</div>
                        </div>
                        <div class="text-right mt-2">
                            <div class="btn yellow"><a href="{{url('/restaurantes')}}" >Ver requisitos</a></div>
                        </div>
                    </li>
                    <li >
                        <div data-target="#homeSlider" data-slide-to="1" >
                            <div class="logo logo_carne"></div>
                            <div class="text">Expendios de Carne</div>
                        </div>
                        <div class="text-right mt-2">
                            <div class="btn yellow"><a href="{{url('/carnicerias')}}" >Ver requisitos</a></div>
                        </div>
                    </li>
                    <li >
                        <div data-target="#homeSlider" data-slide-to="2" >
                            <div class="logo logo_drog"></div>
                            <div class="text">Droguerías</div>
                        </div>
                        <div class="text-right mt-2">
                            <div class="btn yellow"><a href="{{url('/droguerias')}}" >Ver requisitos</a></div>
                        </div>
                    </li>
                    <li >
                        <div data-target="#homeSlider" data-slide-to="3" >
                            <div class="logo logo_salon"></div>
                            <div class="text">Salones de belleza</div>
                        </div>
                        <div class="text-right mt-2">
                            <div class="btn yellow"><a href="{{url('/salonesbelleza')}}" >Ver requisitos</a></div>
                        </div>
                    </li>
                </ol>
            </div>
        </div>

        <div class="row block right">
            <div class="col-12 col-md-5">
                <img class="w-100" src="img/icons/home_1.svg" alt="alternative text">
            </div>
            <div class="col-12 col-md-7 pl-4">
                <div class="subtitle">
                    <h2>Con la autorregulación, la salud de tu negocio está en tus manos.</h2>
                </div>
                <div class="paragraph">
                    <p>
                        Todos los establecimientos que son objeto de inspección, vigilancia y control sanitario, deben vincularse al proceso de autorregulación el cual busca que todos los propietarios o representantes legales de establecimientos de Bogotá D.C., aseguren el cumplimiento de los requisitos vigentes y no generen riesgos a la salud de los habitantes y visitantes de la
                        capital.
                    </p>
                    <p>
                        La Secretaría Distrital de Salud presenta esta plataforma digital que cuenta con herramientas dirigidas a la información sanitaria y que permite la inscripción, autoevaluación, solicitud de concepto y autorregulación de todos los establecimientos.
                    </p>
                </div>
                <div class="text-right">
                    <a href=""><button class="btn yellow">Inscribir mi negocio ahora</button></a>
                </div>
            </div>
        </div>
        <div class="row block right">
            <div class="col-12 col-md-7">
                <div class="subtitle">
                    <h2>Consulte ahora los negocios con Concepto Favorable</h2>
                </div>
                <div class="paragraph">
                    <p>
                        Encuentra aquí todos los establecimientos de la ciudad que cumplen 100 % con la norma sanitaria. La Secretaría Distrital
                        de Salud vigila el cumplimiento de los requisitos legales, para prevenir riesgos en la salud. Si deseas aparecer en este
                        listado, regístrate, verifica si cumple la norma y solicita una visita de inspección.
                    </p>
                </div>
                <div class="text-left">
                    <button class="btn yellow" data-toggle="modal" data-target="#consulta">Consulta aquí</button>
                </div>
            </div>
            <div class="col-12 col-md-5">
                <img class="w-100" src="img/icons/home_concepto_favorable.svg" alt="alternative text">
            </div>
        </div>


    <!-- Modal -->
    <div class="modal fade" id="consulta" tabindex="-1" role="dialog" aria-labelledby="consultaLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="right-content w-100 my-2">
                <input id="term" class="form-control mr-sm-2" type="text" placeholder="Nombre Comercial" aria-label="Nombre Comercial">
            </div>
            <button id="btn_consultar" class="btn yellow mb-4">Consultar</button>
            <table id="modalTable" class="table table-striped table-bordered w-100">
               <thead><tr><th>Nombre Comercial</th><th>Razón Social</th></tr></thead>
            </table>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        </div>
        </div>
    </div>
    </div>

@endsection


@section("redes")
    <main class="container">
        <div class="row block redes">
            <div class="col-12 col-md-7">
                <iframe width="560" height="315" src="https://www.youtube.com/embed/videoseries?list=PLaHEra_rXLyV9NBLjpMAfcEnyxsxyKvHl" frameborder="0"
                    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
            <div class="col-12 col-md-5">
                <div class="twitter-feed">
                    <a class="twitter-timeline" data-height="515" href="https://twitter.com/SectorSalud?ref_src=twsrc%5Etfw">Tweets by SectorSalud</a>
                    <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
                </div>
                
            </div>
        </div>
    </main>
@endsection


@section ("scripts")
    <script languague="javascript">           
    $(document).ready(function () {
        $("#btn_consultar").click(function(){
            //$("#modalTable").html("");
            var theval=$("#term").val();
            $.post("{{url('/consultar')}}",{'term':theval,'_token':'{{ csrf_token() }}'},function(data){
                if(data=="error"){
                    //$("#modalTable").html("No hay resultados");
                    $("#modalTable").DataTable().clear().draw(false);
                }else{
                    //$("#modalTable").DataTable().clear()
                    //var tabla="<thead><tr><th>Nombre Comercial</th><th>Razón Social</th></tr></thead>";
                    var tabla="";
                    jQuery.each(data,function(i,val){
                        tabla="<tr><td>"+val.NombreComercial+"</td><td>"+val.RazonSocial+"</td></tr>";
                        $("#modalTable").DataTable().row.add($(tabla));
                    });
                    
                    //$("#modalTable").html(tabla);
                   
                    $("#modalTable").DataTable().draw();
                }
            });
        });
        $('#modalTable').DataTable( {
            searching: false,
            "pageLength": 5,
            "language": {
                "lengthMenu": "Mostrar _MENU_ Registros por página",
                "zeroRecords": "No hay resultados.",
                "info": "Mostrando página _PAGE_ de _PAGES_",
                "infoEmpty": "No hay registros disponibles",
                "infoFiltered": "(filtered from _MAX_ total records)",
                "paginate": {
                    "first":      "Primero",
                    "previous":   "Anterior",
                    "next":       "Siguiente",
                    "last":       "Último"
                },
            }
        });
        
    });
    </script>
@endsection