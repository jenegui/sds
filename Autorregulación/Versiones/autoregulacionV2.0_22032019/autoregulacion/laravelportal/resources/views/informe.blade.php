@extends('layout',['title' => 'Informes','faqs' => $faq,'te' => $te,'tips' => $tip,'access' => $access ])
@section('content')
        <div class="breadcrumb">
            <ul>
                <li><a href="{{url('/')}}">Inicio</a></li>
                <li><a href="{{url('/informe')}}">Envía tu informe</a></li>
            </ul>      
        </div>
        <div class="home_slider">
            <div id="subsideSlider" class="carousel slide" data-ride="carousel">
                
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <div class="container">
                            <img class="w-100" src="img/slider/restaurante.png" alt="">
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="container">
                            <img class="w-100" src="img/slider/carniceria.png" alt="">
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="container">
                            <img class="w-100" src="img/slider/drogueria.png" alt="">
                        </div>
                    </div>
                </div>
                <ol class="carousel-indicators">
                    <li data-target="#subsideSlider" data-slide-to="0" class="active">
                        <span></span>                        
                    </li>
                    <li data-target="#subsideSlider" data-slide-to="1">
                        <span></span>
                    </li>
                    <li data-target="#subsideSlider" data-slide-to="2">
                        <span></span>
                    </li>
                </ol>
            </div>
        </div>

        <div class="row block right">
            <div class="col-12 col-md-5 text-center">
                <img class="w-60 mx-auto my-5" src="img/informes/registro.svg" alt="alternative text">
            </div>
            <div class="col-12 col-md-7 pl-4">
                <div class="subtitle">
                    Registro de información
                </div>
                <div class="paragraph">
                    <p>
                        Es importante que conozcas que <b>algunas actividades comerciales deben remitir informes a la autoridad sanitaria.</b> Aquí encontrarás los respectivos informes a realizar según la actividad desarrollada.
                    </p>
                    <p>
                        <b>Para el reporte de generación de residuos hospitalarios, a continuación, se detallan algunas de las actividades que deben reportar:</b> clínicas y consultorios veterinarios, laboratorios de tanatopraxia, clínicos y veterinarios, peluquerías, salas de belleza, barberías, peluquerías para cuidado de animales domésticos, droguería y farmacias droguerías, hogares gerontológicos, batallones, cuarteles, establecimientos penitenciaros y carcelarios, sitios de encuentro sexual, anfiteatros, funerarias y sala de cremación, cementerios, moteles y residencias, establecimientos de tatuaje y perforación, entre otros.tablecimientos.
                    </p>
                </div>
            </div>
        </div>


        <div class="row block right">
            <div class="col-12 col-md-7">
                <div class="subtitle">
                    Veterinarias
                </div>
                <div class="paragraph">
                    <p>
                        Diagnóstico de zoonosis y vacunación antirrábica De conformidad con lo descrito en el Decreto 780 de 2016 y la Resolución 446 de 2018, <b>toda clínica y consultorio veterinario de la ciudad, debe reportar obligatoriamente cada mes la vacuna antirrábica administrada, el número de esterilizaciones realizadas y las enfermedades zoonóticas</b> que hayan sido diagnosticadas. 
                    </p>
                    <p>
                        El reporte de vacunación y esterilización, se hace mes vencido durante los primeros 15 días de cada mes; el reporte de enfermedades zoonóticas, se deberá hacer de forma inmediata una vez se tenga el resultado de la prueba que confirme el diagnóstico.
                    </p>
                </div>
                <div class="text-left">
                    <a href="#"><button class="btn yellow">Ir al formulario</button></a>
                </div>
            </div>
            <div class="col-12 col-md-5 text-center">
                <img class="w-80 mx-auto my-5" src="img/informes/veterinaria.svg" alt="alternative text">
            </div>
        </div>
        

        <div class="row block right">
            <div class="col-12 col-md-5 text-center">
                <img class="w-50 mx-auto my-5" src="img/informes/bolsasSuero.svg" alt="alternative text">
            </div>
            <div class="col-12 col-md-7 pl-4">
                <div class="subtitle">
                    Aprovechamiento <br><b>Bolsas de Suero</b>
                </div>
                <div class="paragraph">
                    <p>
                        Toda empresa que aproveche o recicle las bolsas o recipientes generados como residuos de la atención en salud deberá informar, durante el primer trimestre del año, la cantidad de residuos recolectados, almacenados o procesados por mes del año inmediatamente anterior, así como las ciudades de procedencia y los generadores.
                    </p>
                </div>
                <div class="text-left">
                    <a href="#"><button class="btn yellow">Ir al formulario</button></a>
                </div>
            </div>
        </div>

        <div class="row block right">
            <div class="col-12 col-md-7">
                <div class="subtitle">
                    Residuos Hospitalarios
                </div>
                <div class="paragraph">
                    <p>
                        El informe de gestión integral de residuos de riesgo biológico consiste en el <b>reporte y análisis de los indicadores de los generadores de residuos hospitalarios y similares a la secretaria de salud,</b> de forma anual, de acuerdo a lo establecido en el Decreto 780 de 2016, Parte 8. Normas Relativas a la Salud Pública, Titulo 10 Gestión Integral de los Residuos Generados en la Atención de Salud y Otras Actividades.
                    </p>
                </div>
                <div class="text-left">
                    <a href="#"><button class="btn yellow">Ir al formulario</button></a>
                </div>
            </div>
            <div class="col-12 col-md-5 text-center">
                <img class="w-80 mx-auto my-5" src="img/informes/residuos.svg" alt="alternative text">
            </div>
        </div>
@endsection
