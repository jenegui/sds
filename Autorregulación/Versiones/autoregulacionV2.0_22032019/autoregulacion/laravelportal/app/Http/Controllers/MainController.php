<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\{Entrada,Establecimiento,Newsletter,Noticia,Tipos_de_establecimiento,User};


class MainController extends Controller
{
    function home(Request $request){
        $faq=Entrada::where('tipo','faqs')->get();
        $tip=Entrada::where('tipo','tips')->get();
        $te=Tipos_de_establecimiento::all();
        $msg=$request->session()->pull('msg',null);
        $access=$request->session()->get('access',null);
        return view('home',['faq'=>$faq,'te'=>$te,'tip'=>$tip,'msg'=>$msg,'access'=>$access]);
    }
    
    function accesible(Request $request){
        $access=$request->session()->pull('access',null);
        if($access==null){
            $request->session()->put('access','active');
        }
        return redirect("/"); 
    }

    function register(Request $request){
        $faq=Entrada::where('tipo','faqs')->get();
        $tip=Entrada::where('tipo','tips')->get();
        $te=Tipos_de_establecimiento::all();

        $tipo=$request->input('tipo');
        $nombre=$request->input('nombre');
        $correo=$request->input('correo');
        $telefono=$request->input('telefono');

        //dd('registrando');
        $news=new Newsletter;
        
        $news->id_tipo_establecimiento=$tipo;
        $news->name=$nombre;
        $news->correo_e=$correo;
        $news->telefono=$telefono;
        $news->save();
        $msg="";
        if($news->id){
             $msg="Te has inscrito correctamente a nuestro newsletter, espera actualizaciones y más noticias en tu bandeja de correo!";
        }else{
             $msg="Error, intente más tarde.";
        }
        $request->session()->put('msg',$msg);
        return redirect("/"); 
    }

    function autorregulacion(Request $request){
        $faq=Entrada::where('tipo','faqs')->get();
        $tip=Entrada::where('tipo','tips')->get();
        $te=Tipos_de_establecimiento::all();
        $msg=$request->session()->pull('msg',null);
        $access=$request->session()->get('access',null);
        
        return view('autorregulacion',['faq'=>$faq,'te'=>$te,'tip'=>$tip,'access'=>$access]);
    }

    function registrate(Request $request){
        $faq=Entrada::where('tipo','faqs')->get();
        $tip=Entrada::where('tipo','tips')->get();
        $te=Tipos_de_establecimiento::all();
        $access=$request->session()->get('access',null);
        return view('registrate',['faq'=>$faq,'te'=>$te,'tip'=>$tip,'access'=>$access]);
    }

    function buenpropietario(Request $request){
        $faq=Entrada::where('tipo','faqs')->get();
        $tip=Entrada::where('tipo','tips')->get();
        $te=Tipos_de_establecimiento::all();
        $access=$request->session()->get('access',null);
        return view('buenpropietario',['faq'=>$faq,'te'=>$te,'tip'=>$tip,'access'=>$access]);
    }

    function novedades(Request $request){
        $faq=Entrada::where('tipo','faqs')->get();
        $tip=Entrada::where('tipo','tips')->get();
        $te=Tipos_de_establecimiento::all();
        $access=$request->session()->get('access',null);
        $noticias=Noticia::where('estado', 1)->take(10)->get();
        return view('novedades',['faq'=>$faq,'te'=>$te,'tip'=>$tip,'access'=>$access,'noticias'=>$noticias]);
    }

    function informe(Request $request){
        $faq=Entrada::where('tipo','faqs')->get();
        $tip=Entrada::where('tipo','tips')->get();
        $te=Tipos_de_establecimiento::all();
        $access=$request->session()->get('access',null);
        return view('informe',['faq'=>$faq,'te'=>$te,'tip'=>$tip,'access'=>$access]);
    }


    function search_for_establecimiento(Request $request){
        $term=$request->input('term','');
        if(!empty($term)){
            return Establecimiento::where('NombreComercial','like',"%$term%")->get();
        }else{
            //echo json_encode(array("status"=>"error") );
            echo "error";
        }
    }


    
    
    function restaurantes(){
        $faq=Entrada::where('tipo','faqs')->get();
        $tip=Entrada::where('tipo','tips')->get();
        $te=Tipos_de_establecimiento::all();
        $access=$request->session()->get('access',null);
        return view('trestaurantes',['faq'=>$faq,'te'=>$te,'tip'=>$tip,'access'=>$access]);
    }
    function carnicerias(){
        $faq=Entrada::where('tipo','faqs')->get();
        $tip=Entrada::where('tipo','tips')->get();
        $te=Tipos_de_establecimiento::all();
        $access=$request->session()->get('access',null);
        return view('tcarnicerias',['faq'=>$faq,'te'=>$te,'tip'=>$tip,'access'=>$access]);
    }
    function salonesbelleza(){
        $faq=Entrada::where('tipo','faqs')->get();
        $tip=Entrada::where('tipo','tips')->get();
        $te=Tipos_de_establecimiento::all();
        $access=$request->session()->get('access',null);
        return view('tbelleza',['faq'=>$faq,'te'=>$te,'tip'=>$tip,'access'=>$access]);
    }
    function droguerias(){
        $faq=Entrada::where('tipo','faqs')->get();
        $tip=Entrada::where('tipo','tips')->get();
        $te=Tipos_de_establecimiento::all();
        $access=$request->session()->get('access',null);
        return view('tdroguerias',['faq'=>$faq,'te'=>$te,'tip'=>$tip,'access'=>$access]);
    }
}
