<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class AdminController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    function home(){

        //return view('admin.login');
        return redirect(url('admin/stats'));
    }



    function stats(){
        return view('admin.stats');
    }


    function newsletter(){
        $results=[];
        return view('admin.newsletter',['results'=>$results]);
    }


    function entradas(){
        $results=[];
        return view('admin.entradas',['results'=>$results]);
    }
    function editEntrada(){
        $results=['id'=>"",'titulo'=>"",'contenido'=>"",'imagen'=>"",'tipo'=>"",'estado'=>""];
        return view('admin.editEntrada',$results);
    }
    function newEntrada(){
        $results=['id'=>"",'titulo'=>"",'contenido'=>"",'imagen'=>"",'tipo'=>"",'estado'=>""];
        return view('admin.editEntrada',$results);
    }
  

    function noticias(){
        $results=[];
        return view('admin.noticias',['results'=>$results]);
    }

    function editNoticia(){
        $results=['id'=>"",'titulo'=>"",'resumen'=>"",'contenido'=>"",'imagen'=>"",'estado'=>""];
        return view('admin.editNoticia',$results);
    }

    function newNoticia(){
        $results=['id'=>"",'titulo'=>"",'resumen'=>"",'contenido'=>"",'imagen'=>"",'estado'=>""];
        return view('admin.editNoticia',$results);

    }
}