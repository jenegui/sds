<?php

use Illuminate\Database\Seeder;
use App\Models\Noticia;

class NoticiasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Noticia::create([
            'titulo'=>'Secretaría de Salud entrega reconocimiento a 25 restaurantes populares por su calidad y cumplimiento sanitario',
            'resumen'=>'La Secretaría Distrital de Salud (SDS) entregó la distinción "Restaurantes 1A" a 25 establecimientos populares de la ciudad que se destacan por cumplir con las normas sanitarias y contar con menús balanceados y variados, la adecuada manipulación de alimentos y buenos niveles de calidad y atención.',
            'contenido'=>'La Secretaría Distrital de Salud (SDS) entregó la distinción "Restaurantes 1A" a 25 establecimientos populares de la ciudad que se destacan por cumplir con las normas sanitarias y contar con menús balanceados y variados, la adecuada manipulación de alimentos y buenos niveles de calidad y atención.',
            'imagen'=>'noticias/noticia2.jpg',
            'estado'=>'1',
        ]);

        Noticia::create([
            'titulo'=>'Titulo de la noticia',
            'resumen'=>'Este es un resumen',
            'contenido'=>'Esta es una noticia de prueba',
            'imagen'=>'noticias/noticia2.jpg',
            'estado'=>'1',
        ]);

        Noticia::create([
            'titulo'=>'Titulo de la noticia',
            'resumen'=>'Este es un resumen',
            'contenido'=>'Esta es una noticia de prueba',
            'imagen'=>'noticias/noticia2.jpg',
            'estado'=>'1',
        ]);

        Noticia::create([
            'titulo'=>'Titulo de la noticia',
            'resumen'=>'Este es un resumen',
            'contenido'=>'Esta es una noticia de prueba',
            'imagen'=>'noticias/noticia2.jpg',
            'estado'=>'1',
        ]);

        Noticia::create([
            'titulo'=>'Titulo de la noticia',
            'resumen'=>'Este es un resumen',
            'contenido'=>'Esta es una noticia de prueba',
            'imagen'=>'noticias/noticia2.jpg',
            'estado'=>'1',
        ]);

        Noticia::create([
            'titulo'=>'Titulo de la noticia',
            'resumen'=>'Este es un resumen',
            'contenido'=>'Esta es una noticia de prueba',
            'imagen'=>'noticias/noticia2.jpg',
            'estado'=>'1',
        ]);

        Noticia::create([
            'titulo'=>'Titulo de la noticia',
            'resumen'=>'Este es un resumen',
            'contenido'=>'Esta es una noticia de prueba',
            'imagen'=>'noticias/noticia2.jpg',
            'estado'=>'1',
        ]);
    }
}
