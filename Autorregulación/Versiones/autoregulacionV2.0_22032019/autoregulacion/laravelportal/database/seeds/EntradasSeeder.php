<?php

use Illuminate\Database\Seeder;
use App\Models\Entrada;

class EntradaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Entrada::create([
            'enunciado'=>'¿Qué requiero para que me visite la Secretaría Distrital de Salud?',
            'contenido'=>'La Secretaría Distrital de Salud expide el concepto sanitario cuando el establecimiento esté funcionando, a través las Subredes Integradas de Servicios de Salud (SISS), según la localidad donde esté ubicado. Para lo cual, debes haber hecho previamente la Inscripción del establecimiento <a href="">[Registra tu negocio aquí]</a>, luego verificar que cumples con la norma <a href="">[Verifica tu lista de chequeo]</a> y después, solicitar una Visita de inspección, vigilancia y control [Solicita una visita] Como resultado de esta vigilancia, se emite un concepto sanitario al establecimiento, acorde a las condiciones sanitarias evidenciadas en el momento de la visita.',
            'tipo'=>'faqs',
            'estado'=>'1'
        ]);

        Entrada::create([
            'enunciado'=>'¿Qué reglamentación sanitaria aplica para un restaurante?',
            'contenido'=>'La normatividad sanitaria vigente aplicable para la preparación, expendio, almacenamiento, comercialización de alimentos es: Ley 9 de 1979, Resolución 2674 de 2013 específicamente en el capítulo VIII las condiciones sanitarias que deben cumplir los restaurantes y establecimientos gastronómicos, Resolución 5109 de 2005 y demás normas que rigen sobre la materia.',
            'tipo'=>'faqs',
            'estado'=>'1'
        ]);

        Entrada::create([
            'enunciado'=>'¿Qué debo saber si tengo o tendré un restaurante?',
            'contenido'=>'Si tienes un restaurante debes cumplir, entre otros, con aspectos como documentación (siempre deberán estar disponibles en el establecimiento el Plan de Saneamiento, el Plan de Capacitación Continuo y Permanente, la Certificación médica del personal); Buenas prácticas de manufactura, infraestructura adecuada (pisos construidos en material sanitario no poroso ni absorbente, de fácil lavado, paredes de tonos claros, con materiales impermeables, techos que eviten la acumulación de suciedad). Los equipos y utensilios deben estar hechos con materiales que no contaminen los alimentos, con acabado liso y sin grietas.',
            'tipo'=>'faqs',
            'estado'=>'1'
        ]);

        Entrada::create([
            'enunciado'=>'',
            'contenido'=>'Recuerda: el registro o inscripción de establecimientos es obligatorio para todo tipo de negocio.',
            'tipo'=>'tips',
            'imagen'=>'img/icons/tipsauto1.svg',
            'estado'=>'1'
        ]);

        Entrada::create([
            'enunciado'=>'',
            'contenido'=>'El cumplimiento de los requisitos sanitarios vigentes brinda mayor confianza a tus clientes, lo cual conlleva a n incremento de tus ingresos por los servicios brindados en el establecimiento.',
            'tipo'=>'tips',
            'imagen'=>'img/icons/tipsauto2.svg',
            'estado'=>'1'
        ]);

        Entrada::create([
            'enunciado'=>'',
            'contenido'=>'Si realizas la autoevaluación en el proceso de autorregulación, conocerás requisitos para el funcionamiento de los establecimientos y apuntarás a contar con un concepto sanitario favorable.',
            'tipo'=>'tips',
            'imagen'=>'img/icons/tipsauto3.svg',
            'estado'=>'1'
        ]);

        Entrada::create([
            'enunciado'=>'',
            'contenido'=>'Algunas de las normas que regulan el proceso de registro de establecimientos son: Resolución 1229 de 2013, Resolución 5194 de 2010, Resolución 2263 de 2004, Resolución No. 2016041871 de 2016.',
            'tipo'=>'tips',
            'imagen'=>'img/icons/tipsauto4.svg',
            'estado'=>'1'
        ]);
   }
}
