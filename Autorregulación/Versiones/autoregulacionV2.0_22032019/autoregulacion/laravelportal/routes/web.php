<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','MainController@home');
Route::post('/registro','MainController@register');
Route::get('/autorregulacion', 'MainController@autorregulacion');
Route::get('/registrate', 'MainController@registrate');
Route::get('/buenpropietario','MainController@buenpropietario' );
Route::get('/novedades','MainController@novedades' );
Route::get('/informes', 'MainController@informe');

Route::get('/accesibilidad','MainController@accesible');

Route::get('/restaurantes', 'MainController@restaurantes');
Route::get('/salonesbelleza', 'MainController@salonesbelleza');
Route::get('/carnicerias', 'MainController@carnicerias');
Route::get('/droguerias', 'MainController@droguerias');



Route::post('/consultar', 'MainController@search_for_establecimiento');

/* *********************** */
// Rutas de administración

Route::get('/admin', 'AdminController@home');
Route::get('/admin/stats','AdminController@stats');
Route::get('/admin/entradas','AdminController@entradas');
Route::get('/admin/editentrada','AdminController@editEntrada');
Route::get('/admin/nuevaentrada','AdminController@newEntrada');
Route::get('/admin/newsletter', 'AdminController@newsletter');
Route::get('/admin/noticias', 'AdminController@noticias');
Route::get('/admin/editnoticia/{id}','AdminController@editNoticia');
Route::get('/admin/nuevanoticia','AdminController@newNoticia');
