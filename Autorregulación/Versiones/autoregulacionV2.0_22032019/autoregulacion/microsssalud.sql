-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3308
-- Generation Time: Mar 22, 2019 at 08:25 PM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `microsssalud`
--

-- --------------------------------------------------------

--
-- Table structure for table `entradas`
--

CREATE TABLE `entradas` (
  `id` int(10) UNSIGNED NOT NULL,
  `enunciado` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contenido` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `tipo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `imagen` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `estado` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `entradas`
--

INSERT INTO `entradas` (`id`, `enunciado`, `contenido`, `tipo`, `imagen`, `estado`, `created_at`, `updated_at`) VALUES
(1, '¿Qué requiero para que me visite la Secretaría Distrital de Salud?', 'La Secretaría Distrital de Salud expide el concepto sanitario cuando el establecimiento esté funcionando, a través las Subredes Integradas de Servicios de Salud (SISS), según la localidad donde esté ubicado. Para lo cual, debes haber hecho previamente la Inscripción del establecimiento <a href=\"\">[Registra tu negocio aquí]</a>, luego verificar que cumples con la norma <a href=\"\">[Verifica tu lista de chequeo]</a> y después, solicitar una Visita de inspección, vigilancia y control [Solicita una visita] Como resultado de esta vigilancia, se emite un concepto sanitario al establecimiento, acorde a las condiciones sanitarias evidenciadas en el momento de la visita.', 'faqs', NULL, 1, '2019-03-22 20:52:53', '2019-03-22 20:52:53'),
(2, '¿Qué reglamentación sanitaria aplica para un restaurante?', 'La normatividad sanitaria vigente aplicable para la preparación, expendio, almacenamiento, comercialización de alimentos es: Ley 9 de 1979, Resolución 2674 de 2013 específicamente en el capítulo VIII las condiciones sanitarias que deben cumplir los restaurantes y establecimientos gastronómicos, Resolución 5109 de 2005 y demás normas que rigen sobre la materia.', 'faqs', NULL, 1, '2019-03-22 20:52:53', '2019-03-22 20:52:53'),
(3, '¿Qué debo saber si tengo o tendré un restaurante?', 'Si tienes un restaurante debes cumplir, entre otros, con aspectos como documentación (siempre deberán estar disponibles en el establecimiento el Plan de Saneamiento, el Plan de Capacitación Continuo y Permanente, la Certificación médica del personal); Buenas prácticas de manufactura, infraestructura adecuada (pisos construidos en material sanitario no poroso ni absorbente, de fácil lavado, paredes de tonos claros, con materiales impermeables, techos que eviten la acumulación de suciedad). Los equipos y utensilios deben estar hechos con materiales que no contaminen los alimentos, con acabado liso y sin grietas.', 'faqs', NULL, 1, '2019-03-22 20:52:53', '2019-03-22 20:52:53'),
(4, '', 'Recuerda: el registro o inscripción de establecimientos es obligatorio para todo tipo de negocio.', 'tips', 'img/icons/tipsauto1.svg', 1, '2019-03-22 20:52:53', '2019-03-22 20:52:53'),
(5, '', 'El cumplimiento de los requisitos sanitarios vigentes brinda mayor confianza a tus clientes, lo cual conlleva a n incremento de tus ingresos por los servicios brindados en el establecimiento.', 'tips', 'img/icons/tipsauto2.svg', 1, '2019-03-22 20:52:53', '2019-03-22 20:52:53'),
(6, '', 'Si realizas la autoevaluación en el proceso de autorregulación, conocerás requisitos para el funcionamiento de los establecimientos y apuntarás a contar con un concepto sanitario favorable.', 'tips', 'img/icons/tipsauto3.svg', 1, '2019-03-22 20:52:53', '2019-03-22 20:52:53'),
(7, '', 'Algunas de las normas que regulan el proceso de registro de establecimientos son: Resolución 1229 de 2013, Resolución 5194 de 2010, Resolución 2263 de 2004, Resolución No. 2016041871 de 2016.', 'tips', 'img/icons/tipsauto4.svg', 1, '2019-03-22 20:52:53', '2019-03-22 20:52:53');

-- --------------------------------------------------------

--
-- Table structure for table `establecimientos`
--

CREATE TABLE `establecimientos` (
  `id` int(10) UNSIGNED NOT NULL,
  `IdTBLEstablecimientos` bigint(20) UNSIGNED NOT NULL,
  `RazonSocial` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `NombreComercial` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `DireccionComercial` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `CodLocalidad` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Localidad` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `codUPZ` int(10) UNSIGNED NOT NULL,
  `NombreUPZ` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `CodBarrio` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Barrio` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `CodTipoEstablecimiento` bigint(20) UNSIGNED NOT NULL,
  `TipoEstablecimiento` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `FechaVisita` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Concepto` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `establecimientos`
--

INSERT INTO `establecimientos` (`id`, `IdTBLEstablecimientos`, `RazonSocial`, `NombreComercial`, `DireccionComercial`, `CodLocalidad`, `Localidad`, `codUPZ`, `NombreUPZ`, `CodBarrio`, `Barrio`, `CodTipoEstablecimiento`, `TipoEstablecimiento`, `FechaVisita`, `Concepto`, `created_at`, `updated_at`) VALUES
(1, 399080, '8888', 'SALA DE BELLEZA MARIA JO', 'CL 43 SUR 12F 56', '18', 'RAFAEL URIBE', 53, 'MARCO FIDEL SUAREZ', '18144BD ', 'S.C. SAN JORGE SUR', 420, 'PELUQUERÍAS', '2018-06-28 00:00:00', 'Concepto Favorable', '2019-03-22 20:52:53', '2019-03-22 20:52:53'),
(2, 399081, '8888', 'SALA DE BELLEZA MARIA JO', 'CL 43 SUR 12F 56', '18', 'RAFAEL URIBE', 53, 'MARCO FIDEL SUAREZ', '18144BD ', 'S.C. SAN JORGE SUR', 420, 'PELUQUERÍAS', '2018-06-28 00:00:00', 'Concepto Favorable', '2019-03-22 20:52:53', '2019-03-22 20:52:53'),
(3, 399082, '8888', 'SALA DE BELLEZA MARIA JO', 'CL 43 SUR 12F 56', '18', 'RAFAEL URIBE', 53, 'MARCO FIDEL SUAREZ', '18144BD ', 'S.C. SAN JORGE SUR', 420, 'PELUQUERÍAS', '2018-06-28 00:00:00', 'Concepto Favorable', '2019-03-22 20:52:53', '2019-03-22 20:52:53'),
(4, 399083, '8888', 'SALA DE BELLEZA MARIA JO', 'CL 43 SUR 12F 56', '18', 'RAFAEL URIBE', 53, 'MARCO FIDEL SUAREZ', '18144BD ', 'S.C. SAN JORGE SUR', 420, 'PELUQUERÍAS', '2018-06-28 00:00:00', 'Concepto Favorable', '2019-03-22 20:52:53', '2019-03-22 20:52:53'),
(5, 399084, '8888', 'SALA DE BELLEZA MARIA JO', 'CL 43 SUR 12F 56', '18', 'RAFAEL URIBE', 53, 'MARCO FIDEL SUAREZ', '18144BD ', 'S.C. SAN JORGE SUR', 420, 'PELUQUERÍAS', '2018-06-28 00:00:00', 'Concepto Favorable', '2019-03-22 20:52:53', '2019-03-22 20:52:53'),
(6, 399085, '8888', 'SALA DE BELLEZA MARIA JO', 'CL 43 SUR 12F 56', '18', 'RAFAEL URIBE', 53, 'MARCO FIDEL SUAREZ', '18144BD ', 'S.C. SAN JORGE SUR', 420, 'PELUQUERÍAS', '2018-06-28 00:00:00', 'Concepto Favorable', '2019-03-22 20:52:53', '2019-03-22 20:52:53'),
(7, 399086, '8888', 'SALA DE BELLEZA MARIA JO', 'CL 43 SUR 12F 56', '18', 'RAFAEL URIBE', 53, 'MARCO FIDEL SUAREZ', '18144BD ', 'S.C. SAN JORGE SUR', 420, 'PELUQUERÍAS', '2018-06-28 00:00:00', 'Concepto Favorable', '2019-03-22 20:52:53', '2019-03-22 20:52:53'),
(8, 399087, '8888', 'SALA DE BELLEZA MARIA JO', 'CL 43 SUR 12F 56', '18', 'RAFAEL URIBE', 53, 'MARCO FIDEL SUAREZ', '18144BD ', 'S.C. SAN JORGE SUR', 420, 'PELUQUERÍAS', '2018-06-28 00:00:00', 'Concepto Favorable', '2019-03-22 20:52:53', '2019-03-22 20:52:53'),
(9, 399088, '8888', 'SALA DE BELLEZA MARIA JO', 'CL 43 SUR 12F 56', '18', 'RAFAEL URIBE', 53, 'MARCO FIDEL SUAREZ', '18144BD ', 'S.C. SAN JORGE SUR', 420, 'PELUQUERÍAS', '2018-06-28 00:00:00', 'Concepto Favorable', '2019-03-22 20:52:53', '2019-03-22 20:52:53'),
(10, 399089, '8888', 'SALA DE BELLEZA MARIA JO', 'CL 43 SUR 12F 56', '18', 'RAFAEL URIBE', 53, 'MARCO FIDEL SUAREZ', '18144BD ', 'S.C. SAN JORGE SUR', 420, 'PELUQUERÍAS', '2018-06-28 00:00:00', 'Concepto Favorable', '2019-03-22 20:52:53', '2019-03-22 20:52:53'),
(11, 399090, '8888', 'SALA DE BELLEZA MARIA JO', 'CL 43 SUR 12F 56', '18', 'RAFAEL URIBE', 53, 'MARCO FIDEL SUAREZ', '18144BD ', 'S.C. SAN JORGE SUR', 420, 'PELUQUERÍAS', '2018-06-28 00:00:00', 'Concepto Favorable', '2019-03-22 20:52:53', '2019-03-22 20:52:53'),
(12, 399091, '8888', 'SALA DE BELLEZA MARIA JO', 'CL 43 SUR 12F 56', '18', 'RAFAEL URIBE', 53, 'MARCO FIDEL SUAREZ', '18144BD ', 'S.C. SAN JORGE SUR', 420, 'PELUQUERÍAS', '2018-06-28 00:00:00', 'Concepto Favorable', '2019-03-22 20:52:53', '2019-03-22 20:52:53');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_03_17_162922_create_entradas', 1),
(4, '2019_03_17_163036_create_tipos_de_establecimientos', 1),
(5, '2019_03_17_163203_create_newsletters', 1),
(6, '2019_03_17_163220_create_noticias', 1),
(7, '2019_03_17_164133_add_rol_to_user', 1),
(8, '2019_03_20_171002_create_establecimientos', 1);

-- --------------------------------------------------------

--
-- Table structure for table `newsletters`
--

CREATE TABLE `newsletters` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_tipo_establecimiento` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `correo_e` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telefono` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `noticias`
--

CREATE TABLE `noticias` (
  `id` int(10) UNSIGNED NOT NULL,
  `titulo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `resumen` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `contenido` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `imagen` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `estado` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `noticias`
--

INSERT INTO `noticias` (`id`, `titulo`, `resumen`, `contenido`, `imagen`, `estado`, `created_at`, `updated_at`) VALUES
(1, 'Secretaría de Salud entrega reconocimiento a 25 restaurantes populares por su calidad y cumplimiento sanitario', 'La Secretaría Distrital de Salud (SDS) entregó la distinción \"Restaurantes 1A\" a 25 establecimientos populares de la ciudad que se destacan por cumplir con las normas sanitarias y contar con menús balanceados y variados, la adecuada manipulación de alimentos y buenos niveles de calidad y atención.', 'La Secretaría Distrital de Salud (SDS) entregó la distinción \"Restaurantes 1A\" a 25 establecimientos populares de la ciudad que se destacan por cumplir con las normas sanitarias y contar con menús balanceados y variados, la adecuada manipulación de alimentos y buenos niveles de calidad y atención.', 'noticias/noticia2.jpg', 1, '2019-03-22 20:52:53', '2019-03-22 20:52:53'),
(2, 'Titulo de la noticia', 'Este es un resumen', 'Esta es una noticia de prueba', 'noticias/noticia2.jpg', 1, '2019-03-22 20:52:53', '2019-03-22 20:52:53'),
(3, 'Titulo de la noticia', 'Este es un resumen', 'Esta es una noticia de prueba', 'noticias/noticia2.jpg', 1, '2019-03-22 20:52:53', '2019-03-22 20:52:53'),
(4, 'Titulo de la noticia', 'Este es un resumen', 'Esta es una noticia de prueba', 'noticias/noticia2.jpg', 1, '2019-03-22 20:52:53', '2019-03-22 20:52:53'),
(5, 'Titulo de la noticia', 'Este es un resumen', 'Esta es una noticia de prueba', 'noticias/noticia2.jpg', 1, '2019-03-22 20:52:53', '2019-03-22 20:52:53'),
(6, 'Titulo de la noticia', 'Este es un resumen', 'Esta es una noticia de prueba', 'noticias/noticia2.jpg', 1, '2019-03-22 20:52:53', '2019-03-22 20:52:53'),
(7, 'Titulo de la noticia', 'Este es un resumen', 'Esta es una noticia de prueba', 'noticias/noticia2.jpg', 1, '2019-03-22 20:52:53', '2019-03-22 20:52:53');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tipos_de_establecimientos`
--

CREATE TABLE `tipos_de_establecimientos` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre_tipo_de_establecimiento` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `intervencion` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_linea_ambiente` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tipos_de_establecimientos`
--

INSERT INTO `tipos_de_establecimientos` (`id`, `nombre_tipo_de_establecimiento`, `intervencion`, `id_linea_ambiente`, `created_at`, `updated_at`) VALUES
(540, 'Acabados de muebles metálicos', 'IVC A ESTABLECIMIENTOS QUE UTILICEN COMO MATERIA PRIMA EL METAL', 1000001, '2019-03-22 20:52:53', '2019-03-22 20:52:53');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `rol` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `entradas`
--
ALTER TABLE `entradas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `establecimientos`
--
ALTER TABLE `establecimientos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `newsletters`
--
ALTER TABLE `newsletters`
  ADD PRIMARY KEY (`id`),
  ADD KEY `newsletters_id_tipo_establecimiento_foreign` (`id_tipo_establecimiento`);

--
-- Indexes for table `noticias`
--
ALTER TABLE `noticias`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `tipos_de_establecimientos`
--
ALTER TABLE `tipos_de_establecimientos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `entradas`
--
ALTER TABLE `entradas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `establecimientos`
--
ALTER TABLE `establecimientos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `newsletters`
--
ALTER TABLE `newsletters`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `noticias`
--
ALTER TABLE `noticias`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tipos_de_establecimientos`
--
ALTER TABLE `tipos_de_establecimientos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=541;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `newsletters`
--
ALTER TABLE `newsletters`
  ADD CONSTRAINT `newsletters_id_tipo_establecimiento_foreign` FOREIGN KEY (`id_tipo_establecimiento`) REFERENCES `tipos_de_establecimientos` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
