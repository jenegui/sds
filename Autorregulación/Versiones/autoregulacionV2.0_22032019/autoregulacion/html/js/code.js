$(document).ready(function () {


    $(".faqs").niceScroll(".faq_wrapper", { cursorwidth: "6px",autohidemode: false });

    $('.collapse').on('shown.bs.collapse', function () {
        //$(this).prev().addClass('active-acc');
        $(this).parent().addClass('active-acc');
        $(".faqs").getNiceScroll().resize();
    });

    $('.collapse').on('hidden.bs.collapse', function () {
        //$(this).prev().removeClass('active-acc');
        $(this).parent().removeClass('active-acc');
    });

});