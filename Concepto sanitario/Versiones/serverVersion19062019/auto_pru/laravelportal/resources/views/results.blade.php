@extends('layout',['title' => 'Resultados','faqs' => $faq,'te' => $te,'tips' => $tip,'access' => $access,'menu'=>$menu,'logos'=>$logos ])
@section('content')
    <div class="breadcrumb">
        <ul>
            <li><a href="{{url('/')}}">Inicio</a></li>
            <li><a href="{{url('/')}}">Resultados</a></li>
        </ul>      
    </div>
    <div class="home_slider">
        <div id="subsideSlider" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                @php
                    $activo="active";
                @endphp
                @foreach($sliders as $slide)
                    <div class="carousel-item {{$activo}}">
                        <div class="container1">
                            <a href="{{url('/'.$slide->contenido)}}">
                                <img class="w-100" src="{{$slide->archivo}}" alt="">
                            </a>
                        </div>
                    </div>
                    @php
                        $activo="";
                    @endphp
                @endforeach
            </div>
            <ol class="carousel-indicators">
                @php
                    $activo="active";
                    $i=0;
                @endphp
                @foreach($sliders as $slide)
                    <li data-target="#subsideSlider" data-slide-to="{{$i}}" class="{{$activo}}">
                        <span></span>                        
                    </li>
                    @php
                        $activo="";
                        $i++;
                    @endphp
                @endforeach
            </ol>
        </div>
    </div>

   <div class="resultados my-5">
       <div class="subtitle">
           <h2><b>Resultados de Busquedas</b></h2>
       </div>
       @forelse( $noticiasres as $result)
        <div class="my-5 block border border-primary">
            <p>{{$result->titulo}}</p>
            <div>{!!$result->contenido!!}</div>
        </div>
        @empty
            <p>No se encontró ningúna noticia con este término</p>
        @endforelse  


       @forelse( $entradas as $result)
        <div class="my-5 block border border-primary">
            <p>{!!$result->enunciado!!}</p>
            <p>{!!$result->contenido!!}</p>
        </div>
        @empty
            <p>No se encontró ningúna entrada con este término</p>
        @endforelse  
        
    </div>
    <div class="my-5">
        <br>
    </div>
@endsection


@section ("scripts")
    <script languague="javascript">           
        $(document).ready(function () {
            $("#btn_consultar").click(function(){
                var theval=$("#term").val();
                $.post("{{url('/consultar')}}",{'term':theval,'_token':'{{ csrf_token() }}'},function(data){
                    if(data=="error"){
                        $("#modalTable").DataTable().clear().draw(false);
                    }else{
                        var tabla="";
                        jQuery.each(data,function(i,val){
                            tabla="<tr><td>"+val.NombreComercial+"</td><td>"+val.RazonSocial+"</td></tr>";
                            $("#modalTable").DataTable().row.add($(tabla));
                        });
                        $("#modalTable").DataTable().draw();
                    }
                });
            });
            $('#modalTable').DataTable( {
                searching: false,
                "pageLength": 5,
                "language": {
                    "lengthMenu": "Mostrar _MENU_ Registros por página",
                    "zeroRecords": "No hay resultados.",
                    "info": "Mostrando página _PAGE_ de _PAGES_",
                    "infoEmpty": "No hay registros disponibles",
                    "infoFiltered": "(filtered from _MAX_ total records)",
                    "paginate": {
                        "first":      "Primero",
                        "previous":   "Anterior",
                        "next":       "Siguiente",
                        "last":       "Último"
                    },
                }
            });


            $('#newsTable').DataTable( {
                searching: false,
                ordering:  false,
                "info": false,
                "lengthChange": false,
                "pageLength": 2,
                "language": {
                    "lengthMenu": "Mostrar _MENU_ Registros por página",
                    "zeroRecords": "No hay resultados.",
                    "info": "Mostrando página _PAGE_ de _PAGES_",
                    "infoEmpty": "No hay registros disponibles",
                    "infoFiltered": "(filtered from _MAX_ total records)",
                    "paginate": {
                        "first":      "Primero",
                        "previous":   "Anterior",
                        "next":       "Siguiente",
                        "last":       "Último"
                    },
                }
            });
            

        });
    </script>
@endsection