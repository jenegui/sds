@extends('layout',['title' => 'Novedades','faqs' => $faq,'te' => $te,'tips' => $tip,'access' => $access,'logos'=>$logos])
@section('content')
    <div class="breadcrumb">
        <ul>
            <li><a href="{{url('/')}}">Inicio</a></li>
            <li><a href="{{url('/novedades/'.$not->id)}}">Noticia</a></li>
        </ul>      
    </div>

   <div class="resultados my-5">
       <div class="subtitle">
           <h2><b>{{$not->titulo}}</b></h2>
           <small class="mt-4 fecha">{{$not->created_at}}</small>
       </div>
       
       <div class="text-justify">
            <div class="row">
                <div class="col-12 col-md-12 py-4">
                    <div class="noticia-imagen">
                        <img class="w-100 m-auto " src="{{url($not->imagen)}}" alt="{{$not->titulo}}">
                        <div>{!!$not->piefoto!!}</div>
                    </div>
                    <div>{!!$not->contenido!!}</div>
                    
                </div>
            </div>
            
        </div>

                                
                                

        
    </div>
    <div class="my-5">
        <br>
    </div>
@endsection


@section ("scripts")
    <script languague="javascript">           
        $(document).ready(function () {
            $("#btn_consultar").click(function(){
                var theval=$("#term").val();
                $.post("{{url('/consultar')}}",{'term':theval,'_token':'{{ csrf_token() }}'},function(data){
                    if(data=="error"){
                        $("#modalTable").DataTable().clear().draw(false);
                    }else{
                        var tabla="";
                        jQuery.each(data,function(i,val){
                            tabla="<tr><td>"+val.NombreComercial+"</td><td>"+val.RazonSocial+"</td></tr>";
                            $("#modalTable").DataTable().row.add($(tabla));
                        });
                        $("#modalTable").DataTable().draw();
                    }
                });
            });
            $('#modalTable').DataTable( {
                searching: false,
                "pageLength": 5,
                "language": {
                    "lengthMenu": "Mostrar _MENU_ Registros por página",
                    "zeroRecords": "No hay resultados.",
                    "info": "Mostrando página _PAGE_ de _PAGES_",
                    "infoEmpty": "No hay registros disponibles",
                    "infoFiltered": "(filtered from _MAX_ total records)",
                    "paginate": {
                        "first":      "Primero",
                        "previous":   "Anterior",
                        "next":       "Siguiente",
                        "last":       "Último"
                    },
                }
            });


            $('#newsTable').DataTable( {
                searching: false,
                ordering:  false,
                "info": false,
                "lengthChange": false,
                "pageLength": 2,
                "language": {
                    "lengthMenu": "Mostrar _MENU_ Registros por página",
                    "zeroRecords": "No hay resultados.",
                    "info": "Mostrando página _PAGE_ de _PAGES_",
                    "infoEmpty": "No hay registros disponibles",
                    "infoFiltered": "(filtered from _MAX_ total records)",
                    "paginate": {
                        "first":      "Primero",
                        "previous":   "Anterior",
                        "next":       "Siguiente",
                        "last":       "Último"
                    },
                }
            });
            

        });
    </script>
@endsection