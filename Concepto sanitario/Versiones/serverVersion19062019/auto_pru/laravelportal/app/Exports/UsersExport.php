<?php

namespace App\Exports;

use App\Models\Newsletter;
use Maatwebsite\Excel\Concerns\FromCollection;

class UsersExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Newsletter::join('tipos_de_establecimientos','id_tipo_establecimiento','=','tipos_de_establecimientos.id')->get();
    }
}
