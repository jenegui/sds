<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('notes', 'NotesController@index');


Route::group(['middleware' => ['permission:destroy_notes']], function () {
    Route::get('notes/{id}/destroy', 'NotesController@destroy')->name('notes.destroy');
});

//Listado de tramites inicial
Route::any('/ventanilla/tramites', 'TramitesController@index')->middleware('auth')->name('tramites.index');

Route::any('/ventanilla/tramites/edit', 'TramitesController@edit')->middleware('auth')->name('tramites.edit');

//				--		SOLICITUD RAYOS X 				--

//Descripcion del tramite


Route::get('/ventanilla/tramites/solicitud_rayosx', 'RayosxController@index')->name('rayosx.index')->middleware('auth');

//Crear nuevo rayos x
Route::any('/ventanilla/tramites/solicitud_rayosx/crear', 'RayosxController@create')->name('rayosx.create')->middleware('auth');

//RAYOS X - Guardar localizacion entidad
Route::any('/ventanilla/tramites/solicitud_rayosx/formSeccion1', 'RayosxController@guardarLocalizacion')->name('rayosx.guardarLocalizacion')->middleware('auth');

//RAYOS X - Guardar equipos de rayosX
Route::any('/ventanilla/tramites/solicitud_rayosx/formSeccion2', 'RayosxController@guardarEquiposRayosX')->name('rayosx.guardarEquiposRayosX')->middleware('auth');

//RAYOS X - Guardar oficial TOE
Route::any('/ventanilla/tramites/solicitud_rayosx/formSeccion3-1', 'RayosxController@guardarOficialTOE')->name('rayosx.guardarOficialTOE')->middleware('auth');

//RAYOS X - Guardar oficial TOE
Route::any('/ventanilla/tramites/solicitud_rayosx/formSeccion3-2', 'RayosxController@guardarTemporalTOE')->name('rayosx.guardarTemporalTOE')->middleware('auth');

//RAYOS X - VERIFICAR TOE y PASO 4S
Route::any('/ventanilla/tramites/solicitud_rayosx/formSeccion4', 'RayosxController@verificarTOE')->name('rayosx.verificarTOE')->middleware('auth');

//RAYOS X - Guardar oficial TOE
Route::any('/ventanilla/tramites/solicitud_rayosx/formSeccion4-1', 'RayosxController@guardarTalentoHumano')->name('rayosx.guardarTalentoHumano')->middleware('auth');

Route::any('/ventanilla/tramites/solicitud_rayosx/formSeccion4-2', 'RayosxController@guardarEquipoPrueba')->name('rayosx.guardarEquipoPrueba')->middleware('auth');

Route::any('/ventanilla/tramites/solicitud_rayosx/formSeccion5', 'RayosxController@verificarPaso4')->name('rayosx.verificarPaso4')->middleware('auth');

//guardar rayos x
Route::get('/ventanilla/tramites/solicitud_rayosx/store', 'RayosxController@store')->middleware('auth');


//consultar rayos x
//Avanzar tramite
//Devolver tramite

Route::get('/municipio/get/{id}', 'RayosxController@getMpoXDpto');
Route::get('(localidad/get/{id}', 'RayosxController@getLocalidadXMpo');
Route::get('/barrio/get/{id}', 'RayosxController@getBarrioXLocalidad');


