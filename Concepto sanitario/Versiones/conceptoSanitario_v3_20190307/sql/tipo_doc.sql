
--
-- tipo documento
--
CREATE TABLE tipo_documento(
idtipo_documento int primary key auto_increment,
nombre varchar(40),
abrev varchar(5),
activo tinyint(1)
)Engine=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO tipo_documento (idtipo_documento, nombre, abrev, activo) VALUES('1','Cédula de ciudadanía','CC','1');
INSERT INTO tipo_documento (idtipo_documento, nombre, abrev, activo) VALUES('2','Tarjeta de identidad','TI','1');
INSERT INTO tipo_documento (idtipo_documento, nombre, abrev, activo) VALUES('3','Cédula de extranjería','CE','1');
INSERT INTO tipo_documento (idtipo_documento, nombre, abrev, activo) VALUES('4','Pasaporte','PS','1');
INSERT INTO tipo_documento (idtipo_documento, nombre, abrev, activo) VALUES('5','Número de Identificación Tributaria','NIT','1');