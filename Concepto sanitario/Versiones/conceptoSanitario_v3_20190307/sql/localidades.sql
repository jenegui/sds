/*
-- Query: SELECT * FROM concepto.localidades
LIMIT 0, 1000

-- Date: 2019-02-02 20:19
*/

CREATE TABLE IF NOT EXISTS `localidades` (
  `idlocalidad` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_loc` varchar(30) DEFAULT NULL,
  `codigo_loc` int(11) DEFAULT NULL,
  PRIMARY KEY (`idlocalidad`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

INSERT INTO `localidades` (`idlocalidad`,`nombre_loc`,`codigo_loc`) VALUES (1,'Chapinero',2);
INSERT INTO `localidades` (`idlocalidad`,`nombre_loc`,`codigo_loc`) VALUES (2,'Santa Fe',3);
INSERT INTO `localidades` (`idlocalidad`,`nombre_loc`,`codigo_loc`) VALUES (6,'Usaquén',1);
INSERT INTO `localidades` (`idlocalidad`,`nombre_loc`,`codigo_loc`) VALUES (7,'San Cristóbal',4);
INSERT INTO `localidades` (`idlocalidad`,`nombre_loc`,`codigo_loc`) VALUES (8,'Usme',5);
INSERT INTO `localidades` (`idlocalidad`,`nombre_loc`,`codigo_loc`) VALUES (9,'Tunjuelito',6);
INSERT INTO `localidades` (`idlocalidad`,`nombre_loc`,`codigo_loc`) VALUES (10,'Bosa',7);
INSERT INTO `localidades` (`idlocalidad`,`nombre_loc`,`codigo_loc`) VALUES (11,'Kennedy',8);
INSERT INTO `localidades` (`idlocalidad`,`nombre_loc`,`codigo_loc`) VALUES (12,'Fontibón',9);
INSERT INTO `localidades` (`idlocalidad`,`nombre_loc`,`codigo_loc`) VALUES (13,'Engativá',10);
INSERT INTO `localidades` (`idlocalidad`,`nombre_loc`,`codigo_loc`) VALUES (14,'Suba',11);
INSERT INTO `localidades` (`idlocalidad`,`nombre_loc`,`codigo_loc`) VALUES (15,'Barrios Unidos',12);
INSERT INTO `localidades` (`idlocalidad`,`nombre_loc`,`codigo_loc`) VALUES (16,'Teusaquillo',13);
INSERT INTO `localidades` (`idlocalidad`,`nombre_loc`,`codigo_loc`) VALUES (17,'Mártires',14);
INSERT INTO `localidades` (`idlocalidad`,`nombre_loc`,`codigo_loc`) VALUES (18,'Antonio Nariño',15);
INSERT INTO `localidades` (`idlocalidad`,`nombre_loc`,`codigo_loc`) VALUES (19,'Puente Aranda',16);
INSERT INTO `localidades` (`idlocalidad`,`nombre_loc`,`codigo_loc`) VALUES (20,'Candelaria',17);
INSERT INTO `localidades` (`idlocalidad`,`nombre_loc`,`codigo_loc`) VALUES (21,'Rafael Uribe',18);
INSERT INTO `localidades` (`idlocalidad`,`nombre_loc`,`codigo_loc`) VALUES (22,'Ciudad Bolívar',19);
INSERT INTO `localidades` (`idlocalidad`,`nombre_loc`,`codigo_loc`) VALUES (23,'Sumapaz',20);
INSERT INTO `localidades` (`idlocalidad`,`nombre_loc`,`codigo_loc`) VALUES (35,'N.A.',0);
