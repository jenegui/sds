<?php
require_once '../class/solicitudConcepto.php';
require_once '../controller/controllerSolicitudConcepto.php';

$sol= new SolicitudConcepto();
$solicitud = new ControllerSolicitudConcepto();
$now= date("d/m/Y");
?>
<html>
   <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0,shrink-to-fit=no">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Salud</title>

        <link rel="stylesheet" href="../css/bootstrap.min.css">
        <link rel="stylesheet" href="../css/validationEngine.jquery.css">
        <link rel="shortcut icon" href="../img/favicon.png" type="image/x-icon">
        <link rel="stylesheet" href="../css/styles.css">
        <link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet">
        <style type="text/css">
            .auto-style1 {
                width: 100%;
                text-decoration: underline;
            }
            div.row{margin: 20px;}
        </style>
   
    <body>
         <header>
            <div class="container">
                <div class="logos">
                    <div class="row">
                        <div class="col-12 col-md-6">
                            <img class="logo1" src="../img/logos/home_alcaldia.svg" alt="">
                        </div>
                        <div class="col-12 col-md-6 text-right">
                            <img class="logo2" src="../img/logos/home_negocios_rentables.svg" alt="">
                        </div>
                    </div>
                </div>

            </div>
        </header>
        <main class="container">
            <div class="breadcrumb">
                <ul>
                    <li><a href="../index.php">Inicio</a></li>
                    <li><a href="index.php">Solicite su visita</a></li>
                    <li><a href="consulta.php">Consulte su solicitud</a></li>
                </ul>      
            </div>
            <div class="row">	
                <div class="col-xl-2"></div>
                <div class="col-xl-8">
                    <center>
                        <hr class="mb-1">
                        <h3 class="well well-sm" style="background-color: #0069B4; color: #ffffff">Solicitud de visita por concepto sanitario</h3>
                        <hr class="mb-1">
                        <div class="alert alert-danger" role="alert">
                            <strong>Señor Usuario!</strong> Todo campo con <strong>(*)</strong> será de carácter obligatorio en el diligenciamiento del formulario.
                        </div>
                    </center>
                    <form name="frmRegUsuario" id="frmRegUsuario" method="post" action="#"  enctype="multipart/form-data">
                        <input type="hidden" name="action" id="action" value="guardar">
                        <div class="row">
                            <div class="col-md-7">
                                <label>Su establecimiento ya se encuentra inscrito en la Secretaría de salud?</label>
                                 <label for="inscrito">&nbsp Si</label>
                                <input type="radio" name="inscrito" id="inscritosi" value="1">
                                 <label for="inscrito">&nbsp No</label>
                                <input type="radio" name="inscrito" id="inscritono" value="2">
                            </div>
                            <div class="col-md-2" id="url" style="display: none"><a class="btn btn-info" href="http://appb.saludcapital.gov.co/MicroSivigilaDC/ServiciosComuni1.aspx">Inscríbase Aquí</a></div>
                            <div class="col-med-3"> </div>
                        </div>
                        <div id="resultadoAjax" style="color:#31b0d5"></div>
                        <div class="row" id="inscripcion" style="display:none">
                            <div class="col-md-4">
                                <label for="fechaIns">Fecha Inscripción(*)</label>
                                <input type="date"class="form-control" id="fechaIns" name="fechaIns">
                            </div>
                            <div class="col-md-4">
                                <label for="numeroIns">Número de inscripción(*)</label>
<input type="text" class="validate[required,minSize[6],custom[onlyLetterNumber], maxSize[10]] form-control" id="numeroIns" name="numeroIns" placeholder="Digite el número incluyendo el prefijo">
                            </div>
                            <div class="col-md-4">
                                <label for="codigo">Código(*)</label>
                                <input type="number" class="validate[ maxSize[10]] form-control" id="codigo" name="codigo" min="1" onkeypress="return nro(event)">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <label>Matrícula mercantil del establecimiento(*)</label>
                                <label for="matricula">&nbsp Si</label>
                                <input type="radio" name="matricula" id="matriculasi" value="1" required="true">
                                <label for="matricula">&nbsp No</label>
                                <input type="radio" name="matricula" id="matriculano" value="2" required="true">
                            </div>
                        </div>
                        <div class="row" id="matriculam" style="display:none">
                           <!-- <div class="col-md-2">
                                <label for="persona_j">Persona Juridica</label>
                                <input type="text"class="form-control" id="persona_j" name="persona_j">
                            </div>-->
                            <div class="col-md-4">
                                <label for="numeromatricula">Número de Matrícula(*)</label>
                                <input type="number"class="validate[required,minSize[6], maxSize[10]] form-control" id="numeromatricula" name="numeromatricula" min="1" onkeypress="return nro(event)">
                            </div>
                            <div class="col-md-4">
                                <label for="razon_s">Razón Social(*)</label>
                                <input type="text" class="validate[required,maxSize[50],minSize[5]] form-control" id="razon_s" name="razon_s" placeholder="Digite el Nombre">
                            </div>
                            <div class="col-md-4">
                                <label for="Nit">Nit(*)</label>
                                <input type="text" class="validate[required],maxSize[11],minSize[8]] form-control" id="nit" name="nit" placeholder="Digite el número del nit incluya el - y código de verificación" onkeypress="return checknumber(event)">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <label for="nombreEstable">Nombre Comercial(*)</label>
                                <input type="text" class="validate[required,custom[onlyLetterNumber], minSize[5], maxSize[50]] form-control" id="nombreEstable" name="nombreEstable" placeholder="Nombre del establecimiento" onkeypress="return check(event)">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <label>Dirección rural</label>
                                <label for="dir">&nbsp Si</label>
                                <input type="radio" name="rural" id="rurali" value="1" class="validate[required,]">
                                <label for="dir">&nbsp No</label>
                                <input type="radio" name="rural" id="ruralno" value="2" class="validate[required]">
                            </div>
                        </div>
                        
                        <div class="row" id="zrural" style="display: none">
                            <div class="col-md-2">
                                <label for="localidad">Localidad</label>
                                <select class="validate[required] form-control" id="localidadrural" name="localidadrural">
                                    
                                    <?php $sol->select_localidad();?>
                                </select>
                                <input type="hidden" name="codigoloc" id="codigoloc">
                            </div>
                            <div class="col-md-2">
                                <label for="upz">UPZ</label>
                                <select class="validate[required] form-control" id="upzrural" name="upzrural">
                                    <?php 
                                    $soli= new SolicitudConcepto();
                                    $soli->select_upz(0);?>
                                </select>
                                <input type="hidden" name="codigoupz" id="codigoupz">
                            </div>
                            <div class="col-md-2">
                                <label for="barriorural">Barrio</label>
                                <select class="validate[required] form-control" id="barriorural" name="barriorural">
                                    <?php $solbarrios=new SolicitudConcepto();
                                    $solbarrios->select_barrio(0);?>
                                </select>
                            </div>
                            <div class="col-md-6">
                                <label for="dir">Dirección Comercial(*)</label>
                                <input type="text"class="validate[required,minSize[8], maxSize[50]] form-control" id="direccionrural" name="direccionrural" placeholder="Digite la dirección completa">
                            </div>
                        </div>
                        <div class="row" id="urbano" style="display: none">
                        <hr>
                        <h5 class="alert alert-info "><strong>Señor Usuario </strong>Para la dirección del establecimiento por favor seleccione las opciones según corresponda y a continuación pulse el botón Generar. Si la dirección generada es correcta pulse el botón guardar. Si la dirección no es correcta pulse el botón limpiar y  realice el proceso nuevamente.
                            <strong></strong></h5>
                            <div class="col-md-12">
                                <div class="col-md-2">
                                    <label for="direccion">Dirección(*)</label>
                                    <select class="validate[required] form-control input-sm" id="c1">
                                        <option value=""></option>
                                        <option value="AC">Avenida calle</option>
                                        <option value="AK">avenida carrera</option>
                                        <option value="CL">Calle</option>
                                        <option value="KR">Carrera</option>
                                        <option value="DG">Diagonal</option>
                                        <option value="TV">Transversal</option>
                                    </select>
                                </div>
                                <div class="col-xs-1">
                                    <label for="numerod">Num(*)</label>
                                    <input type="text" class="validate[required, maxSize[3], minSize[1]] form-control input-sm" id="c2" min="0" onkeypress="return checknumber(event)">
                                </div>
                                <div class="col-xs-1">
                                    <label for="">B</label>
                                    <select class=" form-control input-sm" id="c3">
                                        <option value=""></option>
                                        <option value="A" >A</option>
                                        <option value="B" >B</option>
                                        <option value="C" >C</option>
                                        <option value="D" >D</option>
                                        <option value="E" >E</option>
                                        <option value="F" >F</option>
                                        <option value="G" >G</option>
                                        <option value="H" >H</option>
                                        <option value="I" >I</option>
                                        <option value="J" >J</option>
                                        <option value="K" >K</option>
                                        <option value="L" >L</option>
                                        <option value="M" >M</option>
                                        <option value="N" >N</option>
                                        <option value="O" >O</option>
                                        <option value="P" >P</option>
                                        <option value="Q" >Q</option>
                                        <option value="R" >R</option>
                                        <option value="S" >S</option>
                                        <option value="T" >T</option>
                                        <option value="U" >U</option>
                                        <option value="V" >V</option>
                                        <option value="W" >W</option>
                                        <option value="X" >X</option>
                                        <option value="Y" >Y</option>
                                        <option value="Z" >Z</option>
                                    </select>
                                </div>
                                <div class="col-xs-1">
                                    <label for="bis">Bis</label>
                                    <select class=" form-control input-sm" id="c4">
                                        <option value=""></option>
                                        <option value="BIS">BIS</option>
                                    </select>
                                </div>
                                <div class="col-xs-1">
                                    <label for="c5">A</label>
                                    <select class=" form-control input-sm" id="c5">
                                        <option></option>
                                        <option value="A" >A</option>
                                        <option value="B" >B</option>
                                        <option value="C" >C</option>
                                        <option value="D" >D</option>
                                        <option value="E" >E</option>
                                        <option value="F" >F</option>
                                        <option value="G" >G</option>
                                        <option value="H" >H</option>
                                        <option value="I" >I</option>
                                        <option value="J" >J</option>
                                        <option value="K" >K</option>
                                        <option value="L" >L</option>
                                        <option value="M" >M</option>
                                        <option value="N" >N</option>
                                        <option value="O" >O</option>
                                        <option value="P" >P</option>
                                        <option value="Q" >Q</option>
                                        <option value="R" >R</option>
                                        <option value="S" >S</option>
                                        <option value="T" >T</option>
                                        <option value="U" >U</option>
                                        <option value="V" >V</option>
                                        <option value="W" >W</option>
                                        <option value="X" >X</option>
                                        <option value="Y" >Y</option>
                                        <option value="Z" >Z</option>
                                    </select>
                                </div>

                                <div class="col-xs-1">
                                    <label for="bis">S/E</label>
                                    <select class=" form-control input-sm" id="c6">
                                        <option></option>
                                        <option value="SUR">SUR</option>
                                        <option value="ESTE">ESTE</option>
                                    </select>
                                </div>
                                <div class="col-xs-1">
                                    <label for="c7">Num(*)</label>
                                    <input type="text" class="validate[required, minSize[1],maxSize[3]] form-control input-sm" id="c7" min="0" onkeypress="return checknumber(event)">
                                </div>
                                <div class="col-xs-1">
                                    <label for="c8">A</label>
                                    <select class="form-control input-sm" id="c8">
                                        <option></option>
                                        <option value="A" >A</option>
                                        <option value="B" >B</option>
                                        <option value="C" >C</option>
                                        <option value="D" >D</option>
                                        <option value="E" >E</option>
                                        <option value="F" >F</option>
                                        <option value="G" >G</option>
                                        <option value="H" >H</option>
                                        <option value="I" >I</option>
                                        <option value="J" >J</option>
                                        <option value="K" >K</option>
                                        <option value="L" >L</option>
                                        <option value="M" >M</option>
                                        <option value="N" >N</option>
                                        <option value="O" >O</option>
                                        <option value="P" >P</option>
                                        <option value="Q" >Q</option>
                                        <option value="R" >R</option>
                                        <option value="S" >S</option>
                                        <option value="T" >T</option>
                                        <option value="U" >U</option>
                                        <option value="V" >V</option>
                                        <option value="W" >W</option>
                                        <option value="X" >X</option>
                                        <option value="Y" >Y</option>
                                        <option value="Z" >Z</option>
                                    </select>
                                </div>
                                <div class="col-xs-1">
                                    <label for="numerod">Num(*)</label>
                                    <input type="text" class="validate[required,minSize[1], maxSize[3]] form-control input-sm" id="c9" min="0" onkeypress="return checknumber(event)">
                                </div>
                                <div class="col-xs-1">
                                    <label for="c10">Sur/E</label>
                                    <select class=" form-control input-sm" id="c10">
                                        <option></option>
                                        <option value="SUR">SUR</option>
                                        <option value="ESTE">ESTE</option>
                                    </select>
                                </div>
                                <div class="col-xs-1">
                                    <br>
                                    <button class="btn btn-primary" id="generar" type="button">Generar</button> 
                                </div>

                            </div>
                        </div>
                        <hr>
                        <div class="row" id="dirgenUrbano" style="display: none">
                            <div class="col-md-6">
                                <label for="">Dirección generada</label>
                                <input type="text" class="validate[required, maxSize[100]] form-control" id="direccionGenerada" name="direccionGenerada" placeholder="Dirección" readonly="true">
                            </div><br>

                            <div class="col-md-2">
                                <input type="button" class="btn btn-info btn-block" id="webs" value="Guardar">
                            </div>
                            <div class="col-md-2">
                                <input type="button" class="btn btn-warning btn-block" id="limpiar" value="Limpiar">
                            </div>
                        </div>
                        <input type="hidden" name="localidad" id="localidad" >
                        <input type="hidden" name="upz" id="upz" >
                        <input type="hidden" name="barrio" id="barrio" >
                        <div id="continuar" style="display: none">
                            <div class="row">
                                <div class="col-md-4">
                                    <label for="telefono">Teléfono</label>
                                    <input type="number" class="validate[required,maxSize[10],minSize[7]] form-control" id="telefono" name="telefono" min="1" onkeypress="return nro(event)">
                                </div>
                                <div class="col-md-4">
                                    <label for="celular">Celular
                                        (*)</label>
                                    <input type="number" min="1" class="validate[required,minSize[10], maxSize[10]] form-control" id="celular" name="celular" min="1" onkeypress="return nro(event)">
                                </div>
                                <div class="col-md-4">
                                    <label for="mail">Correo electrónico </label>
                                    <input type="email" class="validate[ condRequired[info-correo2], minSize[6], maxSize[50]] form-control" id="email" name="email" placeholder="Dirección de correo electronico">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div id="rpt" style="display: none"><label for="representante"  >Representante Legal(*)</label></div>
                                    <div id="ptr" style="display: none"><label for="representante"  >Propietario(*)</label></div>
                                    <input type="text" class="validate[required,minSize[5], maxSize[60]] form-control" id="representante" name="representante" placeholder="Nombres y Apellidos" onkeypress="return check(event)">
                                </div>
                                <div class="col-md-4">
                                    <label for="tipo_doc">Tipo Documento(*)</label>
                                    <select  class="validate[required, maxSize[10]] form-control" id="tipo_doc" name="tipo_doc" >
                                        <?php $solicitud->TipoDoc() ?>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <label for="numero_doc">Número Documento(*)</label>
                                    <input type="text" class="validate[required, minSize[5],maxSize[15]] form-control" id="numero_doc" name="numero_doc" placeholder="Digite Número de documento" >
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <label for="direccion_notf">Dirección de Notificación Fisica (*)</label>
                                    <input type="text" name="direccion_notf" id="direccion_notf" class="validate[required,minSize[6],maxSize[60]] form-control" >
                                </div>
                                <div class="col-md-4">
                                    <label for="email_notif">Dirección de notificación correo electrónica</label>
                                    <input type="email" class="validate[optional, maxSize[100],minSize[8]] form-control" id="email_notif" name="email_notif">
                                </div>
                                <div class="col-md-4">
                                    <label for="Ciudad">Autoriza Notificación electrónica</label><br>
                                    <input type="radio" id="autoriza_notifsi" class="validate[required]" name="autoriza_notif" value="1">
                                    <label>&nbsp; Si&nbsp;&nbsp;</label>
                                    <input type="radio" id="autoriza_notifno" class="validate[required]" name="autoriza_notif" value="2">
                                    <label>&nbsp; No </label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <label for="Ciudad">Ciudad Notificación(*)</label>
                                    <select class="validate[required] form-control" id="ciudad_notif" name="ciudad_notif">
                                        <option></option>
                                        <?php $solicitud->ciudades(); ?>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <label for="actividades">Actividad Principal del establecimiento(*)</label>
                                    <select class="validate[required] form-control" id="actividadP" name="actividadP">
                                        <option></option>
                                        <?php $solicitud->tipoestablecimientos(); ?>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <label for="actividades">Seleccione primera actividad </label>
                                    <select class="form-control" id="actividadescom1" name="actividadescom1">
                                        <option></option>
                                        <?php $solicitud->tipoestablecimientos(); ?>
                                    </select>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                
                                <div class="col-md-4">
                                    <label for="actividades">Seleccione segunda actividad </label>
                                    <select class="form-control" id="actividadescom2" name="actividadescom2">
                                        <option></option>
                                        <?php $solicitud->tipoestablecimientos(); ?>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <label for="actividades">Seleccione tercera actividad </label>
                                    <select class="form-control" id="actividadescom3" name="actividadescom3">
                                        <option></option>
                                        <?php $solicitud->tipoestablecimientos(); ?>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <label for="actividades">Seleccione cuarta actividad </label>
                                    <select class="form-control" id="actividadescom4" name="actividadescom4">
                                        <option></option>
                                        <?php $solicitud->tipoestablecimientos(); ?>
                                    </select>
                                </div>

                            </div>
                            <div class="row">
                                <h5><strong>Alguna vez el establecimiento ha sido inspeccionado en sus condiciones sanitarias por la subred integrada de servicios de salud?</strong></h5>
                                <input type="radio"  id="inspec_antessi" name="inspec_antes" value="1" class="validate[required]">
                                <label>&nbsp; Si&nbsp;&nbsp;</label>
                                <input type="radio"  id="inspec_antesno" name="inspec_antes" value="2" class="validate[required]">
                                <label>&nbsp; No </label>
                            </div>
  
                            <div class="row" id="inspeccionado" style="display: none;">
                                <div class="col-md-4">
                                    <label for="fecha_insp">Fecha de última inspección(*)</label>
                                    <input type="date" class="validate[required] form-control" name="fecha_insp" id="fecha_insp" class="validate[required]" max="<?php $now ?>">
                                </div> 
                                <div class="col-md-4">
                                    <label>Concepto Emitido(*)</label>
                                    <select name="concepto_emitido"  class="validate[required] form-control" id="concepto_emitido">
                                        <option></option>
                                        <option value="CF">Favorable</option>
                                        <option value="CD">Desfavorable</option>
                                        <option value="CFR">Favorable con requerimientos</option>
                                        <option value="CP">Pendiente</option>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <label>Número acta visita (*)</label>
                                    <input type="text" name="numero_acta" class="validate[required,maxSize[15]] form-control" id="numero_acta">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <center>
                                        <input type="submit" class="btn btn-primary btn-lg" name="Enviar" id="Enviar" onclick="validarFrm2()" value="Enviar">
                                        <a href="../index.php" > <button type="button" class="btn btn-primary btn-lg">Regresar</button></a>
                                        <!-- &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <button type="button" class="btn btn-dark btn-lg">Cancelar</button> -->
                                    </center>
                                </div>
                            </div>
                        </div><!--Continuar -->
                    </form>
                </div>
                <!-- -->
                <div class="col-xl-2"></div>
        <div class="modal" id="myModal" tabindex="-1" role="dialog">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header" style="">
                <h5 class="modal-title" style=" color: #ffffff">Atención!!!</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <div class="alert alert-danger" role="alert">
                        <p><ion-icon name="add-circle"></ion-icon><b><strong>Información importante</strong></b> 
                    <div id="respuestaMensaje"></div>
                </p>
                </div>

                <button type="button" class="btn btn-secondary" data-dismiss="modal" style="background-color: #003e65; color: #ffffff;">Cerrar</button>
              </div>
            </div>
          </div>
        </div>
        <div id="resultadoAjax"></div>
                
            </div>
        </main>
        <div class="container enlaces">
            <div class="row">
                <div class="col-12 col-md-3">
                    <div class="title">Entes de Control</div>
                    <ul>
                        <li><a href="http://www.personeriabogota.gov.co/">Personería de Bogotá</a></li>
                        <li><a href="http://www.procuraduria.gov.co/">Procuraduría General de la Nación</a></li>
                        <li><a href="https://www.contraloria.gov.co/">Contraloría General de la Nación</a></li>
                        <li><a href="http://concejodebogota.gov.co/">Concejo de Bogotá</a></li>
                        <li><a href="http://www.veeduriadistrital.gov.co/">Veeduría Distrital</a></li>
                        <li><a href="https://www.contratacionbogota.gov.co/">Portal de Contratación a la Vista</a></li>
                        <li><a href="https://www.contratos.gov.co/puc/buscador.html">Portal de Contratación - SECOP</a></li>
                    </ul>
                </div>
                <div class="col-12 col-md-3">
                    <div class="title">Entes del Gobierno</div>
                    <ul>
                        <li><a href="https://www.minsalud.gov.co/">Ministerio de Salud y Protección Social</a></li>
                        <li><a href="http://estrategia.gobiernoenlinea.gov.co/623/w3-channel.html">Gobierno Digital</a></li>
                        <li><a href="http://www.saludcapital.gov.co/SiteAssets/customdesign/images/boton_no_mas_filas.jpg">No más filas</a></li>
                    </ul>
                </div>
                <div class="col-12 col-md-3">
                    <div class="title">Menú princial</div>
                    <ul>
                        <li><a href="http://www.saludcapital.gov.co/Paginas2/Inicio.aspx">Inicio</a></li>
                        <li><a href="http://www.saludcapital.gov.co/Paginas2/MisionyVision.aspx">La Entidad</a></li>
                        <li><a href="">Salud Ambiental</a></li>
                        <li><a href="http://www.saludcapital.gov.co/Paginas2/Su-Bogota-vital.aspx">Bogotá Vital es Salud Urbana</a></li>
                        <li><a href="http://www.saludcapital.gov.co/Paginas2/Tramitesyservicios.aspx">Agilínea</a></li>
                    </ul>
                </div>
                <div class="col-12 col-md-3">
                    <div class="title">Secretaría Distrital de Salud</div>
                    <ul>
                        <li>Cra 32# 12-81 Bogotá, Colombia</li>
                        <li>Teléfono: (571) 3649090</li>
                        <li>Código Postal: 0571</li>
                        <li>contactenos@saludcapital.gov.co</li>
                    </ul>
                </div>
            </div>
        </div>

        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-4"></div>
                    <div class="col-12 col-md-4 text-md-center">
                        <p>2019. @ Todos los derechos reservados</p>
                        <p><a href="">*Habeas data</a></p>
                        <p><a href="http://www.saludcapital.gov.co/Documents/Politica_Proteccion_Datos_P.pdf">*Terminos y condiciones</a></p>
                    </div>
                    <div class="col-12 col-md-4 text-md-right">
                        <img src="../img/logos/home_footer.svg" alt="">
                    </div>
                </div>
            </div>
        </footer>  
   
<script type="text/javascript" src="../js/jquery.min.js"></script>
<script type="text/javascript" src="../js/jquery-migrate-1.2.1.js"></script>   
<script type="text/javascript" src="../js/bootstrap.min.js"></script>
<script type="text/javascript" src="../js/jquery.validationEngine.js"></script>
<script type="text/javascript" src="../js/jquery.validationEngine-es.js"></script>      
<script type="text/javascript" src="../js/direccion.js"></script>
<script type="text/javascript" src="../js/validaciones.js"></script>

</body>
</html>
