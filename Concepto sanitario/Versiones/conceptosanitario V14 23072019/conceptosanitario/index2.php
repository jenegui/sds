<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0,shrink-to-fit=no">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Salud</title>

        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/validationEngine.jquery.css">
        <link rel="shortcut icon" href="img/favicon.png" type="image/x-icon">
        <link rel="stylesheet" href="css/enesima-salud.css">
        <link rel="stylesheet" href="css/styles.css">
        <link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet">
        <style type="text/css">
            .auto-style1 {
                width: 100%;
                text-decoration: underline;
            }
        </style>
    </head>
    <body>
        <header>
            <div class="container">
                <div class="logos">
                    <div class="row">
                        <div class="col-12 col-md-6">
                            <img class="logo1" src="img/logos/home_alcaldia.svg" alt="">
                        </div>
                        <div class="col-12 col-md-6 text-right">
                            <img class="logo2" src="img/logos/home_negocios_rentables.svg" alt="">
                        </div>
                    </div>
                </div>

            </div>

        </header>

        <main class="container">
            <div class="breadcrumb">
                <ul>
                    <li><a href="index.php">Inicio</a></li>
                    <li><a href="pages/index.php">Solicite su visita</a></li>
                    <li><a href="pages/consulta.php">Consulte su solicitud</a></li>
                </ul>      
            </div>

            <div class="row block right">
                <div class="col-12 col-md-7">
                    <div class="subtitle">
                        Solicitud visita
                    </div>
                    <div class="paragraph">
                        <p>
                            Solicite aquí su visita para la obtención del concepto sanitario favorable para su establecimiento comercial en la ciudad de Bogotá
                        </p>
                    </div>
                    <div class="text-left">
                        <a href="pages/index.php"><button class="btn yellow">Solicitud concepto</button></a>
                    </div>
                </div>
                <div class="col-12 col-md-5">
                </div>
            </div>
            <div class="row block right">
                <div class="col-12 col-md-7">
                    <div class="subtitle">
                        Consulta estado solicitud
                    </div>
                    <div class="paragraph">
                        <p>
                            Consulte el estado de su solicitud
                        </p>
                    </div>
                    <div class="text-left">
                        <a href="pages/consulta.php"><button class="btn yellow">Consultar solicitud</button></a>
                    </div>
                </div>
                <div class="col-12 col-md-5">
                   
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-md-6"></div>
                <div class="col-12 col-md-6"></div>
            </div>
            <div class="row">
                <div class="col-12 col-md-7"></div>
                <div class="col-12 col-md-5"></div>
            </div>
            <div class="row">
                <div class="col-12 col-md-7"></div>
                <div class="col-12 col-md-5"></div>
            </div>
        </main>
        <div class="container enlaces">
            <div class="row">
                <div class="col-12 col-md-3">
                    <div class="title">Entes de Control</div>
                    <ul>
                        <li><a href="http://www.personeriabogota.gov.co/">Personería de Bogotá</a></li>
                        <li><a href="http://www.procuraduria.gov.co/">Procuraduría General de la Nación</a></li>
                        <li><a href="https://www.contraloria.gov.co/">Contraloría General de la Nación</a></li>
                        <li><a href="http://concejodebogota.gov.co/">Concejo de Bogotá</a></li>
                        <li><a href="http://www.veeduriadistrital.gov.co/">Veeduría Distrital</a></li>
                        <li><a href="https://www.contratacionbogota.gov.co/">Portal de Contratación a la Vista</a></li>
                        <li><a href="https://www.contratos.gov.co/puc/buscador.html">Portal de Contratación - SECOP</a></li>
                    </ul>
                </div>
                <div class="col-12 col-md-3">
                    <div class="title">Entes del Gobierno</div>
                    <ul>
                        <li><a href="https://www.minsalud.gov.co/">Ministerio de Salud y Protección Social</a></li>
                        <li><a href="http://estrategia.gobiernoenlinea.gov.co/623/w3-channel.html">Gobierno Digital</a></li>
                        <li><a href="http://www.saludcapital.gov.co/SiteAssets/customdesign/images/boton_no_mas_filas.jpg">No más filas</a></li>
                    </ul>
                </div>
                <div class="col-12 col-md-3">
                    <div class="title">Menú princial</div>
                    <ul>
                        <li><a href="http://www.saludcapital.gov.co/Paginas2/Inicio.aspx">Inicio</a></li>
                        <li><a href="http://www.saludcapital.gov.co/Paginas2/MisionyVision.aspx">La Entidad</a></li>
                        <li><a href="">Salud Ambiental</a></li>
                        <li><a href="http://www.saludcapital.gov.co/Paginas2/Su-Bogota-vital.aspx">Bogotá Vital es Salud Urbana</a></li>
                        <li><a href="http://www.saludcapital.gov.co/Paginas2/Tramitesyservicios.aspx">Agilínea</a></li>
                    </ul>
                </div>
                <div class="col-12 col-md-3">
                    <div class="title">Secretaría Distrital de Salud</div>
                    <ul>
                        <li>Cra 32# 12-81 Bogotá, Colombia</li>
                        <li>Teléfono: (571) 3649090</li>
                        <li>Código Postal: 0571</li>
                        <li>contactenos@saludcapital.gov.co</li>
                    </ul>
                </div>
            </div>
        </div>

        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-4"></div>
                    <div class="col-12 col-md-4 text-md-center">
                        <p>2019. @ Todos los derechos reservados</p>
                        <p><a href="">*Habeas data</a></p>
                        <p><a href="http://www.saludcapital.gov.co/Documents/Politica_Proteccion_Datos_P.pdf">*Terminos y condiciones</a></p>
                    </div>
                    <div class="col-12 col-md-4 text-md-right">
                        <img src="img/logos/home_footer.svg" alt="">
                    </div>
                </div>
            </div>
        </footer>    

        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/jquery-migrate-1.2.1.js"></script>   
        <script type="text/javascript" src="js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/jquery.validationEngine.js"></script>
        <script type="text/javascript" src="js/jquery.validationEngine-es.js"></script>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
                integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
                integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
        <script src="js/bootstrap.bundle.min.js"></script>
    </body>
</html>