/*
-- Query: SELECT * FROM concepto.upz
LIMIT 0, 1000

-- Date: 2019-02-02 20:17
*/

CREATE TABLE IF NOT EXISTS `upz` (
  `idupz` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_upz` varchar(45) DEFAULT NULL,
  `fk_idlocalidad` int(11) DEFAULT NULL,
  `codigo_upz` int(11) DEFAULT NULL,
  PRIMARY KEY (`idupz`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (1,'PASEO DE LOS LIBERTADORES',6,1);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (2,'LA ACADEMIA',14,2);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (3,'GUAYMARAL',14,3);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (4,'VERBENAL',6,9);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (5,'LA URIBE',6,10);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (6,'SAN CRISTOBAL NORTE',6,11);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (7,'TOBERIN',6,12);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (8,'LOS CEDROS',6,13);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (9,'USAQUEN',6,14);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (10,'COUNTRY CLUB',6,15);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (11,'SANTA BARBARA',6,16);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (12,'SAN JOSE DE BAVARIA',14,17);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (13,'BRITALIA',14,18);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (14,'EL PRADO',14,19);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (15,'LA ALHAMBRA',14,20);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (16,'LOS ANDES',15,21);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (17,'DOCE DE OCTUBRE',15,22);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (18,'CASA BLANCA SUBA',14,23);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (19,'NIZA',14,24);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (20,'LA FLORESTA',14,25);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (21,'LAS FERIAS',13,26);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (22,'SUBA',14,27);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (23,'EL RINCON',14,28);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (24,'EL MINUTO DE DIOS',13,29);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (25,'BOYACA REAL',13,30);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (26,'SANTA CECILIA',13,31);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (27,'SAN BLAS',7,32);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (28,'SOSIEGO',7,33);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (29,'20 DE JULIO',7,34);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (30,'CIUDAD JARDIN',18,35);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (31,'SAN JOSE',21,36);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (32,'SANTA ISABEL',17,37);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (33,'RESTREPO',18,38);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (34,'QUIROGA',21,39);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (35,'CIUDAD MONTES',19,40);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (36,'MUZU',19,41);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (37,'VENECIA',9,42);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (38,'SAN RAFAEL',19,43);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (39,'AMERICAS',11,44);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (40,'CARVAJAL',11,45);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (41,'CASTILLA',11,46);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (42,'KENNEDY CENTRAL',11,47);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (43,'TIMIZA',11,48);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (44,'APOGEO',10,49);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (45,'LA GLORIA',7,50);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (46,'LOS LIBERTADORES',7,51);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (47,'LA FLORA',8,52);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (48,'MARCO FIDEL SUAREZ',21,53);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (49,'MARRUECOS',21,54);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (50,'DIANA TURBAY',21,55);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (51,'DANUBIO',8,56);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (52,'GRAN YOMASA',8,57);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (53,'COMUNEROS',8,58);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (54,'ALFONSO LOPEZ',8,59);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (55,'PARQUE ENTRENUBES',8,60);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (56,'CIUDAD USME',8,61);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (57,'TUNJUELITO',9,62);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (58,'EL MOCHUELO',22,63);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (59,'MONTE BLANCO',22,64);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (60,'ARBORIZADORA',22,65);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (61,'SAN FRANCISCO',22,66);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (62,'LUCERO',22,67);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (63,'EL TESORO',22,68);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (64,'ISMAEL PERDOMO',22,69);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (65,'JERUSALEM',22,70);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (66,'TIBABUYES',14,71);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (67,'BOLIVIA',13,72);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (68,'GARCES NAVAS',13,73);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (69,'ENGATIVA',13,74);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (70,'FONTIBON',12,75);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (71,'FONTIBON SAN PABLO',12,76);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (72,'ZONA FRANCA',12,77);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (73,'TINTAL NORTE',11,78);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (74,'CALANDAIMA',11,79);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (75,'CORABASTOS',11,80);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (76,'GRAN BRITALIA',11,81);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (77,'PATIO BONITO',11,82);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (78,'LAS MARGARITAS',11,83);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (79,'BOSA OCCIDENTAL',10,84);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (80,'BOSA CENTRAL',10,85);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (81,'EL PORVENIR',10,86);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (82,'TINTAL SUR',10,87);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (83,'EL REFUGIO',1,88);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (84,'SAN ISIDRO-PATIOS',1,89);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (85,'PARDO RUBIO',1,90);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (86,'SAGRADO CORAZON',2,91);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (87,'LA MACARENA',2,92);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (88,'LAS NIEVES',2,93);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (89,'LA CANDELARIA',20,94);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (90,'LAS CRUCES',2,95);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (91,'LOURDES',2,96);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (92,'CHICO LAGO',1,97);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (93,'LOS ALCAZARES',15,98);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (94,'CHAPINERO',1,99);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (95,'GALERIAS',16,100);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (96,'TEUSAQUILLO',16,101);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (97,'LA SABANA',17,102);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (98,'PARQUE EL SALITRE',15,103);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (99,'PARQUE SIMON BOLIVAR-CAN',16,104);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (100,'JARDIN BOTANICO',13,105);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (101,'LA ESMERALDA',16,106);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (102,'QUINTA PAREDES',16,107);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (103,'ZONA INDUSTRIAL',19,108);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (104,'CIUDAD SALITRE ORIENTAL',16,109);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (105,'CIUDAD SALITRE OCCIDENTAL',12,110);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (106,'PUENTE ARANDA',19,111);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (107,'GRANJAS DE TECHO',12,112);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (108,'BAVARIA',11,113);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (109,'MODELIA',12,114);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (110,'CAPELLANIA',12,115);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (111,'ALAMOS',13,116);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (112,'AEROPUERTO EL DORADO',12,117);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (113,'UPZ Sin Asignar',1,NULL);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (115,'N.A.',35,NULL);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (116,'UPR Rio Blanco',23,120);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (117,'UPR Rio San Juan',23,121);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (118,'UPR Cerros Orientales',6,122);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (119,'UPR Rio Tunjuelito (Ciudad Bolivar)',22,123);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (120,'UPR Rio Tunjuelito (Usme)',8,124);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (121,'UPR Zona Norte (Suba)',14,125);
INSERT INTO `upz` (`idupz`,`nombre_upz`,`fk_idlocalidad`,`codigo_upz`) VALUES (122,'UPR Zona Norte (UsaquÃ©n)',6,126);
