<?php

use Illuminate\Database\Seeder;

class flujo_tramite extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Note: these dump files must be generated with DELETE (or TRUNCATE) + INSERT statements
        $sql = "TRUNCATE flujo_tramite;";
        $sql +=" INSERT INTO `flujo_tramite` (`id`, `tipo_tramite`, `flujo_actividad_inicial`, `flujo_actividad_final`, `flujo_accion_permitida`, `tipo_estado`) VALUES (NULL, 'rayosx', 'registrar_solicitud', 'revisar_solicitud', 'avanzar', '1');";

        if (! str_contains($sql, ['DELETE', 'TRUNCATE'])) {
            throw new Exception('Invalid sql file. This will not empty the tables first.');
        }

        // split the statements, so DB::statement can execute them.
        $statements = array_filter(array_map('trim', explode(';', $sql)));

        foreach ($statements as $stmt) {
            DB::statement($stmt);
        }
    }
}
