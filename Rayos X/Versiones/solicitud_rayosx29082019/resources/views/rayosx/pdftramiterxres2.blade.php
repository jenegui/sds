<html>
<head>
  <title>Certificación de Contratistas - Tramites en Linea . SDS</title>
  <meta charset="utf-8">
  <style type="text/css">

    @page { margin: 180px 50px; }
    #header { position: fixed; left: 0px; top: -150px; right: 0px; height: 0px; text-align: center; }
    #footer { position: fixed; bottom: -60px; left: 0px; right: 0px; height: 50px; text-align: left; font-size:10px;}

    body,td,th {
      font-family: Arial, Helvetica, sans-serif;
    }
    p {
      color: #000000;
      line-height: 1.5em;
      font-size:13px;
    }
    li {
      margin-bottom: 1em;
      color: #000000;
      line-height: 1.5em;
      font-size:13px;
      letter-spacing: 0px;
    }
    body {
      margin-left: 0cm;
      margin-top: 0cm;
      margin-right: 0cm;
      margin-bottom: 0cm;
    }
    i {
      color: #000000;
      padding-right: 1em;
    }
    h1{
      font-size: 36px;
      color: #000000;
    }
    h3{
      font-size: 24px;
      color: #000000;
    }
    span {
      color: #000000;
    }
  </style>
</head>
<body>
<div id="header">
  <img src="./img/memologo.jpg" />
</div>
  <div style="width:100%;">
      <div style="margin-left:45px;margin-right:45px;margin-top:-50px;">
        <p align="center">
            <br><b><em>Dirección de Calidad de Servicios de Salud<br>
              Subdirección Inspección, Vigilancia y Control de Servicios de Salud</em>
                </b>
        </p>
        <p align="center">
            <b><em>Resolución No. {{$tramitesrx->id_tramite}} de {{$tramitesrx->created_at}}<br>
              “Por la cual se niega La Licencia de Práctica Médica Categoría {{$tramitesrx->categoria}} para Equipo de Radiaciones Ionizantes”
              </em></b>
        </p>
        <p aling="center"><b>LA DIRECTORA DE CALIDAD DE SERVICIOS DE SALUD</b><br></p>

        <p align="justify">En uso de sus facultades legales conferidas en el Decreto 507 del 06 de Noviembre de 2013 de la Alcaldía Mayor
          de Bogotá y Resolución 482 de 2018,</p>
        <p align="center"><b>CONSIDERANDO:</b></p>
        <p align="justify">Que la Resolución 482 del 22 de febrero de 2018, expedida por el Ministerio de Salud y Protección Social,
          “Por la cual se reglamenta el uso de equipos generadores de radiación ionizante, su control de calidad, la prestación de
          servicios de protección radiológica y se dictan otras disposiciones”, establece:</p>
        <p align="justify"><em>“(….) Artículo 26. Trámite de las licencias de prácticas médicas categoría I o II. El estudio de la
          documentación para el otorgamiento de la licencia de prácticas médicas categoría I o II, estará sujeto al siguiente
          procedimiento:</em></p>
        <p align="justify"><em>26.1. Radicada la solicitud en el formato dispuesto en el Anexo No. 3, con los documentos requeridos
          en los artículos 21 o 23, según la categoría, la entidad territorial de salud departamental o distrital, según corresponda,
          procederá a revisarla dentro de los veinte (20) días hábiles siguientes y, de encontrar la documentación incompleta,
          requerirá al solicitante para que la suministre dentro de los veinte (20) días hábiles siguientes.</em></p>
        <p align="justify"><em>26.3.1. Para las licencias de práctica médica categoría I, se procederá a estudiar la documentación y dentro
          de los cuarenta y cinco (45) días hábiles siguientes, a emitir el acto administrativo</em></p>
        <p align="justify"><em>26.3.2. Para las licencias de práctica médica categoría II se programará visita con enfoque de riesgo,
          encaminada a la verificación de los requisitos a que refiere el artículo 24, la cual se realizará en un término no
          superior a sesenta (60) días hábiles, contado a partir de la radicación de la solicitud o de la complementación de esta,
          según sea el caso.</em></p>
        <p align="justify"><em>26.3.3. Si como consecuencia de la mencionada visita se determina la necesidad de complementar la
          información o la documentación de que trata el artículo 24 de la presente resolución, en el acta que se suscriba,
          producto de la referida visita, se dejará constancia de ello y el solicitante dispondrá de veinte (20) días hábiles
          para allegar dicha información.</em>
        </p>
        <p align="justify"><em>26.3.4. Rebasado el término del requerimiento o el de la visita, la entidad territorial de salud
          de carácter departamental o distrital, según corresponda, dentro de los cuarenta y cinco (45) días hábiles siguientes,
          entrará a resolver de fondo la solicitud con la documentación e información de que disponga, decisión que será notificada
          de acuerdo con lo establecido por el Código de Procedimiento Administrativo y de lo Contencioso Administrativo y susceptible
          de los recursos en este contemplados</em></p>
        <p align="justify">Que la Dirección de Calidad de Servicios de Salud, mediante radicado {{$tramitesrx->id_tramite}} de {{$tramitesrx->created_at}},
          otorgó plazo de 20 días hábiles para allegar los documentos faltantes para continuar con el trámite de la Licencia
          de Práctica Médica Categoría {{$tramitesrx->categoria}}. (Folio XX)</p>
        <p align="justify">Que mediante radicado {{$tramitesrx->id_tramite}} del {{$tramitesrx->created_at}} el prestador
          {{$tramitesrx->name}} identificado XX {{$tramitesrx->name}}, envío la documentación solicitada por la Dirección de
          Calidad de Servicios de Salud. Una vez revisada, se encontró lo siguiente: Folio (XX a XX)</p>
        <p align="justify">En mérito a lo expuesto, este Despacho</p>
        <p align="center"><b>RESUELVE:</b></p>
        <p align="justify"><b>ARTÍCULO PRIMERO:</b> Negar la expedición de la Licencia de Práctica Médica Categoría
          {{$tramitesrx->categoria}} al (la) señor(a) {{$tramitesrx->name}}, identificado con XX No {{$tramitesrx->name}},
          por las razones expuestas en la parte considerativa de ésta Resolución.</p>
        <p align="justify"><b>ARTICULO SEGUNDO:</b> Notificar personalmente al señor {{$tramitesrx->name}},
          identificado con XX No {{$tramitesrx->name}} en su calidad de representante legal, quien haga sus veces o a
          quien él delegue, el contenido de la presente Resolución, informándole que de conformidad con el artículo 74
          del Código de Procedimiento Administrativo y de lo Contencioso Administrativo (Ley 1437 de 2011) contra la
          misma proceden los recursos de reposición y en subsidio apelación, los cuales podrá interponer ante ésta Dirección,
          dentro de los diez (10) días hábiles siguientes a la notificación de este acto administrativo</p>
        <p align="center"><b>NOTIFÍQUESE Y CÚMPLASE</b></p>
        <p align="center">Dado en Bogotá, D.C. a los{{$tramitesrx->created_at}}</p>
        <p align="justify"><b>SANDRA PATRICIA CHARRY ROJAS</b></p>
        <p align="justify">Director de Calidad de Servicios de Salud</p>

      </div>
  </div>
  <div id="footer" style="margin-left:45px;margin-right:45px">



  </div>

</body>
</html>
