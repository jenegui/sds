@extends('layouts.layout')

@section('content')


<h2 class="text-blue"><b>Registro y autorización de licencias de Rayos X </b></h2>
<div id="divPasos" class="text-center">
   <div class="registro_triada">
      <button class="btn yellow" onClick="step1();">1 - Localizacíon Entidad</button>
      <i class="fa fa-chevron-right"></i>
      <button class="btn yellow" onClick="step2();">2 - Equipos Rayos x</button>
      <i class="fa fa-chevron-right"></i>
      <button class="btn yellow" onClick="step3();">3 - Trabajadores TOE</button>
      <i class="fa fa-chevron-right"></i>
      <button class="btn yellow" onClick="step4();">4 - Talento Humano</button>
      <i class="fa fa-chevron-right"></i>
      <button class="btn yellow" onClick="step5();">5 - Documentos Adjuntos</button>
   </div>
</div>


<!-- localizacion Identidad -->
<form id="formSeccion1" type="submit" name="form_tramite" action="{{route('rayosx.guardarLocalizacion')}}" method="post" class="form-row">
   {{ csrf_field() }}

   <!-- PRIMERA PARTE DEL REGISTRO DE LA SOLICITUD - LOCALIZACION tipoTramite  -->
   <div id="paso1" class="row block w-100 newsletter ">

      <div class="w-100">
        <div class="subtitle">
            <h3><b>Localizacíon Entidad2:</b></h3>
        </div>
        @if($rayosxDireccion != NULL)
        <div class="row">
          <div class="alert alert-info">
            <strong><b>Mensaje Informativo!</b></strong> Esta sección del formulario ya fue guardada. Si desea editar esta sección complete el formulario en todos sus pasos. Y podra editarlo antes de Finalizar el Trámite.
          </div>
        </div>
        @endif
        <div class="row">
            <div class="col">
               <span class="text-orange">•</span><label for="depto_entidad">Departamento(*)</label>
               <input id="id_tramite" name="id_tramite" type="text" value="{{ isset($idTramiterx) ? $idTramiterx->id_tramite : '' }}">
               <select id="depto_entidad" name="depto_entidad" class="form-control validate[required]" required>
                  <option value=""> - Seleccione Departamento -</option>
                  @foreach ($departamentos as $departamento => $value)
                   <option value="{{ $value}}"
                  {{(isset($rayosxDireccion->depto_entidad) && $rayosxDireccion->depto_entidad == $value)  ? 'selected' : '' }}>
                      {{ $departamento}}</option>
                   @endforeach
               </select>
            </div>

            <div class="col">
               <span class="text-orange">•</span><label for="mpio_entidad">Municipio(*)</label>
               <select id="mpio_entidad" name="mpio_entidad" class="form-control validate[required]" required>
                 <option value="{{ $value}}"
                {{(isset($rayosxDireccion->mpio_entidad) && $rayosxDireccion->mpio_entidad == $value)  ? 'selected' : '' }}>
               </select>
               <div class="col-md-2" style="visibility:hidden;"><span id="loader"><i class="fa fa-spinner fa-3x fa-spin"></i></span></div>
            </div>

            <!--div class="col">
               <span class="text-orange">•</span><label for="loc_entidad">Localidad</label>
               <select id="loc_entidad" name="loc_entidad" class="form-control validate[required]">
                  <option value="">Seleccione Localidad...</option>
                  <option value = "Prueba">Prueba</option>
               </select>
            </div>

            <div class="col">
               <span class="text-orange">•</span><label for="barrio_entidad">Barrio</label>
               <select id="barrio_entidad" name="barrio_entidad" class="form-control validate[required]">
                  <option value="">Seleccione Barrio...</option>
                  <option value = "Prueba">Prueba</option>
               </select>
            </div-->

        </div>

        <div class="form-group">
            <span class="text-orange">•</span><label for="tipo_tramite">Dirección de la Instalación(*)</label>
            <input id="dire_entidad" name="dire_entidad" class="form-control input-md validate[required, minSize[4], maxSize[30]]" type="text" required placeholder="Ingresar la Dirección Entidad" value="{{ isset($rayosxDireccion) ? $rayosxDireccion->dire_entidad : '' }}" onkeyup="javascript:this.value=this.value.toUpperCase();" pattern="{4,30}"  minlength="4" maxlength="30" title="Ingresar un Tamaño mínimo: 4 carácteres a un Tamaño máximo: 30 carácteres">
         </div>

         <div class="form-group">
            <span class="text-orange">•</span><label for="dire_entidad">Sede de la Instalación(*)</label>
            <input id="sede_entidad" name="sede_entidad" class="form-control input-md validate[required, minSize[4], maxSize[30]]" type="text" required placeholder="Ingresar el Nombre Distintivo de la sede que sera sujeta a inspección" value="{{ isset($rayosxDireccion) ? $rayosxDireccion->sede_entidad : '' }}" onkeyup="javascript:this.value=this.value.toUpperCase();" pattern="{4,30}" minlength="4" maxlength="30"  title="Ingresar un Tamaño mínimo: 4 carácteres a un Tamaño máximo: 30 carácteres">
         </div>
         <!-- 2 campos en la fila --->
         <div class="row">
            <div class="col">
               <span class="text-orange">•</span><label for="email_entidad">Correo Electrónico(*)</label>
               <input id="email_entidad" name="email_entidad" class="form-control validate[required, custom[email]] input-md" type="email" required placeholder="Ingresar Correo Electrónico"  value="{{ isset($rayosxDireccion) ? $rayosxDireccion->email_entidad : '' }}" onkeyup="javascript:this.value=this.value.toUpperCase();" pattern="{4,30}" minlength="4" maxlength="30"  title="Ingresar un Tamaño mínimo: 4 carácteres a un Tamaño máximo: 30 carácteres">
            </div>

            <div class="col">
               <span class="text-orange">•</span><label for="celular_entidad">Número celular(*)</label>
               <input id="celular_entidad" name="celular_entidad" placeholder="Ingresar Número Celular de contacto en sede de la Instalación" class="form-control input-md validate[required, minSize[10], maxSize[15]]" type="number" value="{{ isset($rayosxDireccion) ? $rayosxDireccion->celular_entidad : '' }}" pattern="[0-9]{10,15}" minlength="10" maxlength="15"  title="Ingresar un Tamaño mínimo: 10 carácteres a un Tamaño máximo: 15 carácteres" onKeyPress="if(this.value.length==15) return false;">
            </div>
         </div>
         <div class="row">
            <div class="col col-md-6">
               <span class="text-orange">•</span><label for="telefono_entidad">Número telefónico fijo</label>
               <input id="telefono_entidad" name="telefono_entidad" placeholder="Ingresar Número telefonico de contacto en sede de la Instalación" class="form-control input-md validate[required, minSize[7], maxSize[15]]" type="number" value="{{ isset($rayosxDireccion) ? $rayosxDireccion->telefono_entidad : '' }}"  pattern="[0-9]{7,15}" minlength="7" maxlength="15"  title="Ingresar un Tamaño mínimo: 7 carácteres a un Tamaño máximo: 15 carácteres" onKeyPress="if(this.value.length==15) return false;">
            </div>

            <div class="col">
               <span class="text-orange">•</span><label for="extension_entidad">Extensión</label>
               <input id="extension_entidad" name="extension_entidad" placeholder="Ingresar Extensión telefonica de contacto en sede" class="form-control input-md validate[required, minSize[3], maxSize[5]]" type="number" value="{{ isset($rayosxDireccion) ? $rayosxDireccion->extension_entidad : '' }}" pattern="[0-9]{3,5}"  minlength="3" maxlength="5"  title="Ingresar un Tamaño mínimo: 3 carácteres a un Tamaño máximo: 5 carácteres" onKeyPress="if(this.value.length==5) return false;">
            </div>
         </div>
@if($rayosxDireccion==NULL)
          <div id="btnRegistrarGuardarEntidad" class="col-md-12 pt-200">
            <p align="center">
               <br/>
               <!-- Primer Collapsible - Localizacion Entidad -->
               <button type="submit" class="btn yellow">
                  Guardar y Continuar
               </button>
            </p>
          </div>
@endif
      </div>
   </div>
</form>


<!-- Equipos generadores de radiación ionizante -->
<form id="formSeccion2" type="submit" name="formSeccion2" action="{{route('rayosx.guardarEquiposRayosX')}}" method="post" class="form-row collapse">
   {{ csrf_field() }}
   <!-- PRIMERA PARTE DEL REGISTRO DE LA SOLICITUD - LOCALIZACION tipoTramite  -->
   <div id="paso2" class="row block w-100 newsletter ">

      <div class="w-100">
        <div class="subtitle">
            <h3><b>Equipos generadores de radiación ionizante:</b></h3>
        </div>

        @if($rayosxEquipo != NULL)
        <div class="row">
          <div class="alert alert-info">
            <strong><b>Mensaje Informativo!</b></strong> Esta sección del formulario ya fue guardada. Si desea editar esta sección complete el formulario en todos sus pasos. Y podra editarlo antes de Finalizar el Trámite.
          </div>
        </div>
        @endif

        <div class="row">

           <div class="col">
               <span class="text-orange">•</span><label for="categoria">Categoría</label>
               <input id="id_tramite" name="id_tramite" type="text" value="{{ isset($idTramiterx) ? $idTramiterx->id_tramite : '' }}">
               <select id="categoria" name="categoria" class="form-control validate[required]" required>
                  <option value="">Seleccione...</option>
                  <option value="1"
                  {{(isset($rayosxEquipo->categoria) && $rayosxEquipo->categoria == 1)  ? 'selected' : '' }}>
                    Categoria I
                  </option>
                  <option value="2"
                  {{(isset($rayosxEquipo->categoria) && $rayosxEquipo->categoria == 2)  ? 'selected' : '' }}>
                  Categoria II
                  </option>
               </select>
            </div>

            <div class="col">
               <span class="text-orange">•</span><label for="categoria1">Equipos generadores de radicación ionizante</label>
               <select id="categoria1" name="categoria1" class="form-control validate[required]" required>
                  <option value="">Seleccione...</option>
               </select>
            </div>

         </div>

         <div class="row">

            <div class="col">
                  <span class="text-orange">•</span><label for="categoria2">Equipos generadores de radicación ionizante</label>
                  <select id="categoria2" name="categoria2" class="form-control validate[required]" required>
                     <option value="">Seleccione...</option>
                     <option value="1"
                     {{(isset($rayosxEquipo->categoria2) && $rayosxEquipo->categoria2 == 1 )  ? 'selected' : '' }}>
                     Radioterapia
                     </option>
                     <option value="2"
                     {{(isset($rayosxEquipo->categoria2) && $rayosxEquipo->categoria2 == 2 )  ? 'selected' : '' }}>
                     Radio diagnóstico de alta complejidad
                     </option>
                     <option value="3"
                     {{(isset($rayosxEquipo->categoria2) && $rayosxEquipo->categoria2 == 3 )  ? 'selected' : '' }}>
                     Radio diagnóstico de media complejidad
                     </option>
                     <option value="4"
                     {{(isset($rayosxEquipo->categoria2) && $rayosxEquipo->categoria2 == 4 )  ? 'selected' : '' }}>
                     Radio diagnóstico de baja complejidad
                     </option>
                     <option value="5"
                     {{(isset($rayosxEquipo->categoria2) && $rayosxEquipo->categoria2 == 5 )  ? 'selected' : '' }}>
                     Radiografias odontológicas panóramicas y tomografias orales
                     </option>
                  </select>
               </div>

            <div class="col" id="div_radperia" style="display:none;">
               <span class="text-orange">•</span><label for="categoria1-1">Radiolog&iacute;a odontológica periapical</label>
               <select id="categoria1_1" name="categoria1_1" class="form-control validate[required]">
                  <option value="">Seleccione...</option>
                  <option value="1"
                  {{(isset($rayosxEquipo->categoria1_1) && $rayosxEquipo->categoria1_1 == 1 )  ? 'selected' : '' }}>
                  Equipo de RX odontológico periapical
                  </option>
                  <option value="2"
                  {{(isset($rayosxEquipo->categoria) && $rayosxEquipo->categoria1_1 == 2)  ? 'selected' : '' }}>
                  Equipo de RX odontológico periapical portat&iacute;l
                  </option>
               </select>
            </div>

         </div>

         <div class="row">



         </div>

         <div class="form-group">
            <span class="text-orange">•</span><label for="tipo_visualizacion">Tipo de visualización de la imagen</label>
            <select id="tipo_visualizacion" name="tipo_visualizacion" class="form-control validate[required]" required>
               <option value="1"
               {{(isset($rayosxEquipo->tipo_visualizacion) && $rayosxEquipo->tipo_visualizacion == 1 )  ? 'selected' : '' }}>
                 Digital
               </option>
               <option value="2"
               {{(isset($rayosxEquipo->tipo_visualizacion) && $rayosxEquipo->tipo_visualizacion == 2 )  ? 'selected' : '' }}>
                 Digitalizado
               </option>
               <option value="3"
               {{(isset($rayosxEquipo->tipo_visualizacion) && $rayosxEquipo->tipo_visualizacion == 3 )  ? 'selected' : '' }}>
                 Analogo
               </option>
               <option value="4"
               {{(isset($rayosxEquipo->tipo_visualizacion) && $rayosxEquipo->tipo_visualizacion == 4 )  ? 'selected' : '' }}>
                 Revelado Automatico
               </option>
               <option value="5"
               {{(isset($rayosxEquipo->tipo_visualizacion) && $rayosxEquipo->tipo_visualizacion == 5 )  ? 'selected' : '' }}>
               Revelado Manual
               </option>
               <option value="6"
               {{(isset($rayosxEquipo->tipo_visualizacion) && $rayosxEquipo->tipo_visualizacion == 6 )  ? 'selected' : '' }}>
               Monitor Analogo
               </option>
               <option value="7"
               {{(isset($rayosxEquipo->tipo_visualizacion) && $rayosxEquipo->tipo_visualizacion == 7 )  ? 'selected' : '' }}>
               No Aplica
               </option>
            </select>
         </div>

         <div class="row">

            <div class="col">
               <span class="text-orange">•</span><label for="marca_equipo">Marca equipo</label>
               <input id="marca_equipo" name="marca_equipo" placeholder="Ingresar Marca equipo" class="form-control input-md validate[required, minSize[4], maxSize[30]]"  required type="text"  value="{{ isset($rayosxEquipo) ? $rayosxEquipo->marca_equipo : '' }}" onkeyup="javascript:this.value=this.value.toUpperCase();" pattern="{4,30}"  minlength="4" maxlength="30" title="Ingresar un Tamaño mínimo: 4 carácteres a un Tamaño máximo: 30 carácteres">
            </div>

            <div class="col">
               <span class="text-orange">•</span><label for="modelo_equipo">Modelo equipo</label>
               <input id="modelo_equipo" name="modelo_equipo" placeholder="Ingresar Modelo equipo" class="form-control input-md validate[required, minSize[4], maxSize[30]]" required  type="text"  value="{{ isset($rayosxEquipo) ? $rayosxEquipo->modelo_equipo : '' }}" onkeyup="javascript:this.value=this.value.toUpperCase();" pattern="{4,30}"  minlength="4" maxlength="30" title="Ingresar un Tamaño mínimo: 4 carácteres a un Tamaño máximo: 30 carácteres">
            </div>

            <div class="col">
               <span class="text-orange">•</span><label for="serie_equipo">Serie equipo</label>
               <input id="serie_equipo" name="serie_equipo" placeholder="Ingresar Serie equipo" class="form-control input-md validate[required, minSize[4], maxSize[30]]"  required  type="text" value="{{ isset($rayosxEquipo) ? $rayosxEquipo->serie_equipo : '' }}" onkeyup="javascript:this.value=this.value.toUpperCase();" pattern="{4,30}"  minlength="4" maxlength="30" title="Ingresar un Tamaño mínimo: 4 carácteres a un Tamaño máximo: 30 carácteres">
            </div>

            <div class="col">
               <span class="text-orange">•</span><label for="marca_tubo_rx">Marca tubo RX</label>
               <input id="marca_tubo_rx" name="marca_tubo_rx" placeholder="Ingresar Marca tubo RX" class="form-control input-md validate[required, minSize[4], maxSize[30]]" required type="text" value="{{ isset($rayosxEquipo) ? $rayosxEquipo->marca_tubo_rx : '' }}"onkeyup="javascript:this.value=this.value.toUpperCase();" pattern="{4,30}"  minlength="4" maxlength="30" title="Ingresar un Tamaño mínimo: 4 carácteres a un Tamaño máximo: 30 carácteres">
            </div>

         </div>

         <div class="row">

            <div class="col">
               <span class="text-orange">•</span><label for="modelo_tubo_rx">Modelo tubo RX</label>
               <input id="modelo_tubo_rx" name="modelo_tubo_rx" placeholder="Ingresar Modelo tubo RX" class="form-control input-md validate[required, minSize[4], maxSize[30]]"  required  type="text" value="{{ isset($rayosxEquipo) ? $rayosxEquipo->modelo_tubo_rx : '' }}" onkeyup="javascript:this.value=this.value.toUpperCase();" pattern="{4,30}"  minlength="4" maxlength="30" title="Ingresar un Tamaño mínimo: 4 carácteres a un Tamaño máximo: 30 carácteres">
            </div>

            <div class="col">
               <span class="text-orange">•</span><label for="serie_tubo_rx">Serie tubo RX</label>
               <input id="serie_tubo_rx" name="serie_tubo_rx" placeholder="Ingresar Serie tubo RX" class="form-control input-md validate[required, minSize[4], maxSize[30]]" required  type="text" value="{{ isset($rayosxEquipo) ? $rayosxEquipo->serie_tubo_rx : '' }}" onkeyup="javascript:this.value=this.value.toUpperCase();" pattern="{4,30}"  minlength="4" maxlength="30" title="Ingresar un Tamaño mínimo: 4 carácteres a un Tamaño máximo: 30 carácteres">
            </div>

            <div class="col">
               <span class="text-orange">•</span><label for="tension_tubo_rx">Tensión máxima tubo RX [kV]</label>
               <input id="tension_tubo_rx" name="tension_tubo_rx" placeholder="Ingresar Tensión máxima tubo RX [kV]" class="form-control input-md validate[required, minSize[1], maxSize[3]]"  required type="number" step="any" value="{{ isset($rayosxEquipo) ? $rayosxEquipo->tension_tubo_rx : '' }}" pattern="^\d*(\.\d{0,4})?$"  min="0" max="10" step="0.01" title="Ingresar un máximo 4 decimales, separador decimal punto ó coma" onKeyPress="if(this.value.length==6) return false;" >
            </div>

            <div class="col">
               <span class="text-orange">•</span><label for="contiene_tubo_rx">Cont. Max del tubo RX [mA]</label>
               <input id="contiene_tubo_rx" name="contiene_tubo_rx" placeholder="Ingresar Contiene máxima del tubo RX [mA]" class="form-control input-md validate[required, minSize[1], maxSize[3]]" required type="number" step="any" value="{{ isset($rayosxEquipo) ? $rayosxEquipo->contiene_tubo_rx : '' }}" pattern="^\d*(\.\d{0,4})?$"  min="0" max="10" step="0.01" title="Ingresar un máximo 4 decimales, separador decimal punto ó coma" onKeyPress="if(this.value.length==6) return false;">
            </div>
         </div>

         <div class="row">

            <div class="col">
               <span class="text-orange">•</span><label for="energia_fotones">Energ&iacute;a de fotones [MeV]</label>
               <input id="energia_fotones" name="energia_fotones" placeholder="Ingresar Energ&iacute;a de fotones [MeV]" class="form-control input-md validate[required, minSize[1], maxSize[3]]" required type="number" step="any" value="{{ isset($rayosxEquipo) ? $rayosxEquipo->energia_fotones : '' }}" pattern="^\d*(\.\d{0,4})?$"  min="0" max="10" step="0.01" title="Ingresar un máximo 4 decimales, separador decimal punto ó coma" onKeyPress="if(this.value.length==6) return false;">
            </div>

            <div class="col">
               <span class="text-orange">•</span><label for="energia_electrones">Energ&iacute;a de electrones [MeV]</label>
               <input id="energia_electrones" name="energia_electrones" placeholder="Ingresar Energ&iacute;a de electrones [MeV]" class="form-control input-md validate[required, minSize[1], maxSize[3]]" required type="number" step="any" value="{{ isset($rayosxEquipo) ? $rayosxEquipo->energia_fotones : '' }}" pattern="^\d*(\.\d{0,4})?$"  min="0" max="10" step="0.01" title="Ingresar un máximo 4 decimales, separador decimal punto ó coma" onKeyPress="if(this.value.length==6) return false;">
            </div>

            <div class="col">
               <span class="text-orange">•</span><label for="carga_trabajo">Carga de trabajo [mA.min/semana]</label>
               <input id="carga_trabajo" name="carga_trabajo" placeholder="Ingresar Carga de trabajo [mA.min/semana]" class="form-control input-md validate[required, minSize[1], maxSize[3]]" required type="number" step="any" value="{{ isset($rayosxEquipo) ? $rayosxEquipo->carga_trabajo : '' }}" pattern="^\d*(\.\d{0,4})?$"  min="0" max="10" step="0.01" title="Ingresar un máximo 4 decimales, separador decimal punto ó coma" onKeyPress="if(this.value.length==6) return false;">
            </div>

         </div>



         <div class="form-group">
            <span class="text-orange">•</span><label for="ubicacion_equipo">Ubicación del equipo de la instalación</label>
            <input id="ubicacion_equipo" name="ubicacion_equipo" placeholder="Ingresar Ubicación del equipo de la instalación" class="form-control input-md validate[required, minSize[4], maxSize[30]]" required type="text" value="{{ isset($rayosxEquipo) ? $rayosxEquipo->ubicacion_equipo : '' }}" onkeyup="javascript:this.value=this.value.toUpperCase();" pattern="{4,30}"  minlength="4" maxlength="30" title="Ingresar un Tamaño mínimo: 4 carácteres a un Tamaño máximo: 30 carácteres">
         </div>

         <div class="row">

            <div class="col">
               <span class="text-orange">•</span><label for="anio_fabricacion">A&ntilde;o de fabricación del equipo</label>
               <input id="anio_fabricacion" name="anio_fabricacion" placeholder="Ingresar A&ntilde;o de fabricación del equipo" class="form-control input-md validate[required, minSize[4], maxSize[4]]" required type="number" value="{{ isset($rayosxEquipo) ? $rayosxEquipo->anio_fabricacion : '' }}" onKeyPress="if(this.value.length==4) return false;">
            </div>

            <div class="col">
               <span class="text-orange">•</span><label for="anio_fabricacion_tubo">A&ntilde;o de fabricación del tubo</label>
               <input id="anio_fabricacion_tubo" name="anio_fabricacion_tubo" placeholder="Ingresar A&ntilde;o de fabricación del tubo" class="form-control input-md validate[required, minSize[4], maxSize[4]]" required type="number" value="{{ isset($rayosxEquipo) ? $rayosxEquipo->anio_fabricacion_tubo : '' }}" onKeyPress="if(this.value.length==4) return false;">
            </div>

            <div class="col" id="div_numpermiso" style="display:none">
               <span class="text-orange">•</span><label for="numero_permiso">Número de permiso de comercialización</label>
               <input id="numero_permiso" name="numero_permiso" placeholder="Ingresar Número de permiso de comercialización" class="form-control input-md validate[required, minSize[4], maxSize[30]]" type="text" value="{{ isset($rayosxEquipo) ? $rayosxEquipo->numero_permiso : '' }}"  onkeyup="javascript:this.value=this.value.toUpperCase();" pattern="{4,30}"  minlength="4" maxlength="30" title="Ingresar un Tamaño mínimo: 4 carácteres a un Tamaño máximo: 30 carácteres">
            </div>
         </div>

@if($rayosxEquipo==NULL)
          <div id="btnGuardarEquipos" class="col-md-12 pt-200">
            <p align="center">
               <br/>
               <!-- Primer Collapsible - Localizacion Entidad -->
               <button type="submit" class="btn yellow">
                  Guardar y Continuar
               </button>
            </p>
          </div>
@endif
      </div>
   </div>
</form>

<!-- PRIMERA PARTE DEL REGISTRO DE LA SOLICITUD - LOCALIZACION tipoTramite  -->
<!-- Equipos generadores de radiación ionizante -->
      <form id="formSeccion3-1" type="submit" name="formSeccion3-1" action="{{route('rayosx.guardarOficialTOE')}}" method="post" >
        <div id="paso3-1" class="row block w-100 newsletter collapse">
           <div class="w-100">
             <div class="subtitle">
                 <h3><b>Trabajadores ocupacionalmente expuestos - TOE:</b></h3>
             </div>
             <div class="row">

                <div class="col">
                  {{ csrf_field() }}
                 <h4><b><span class="text-orange">•</span>Oficial de protección radiológica/Encargado de protección Radiológica</b></h4>
                </div>
             </div>

             @if($rayosxOficialToe != NULL)
             <div class="row">
               <div class="alert alert-info">
                 <strong><b>Mensaje Informativo!</b></strong> Esta sección del formulario ya fue guardada. Si desea editar esta sección complete el formulario en todos sus pasos. Y podra editarlo antes de Finalizar el Trámite.
               </div>
             </div>
             @endif

                 <div class="row">

                     <div class="col">
                        <span class="text-orange">•</span><label for="encargado_pnombre">Primer Nombre</label>
                        <input id="id_tramite" name="id_tramite" type="text" value="{{ isset($idTramiterx) ? $idTramiterx->id_tramite : '' }}">
                        <input id="encargado_pnombre" name="encargado_pnombre" placeholder="Ingresar Primer Nombre" class="form-control input-md validate[required, minSize[4], maxSize[30]]" required type="text" value="{{ isset($rayosxOficialToe) ? $rayosxOficialToe->toe_pnombre : '' }}" onkeyup="javascript:this.value=this.value.toUpperCase();" pattern="{4,30}"  minlength="4" maxlength="30" title="Ingresar un Tamaño mínimo: 4 carácteres a un Tamaño máximo: 30 carácteres">
                     </div>

                     <div class="col">
                        <span class="text-orange">•</span><label for="encargado_snombre">Segundo Nombre</label>
                        <input id="encargado_snombre" name="encargado_snombre" placeholder="Ingresar Segundo Nombre" class="form-control input-md validate[minSize[4], maxSize[30]]"  type="text" value="{{ isset($rayosxOficialToe) ? $rayosxOficialToe->toe_snombre : '' }}" onkeyup="javascript:this.value=this.value.toUpperCase();" pattern="{4,30}"  minlength="4" maxlength="30" title="Ingresar un Tamaño mínimo: 4 carácteres a un Tamaño máximo: 30 carácteres">
                     </div>

                     <div class="col">
                        <span class="text-orange">•</span><label for="encargado_papellido">Primer Apellido</label>
                        <input id="encargado_papellido" name="encargado_papellido" placeholder="Ingresar Primer Apellido" class="form-control input-md validate[required, minSize[4], maxSize[30]]" required type="text" value="{{ isset($rayosxOficialToe) ? $rayosxOficialToe->toe_papellido : '' }}" onkeyup="javascript:this.value=this.value.toUpperCase();" pattern="{4,30}"  minlength="4" maxlength="30" title="Ingresar un Tamaño mínimo: 4 carácteres a un Tamaño máximo: 30 carácteres">
                     </div>
                     <div class="col">
                        <span class="text-orange">•</span><label for="encargado_sapellido">Segundo Apellido</label>
                        <input id="encargado_sapellido" name="encargado_sapellido" placeholder="Ingresar Segundo Apellido" class="form-control input-md validate[minSize[4], maxSize[30]]" type="text" value="{{ isset($rayosxOficialToe) ? $rayosxOficialToe->toe_sapellido : '' }}" onkeyup="javascript:this.value=this.value.toUpperCase();" pattern="{4,30}"  minlength="4" maxlength="30" title="Ingresar un Tamaño mínimo: 4 carácteres a un Tamaño máximo: 30 carácteres">
                     </div>
                  </div>

                  <div class="row">
                     <div class="col">
                        <span class="text-orange">•</span><label for="encargado_tdocumento">Tipo Documento</label>
                        <select id="encargado_tdocumento" name="encargado_tdocumento" class="form-control validate[required]" required>
                           <option value=""> - Seleccione Tipo Documento -</option>
                           @foreach ($tiposIdentificacion as $tipoIdentificacion => $value)
                           @if($value!=5)
                           <option value="{{ $value}}"
                           {{(isset($rayosxOficialToe->toe_tdocumento) && $rayosxOficialToe->toe_tdocumento == $value )  ? 'selected' : '' }}>
                              {{ $tipoIdentificacion}}</option>
                            @endif
                           @endforeach
                        </select>
                     </div>

                     <div class="col">
                        <span class="text-orange">•</span><label for="encargado_ndocumento">Número Documento</label>
                        <input id="encargado_ndocumento" name="encargado_ndocumento" placeholder="Ingresar Número Documento" class="form-control input-md validate[required, minSize[4], maxSize[15]]" required type="number" value="{{ isset($rayosxOficialToe) ? $rayosxOficialToe->toe_ndocumento : '' }}" onKeyPress="if(this.value.length==15) return false;">
                     </div>

                     <div class="col">
                        <span class="text-orange">•</span><label for="encargado_lexpedicion">Lugar Expedición</label>
                        <input id="encargado_lexpedicion" name="encargado_lexpedicion" placeholder="Ingresar Lugar Expedición" required class="form-control input-md validate[required, minSize[4], maxSize[30]]" type="text" value="{{ isset($rayosxOficialToe) ? $rayosxOficialToe->toe_lexpedicion : '' }}" onkeyup="javascript:this.value=this.value.toUpperCase();" pattern="{4,30}"  minlength="4" maxlength="30" title="Ingresar un Tamaño mínimo: 4 carácteres a un Tamaño máximo: 30 carácteres">
                     </div>

                  </div>

                  <div class="form-group">
                     <span class="text-orange">•</span><label for="encargado_correo">Correo Electrónico</label>
                     <input id="encargado_correo" name="encargado_correo" placeholder="Ingresar Correo Electrónico" class="form-control validate[required, custom[email]]" type="email" value="{{ isset($rayosxOficialToe) ? $rayosxOficialToe->toe_correo : '' }}" required>
                  </div>

                  <div class="row">
                     <div class="col">
                        <span class="text-orange">•</span><label for="encargado_nivel">Nivel Académico</label>
                        <select id="encargado_nivel" name="encargado_nivel" class="form-control validate[required]" required>
                           <option value="">Seleccione...</option>
                           @foreach ($prNivelacademico as $nivelAcademico => $value)
                            <option value="{{$value}}"
                            {{(isset($rayosxOficialToe->toe_nivel) && $rayosxOficialToe->toe_nivel == $value )  ? 'selected' : '' }}>
                            {{ $nivelAcademico}}
                            </option>
                            @endforeach
                        </select>
                     </div>

                     <div class="col">
                        <span class="text-orange">•</span><label for="encargado_profesion">Profesión</label>
                        <select id="encargado_profesion" name="encargado_profesion" class="form-control validate[required]" required>
                           <option value="">Seleccione...</option>
                           @foreach ($prProgramasUniv as $programaAcademico => $value)
                            <option value="{{$value}}"
                            {{(isset($rayosxOficialToe->toe_profesion) && $rayosxOficialToe->toe_profesion == $value )  ? 'selected' : '' }}>
                            {{ $programaAcademico}}
                            </option>
                            @endforeach
                        </select>
                     </div>
                  </div>

@if($rayosxOficialToe==NULL)
      <div id="btnGuardarOficialTOE" class="col-md-12 pt-200">
         <p align="center">
            <br/>
            <!-- Primer Collapsible - Localizacion Entidad -->
            <button type="submit" class="btn yellow">
               Guardar Encargado TOE
            </button>
         </p>
       </div>
@endif
     </div>
  </div>
  </form>

   <!-- Equipos generadores de radiación ionizante -->
   <form id="formSeccion3-2" type="submit" name="formSeccion3-2" action="{{route('rayosx.guardarTemporalTOE')}}"  method="post" >
   <!-- PRIMERA PARTE DEL REGISTRO DE LA SOLICITUD - LOCALIZACION tipoTramite  -->
      {{ csrf_field() }}

      <div id="paso3-2" class="row block w-100 newsletter collapse">

      <div class="w-100">
             <h4><b><span class="text-orange">•</span>TOE - Trabajadores Ocacionalmente Expuestos</b></h4>

             @if($rayosxTemporalToe == NULL)
             <div class="row">
               <div class="alert alert-warning">
                 <strong><b>Mensaje Informativo!</b></strong> Esta sección del formulario no ha sido ingresada. Favor ingrese por lo menos un Trabajador Ocasionalmente Expuesto.
               </div>
             </div>
             @endif
             @if($rayosxTemporalToe != NULL)
             <div class="row">
               <div class="alert alert-info">
                 <strong><b>Mensaje Informativo!</b></strong> Esta sección del formulario ya fue guardada. Si desea editar esta sección elimine el Trabajador Ocasionalmente Expuesto y crearlo nuevamente.
               </div>
             </div>
             @endif



        <div class="row">
           <div class="col">
             <a id="nuevoTOE" class="btn yellow"> Nuevo Toe</a>
           </div>
         </div>
      <div id="seccion3-2" style="display:none">

      <div class="row">

         <div class="col">
            <span class="text-orange">•</span><label for="encargado_pnombre">Primer Nombre</label>
            <input id="id_tramite" name="id_tramite" type="text" value="{{ isset($idTramiterx) ? $idTramiterx->id_tramite : '' }}">
            <input id="encargado_pnombre" name="encargado_pnombre" placeholder="Ingresar Primer Nombre" class="form-control input-md validate[required, minSize[4], maxSize[30]]"  type="text" onkeyup="javascript:this.value=this.value.toUpperCase();" pattern="{4,30}"  minlength="4" maxlength="30" title="Ingresar un Tamaño mínimo: 4 carácteres a un Tamaño máximo: 30 carácteres">
         </div>

         <div class="col">
            <span class="text-orange">•</span><label for="encargado_snombre">Segundo Nombre</label>
            <input id="encargado_snombre" name="encargado_snombre" placeholder="Ingresar Segundo Nombre" class="form-control input-md validate[required, minSize[4], maxSize[30]]"  type="text" onkeyup="javascript:this.value=this.value.toUpperCase();" pattern="{4,30}"  minlength="4" maxlength="30" title="Ingresar un Tamaño mínimo: 4 carácteres a un Tamaño máximo: 30 carácteres">
         </div>

         <div class="col">
            <span class="text-orange">•</span><label for="encargado_papellido">Primer Apellido</label>
            <input id="encargado_papellido" name="encargado_papellido" placeholder="Ingresar Primer Apellido" class="form-control input-md validate[required, minSize[4], maxSize[30]]"  type="text" onkeyup="javascript:this.value=this.value.toUpperCase();" pattern="{4,30}"  minlength="4" maxlength="30" title="Ingresar un Tamaño mínimo: 4 carácteres a un Tamaño máximo: 30 carácteres">
         </div>
         <div class="col">
            <span class="text-orange">•</span><label for="encargado_sapellido">Segundo Apellido</label>
            <input id="encargado_sapellido" name="encargado_sapellido" placeholder="Ingresar Segundo Apellido" class="form-control input-md validate[required, minSize[4], maxSize[30]]"  type="text" onkeyup="javascript:this.value=this.value.toUpperCase();" pattern="{4,30}"  minlength="4" maxlength="30" title="Ingresar un Tamaño mínimo: 4 carácteres a un Tamaño máximo: 30 carácteres">
         </div>
      </div>

      <div class="row">
         <div class="col">
            <span class="text-orange">•</span><label for="encargado_correo">Correo Electrónico</label>
            <input id="encargado_correo" name="encargado_correo" placeholder="Ingresar Correo Electrónico" class="form-control validate[required, custom[email]]" type="email" required>
         </div>
         <div class="col">
            <span class="text-orange">•</span><label for="encargado_tdocumento">Tipo Documento</label>
            <select id="encargado_tdocumento" name="encargado_tdocumento" class="form-control validate[required]" required>
               <option value=""> - Seleccione Tipo Documento -</option>
               @foreach ($tiposIdentificacion as $tipoIdentificacion => $value)
               @if($value!=5)
               <option value="{{ $value}}"
               {{(isset($rayosxOficialToe->toe_tdocumento) && $rayosxOficialToe->toe_tdocumento == $value )  ? 'selected' : '' }}>
                  {{ $tipoIdentificacion}}</option>
                @endif
               @endforeach
            </select>
         </div>

         <div class="col">
            <span class="text-orange">•</span><label for="encargado_ndocumento">Número Documento</label>
            <input id="encargado_ndocumento" name="encargado_ndocumento" placeholder="Ingresar Número Documento" class="form-control input-md validate[required, minSize[4], maxSize[15]]"  type="number" onKeyPress="if(this.value.length==15) return false;">
         </div>

         <div class="col">
            <span class="text-orange">•</span><label for="encargado_lexpedicion">Lugar Expedición</label>
            <input id="encargado_lexpedicion" name="encargado_lexpedicion" placeholder="Ingresar Lugar Expedición" class="form-control input-md validate[required, minSize[4], maxSize[30]]"  type="text" onkeyup="javascript:this.value=this.value.toUpperCase();" pattern="{4,30}"  minlength="4" maxlength="30" title="Ingresar un Tamaño mínimo: 4 carácteres a un Tamaño máximo: 30 carácteres">
         </div>

      </div>

      <div class="row">

         <div class="col">
            <span class="text-orange">•</span><label for="encargado_nivel">Nivel Académico</label>
            <select id="encargado_nivel" name="encargado_nivel" class="form-control validate[required]" required>
               <option value="">Seleccione...</option>
               @foreach ($prNivelacademico as $nivelAcademico => $value)
                <option value="{{$value}}"> {{ $nivelAcademico}}</option>
                @endforeach
            </select>
         </div>

         <div class="col">
            <span class="text-orange">•</span><label for="encargado_profesion">Profesión</label>
            <select id="encargado_profesion" name="encargado_profesion" class="form-control validate[required]">
               <option value="">Seleccione...</option>
               @foreach ($prProgramasUniv as $programaAcademico => $value)
                <option value="{{$value}}"> {{ $programaAcademico}}</option>
                @endforeach
            </select>
         </div>

      </div>

      <div class="row">
         <div class="col">
            <span class="text-orange">•</span><label for="toe_ult_entrenamiento">Fecha último entrenamiento protección radiológica</label>
            <input id="toe_ult_entrenamiento" name="toe_ult_entrenamiento" placeholder="Ingresar Fecha del último entrenamiento en protección radiológica" class="form-control validate[required] input-md" type="date" required>
         </div>

         <div class="col">
            <span class="text-orange">•</span><label for="toe_pro_entrenamiento">Fecha próximo entrenamiento protección radiológica</label>
            <input id="toe_pro_entrenamiento" name="toe_pro_entrenamiento" placeholder="Ingresar Fecha del próximo entrenamiento en protección radiológica" class="form-control validate[required] input-md" type="date" required>
         </div>

         <div class="col">
            <span class="text-orange">•</span><label for="toe_registro">Número del registro profesional de salud</label>
            <input id="toe_registro" name="toe_registro" placeholder="Ingresar Número del registro profesional de salud" class="form-control input-md validate[required, minSize[4], maxSize[15]]" type="number" onKeyPress="if(this.value.length==15) return false;">
         </div>
      </div>


      <div id="btnGuardarOficialTOE" class="col-md-12 pt-200">
         <p align="center">
            <br/>
            <!-- Primer Collapsible - Localizacion Entidad -->
            <button type="submit" class="btn yellow">
               Guardar Ocacionalmente TOE
            </button>
         </p>
       </div>

   </form>
 </div>

      <table class="display nowrap table table-hover" id="tabla_toe" style="width:100%;">
         <thead>
            <tr>
               <th>ID</th>
               <th>Nombres y Apellidos </th>
               <th>Ver Más</th>
               <th>Eliminar</th>
            </tr>
         </thead>

      <tbody>
      @if(isset($rayosxTemporalToe))
      @forelse($rayosxTemporalToe as $rayoxTemporalToe)
        <tr>

          <td>{{$rayoxTemporalToe->id_toe_rayosx}}</td>
          <td>{{$rayoxTemporalToe->toe_pnombre}} {{$rayoxTemporalToe->toe_snombre}} {{$rayoxTemporalToe->toe_papellido}} {{$rayoxTemporalToe->toe_sapellido}}</td>
          <td align="center"><a href="{{url('/ventanilla/tramites/solicitud_rayosx/tramite/'.$idTramiterx->id_tramite)}}"><i class="fa fa-file"></i></a></td>
          <td align="center">
            <a href="" data-toggle="modal" data-target="#MyModal" data-id="{{$idTramiterx->id}}" href="#addIdModal" class="open-AddIdModal">
              <i class="fa fa-trash"></i>
            </a>
            </td>
        </tr>
      <!-- Conditional Variable Empty Variable listado-->
      @empty
        <tr>
          <td colspan="6" scope="col">No Existen Objetos de Prueba Registrados</td>
        </tr>
      @endforelse
      </tbody></table>
      <!-- Conditional Pagination Bootstrap -->
      <div class="box-footer clearfix">
        @if(count($rayosxObjprueba))
          <div class="mt-2 mx-auto">
            {{ $rayosxObjprueba->links('pagination::bootstrap-4') }}
          </div>
        @endif
      </div>
      @endif
   </div>
</div>





   <!-- PRIMERA PARTE DEL REGISTRO DE LA SOLICITUD - LOCALIZACION tipoTramite  -->
         <!-- Equipos generadores de radiación ionizante -->
         <form id="formSeccion4-1" type="submit" name="formSeccion4-1" action="{{route('rayosx.guardarTalentoHumano')}}" method="post">
            {{ csrf_field() }}
            <div id="paso4-1" class="row block w-100 newsletter collapse">

               <div class="w-100">
                 <div class="subtitle">
                     <h3><b>Talento Humano:</b></h3>
                 </div>
      

                 <!--div class="col">
                     <span class="text-orange">•</span><label for="tipo_titulo">La IPS cuenta con el talento humano estipulado en el articulo 6 y 7, numeral 7.1?</label>
                     <select id="visita_previa" name="visita_previa_th" class="form-control validate[required]">
                        <option value="">Seleccione...</option>
                        <option value="1">SI</option>
                        <option value="2">NO</option>
                     </select>
                  </div-->

                  <h4><b><span class="text-orange">•</span>Director Técnico</b></h4>

         <div class="row">

            <div class="col">
               <span class="text-orange">•</span><label for="talento_pnombre">Primer Nombre</label>
               <input id="id_tramite" name="id_tramite" type="text" value="{{ isset($idTramiterx) ? $idTramiterx->id_tramite : '' }}">
               <input id="talento_pnombre" name="talento_pnombre" placeholder="Ingresar Primer Nombre" class="form-control input-md validate[required, minSize[4], maxSize[30]]" required type="text" value="{{ isset($rayosxTalento) ? $rayosxTalento->talento_pnombre : '' }}" onkeyup="javascript:this.value=this.value.toUpperCase();" pattern="{4,30}"  minlength="4" maxlength="30" title="Ingresar un Tamaño mínimo: 4 carácteres a un Tamaño máximo: 30 carácteres">
            </div>

            <div class="col">
               <span class="text-orange">•</span><label for="talento_snombre">Segundo Nombre</label>
               <input id="talento_snombre" name="talento_snombre" placeholder="Ingresar Segundo Nombre" class="form-control input-md validate[required, minSize[4], maxSize[30]]"  type="text" value="{{ isset($rayosxTalento) ? $rayosxTalento->talento_snombre : '' }}" onkeyup="javascript:this.value=this.value.toUpperCase();" pattern="{4,30}"  minlength="4" maxlength="30" title="Ingresar un Tamaño mínimo: 4 carácteres a un Tamaño máximo: 30 carácteres">
            </div>

            <div class="col">
               <span class="text-orange">•</span><label for="talento_papellido">Primer Apellido</label>
               <input id="talento_papellido" name="talento_papellido" placeholder="Ingresar Primer Apellido" class="form-control input-md validate[required, minSize[4], maxSize[30]]" required  type="text" value="{{ isset($rayosxTalento) ? $rayosxTalento->talento_papellido : '' }}" onkeyup="javascript:this.value=this.value.toUpperCase();" pattern="{4,30}"  minlength="4" maxlength="30" title="Ingresar un Tamaño mínimo: 4 carácteres a un Tamaño máximo: 30 carácteres">
            </div>

            <div class="col">
               <span class="text-orange">•</span><label for="talento_sapellido">Segundo Apellido</label>
               <input id="talento_sapellido" name="talento_sapellido" placeholder="Ingresar Segundo Apellido"  class="form-control input-md validate[required, minSize[4], maxSize[30]]"  type="text" value="{{ isset($rayosxTalento) ? $rayosxTalento->talento_sapellido : '' }}" onkeyup="javascript:this.value=this.value.toUpperCase();" pattern="{4,30}"  minlength="4" maxlength="30" title="Ingresar un Tamaño mínimo: 4 carácteres a un Tamaño máximo: 30 carácteres">
            </div>
         </div>

         <div class="row">

            <div class="col">
               <span class="text-orange">•</span><label for="talento_tdocumento">Tipo Documento</label>
               <select id="talento_tdocumento" name="talento_tdocumento" class="form-control validate[required]" required>
                  <option value=""> - Seleccione Tipo Documento -</option>
                  @foreach ($tiposIdentificacion as $tipoIdentificacion => $value)
                  @if($value!=5)
                  <option value="{{ $value}}"
                  {{(isset($rayosxTalento->talento_tdocumento) && $rayosxTalento->talento_tdocumento == $value )  ? 'selected' : '' }}>
                     {{ $tipoIdentificacion}}</option>
                   @endif
                  @endforeach
                  </select>
            </div>

            <div class="col">
               <span class="text-orange">•</span><label for="talento_ndocumento">Número Documento</label>
               <input id="talento_ndocumento" name="talento_ndocumento" placeholder="Ingresar Número Documento"  class="form-control input-md validate[required, minSize[4], maxSize[15]]" required type="number" value="{{ isset($rayosxTalento) ? $rayosxTalento->talento_ndocumento : '' }}" onKeyPress="if(this.value.length==15) return false;">
            </div>

            <div class="col">
               <span class="text-orange">•</span><label for="talento_lexpedicion">Lugar Expedición</label>
               <input id="talento_lexpedicion" name="talento_lexpedicion" placeholder="Ingresar Lugar Expedición"  class="form-control input-md validate[required, minSize[4], maxSize[30]]" required  type="text" value="{{ isset($rayosxTalento) ? $rayosxTalento->talento_lexpedicion : '' }}" onkeyup="javascript:this.value=this.value.toUpperCase();" pattern="{4,30}"  minlength="4" maxlength="30" title="Ingresar un Tamaño mínimo: 4 carácteres a un Tamaño máximo: 30 carácteres">
            </div>

         </div>

         <div class="col">
            <span class="text-orange">•</span><label for="talento_correo">Correo Electrónico</label>
            <input id="talento_correo" name="talento_correo" placeholder="Ingresar Correo Electrónico" class="form-control validate[required, custom[email]] input-md" type="email" required value="{{ isset($rayosxTalento) ? $rayosxTalento->talento_correo : '' }}" required>
         </div>

         <br/>

         <h4><b><span class="text-orange">•</span>Idoneidad Profesional</b></h4>

         <div class="row">

            <div class="col">
               <span class="text-orange">•</span><label for="talento_titulo">Titulo de pregrado obtenido</label>
               <input id="talento_titulo" name="talento_titulo" placeholder="Titulo de pregrado obtenido"  class="form-control input-md validate[required, minSize[4], maxSize[30]]" required  type="text" value="{{ isset($rayosxTalento) ? $rayosxTalento->talento_titulo : '' }}" onkeyup="javascript:this.value=this.value.toUpperCase();" pattern="{4,30}"  minlength="4" maxlength="30" title="Ingresar un Tamaño mínimo: 4 carácteres a un Tamaño máximo: 30 carácteres">
            </div>

            <div class="col">
               <span class="text-orange">•</span><label for="talento_universidad">Universidad que otorgó el titulo de pregrado</label>
               <input id="talento_universidad" name="talento_universidad" placeholder="Universidad que otorgó el titulo de pregrado"  class="form-control input-md validate[required, minSize[4], maxSize[30]]" required  value="{{ isset($rayosxTalento) ? $rayosxTalento->talento_universidad : '' }}" onkeyup="javascript:this.value=this.value.toUpperCase();" pattern="{4,30}"  minlength="4" maxlength="30" title="Ingresar un Tamaño mínimo: 4 carácteres a un Tamaño máximo: 30 carácteres">
            </div>

            <div class="col">
               <span class="text-orange">•</span><label for="talento_libro">Libro del diploma de pregrado</label>
               <input id="talento_libro" name="talento_libro" placeholder="Libro del diploma de pregrado"  class="form-control input-md validate[required, minSize[4], maxSize[15]]" type="text" value="{{ isset($rayosxTalento) ? $rayosxTalento->talento_libro : '' }}" onkeyup="javascript:this.value=this.value.toUpperCase();" pattern="{4,15}"  minlength="4" maxlength="15" title="Ingresar un Tamaño mínimo: 4 carácteres a un Tamaño máximo: 15 carácteres">
            </div>

         </div>

         <div class="row">

            <div class="col">
               <span class="text-orange">•</span><label for="talento_registro">Registro del diploma de pregrado</label>
               <input id="talento_registro" name="talento_registro" placeholder="Registro del diploma de pregrado"  class="form-control input-md validate[required, minSize[4], maxSize[15]]" required type="text" value="{{ isset($rayosxTalento) ? $rayosxTalento->talento_registro : '' }}" onkeyup="javascript:this.value=this.value.toUpperCase();" pattern="{4,15}"  minlength="4" maxlength="15" title="Ingresar un Tamaño mínimo: 4 carácteres a un Tamaño máximo: 15 carácteres">
            </div>

            <div class="col">
               <span class="text-orange">•</span><label for="talento_fecha_diploma">Fecha diploma de pregrado</label>
               <input id="talento_fecha_diploma" name="talento_fecha_diploma" placeholder="Fecha diploma de pregrado" class="form-control input-md validate[required]" required  type="date" value="{{ isset($rayosxTalento) ? $rayosxTalento->talento_fecha_diploma : '' }}">
            </div>

            <div class="col">
               <span class="text-orange">•</span><label for="talento_resolucion">Resolución convalidación titulo pregrado</label>
               <input id="talento_resolucion" name="talento_resolucion" placeholder="Resolución convalidación titulo pregrado"  class="form-control input-md validate[required, minSize[4], maxSize[30]]" required  type="text" value="{{ isset($rayosxTalento) ? $rayosxTalento->talento_resolucion : '' }}" onkeyup="javascript:this.value=this.value.toUpperCase();" pattern="{4,15}"  minlength="4" maxlength="30" title="Ingresar un Tamaño mínimo: 4 carácteres a un Tamaño máximo: 30 carácteres">
            </div>

         </div>

         <div class="row">

            <div class="col">
               <span class="text-orange">•</span><label for="talento_fecha_convalida">Fecha convalidación titulo de pregrado</label>
               <input id="talento_fecha_convalida" name="talento_fecha_convalida" placeholder="Fecha convalidación titulo de pregrado" class="form-control validate[required] input-md" type="date" required value="{{ isset($rayosxTalento) ? $rayosxTalento->talento_fecha_convalida : '' }}">
            </div>

            <div class="col">
               <span class="text-orange">•</span><label for="talento_nivel">Nivel Académico último posgrado</label>
               <select id="talento_nivel" name="talento_nivel" class="form-control validate[required]" required>
                  <option value="">Seleccione...</option>
                  @foreach ($prNivelacademico as $nivelAcademico => $value)
                   <option value="{{$value}}"
                   {{(isset($rayosxTalento->talento_nivel) && $rayosxTalento->talento_nivel == $value )  ? 'selected' : '' }}>
                   {{ $nivelAcademico}}
                   </option>
                   @endforeach
               </select>
            </div>

            <div class="col">
               <span class="text-orange">•</span><label for="talento_titulo_pos">Título de posgrado obtenido</label>
               <input id="talento_titulo_pos" name="talento_titulo_pos" placeholder="Título de posgrado obtenido"  class="form-control input-md validate[required, minSize[4], maxSize[30]]"  type="text" value="{{ isset($rayosxTalento) ? $rayosxTalento->talento_titulo_pos : '' }}" onkeyup="javascript:this.value=this.value.toUpperCase();" pattern="{4,30}"  minlength="4" maxlength="30" title="Ingresar un Tamaño mínimo: 4 carácteres a un Tamaño máximo: 30 carácteres">
            </div>

         </div>

         <div class="row">

            <div class="col">
               <span class="text-orange">•</span><label for="talento_universidad_pos">Universidad que otorgó el titulo de posgrado</label>
               <input id="talento_universidad_pos" name="talento_universidad_pos" placeholder="Universidad que otorgó el titulo de posgrado"  class="form-control input-md validate[required, minSize[4], maxSize[30]]"  type="text" value="{{ isset($rayosxTalento) ? $rayosxTalento->talento_universidad_pos : '' }}" onkeyup="javascript:this.value=this.value.toUpperCase();" pattern="{4,30}"  minlength="4" maxlength="30" title="Ingresar un Tamaño mínimo: 4 carácteres a un Tamaño máximo: 30 carácteres">
            </div>

            <div class="col">
               <span class="text-orange">•</span><label for="talento_libro_pos">Libro del diploma de posgrado</label>
               <input id="talento_libro_pos" name="talento_libro_pos" placeholder="Libro del diploma de posgrado"class="form-control input-md validate[required, minSize[4], maxSize[15]]"  type="text" value="{{ isset($rayosxTalento) ? $rayosxTalento->talento_libro_pos : '' }}" onkeyup="javascript:this.value=this.value.toUpperCase();" pattern="{4,15}"  minlength="4" maxlength="15" title="Ingresar un Tamaño mínimo: 4 carácteres a un Tamaño máximo: 15 carácteres">
            </div>

            <div class="col">
               <span class="text-orange">•</span><label for="talento_registro_pos">Registro del diploma de posgrado</label>
               <input id="talento_registro_pos" name="talento_registro_pos" placeholder="Registro del diploma de posgrado" class="form-control input-md validate[required, minSize[4], maxSize[15]]"  type="text" value="{{ isset($rayosxTalento) ? $rayosxTalento->talento_registro_pos : '' }}" onkeyup="javascript:this.value=this.value.toUpperCase();" pattern="{4,15}"  minlength="4" maxlength="15" title="Ingresar un Tamaño mínimo: 4 carácteres a un Tamaño máximo: 15 carácteres">
            </div>
         </div>

         <div class="row">

            <div class="col">
               <span class="text-orange">•</span><label for="talento_fecha_diploma_pos">Fecha diploma de posgrado</label>
               <input id="talento_fecha_diploma_pos" name="talento_fecha_diploma_pos" placeholder="Fecha diploma de posgrado" class="form-control validate[required]" type="date" value="{{ isset($rayosxTalento) ? $rayosxTalento->talento_fecha_diploma_pos : '' }}">
            </div>

            <div class="col">
               <span class="text-orange">•</span><label for="talento_resolucion">Resolución convalidación titulo posgrado</label>
               <input id="talento_resolucion_pos" name="talento_resolucion_pos" placeholder="Resolución convalidación titulo posgrado" class="form-control input-md validate[required, minSize[4], maxSize[15]]"  type="text" value="{{ isset($rayosxTalento) ? $rayosxTalento->talento_resolucion_pos : '' }}" onkeyup="javascript:this.value=this.value.toUpperCase();" pattern="{4,15}"  minlength="4" maxlength="15" title="Ingresar un Tamaño mínimo: 4 carácteres a un Tamaño máximo: 15 carácteres">
            </div>

            <div class="col">
               <span class="text-orange">•</span><label for="talento_fecha_convalida_pos">Fecha convalidación titulo de posgrado</label>
               <input id="talento_fecha_convalida_pos" name="talento_fecha_convalida_pos" placeholder="Fecha convalidación titulo de posgrado" class="form-control validate[required] input-md" type="date" value="{{ isset($rayosxTalento) ? $rayosxTalento->talento_fecha_convalida_pos : '' }}">
            </div>
         </div>
@if($rayosxTalento==NULL)
         <div id="btnGuardarDirector" class="col-md-12 pt-200">
            <p align="center">
               <br/>
               <!-- Primer Collapsible - Localizacion Entidad -->
               <button type="submit" class="btn yellow">
                  Guardar Director Técnico
               </button>
            </p>
          </div>
@endif
</div>
</div>
       </form>

         <form id="formSeccion4-2" type="submit" name="formSeccion4-2" action="{{route('rayosx.guardarEquipoPrueba')}}" method="post">
            {{ csrf_field() }}
          <div id="paso4-2" class="row block w-100 newsletter collapse">

          <div class="w-100">
                 <h4><b><span class="text-orange">•</span>Equipos u objetos de prueba</b></h4>
            <div class="row">
               <div class="col">
                 <a id="nuevoEquipo" class="btn yellow"> Agregar Equipo</a>
               </div>
             </div>
          <div id="seccion4-2" style="display:none">
            <div class="row">
               <div class="col">
                  <span class="text-orange">•</span><label for="obj_nombre">Nombre del Equipo:</label>
                  <input id="id_tramite" name="id_tramite" type="text" value="{{ isset($idTramiterx) ? $idTramiterx->id_tramite : '' }}">
                  <input id="obj_nombre" name="obj_nombre" placeholder="Nombre del Equipo" class="form-control input-md validate[required, minSize[4], maxSize[30]]" required  type="text" onkeyup="javascript:this.value=this.value.toUpperCase();" pattern="{4,30}"  minlength="4" maxlength="30" title="Ingresar un Tamaño mínimo: 4 carácteres a un Tamaño máximo: 30 carácteres">
               </div>

               <div class="col">
                  <span class="text-orange">•</span><label for="obj_marca">Marca del equipo</label>
                  <input id="obj_marca" name="obj_marca" placeholder="Marca del equipo" class="form-control input-md validate[required, minSize[4], maxSize[30]]" required type="text" onkeyup="javascript:this.value=this.value.toUpperCase();" pattern="{4,30}"  minlength="4" maxlength="30" title="Ingresar un Tamaño mínimo: 4 carácteres a un Tamaño máximo: 30 carácteres">
               </div>

               <div class="col">
                  <span class="text-orange">•</span><label for="obj_modelo">Modelo del equipo</label>
                  <input id="obj_modelo" name="obj_modelo" placeholder="Modelo del Equipo" class="form-control input-md validate[required, minSize[4], maxSize[30]]" required type="text" onkeyup="javascript:this.value=this.value.toUpperCase();" pattern="{4,30}"  minlength="4" maxlength="30" title="Ingresar un Tamaño mínimo: 4 carácteres a un Tamaño máximo: 30 carácteres">
               </div>
            </div>

            <div class="row">

               <div class="col">
                  <span class="text-orange">•</span><label for="obj_serie">Serie del equipo</label>
                  <input id="obj_serie" name="obj_serie" placeholder="Serie del Equipo" class="form-control input-md validate[required, minSize[4], maxSize[30]]" required type="text" onkeyup="javascript:this.value=this.value.toUpperCase();" pattern="{4,30}"  minlength="4" maxlength="30" title="Ingresar un Tamaño mínimo: 4 carácteres a un Tamaño máximo: 30 carácteres">
               </div>

               <div class="col">
                  <span class="text-orange">•</span><label for="obj_calibracion">Calibración</label>
                  <input id="obj_calibracion" name="obj_calibracion" placeholder="Calibración" class="form-control input-md validate[required, minSize[4], maxSize[30]]" required type="text" onkeyup="javascript:this.value=this.value.toUpperCase();" pattern="{4,30}"  minlength="4" maxlength="30" title="Ingresar un Tamaño mínimo: 4 carácteres a un Tamaño máximo: 30 carácteres">
               </div>

               <div class="col">
                  <span class="text-orange">•</span><label for="obj_vigencia">Vigencia de calibración</label>
                  <select id="obj_vigencia" name="obj_vigencia" class="form-control validate[required]" required>
                     <option value="">Seleccione...</option>
                     <option value="1">Un (1) A&ntilde;o</option>
                     <option value="2">Dos (2) A&ntilde;os</option>
                     <option value="3">Otra, definida por el fabricante</option>
                  </select>
               </div>
            </div>

            <div class="row">

               <div class="col">
                  <span class="text-orange">•</span><label for="obj_fecha">Fecha de calibración</label>
                  <input id="obj_fecha" name="obj_fecha" placeholder="Fecha de la calibracion" class="form-control validate[required] input-md" type="date" required>
               </div>

               <div class="col">
                  <span class="text-orange">•</span><label for="obj_manual">Manual Técnico y ficha Técnica</label>
                  <select id="obj_manual" name="obj_manual" class="form-control validate[required]" required>
                     <option value="">Seleccione...</option>
                     <option value="1">Posee manual técnico</option>
                     <option value="2">Pose ficha técnica</option>
                  </select>
               </div>

               <div class="col">
                  <span class="text-orange">•</span><label for="obj_uso">Usos</label>
                  <textarea id="obj_uso" name="obj_uso" class="form-control validate[required] input-md" required></textarea>
               </div>
            </div>

            <div id="btnGuardarEquipoPrueba" class="col-md-12 pt-200">
               <p align="center">
                  <br/>
                  <!-- Primer Collapsible - Localizacion Entidad -->
                  <button type="submit" class="btn yellow">
                     Guardar Equipo/Objeto Prueba
                  </button>
               </p>
             </div>
         </form>
       </div>


         <table class="display nowrap" id="tabla_equiprueba" style="width:100%">
            <thead>
               <tr>
                  <th>ID</th>
                  <th>Nombre del equipo</th>
                  <th>Marca del equipo</th>
                  <th>Modelo del equipo</th>
                  <th>Ver Más</th>
                  <th>Eliminar</th>

               </tr>
            </thead>

            <tbody>
              @if(isset($rayosxObjprueba))
              @forelse($rayosxObjprueba as $rayoxObjprueba)
                <tr>
                  <td>{{ $rayoxObjprueba->id_obj_rayosx }}</td>
                  <td>{{ $rayoxObjprueba->obj_nombre }}</td>
                  <td>{{ $rayoxObjprueba->obj_marca }}</td>
                  <td>{{ $rayoxObjprueba->obj_modelo }}</td>
                  <td align="center"><a href="{{url('/ventanilla/tramites/solicitud_rayosx/tramite/'.$idTramiterx->id_tramite)}}"><i class="fa fa-file"></i></a></td>
                  <td align="center">
                    <a href="" data-toggle="modal" data-target="#MyModal" data-id="{{$idTramiterx->id}}" href="#addIdModal" class="open-AddIdModal">
                      <i class="fa fa-trash"></i>
                    </a>
                    </td>
                </tr>
              <!-- Conditional Variable Empty Variable listado-->
              @empty
                <tr>
                  <td colspan="6" scope="col">No Existen Objetos de Prueba Registrados</td>
                </tr>
              @endforelse
              </tbody></table>
              <!-- Conditional Pagination Bootstrap -->
              <div class="box-footer clearfix">
                @if(count($rayosxObjprueba))
                  <div class="mt-2 mx-auto">
                    {{ $rayosxObjprueba->links('pagination::bootstrap-4') }}
                  </div>
                @endif
              </div>
              @endif

</div>
</div>



@if((isset($rayosxEquipo->categoria) && $rayosxEquipo->categoria == 1))

<!-- Equipos generadores de radiación ionizante -->
<form id="formSeccion5-1" type="submit" name="formSeccion5" action="{{route('rayosx.guardarDocumentos1')}}" method="post" class="form-row" enctype="multipart/form-data">
   {{ csrf_field() }}
   <!-- PRIMERA PARTE DEL REGISTRO DE LA SOLICITUD - LOCALIZACION tipoTramite  -->
   <div id="paso5-1" class="row block w-100 newsletter collapse ">

     <div class="w-100">
       <div class="subtitle">
           <h3><b>Documentos Adjuntos:</b></h3>
       </div>
       <h4><b><span class="text-orange">•</span>Documentos Categoría I-2</b></h4>

       <table class="table">
           <thead>
              <tr>
                 <th>Descripción</th>
                 <th>Documento</th>
              </tr>
           </thead>
           <tbody>
              <tr>
                 <td>Copia documento identificación del encargado de protección radiológica</td>
                 <td>
                   <input type="hidden" name="id_tramite" value="{{ isset($idTramiterx) ? $idTramiterx->id_tramite : '' }}">
                   <input id="fi_doc_encargado" name="fi_doc_encargado" type="file" class="archivopdf validate[required]" required></td>
              </tr>
              <tr>
                 <td>Copia del diploma del encargado de protección radiológica</td>
                 <td><input id="fi_diploma_encargado" name="fi_diploma_encargado" type="file" class="archivopdf validate[required]" required></td>
              </tr>
              <tr>
                 <td>Descripción de los blindajes estructurales o portátiles y el cálculo del blindaje</td>
                 <td><input id="fi_blindajes" name="fi_blindajes" type="file" class="archivopdf validate[required]" required></td>
              </tr>
              <tr>
                 <td>Informe sobre los resultados del control de calidad</td>
                 <td><input id="fi_control_calidad" name="fi_control_calidad" type="file" class="archivopdf validate[required]" required></td>
              </tr>
              <tr>
                 <td>Registros dosimétricos del último periodo de los trabajadores ocupacionalmente expuestos</td>
                 <td><input id="fi_registro_dosimetrico" name="fi_registro_dosimetrico" type="file" class="archivopdf validate[required]" required></td>
              </tr>
              <tr>
                 <td>Registro del cumplimiento de los niveles de referencia para diagnóstico</td>
                 <td><input id="fi_registro_niveles" name="fi_registro_niveles" type="file" class="archivopdf validate[required]" required></td>
              </tr>
              <tr>
                 <td>Plano general de las instalaciones</td>
                 <td><input id="fi_plano" name="fi_plano" type="file" class="archivopdf validate[required]" required></td>
              </tr>
              <tr>
                 <td>Certificado de la capacitación en protección radiológica de cada trabajador ocupacionalmente expuesto reportado en el formulario</td>
                 <td><input id="fi_certificado_capacitacion" name="fi_certificado_capacitacion" type="file" class="archivopdf validate[required]" required></td>
              </tr>
              <tr>
                 <td>Programa de capacitación en protección radiológica</td>
                 <td><input id="fi_programa_capacitacion" name="fi_programa_capacitacion" type="file" class="archivopdf validate[required]" required></td>
              </tr>
              <tr>
                 <td>Procedimientos de mantenimiento de conformidad a lo establecido por el fabricante</td>
                 <td><input id="fi_procedimiento_mantenimiento" name="fi_procedimiento_mantenimiento" type="file" class="archivopdf validate[required]" required></td>
              </tr>
              <tr>
                 <td>Pruebas iniciales de caracterización de los equipos</td>
                 <td><input id="fi_pruebas_caracterizacion" name="fi_pruebas_caracterizacion" type="file" class="archivopdf validate[required]" required></td>
              </tr>
              <tr>
                 <td>Programa de Tecnovigilancia</td>
                 <td><input id="fi_programa_tecno" name="fi_programa_tecno" type="file" class="archivopdf validate[required]" required></td>
              </tr>
              <tr>
                 <td>Programa de protección radiológica</td>
                 <td><input id="fi_programa_proteccion" name="fi_programa_proteccion" type="file" class="archivopdf validate[required]" required></td>
              </tr>
              <tr>
                 <td>Documentación de soporte de talento humano e infraestructura técnica. En el evento contemplado en el paragrafo 1 del articulo 21</td>
                 <td><input id="fi_soporte_talento" name="fi_soporte_talento" type="file" class="archivopdf validate[required]" required></td>
              </tr>
              <tr>
                 <td>Fotocopia de Diploma de Posgrado del Director Técnico</td>
                 <td><input id="fi_diploma_director" name="fi_diploma_director" type="file" class="archivopdf validate[required]" required></td>
              </tr>
              <tr>
                 <td>Fotocopia de la Resolución de convalidación del t&iacute;tulo ante el Ministerio de Educación Nacional - MEN del Director Técnico  </td>
                 <td><input id="fi_res_convalida_director" name="fi_res_convalida_director" type="file" class="archivopdf validate[required]" required></td>
              </tr>
              <tr>
                 <td>Fotocopia de Diploma de posgrado del (los) profesional(es) que realiza(n) control de calidad</td>
                 <td><input id="fi_diploma_pos_profe" name="fi_diploma_pos_profe" type="file" class="archivopdf validate[required]" required></td>
              </tr>
              <tr>
                 <td>Fotocopia de la Resolución de convalidación del t&iacute;tulo ante el Ministerio de Educación Nacional - MEN del (los) profesional(es) que realiza(n) control de calidad  </td>
                 <td><input id="fi_res_convalida_profe" name="fi_res_convalida_profe" type="file" class="archivopdf validate[required]" required></td>
              </tr>
              <tr>
                 <td>Certificados de calibración con una vigencia superior a seis (6) meses por cada equipo reportado</td>
                 <td><input id="fi_cert_calibracion" name="fi_cert_calibracion" type="file" class="archivopdf validate[required]" required></td>
              </tr>
              <tr>
                 <td>Declaraciones de primera parte por cada objeto de prueba reportado</td>
                 <td><input id="fi_declaraciones" name="fi_declaraciones" type="file" class="archivopdf validate[required]" required></td>
              </tr>
           </tbody>
        </table>
        <div id="btnGuardarDocumentos" class="col-md-12 pt-200">
           <p align="center">
              <br/>
              <!-- Primer Collapsible - Localizacion Entidad -->
              <button type="submit" class="btn yellow">
                 Guardar Documento
              </button>
           </p>
         </div>
     </div>
  </div>
</form>
@endif
@if((isset($rayosxEquipo->categoria) && $rayosxEquipo->categoria == 2))
<!-- Equipos generadores de radiación ionizante -->
<form id="formSeccion5-2" type="submit" name="formSeccion5" action="{{route('rayosx.guardarDocumentos2')}}" method="post" class="form-row" enctype="multipart/form-data">
   {{ csrf_field() }}
   <!-- PRIMERA PARTE DEL REGISTRO DE LA SOLICITUD - LOCALIZACION tipoTramite  -->
   <div id="paso5-2" class="row block w-100 newsletter collapse ">

     <div class="w-100">
       <div class="subtitle">
           <h3><b>Documentos Adjuntos:</b></h3>
       </div>
       <h4><b><span class="text-orange">•</span>Documentos Categoría II</b></h4>

       <table class="table">
           <thead>
              <tr>
                 <th>Descripción</th>
                 <th>Documento</th>
              </tr>
           </thead>
           <tbody>
              <tr>
                 <td>Copia documento identificación del oficial de protección radiológica</td>
                 <td>
                   <input type="hidden" name="id_tramite" value="{{ isset($idTramiterx) ? $idTramiterx->id_tramite : '' }}">
                   <input id="fi_doc_oficial" name="fi_doc_oficial" type="file" class="archivopdf validate[required]" required></td>
              </tr>
              <tr>
                 <td>Copia del diploma del oficial de protección radiológica</td>
                 <td><input id="fi_diploma_oficial" name="fi_diploma_oficial" type="file" class="archivopdf validate[required]" required></td>
              </tr>
              <tr>
                 <td>Descripción de los blindajes estructurales o portátiles y el cálculo del blindaje</td>
                 <td><input id="fi_blindajes" name="fi_blindajes" type="file" class="archivopdf validate[required]" required></td>
              </tr>
              <tr>
                 <td>Informe sobre los resultados del control de calidad</td>
                 <td><input id="fi_control_calidad" name="fi_control_calidad" type="file" class="archivopdf validate[required]" required></td>
              </tr>
              <tr>
                 <td>Registros dosimétricos del último periodo de los trabajadores ocupacionalmente expuestos. Para alta complejidad, registros del segundo dosimetro</td>
                 <td><input id="fi_registro_dosimetrico" name="fi_registro_dosimetrico" type="file" class="archivopdf validate[required]" required></td>
              </tr>
              <tr>
                 <td>Plano general de las instalaciones</td>
                 <td><input id="fi_plano" name="fi_plano" type="file" class="archivopdf validate[required]"></td>
              </tr>
              <tr>
                 <td>Documentación de soporte de talento humano e infraestructura técnica. En el evento contemplado en el paragrafo del articulo 23</td>
                 <td><input id="fi_soporte_talento" name="fi_soporte_talento" type="file" class="archivopdf validate[required]" required></td>
              </tr>
              <tr>
                 <td>Fotocopia de Diploma de Posgrado del Director Técnico</td>
                 <td><input id="fi_diploma_director" name="fi_diploma_director" type="file" class="archivopdf validate[required]" required></td>
              </tr>
              <tr>
                 <td>Fotocopia de la Resolución de convalidación del t&iacute;tulo ante el Ministerio de Educación Nacional - MEN del Director Técnico  </td>
                 <td><input id="fi_res_convalida_director" name="fi_res_convalida_director" type="file" class="archivopdf validate[required]" required></td>
              </tr>
              <tr>
                 <td>Fotocopia de Diploma de posgrado del (los) profesional(es) que realiza(n) control de calidad</td>
                 <td><input id="fi_diploma_pos_profe" name="fi_diploma_pos_profe" type="file" class="archivopdf validate[required]" required></td>
              </tr>
              <tr>
                 <td>Fotocopia de la Resolución de convalidación del t&iacute;tulo ante el Ministerio de Educación Nacional - MEN del (los) profesional(es) que realiza(n) control de calidad  </td>
                 <td><input id="fi_res_convalida_profe" name="fi_res_convalida_profe" type="file" class="archivopdf validate[required]" required></td>
              </tr>
              <tr>
                 <td>Certificados de calibración con una vigencia superior a seis (6) meses por cada equipo reportado</td>
                 <td><input id="fi_cert_calibracion" name="fi_cert_calibracion" type="file" class="archivopdf validate[required]" required></td>
              </tr>
              <tr>
                 <td>Declaraciones de primera parte por cada objeto de prueba reportado</td>
                 <td><input id="fi_declaraciones" name="fi_declaraciones" type="file" class="archivopdf validate[required]" required></td>
              </tr>
           </tbody>
        </table>
        <div id="btnGuardarDocumentos" class="col-md-12 pt-200">
           <p align="center">
              <br/>
              <!-- Primer Collapsible - Localizacion Entidad -->
              <button type="submit" class="btn yellow">
                 Guardar Documento
              </button>
           </p>
         </div>
     </div>
  </div>
</form>

@endif


@endsection

@section ("scripts")

 <script languague="javascript">


 /*$.datepicker.regional['es'] = {
     closeText: 'Cerrar',
     prevText: '<Ant',
     nextText: 'Sig>',
     currentText: 'Hoy',
     monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
     monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
     dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
     dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
     dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
     weekHeader: 'Sm',
     dateFormat: 'dd/mm/yy',
     firstDay: 1,
     isRTL: false,
     yearRange: '-100y:-0d',
     showMonthAfterYear: false,
     yearSuffix: ''
     };
$.datepicker.setDefaults($.datepicker.regional['es']);
*/

 function step1() {
   $( "#paso1" ).collapse('show');
   $( "#paso2" ).collapse('hide');
   $( "#paso3-1" ).collapse('hide');
   $( "#paso3-2" ).collapse('hide');
   $( "#paso4-1" ).collapse('hide');
   $( "#paso4-2" ).collapse('hide');
   $( "#paso5-1" ).collapse('hide');
   $( "#paso5-2" ).collapse('hide');
   $( "#formSeccion1" ).collapse('show');
   $( "#formSeccion2" ).collapse('hide');
   $( "#formSeccion3-1" ).collapse('hide');
   $( "#formSeccion3-2" ).collapse('hide');
   $( "#formSeccion3-3" ).collapse('hide');
   $( "#formSeccion4-1" ).collapse('hide');
   $( "#formSeccion4-2" ).collapse('hide');
   $( "#formSeccion5" ).collapse('hide');
 }

 function step2() {
   $( "#paso1" ).collapse('hide');
   $( "#paso2" ).collapse('show');
   $( "#paso3" ).collapse('hide');
   $( "#paso3-1" ).collapse('hide');
   $( "#paso3-2" ).collapse('hide');
   $( "#paso4-1" ).collapse('hide');
   $( "#paso4-2" ).collapse('hide');
   $( "#paso5-1" ).collapse('hide');
   $( "#paso5-2" ).collapse('hide');
   $( "#formSeccion1" ).collapse('hide');
   $( "#formSeccion2" ).collapse('show');
   $( "#formSeccion3-1" ).collapse('hide');
   $( "#formSeccion3-2" ).collapse('hide');
   $( "#formSeccion3-3" ).collapse('hide');
   $( "#formSeccion4-1" ).collapse('hide');
   $( "#formSeccion4-2" ).collapse('hide');
   $( "#formSeccion5" ).collapse('hide');
 }

 function step3() {
   $( "#paso1" ).collapse('hide');
   $( "#paso2" ).collapse('hide');
   $( "#paso3-1" ).collapse('show');
   $( "#paso3-2" ).collapse('show');
   $( "#paso4-1" ).collapse('hide');
   $( "#paso4-2" ).collapse('hide');
   $( "#paso5-1" ).collapse('hide');
   $( "#paso5-2" ).collapse('hide');
   $( "#formSeccion1" ).collapse('hide');
   $( "#formSeccion2" ).collapse('hide');
   $( "#formSeccion3-1" ).collapse('hide');
   $( "#formSeccion3-2" ).collapse('hide');
   $( "#formSeccion3-3" ).collapse('hide');
   $( "#formSeccion4-1" ).collapse('show');
   $( "#formSeccion4-2" ).collapse('show');
   $( "#formSeccion5" ).collapse('hide');
 }

 function step4() {
   $( "#paso1" ).collapse('hide');
   $( "#paso2" ).collapse('hide');
   $( "#paso3-1" ).collapse('hide');
   $( "#paso3-2" ).collapse('hide');
   $( "#paso4-1" ).collapse('show');
   $( "#paso4-2" ).collapse('show');
   $( "#paso5-1" ).collapse('hide');
   $( "#paso5-2" ).collapse('hide');
   $( "#formSeccion1" ).collapse('hide');
   $( "#formSeccion2" ).collapse('hide');
   $( "#formSeccion3-1" ).collapse('hide');
   $( "#formSeccion3-2" ).collapse('hide');
   $( "#formSeccion3-3" ).collapse('hide');
   $( "#formSeccion4-1" ).collapse('show');
   $( "#formSeccion4-2" ).collapse('show');
   $( "#formSeccion5" ).collapse('hide');
 }

 function step5() {
   $( "#paso1" ).collapse('hide');
   $( "#paso2" ).collapse('hide');
   $( "#paso3-1" ).collapse('hide');
   $( "#paso3-2" ).collapse('hide');
   $( "#paso4-1" ).collapse('hide');
   $( "#paso4-2" ).collapse('hide');
   $( "#paso5-1" ).collapse('show');
   $( "#paso5-2" ).collapse('show');
   $( "#formSeccion1" ).collapse('hide');
   $( "#formSeccion2" ).collapse('hide');
   $( "#formSeccion3-1" ).collapse('hide');
   $( "#formSeccion3-2" ).collapse('hide');
   $( "#formSeccion3-3" ).collapse('hide');
   $( "#formSeccion4-1" ).collapse('show');
   $( "#formSeccion4-2" ).collapse('show');
   $( "#formSeccion5" ).collapse('hide');
 }

 $("#tension_tubo_rx").change(function(){
   this.value = parseFloat(this.value).toFixed(4);
 });

 $("#contiene_tubo_rx").change(function(){
   this.value = parseFloat(this.value).toFixed(4);
 });

 $("#energia_fotones").change(function(){
   this.value = parseFloat(this.value).toFixed(4);
 });

 $("#energia_electrones").change(function(){
   this.value = parseFloat(this.value).toFixed(4);
 });

 $("#carga_trabajo").change(function(){
   this.value = parseFloat(this.value).toFixed(4);
 });



    $(document).ready(function () {

      var $signupForm1 = $( '#formSeccion1' );

      $signupForm1.validationEngine({
         promptPosition : "topRight",
         scroll: false,
         autoHidePrompt: true,
         autoHideDelay: 2000
      });

      $signupForm1.submit(function() {
          var $resultado=$signupForm.validationEngine("validate");

          if ($resultado) {

              return true;
          }
          return false;
      });

      var $signupForm2 = $( '#formSeccion2' );

      $signupForm2.validationEngine({
         promptPosition : "topRight",
         scroll: false,
         autoHidePrompt: true,
         autoHideDelay: 2000
      });

      $signupForm2.submit(function() {
          var $resultado=$signupForm.validationEngine("validate");

          if ($resultado) {

              return true;
          }
          return false;
      });


      var $signupForm3_1 = $( '#formSeccion3' );

      $signupForm3_1.validationEngine({
         promptPosition : "topRight",
         scroll: false,
         autoHidePrompt: true,
         autoHideDelay: 2000
      });

      $signupForm3_1.submit(function() {
          var $resultado=$signupForm.validationEngine("validate");

          if ($resultado) {

              return true;
          }
          return false;
      });

      var $signupForm3_2 = $( '#formSeccion3-2' );

      $signupForm3_2.validationEngine({
         promptPosition : "topRight",
         scroll: false,
         autoHidePrompt: true,
         autoHideDelay: 2000
      });

      $signupForm3_2.submit(function() {
          var $resultado=$signupForm.validationEngine("validate");

          if ($resultado) {

              return true;
          }
          return false;
      });

      var $signupForm4_1 = $( '#formSeccion4-1' );

      $signupForm4_1.validationEngine({
         promptPosition : "topRight",
         scroll: false,
         autoHidePrompt: true,
         autoHideDelay: 2000
      });

      $signupForm4_1.submit(function() {
          var $resultado=$signupForm.validationEngine("validate");
          if ($resultado) {
              return true;
          }
          return false;
      });

      var $signupForm4_2 = $( '#formSeccion4-2' );

      $signupForm4_2.validationEngine({
         promptPosition : "topRight",
         scroll: false,
         autoHidePrompt: true,
         autoHideDelay: 2000
      });

      $signupForm4_2.submit(function() {
          var $resultado=$signupForm.validationEngine("validate");
          if ($resultado) {
              return true;
          }
          return false;
      });


      var $signupForm5_1 = $( '#formSeccion5-1' );

      $signupForm5_1.validationEngine({
         promptPosition : "topRight",
         scroll: false,
         autoHidePrompt: true,
         autoHideDelay: 2000
      });

      $signupForm5_1.submit(function() {
          var $resultado=$signupForm.validationEngine("validate");
          if ($resultado) {
              return true;
          }
          return false;
      });

      var $signupForm5_2 = $( '#formSeccion5-2' );

      $signupForm5_2.validationEngine({
         promptPosition : "topRight",
         scroll: false,
         autoHidePrompt: true,
         autoHideDelay: 2000
      });

      $signupForm5_2.submit(function() {
          var $resultado=$signupForm.validationEngine("validate");
          if ($resultado) {
              return true;
          }
          return false;
      });
/*
      $( "#toe_ult_entrenamiento" ).datepicker({
              changeMonth: true,
              changeYear: true,
              defaultDate: "-0y",
              dateFormat: "yy-mm-dd"
      });

      $( "#toe_pro_entrenamiento" ).datepicker({
              changeMonth: true,
              changeYear: true,
              defaultDate: "-0y",
              dateFormat: "yy-mm-dd"
      });

      $( "#talento_fecha_diploma" ).datepicker({
              changeMonth: true,
              changeYear: true,
              defaultDate: "-0y",
              dateFormat: "yy-mm-dd"
      });

      $( "#talento_fecha_convalida" ).datepicker({
              changeMonth: true,
              changeYear: true,
              defaultDate: "-0y",
              dateFormat: "yy-mm-dd"
      });

      $( "#talento_fecha_diploma_pos" ).datepicker({
              changeMonth: true,
              changeYear: true,
              defaultDate: "-0y",
              dateFormat: "yy-mm-dd"
      });

      $( "#talento_fecha_convalida_pos" ).datepicker({
              changeMonth: true,
              changeYear: true,
              defaultDate: "-0y",
              dateFormat: "yy-mm-dd"
      });
*/
      //Inicial
      $("#btnRegistrarSolicitud").collapse('hide');
      $( "#divPasos" ).collapse('hide');
      $( "#formSeccion1" ).collapse('hide');
      $( "#formSeccion2" ).collapse('hide');
      $( "#formSeccion3-1" ).collapse('hide');
      $( "#formSeccion3-2" ).collapse('hide');
      $( "#formSeccion3-3" ).collapse('hide');
      $( "#formSeccion4-1" ).collapse('hide');
      $( "#formSeccion4-2" ).collapse('hide');
      $( "#formSeccion5" ).collapse('hide');
      $( "#paso3" ).collapse('hide');
      $( "#paso4" ).collapse('hide');

      if($("#anio_fabricacion_equipo").val() <= 2009){
         $("#div_numpermiso").show();
         $("#numero_permiso").attr('required',true);
      }else{
         $("#div_numpermiso").hide();
         $("#numero_permiso").attr('required',false);
      }



      //Controla la validacion de la creacion del tramite
      var tramiteExiste=<?php if(isset($existeTramite)) echo $existeTramite; else echo 0;?>;
      var paso=<?php if(isset($paso)) echo $paso; else echo 0;?>;
      var idTramite=<?php if(isset($idTramite->id)) echo $idTramite->id; else echo 0;?>;
      //En que paso del registro del formulario se encuentra, esta dividido segun las secciones


      var $inputExisteTramite = $('<input/>',{type:'hidden',id:'existeTramite',name:'existeTramite',value:tramiteExiste});

      var $inputIdTramite = $('<input/>',{type:'hidden',id:'idTramite',name:'idTramite',value:idTramite});

      var $inputPaso = $('<input/>',{type:'hidden',id:'paso',name:'paso',value:paso});




      //de accuerdo al paso crea los campos en cada uno
      if(paso==0){
         $inputExisteTramite.appendTo('#formInicial');
         $inputIdTramite.appendTo('#formInicial');
         $inputPaso.appendTo('#formInicial');
      }
      if(tramiteExiste==1){

         $( "#divPasos" ).collapse('show');
         $( "#tipo_tramite" ).prop( "disabled", true );
         $( "#visita_previa" ).prop( "disabled", true );

         if(paso==1){
            $inputExisteTramite.appendTo('#formSeccion1');
            $inputIdTramite.appendTo('#formSeccion1');
            $inputPaso.appendTo('#formSeccion1');

            $( "#formSeccion1" ).collapse('show');
            alert("El tramite número "+$( "#idTramite" ).val()+" ha sido creado exitosamente, los campos necesariós han sido habilitados para su registro. El número del tramite servira para continuar el registro de la solicitud o revisar su proceso");
         }
         if(paso==2){
            $inputExisteTramite.appendTo('#formSeccion2');
            $inputIdTramite.appendTo('#formSeccion2');
            $inputPaso.appendTo('#formSeccion2');

            $( "#formSeccion2" ).collapse('show');
         }

         if(paso==3){
            $inputExisteTramite.appendTo('#formSeccion3-1');
            $inputIdTramite.appendTo('#formSeccion3-1');
            $inputPaso.appendTo('#formSeccion3-1');

            $( "#formSeccion3-1" ).collapse('show');
            $( "#formSeccion3-3" ).collapse('show');
            $( "#paso3" ).collapse('show');

         }

         if(paso==3.5){
            $inputExisteTramite.appendTo('#formSeccion3-1');
            $inputIdTramite.appendTo('#formSeccion3-1');
            $inputPaso.appendTo('#formSeccion3-1');
            $( "#formSeccion3-1" ).collapse('show');
            $( "#formSeccion3-3" ).collapse('show');
            $( "#paso3" ).collapse('show');
            alert("Es obligatorio registrar la información requerida.");
         }

         if(paso==4){
            $inputExisteTramite.appendTo('#formSeccion4-1');
            $inputIdTramite.appendTo('#formSeccion4-1');
            $inputPaso.appendTo('#formSeccion4-1');
            $( "#formSeccion4" ).collapse('show');
            $( "#paso4" ).collapse('show');
         }

         if(paso==4.5){
            $inputExisteTramite.appendTo('#formSeccion4-1');
            $inputIdTramite.appendTo('#formSeccion4-1');
            $inputPaso.appendTo('#formSeccion4-1');
            $( "#formSeccion4" ).collapse('show');
            $( "#paso4" ).collapse('show');
            alert("Es obligatorio registrar la información requerida.");
         }

         if(paso==5){
            $inputExisteTramite.appendTo('#formSeccion5');
            $inputIdTramite.appendTo('#formSeccion5');
            $inputPaso.appendTo('#formSeccion5');
            $( "#formSeccion5" ).collapse('show');
         }



      }else{
         $( "#formInicial" ).collapse('show');
      }


      $("#nuevoTOE").click(function() {
        $( "#formSeccion3-2" ).collapse('show');
        $( "#seccion3-2" ).show( "slow" );

         $inputExisteTramite.appendTo('#formSeccion3-2');
         $inputIdTramite.appendTo('#formSeccion3-2');
         $inputPaso.appendTo('#formSeccion3-2');
      });

      $("#nuevoEquipo").click(function() {
        $( "#formSeccion4-2" ).collapse('show');
        $( "#seccion4-2" ).show( "slow" );
         $inputExisteTramite.appendTo('#formSeccion4-2');
         $inputIdTramite.appendTo('#formSeccion4-2');
         $inputPaso.appendTo('#formSeccion4-2');
      });



      $("#button3-3").click(function() {
         $inputExisteTramite.appendTo('#formSeccion3-3');
         $inputIdTramite.appendTo('#formSeccion3-3');
         $inputPaso.appendTo('#formSeccion3-3');
      });

      $("#button4-3").click(function() {
         $inputExisteTramite.appendTo('#formSeccion4-3');
         $inputIdTramite.appendTo('#formSeccion4-3');
         $inputPaso.appendTo('#formSeccion4-3');
      });


      //Validaciónes para crear el tramite de rayos x
      $("#tipo_tramite").change(function(){

         var tipoTramite=$("#tipo_tramite").val();
         if($("#tipo_tramite").val() == 1 && $("#visita_previa").val() == 1){
            $("#btnRegistrarSolicitud").collapse('show');
         }else{
            $("#btnRegistrarSolicitud").collapse('hide');
            //$(".collapse").collapse('hide');
         }

      });

      $("#visita_previa").change(function(){

         if($("#visita_previa").val() == 1 && $("#tipo_tramite").val() == 1){
            $("#btnRegistrarSolicitud").collapse('show');
         }else{
            $("#btnRegistrarSolicitud").collapse('hide');
            alert("No es posible que realices el registro de la solicitud, comunicate con ______________ para revisar tu caso")
         }
      });


      //Actualiza la lista de equipos de acuerdo  a la entidad seleccionada
      $('select[name="categoria"]').on('change', function(){
        var categoriaId = $(this).val();
        if(categoriaId) {
            $.ajax({
                url: "{{url('/categoriaequipo/get')}}/"+categoriaId,
                type:"GET",
                dataType:"json",
                beforeSend: function(){
                    $('#loader').css("visibility", "visible");
                },

                success:function(data) {

                    $('select[name="categoria1"]').empty();

                    $.each(data, function(key, value){


                        $('select[name="categoria1"]').append('<option value="'+ value +'">' + key+ '</option>');

                    });


                },
                complete: function(){
                    $('#loader').css("visibility", "hidden");
                }
            });
        } else {
            $('select[name="categoria1"]').empty();
        }

    });


    $("#categoria1").change(function(){
       if($("#categoria1").val() == 2){
          $("#div_radperia").show();
          $("#categoria1_1").attr('required',true);
       }else{
          $("#div_radperia").hide();
          $("#categoria1_1").attr('required',false);
       }
    });


    $("#anio_fabricacion_equipo").change(function(){
       if($("#anio_fabricacion_equipo").val() >= 2009){
          $("#div_numpermiso").show();
          $("#numero_permiso").attr('required',true);
       }else{
          $("#div_numpermiso").hide();
          $("#numero_permiso").attr('required',false);
       }
    });


    //Actualiza la lista de municipios de acuerdo  a la entidad seleccionada
    $('select[name="depto_entidad"]').on('change', function(){
      var departamentoId = $(this).val();
      if(departamentoId) {
          $.ajax({
              url: "{{url('/municipio/get')}}/"+departamentoId,
              type:"GET",
              dataType:"json",
              beforeSend: function(){
                  $('#loader').css("visibility", "visible");
              },

              success:function(data) {

                  $('select[name="mpio_entidad"]').empty();

                  $.each(data, function(key, value){

                      $('select[name="mpio_entidad"]').append('<option value="'+ value +'">' + key+ '</option>');

                  });


              },
              complete: function(){
                  $('#loader').css("visibility", "hidden");
              }
          });
      } else {
          $('select[name="mpio_entidad"]').empty();
      }

  });

    });
 </script>

@endsection
