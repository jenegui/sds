<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tramite extends Model
{
    protected $table = 'tramite';

    protected $primaryKey = 'id_tramite';

    protected $fillable = [
        'id_tramite',
        'user',
        'tipo_tramite',
        'estado',
        'created_at',
    ];

}
