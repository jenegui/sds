<?php

namespace App\Models\Rayosx;

use Illuminate\Database\Eloquent\Model;

class Rayosx_equipos extends Model
{
    protected $table = 'rayosx_equipos';

    protected $primaryKey = 'id_equipo_rayosx';

    protected $fillable = [
        'id_tramite_rayosx',
        'categoria',
        'categoria1',
        'categoria1_1',
        'categoria1_2',
        'categoria2',
        'equipo_generador',
        'tipo_visualizacion',
        'marca_equipo',
        'modelo_equipo',
        'serie_equipo',
        'marca_tubo_rx',
        'serie_tubo_rx',
        'tension_tubo_rx',
        'contiene_tubo_rx',
        'energia_fotones',
        'energia_electrones',
        'carga_trabajo',
        'ubicacion_equipo',
        'numero_permiso',
        'anio_fabricacion',
        'anio_fabricacion_tubo',
        'estado',
    ];
}
