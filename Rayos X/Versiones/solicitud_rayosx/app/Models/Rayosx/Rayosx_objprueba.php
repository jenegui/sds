<?php

namespace App\Models\Rayosx;

use Illuminate\Database\Eloquent\Model;

class Rayosx_objprueba extends Model
{
    protected $table = 'rayosx_objprueba';

    protected $primaryKey = 'id_obj_rayosx';

    protected $fillable = [
        'id_tramite_rayosx',
        'obj_nombre',
        'obj_marca',
        'obj_modelo',
        'obj_serie',
        'obj_calibracion',
        'obj_vigencia',
        'obj_fecha',
        'obj_manual',
        'obj_uso',
    ];
}
