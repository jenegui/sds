<html>
<head>
  <title>Certificación de Contratistas - Tramites en Linea . SDS</title>
  <meta charset="utf-8">
  <style type="text/css">

    @page { margin: 180px 50px; }
    #header { position: fixed; left: 0px; top: -150px; right: 0px; height: 0px; text-align: center; }
    #footer { position: fixed; bottom: -60px; left: 0px; right: 0px; height: 50px; text-align: left; font-size:10px;}

    body,td,th {
      font-family: Arial, Helvetica, sans-serif;
    }
    p {
      color: #000000;
      line-height: 1.5em;
      font-size:13px;
    }
    li {
      margin-bottom: 1em;
      color: #000000;
      line-height: 1.5em;
      font-size:13px;
      letter-spacing: 0px;
    }
    body {
      margin-left: 0cm;
      margin-top: 0cm;
      margin-right: 0cm;
      margin-bottom: 0cm;
    }
    i {
      color: #000000;
      padding-right: 1em;
    }
    h1{
      font-size: 36px;
      color: #000000;
    }
    h3{
      font-size: 24px;
      color: #000000;
    }
    span {
      color: #000000;
    }
  </style>
</head>
<body>
<div id="header">
  <img src="./img/memologo.jpg" />
</div>
  <div style="width:100%;">
      <div style="margin-left:45px;margin-right:45px;margin-top:-50px;">
        <p align="center">
            <br><b><em>Dirección de Calidad de Servicios de Salud<br>
              Subdirección Inspección, Vigilancia y Control de Servicios de Salud</em>
                </b>
        </p>
        <p align="center">
            <b><em>Resolución No. {{$tramitesrx->id_tramite}} de {{$tramitesrx->created_at}}<br>
              “Por la cual se Ordena el Archivo de una solicitud de Licencia de Práctica Médica”
              </em></b>
        </p>
        <p aling="center"><b>LA DIRECTORA DE CALIDAD DE SERVICIOS DE SALUD</b><br></p>

        <p align="justify">En uso de sus facultades legales y en especial las que confiere el Decreto Distrital
          507 de 2013 expedido por el Alcalde Mayor de Bogotá y la Resolución 482 de 2018 del Ministerio de Salud y
          Protección Social y</p>
        <p align="center"><b>CONSIDERANDO:</b></p>
        <p align="justify">Que la Resolución 482 del 22 de febrero de 2018, expedida por el Ministerio de Salud y
          Protección Social, por la cual se reglamenta el uso de equipos generadores de radiación ionizante, su control
          de calidad, la prestación de servicios de protección radiológica y se dictan otras disposiciones, establece</p>
        <p align="justify"><em>“….Artículo 26. Trámite de las licencias de prácticas médicas categoría I o II.
          El estudio de la documentación para el otorgamiento de la licencia de prácticas médicas categoría I o II,
          estará sujeto al siguiente procedimiento:</em></p>
        <p align="justify"><em>26.1. Radicada la solicitud en el formato dispuesto en el Anexo No. 3, con los documentos requeridos
          en los artículos 21 o 23, según la categoría, la entidad territorial de salud departamental o distrital, según corresponda,
          procederá a revisarla dentro de los veinte (20) días hábiles siguientes y, de encontrar la documentación incompleta,
          requerirá al solicitante para que la suministre dentro de los veinte (20) días hábiles siguientes.</em></p>
        <p align="justify">Que la Dirección de Calidad de Servicios de Salud, mediante radicado {{$tramitesrx->id_tramite}} de
          {{$tramitesrx->created_at}}, otorgó plazo de 20 días hábiles para allegar los documentos faltantes para continuar
          con el trámite de la Licencia de Práctica Médica Categoría I y Licencia de Práctica Médica Categoría II. Folio (147-148)</p>
        <p align="justify"><b><u>Formato de Solicitud Anexo 3:</u></b></p>
        <p align="justify">En el formato de solicitud no se registra número de permiso de comercialización y año de fabricación
          del equipo, año de fabricación del tubo, datos de la instalación de los equipos, carga de trabajo ubicación del equipo
          dentro de la instalación, en el informe de calidad indican que son 2 trabajadores ocupacionalmente expuestos pero en
          el formato solo relacionan a una persona.</p>
        <p align="justify"><b><u>Anexos:</u></b></p>
        <p align="justify"><b>1. Oficial de Radioprotección</b></p>
        <p align="justify">El oficial de radioprotección designado no se ajusta a lo definido en la resolución 482 de 2018
          artículo 4 definiciones literal 4.13 “Oficial de protección radiológica. Es el profesional que elabora, ejecuta y
          supervisa la óptima aplicación de los principios de protección y seguridad radiológica y actividades de control
          de calidad, con quien debe contar el titular de la licencia de práctica médica categoría II. Este profesional
          deberá contar con el certificado expedido por una institución de educación superior o por una institución de
          Educación para el Trabajo y el Desarrollo Humano en el que se acredite la formación en materia de protección
          radiológica.”</p>
        <p align="justify">En mérito a lo expuesto, este Despacho</p>
        <p align="center"><b>RESUELVE:</b></p>
        <p align="justify"><b>ARTÍCULO PRIMERO:</b> Tener por desistida la solicitud de la renovación de la Licencia de
          Práctica Médica Categoría XX, del (la) Señor(a) {{$tramitesrx->name}}, identificado(a) con XX No: {{$tramitesrx->name}},
          por las razones expuestas en la parte considerativa de esta Resolución.</p>
        <p align="justify"><b>ARTÍCULO SEGUNDO:</b> Ordenar el archivo del trámite de la solicitud de la {{$tramitesrx->id_tramite}}
          de la Licencia de Practica Medica Categoría {{$tramitesrx->categoria}}, del (la) Señor(a) {{$tramitesrx->name}},
          identificado(a) con XX No {{$tramitesrx->name}}, por las razones expuestas en la parte considerativa de esta Resolución.</p>
        <p align="justify"><b>ARTÍCULO TERCERO:</b> Notificar personalmente al (la) Señor(a){{$tramitesrx->name}}, identificado(a)
          con XX No {{$tramitesrx->name}}, quien haga sus veces o a quien delegue, el contenido de la presente Resolución,
          informándole que de conformidad con el artículo 74 del Código de Procedimiento Administrativo y de lo Contencioso
          Administrativo (Ley 1437 de 2011) contra la misma proceden los recursos de reposición y en subsidio apelación,
          los cuales podrá interponer ante ésta Dirección, dentro de los diez (10) días hábiles siguientes a la notificación
          de este acto administrativo</p>
        <p align="center"><b>NOTIFÍQUESE Y CÚMPLASE</b></p>
        <p align="center">Dado en Bogotá, D.C. a los{{$tramitesrx->created_at}}</p>
        <p align="justify"><b>SANDRA PATRICIA CHARRY ROJAS</b></p>
        <p align="justify">Director de Calidad de Servicios de Salud</p>

      </div>
  </div>
  <div id="footer" style="margin-left:45px;margin-right:45px">



  </div>

</body>
</html>
