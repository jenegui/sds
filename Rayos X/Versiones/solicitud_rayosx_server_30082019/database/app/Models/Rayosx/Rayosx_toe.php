<?php
namespace App\Models\Rayosx;

use Illuminate\Database\Eloquent\Model;

class Rayosx_toe extends Model
{

    protected $table = 'rayosx_toe';

    protected $primaryKey = 'id_toe_rayosx';

    protected $fillable = [
        'id_tramite_rayosx',
        'toe_pnombre',
        'toe_snombre',
        'toe_papellido',
        'toe_sapellido',
        'toe_tdocumento',
        'toe_ndocumento',
        'toe_lexpedicion',
        'toe_correo',
        'toe_profesion',
        'toe_nivel',
        'toe_ult_entrenamiento',
        'toe_pro_entrenamiento',
        'toe_registro',
        'toe_tipo',
    ];
}
