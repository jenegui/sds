<?php

namespace App\Http\Controllers;

use PDF;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\DB;
use App\Tramite;
use App\Tipo_Tramite_Flujo;
use App\Tramite_Flujo;
use App\Models\Rayosx\Rayosx_direccion;
use App\Models\Rayosx\Rayosx_director;
use App\Models\Rayosx\Rayosx_documentos;
use App\Models\Rayosx\Rayosx_encargado;
use App\Models\Rayosx\Rayosx_equipos;
use App\Models\Rayosx\Rayosx_objprueba;
use App\Models\Rayosx\Rayosx_toe;
use App\Models\Parametricas\Pr_Tipoidentificacion;

use Illuminate\Http\Request;

class PdfController extends Controller
{
  public function resolucion1($id_tramite){

  $iduser = Auth::user()->id;
  $idTramite = $id_tramite;

  $tramitesrx=Tramite::select('tramite.id_tramite','tipo_tramite.descripcion','users.name','rayosx_equipos.categoria'
  ,'rayosx_equipos.categoria2','rayosx_equipos.categoria1_1','rayosx_equipos.marca_equipo','rayosx_equipos.marca_equipo'
  ,'rayosx_equipos.modelo_equipo','rayosx_equipos.serie_equipo','rayosx_equipos.marca_tubo_rx','rayosx_equipos.modelo_tubo_rx'
  ,'rayosx_equipos.serie_tubo_rx','rayosx_equipos.tension_tubo_rx','rayosx_equipos.contiene_tubo_rx','rayosx_equipos.energia_fotones'
  ,'rayosx_equipos.energia_electrones','rayosx_equipos.carga_trabajo'
  ,'rayosx_direccion.dire_entidad','rayosx_toe.toe_pnombre','rayosx_toe.toe_snombre'
  ,'rayosx_toe.toe_papellido','rayosx_toe.toe_sapellido','rayosx_toe.toe_tdocumento','rayosx_toe.toe_ndocumento'
  ,'rayosx_toe.toe_profesion'
  ,'tramite.estado','tramite.created_at')
  ->join('tipo_tramite','tipo_tramite.id','=','tramite.tipo_tramite')
  ->join('rayosx_direccion','rayosx_direccion.id_tramite_rayosx','=','tramite.id_tramite')
  ->join('rayosx_director','rayosx_director.id_tramite_rayosx','=','tramite.id_tramite')
  ->join('rayosx_documentos','rayosx_documentos.id_tramite_rayosx','=','tramite.id_tramite')
  ->join('rayosx_encargado','rayosx_encargado.id_tramite_rayosx','=','tramite.id_tramite')
  ->join('rayosx_equipos','rayosx_equipos.id_tramite_rayosx','=','tramite.id_tramite')
  ->join('rayosx_objprueba','rayosx_objprueba.id_tramite_rayosx','=','tramite.id_tramite')
  ->join('rayosx_toe','rayosx_toe.id_tramite_rayosx','=','tramite.id_tramite')
  ->join('users','users.id','=','tramite.user')
  ->where('tramite.user', $iduser)
  ->where('tramite.id_tramite', $idTramite)
  ->where('rayosx_toe.toe_tipo', 1)
  ->orderBy('tramite.created_at','DESC')->first();

  if($tramitesrx != NULL){
  $pdf = PDF::loadView('rayosx.pdftramiterxres1', compact('tramitesrx'));
  return $pdf->stream('analisis-intro');
  }
  else{
    return redirect()->route('rayosx.index');
  }

  }

  public function resolucion2($id_tramite){

  $iduser = Auth::user()->id;
  $idTramite = $id_tramite;

  $tramitesrx=Tramite::select('tramite.id_tramite','tipo_tramite.descripcion','users.name','rayosx_equipos.categoria'
  ,'rayosx_equipos.categoria2','rayosx_equipos.categoria1_1','rayosx_equipos.marca_equipo','rayosx_equipos.marca_equipo'
  ,'rayosx_equipos.modelo_equipo','rayosx_equipos.serie_equipo','rayosx_equipos.marca_tubo_rx','rayosx_equipos.modelo_tubo_rx'
  ,'rayosx_equipos.serie_tubo_rx','rayosx_equipos.tension_tubo_rx','rayosx_equipos.contiene_tubo_rx','rayosx_equipos.energia_fotones'
  ,'rayosx_equipos.energia_electrones','rayosx_equipos.carga_trabajo'
  ,'rayosx_direccion.dire_entidad','rayosx_toe.toe_pnombre','rayosx_toe.toe_snombre'
  ,'rayosx_toe.toe_papellido','rayosx_toe.toe_sapellido','rayosx_toe.toe_tdocumento','rayosx_toe.toe_ndocumento'
  ,'rayosx_toe.toe_profesion'
  ,'tramite.estado','tramite.created_at')
  ->join('tipo_tramite','tipo_tramite.id','=','tramite.tipo_tramite')
  ->join('rayosx_direccion','rayosx_direccion.id_tramite_rayosx','=','tramite.id_tramite')
  ->join('rayosx_director','rayosx_director.id_tramite_rayosx','=','tramite.id_tramite')
  ->join('rayosx_documentos','rayosx_documentos.id_tramite_rayosx','=','tramite.id_tramite')
  ->join('rayosx_encargado','rayosx_encargado.id_tramite_rayosx','=','tramite.id_tramite')
  ->join('rayosx_equipos','rayosx_equipos.id_tramite_rayosx','=','tramite.id_tramite')
  ->join('rayosx_objprueba','rayosx_objprueba.id_tramite_rayosx','=','tramite.id_tramite')
  ->join('rayosx_toe','rayosx_toe.id_tramite_rayosx','=','tramite.id_tramite')
  ->join('users','users.id','=','tramite.user')
  ->where('tramite.user', $iduser)
  ->where('tramite.id_tramite', $idTramite)
  ->where('rayosx_toe.toe_tipo', 1)
  ->orderBy('tramite.created_at','DESC')->first();

  if($tramitesrx != NULL){
  $pdf = PDF::loadView('rayosx.pdftramiterxres2', compact('tramitesrx'));
  return $pdf->stream('analisis-intro');
  }
  else{
    return redirect()->route('rayosx.index');
  }

  }

  public function resolucion3($id_tramite){

  $iduser = Auth::user()->id;
  $idTramite = $id_tramite;

  $tramitesrx=Tramite::select('tramite.id_tramite','tipo_tramite.descripcion','users.name','rayosx_equipos.categoria'
  ,'rayosx_equipos.categoria2','rayosx_equipos.categoria1_1','rayosx_equipos.marca_equipo','rayosx_equipos.marca_equipo'
  ,'rayosx_equipos.modelo_equipo','rayosx_equipos.serie_equipo','rayosx_equipos.marca_tubo_rx','rayosx_equipos.modelo_tubo_rx'
  ,'rayosx_equipos.serie_tubo_rx','rayosx_equipos.tension_tubo_rx','rayosx_equipos.contiene_tubo_rx','rayosx_equipos.energia_fotones'
  ,'rayosx_equipos.energia_electrones','rayosx_equipos.carga_trabajo'
  ,'rayosx_direccion.dire_entidad','rayosx_toe.toe_pnombre','rayosx_toe.toe_snombre'
  ,'rayosx_toe.toe_papellido','rayosx_toe.toe_sapellido','rayosx_toe.toe_tdocumento','rayosx_toe.toe_ndocumento'
  ,'rayosx_toe.toe_profesion'
  ,'tramite.estado','tramite.created_at')
  ->join('tipo_tramite','tipo_tramite.id','=','tramite.tipo_tramite')
  ->join('rayosx_direccion','rayosx_direccion.id_tramite_rayosx','=','tramite.id_tramite')
  ->join('rayosx_director','rayosx_director.id_tramite_rayosx','=','tramite.id_tramite')
  ->join('rayosx_documentos','rayosx_documentos.id_tramite_rayosx','=','tramite.id_tramite')
  ->join('rayosx_encargado','rayosx_encargado.id_tramite_rayosx','=','tramite.id_tramite')
  ->join('rayosx_equipos','rayosx_equipos.id_tramite_rayosx','=','tramite.id_tramite')
  ->join('rayosx_objprueba','rayosx_objprueba.id_tramite_rayosx','=','tramite.id_tramite')
  ->join('rayosx_toe','rayosx_toe.id_tramite_rayosx','=','tramite.id_tramite')
  ->join('users','users.id','=','tramite.user')
  ->where('tramite.user', $iduser)
  ->where('tramite.id_tramite', $idTramite)
  ->where('rayosx_toe.toe_tipo', 1)
  ->orderBy('tramite.created_at','DESC')->first();

  if($tramitesrx != NULL){
  $pdf = PDF::loadView('rayosx.pdftramiterxres3', compact('tramitesrx'));
  return $pdf->stream('analisis-intro');
  }
  else{
    return redirect()->route('rayosx.index');
  }

  }
}
