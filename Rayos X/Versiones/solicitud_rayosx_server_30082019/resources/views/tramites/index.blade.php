@extends('layouts.layout')

@section('content')

            <!-- Acceso a la creacion de nuevos tramites -->
            <div class="row subsections pasos my-5" id="proceso">
              <ol class="w-100">
                  <li class="active text-center">
                      <a href="{{ route('rayosx.index') }}">
                      <div class="text-big">{{ __('Licencia Rayos X') }}</div>
                      <div class="line"></div>
                      <div>{{ __('Solicita una nueva licencia de rayos x') }} </div>
                      </a>
                  </li>
                  <li class="active text-center">
                      <a href="#paso2">
                      <div class="text-big">{{ __('Titulos') }}</div>
                      <div class="line"></div>
                      <div>{{ __('texto por definir ') }}</div>
                      </a>
                  </li>
                  <li class="active text-center">
                      <a href="#paso2">
                      <div class="text-big">{{ __('Exhumacion') }}</div>
                      <div class="line"></div>
                      <div>{{ __('texto por definir ') }}</div>
                      </a>
                  </li>
                  <li class="active text-center">
                      <a href="#paso2">
                      <div class="text-big">{{ __(' Expendedor de drogas
                      ') }}</div>
                      <div class="line"></div>
                      <div>{{ __('texto por definir ') }}</div>
                      </a>
                  </li>
              </ol>
          </div>


            <table id="mytable" class="table table-bordred table-striped">
             <thead>
               <th>Id Solicitud</th>
               <th>Tipo Tramite</th>
               <th>Fecha de Creacion</th>
               <th>Fecha Edicion</th>
               <th>Estado</th>
               <th>Descripción Tarea</th>
               <th>Editar</th>
               <th>Eliminar</th>
             </thead>
             <tbody>
              @if($tramites->count())
              @foreach($tramites as $tramite)
              <tr>
                <td>{{$tramite->id_tramite}}</td>
                <td>{{$tramite->user}}</td>
                <td>{{$tramite->tipo_tramite}}</td>
                <td>{{$tramite->estado}}</td>
                <td>{{$tramite->created_at}}</td>
                <td>{{$tramite->updated_at}}</td>

               </tr>
               @endforeach
               @else
               <tr>
                <td colspan="8">0 Tramites Encontrados</td>
              </tr>
              @endif
            </tbody>

          </table>
      {{ $tramites->links() }}
</section>

@endsection
