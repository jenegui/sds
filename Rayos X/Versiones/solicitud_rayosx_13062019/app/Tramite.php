<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tramite extends Model
{
    protected $table = 'tramite';

    protected $fillable = [
        'user',
        'tipo_tramite',
        'estado',
    ];

}
