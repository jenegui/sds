@extends('layouts.layout')

@section('title', '| Editar Privilegios')

@section('content')


    {{-- @include ('errors.list') --}}

    <h1><i class='fa fa-key'></i> Editar {{$permission->name}}</h1>
    <br>
    {{ Form::model($permission, array('route' => array('permissions.update', $permission->id), 'method' => 'PUT')) }}

    <div class="form-group">
        {{ Form::label('name', 'Nombre Privilegio') }}
        {{ Form::text('name', null, array('class' => 'form-control')) }}
    </div>
    <br>
    {{ Form::submit('Editar', array('class' => 'btn btn-primary')) }}

    {{ Form::close() }}


@endsection