@extends('layouts.layout')

@section('content')


<h2 class="text-blue"><b>Registro y autorización de licencias de Rayos X </b></h2>


<div id="divPasos" class="text-center">
   <div class="registro_triada">
      <button class="btn yellow" onClick="step1();">1 - Localizacíon Entidad</button>
      <i class="fa fa-chevron-right"></i>
      <button class="btn yellow" onClick="step2();">2 - Equipos Rayos x</button>
      <i class="fa fa-chevron-right"></i>
      <button class="btn yellow" onClick="step3();">3 - Trabajadores TOE</button></a>
      <i class="fa fa-chevron-right"></i>
      <a href="/algo" target="_blank"><button class="btn yellow">4 - Talento Humano</button></a>
      <i class="fa fa-chevron-right"></i>
      <a href="/algo" target="_blank"><button class="btn yellow">5 - Documentos Adjuntos</button></a>
   </div>
</div>
<!-- localizacion Identidad -->
<form id="formSeccion1" type="submit" name="form_tramite" action="{{ url('/tramite/editardireccion2/') }}" method="post" class="form-row">
   {{ csrf_field() }}

   <!-- PRIMERA PARTE DEL REGISTRO DE LA SOLICITUD - LOCALIZACION tipoTramite  -->
   <div id="paso1" class="row block w-100 newsletter ">

      <div class="w-100">
        <div class="subtitle">
            <h3><b>Localizacíon Entidad:</b></h3>
        </div>

        <div class="row">
            <div class="col">
              <form name="prueba" action="{{ route('rayosx.prueba')}}" method="post">
              {{ csrf_field() }}
              <input type="text" value="1">
              <button type="submit">Enviar</button>
              </form>
   </div>
</div>

@endsection

@section ("scripts")

 <script languague="javascript">

 function step1() {
   alert("Mario");
   $( "#paso1" ).collapse('show');
   $( "#paso2" ).collapse('hide');
   $( "#paso3" ).collapse('hide');
   $( "#formSeccion1" ).collapse('show');
   $( "#formSeccion2" ).collapse('hide');
   $( "#formSeccion3-1" ).collapse('hide');
   $( "#formSeccion3-2" ).collapse('hide');
   $( "#formSeccion3-3" ).collapse('hide');
   $( "#formSeccion4-1" ).collapse('hide');
   $( "#formSeccion4-2" ).collapse('hide');
   $( "#formSeccion5" ).collapse('hide');
 }

 function step2() {
   alert("Edwin");
   $( "#paso1" ).collapse('hide');
   $( "#paso2" ).collapse('show');
   $( "#paso3" ).collapse('hide');
   $( "#formSeccion1" ).collapse('hide');
   $( "#formSeccion2" ).collapse('show');
   $( "#formSeccion3-1" ).collapse('hide');
   $( "#formSeccion3-2" ).collapse('hide');
   $( "#formSeccion3-3" ).collapse('hide');
   $( "#formSeccion4-1" ).collapse('hide');
   $( "#formSeccion4-2" ).collapse('hide');
   $( "#formSeccion5" ).collapse('hide');
 }

 function step3() {
   alert("Edwin");
   $( "#paso1" ).collapse('hide');
   $( "#paso2" ).collapse('hide');
   $( "#paso3" ).collapse('show');
   $( "#formSeccion1" ).collapse('hide');
   $( "#formSeccion2" ).collapse('hide');
   $( "#formSeccion3-1" ).collapse('show');
   $( "#formSeccion3-2" ).collapse('show');
   $( "#formSeccion3-3" ).collapse('show');
   $( "#formSeccion4-1" ).collapse('hide');
   $( "#formSeccion4-2" ).collapse('hide');
   $( "#formSeccion5" ).collapse('hide');
 }
    $(document).ready(function () {

      //Inicial
      $("#btnRegistrarSolicitud").collapse('hide');
      $( "#divPasos" ).collapse('hide');
      $( "#formSeccion1" ).collapse('hide');
      $( "#formSeccion2" ).collapse('hide');
      $( "#formSeccion3-1" ).collapse('hide');
      $( "#formSeccion3-2" ).collapse('hide');
      $( "#formSeccion3-3" ).collapse('hide');
      $( "#formSeccion4-1" ).collapse('hide');
      $( "#formSeccion4-2" ).collapse('hide');
      $( "#formSeccion5" ).collapse('hide');
      $( "#paso3" ).collapse('hide');
      $( "#paso4" ).collapse('hide');

      if($("#anio_fabricacion_equipo").val() <= 2009){
         $("#div_numpermiso").show();
         $("#numero_permiso").attr('required',true);
      }else{
         $("#div_numpermiso").hide();
         $("#numero_permiso").attr('required',false);
      }



      //Controla la validacion de la creacion del tramite
      var tramiteExiste=<?php if(isset($existeTramite)) echo $existeTramite; else echo 0;?>;
      var paso=<?php if(isset($paso)) echo $paso; else echo 0;?>;
      var idTramite=<?php if(isset($idTramite->id)) echo $idTramite->id; else echo 0;?>;
      //En que paso del registro del formulario se encuentra, esta dividido segun las secciones


      var $inputExisteTramite = $('<input/>',{type:'hidden',id:'existeTramite',name:'existeTramite',value:tramiteExiste});

      var $inputIdTramite = $('<input/>',{type:'hidden',id:'idTramite',name:'idTramite',value:idTramite});

      var $inputPaso = $('<input/>',{type:'hidden',id:'paso',name:'paso',value:paso});




      //de accuerdo al paso crea los campos en cada uno
      if(paso==0){
         $inputExisteTramite.appendTo('#formInicial');
         $inputIdTramite.appendTo('#formInicial');
         $inputPaso.appendTo('#formInicial');
      }
      if(tramiteExiste==1){

         $( "#divPasos" ).collapse('show');
         $( "#tipo_tramite" ).prop( "disabled", true );
         $( "#visita_previa" ).prop( "disabled", true );

         if(paso==1){
            $inputExisteTramite.appendTo('#formSeccion1');
            $inputIdTramite.appendTo('#formSeccion1');
            $inputPaso.appendTo('#formSeccion1');

            $( "#formSeccion1" ).collapse('show');
            alert("El tramite número "+$( "#idTramite" ).val()+" ha sido creado exitosamente, los campos necesariós han sido habilitados para su registro. El número del tramite servira para continuar el registro de la solicitud o revisar su proceso");
         }
         if(paso==2){
            $inputExisteTramite.appendTo('#formSeccion2');
            $inputIdTramite.appendTo('#formSeccion2');
            $inputPaso.appendTo('#formSeccion2');

            $( "#formSeccion2" ).collapse('show');
         }

         if(paso==3){
            $inputExisteTramite.appendTo('#formSeccion3-1');
            $inputIdTramite.appendTo('#formSeccion3-1');
            $inputPaso.appendTo('#formSeccion3-1');

            $( "#formSeccion3-1" ).collapse('show');
            $( "#formSeccion3-3" ).collapse('show');
            $( "#paso3" ).collapse('show');

         }

         if(paso==3.5){
            $inputExisteTramite.appendTo('#formSeccion3-1');
            $inputIdTramite.appendTo('#formSeccion3-1');
            $inputPaso.appendTo('#formSeccion3-1');
            $( "#formSeccion3-1" ).collapse('show');
            $( "#formSeccion3-3" ).collapse('show');
            $( "#paso3" ).collapse('show');
            alert("Es obligatorio registrar la información requerida.");
         }

         if(paso==4){
            $inputExisteTramite.appendTo('#formSeccion4-1');
            $inputIdTramite.appendTo('#formSeccion4-1');
            $inputPaso.appendTo('#formSeccion4-1');
            $( "#formSeccion4" ).collapse('show');
            $( "#paso4" ).collapse('show');
         }

         if(paso==4.5){
            $inputExisteTramite.appendTo('#formSeccion4-1');
            $inputIdTramite.appendTo('#formSeccion4-1');
            $inputPaso.appendTo('#formSeccion4-1');
            $( "#formSeccion4" ).collapse('show');
            $( "#paso4" ).collapse('show');
            alert("Es obligatorio registrar la información requerida.");
         }

         if(paso==5){
            $inputExisteTramite.appendTo('#formSeccion5');
            $inputIdTramite.appendTo('#formSeccion5');
            $inputPaso.appendTo('#formSeccion5');
            $( "#formSeccion5" ).collapse('show');
         }



      }else{
         $( "#formInicial" ).collapse('show');
      }


      $("#nuevoTOE").click(function() {
        $( "#formSeccion3-2" ).collapse('show');
         $inputExisteTramite.appendTo('#formSeccion3-2');
         $inputIdTramite.appendTo('#formSeccion3-2');
         $inputPaso.appendTo('#formSeccion3-2');
      });

      $("#nuevoEquipo").click(function() {
        $( "#formSeccion4-2" ).collapse('show');
         $inputExisteTramite.appendTo('#formSeccion4-2');
         $inputIdTramite.appendTo('#formSeccion4-2');
         $inputPaso.appendTo('#formSeccion4-2');
      });


      $("#button3-3").click(function() {
         $inputExisteTramite.appendTo('#formSeccion3-3');
         $inputIdTramite.appendTo('#formSeccion3-3');
         $inputPaso.appendTo('#formSeccion3-3');
      });

      $("#button4-3").click(function() {
         $inputExisteTramite.appendTo('#formSeccion4-3');
         $inputIdTramite.appendTo('#formSeccion4-3');
         $inputPaso.appendTo('#formSeccion4-3');
      });


      //Validaciónes para crear el tramite de rayos x
      $("#tipo_tramite").change(function(){

         var tipoTramite=$("#tipo_tramite").val();
         if($("#tipo_tramite").val() == 1 && $("#visita_previa").val() == 1){
            $("#btnRegistrarSolicitud").collapse('show');
         }else{
            $("#btnRegistrarSolicitud").collapse('hide');
            //$(".collapse").collapse('hide');
         }

      });

      $("#visita_previa").change(function(){

         if($("#visita_previa").val() == 1 && $("#tipo_tramite").val() == 1){
            $("#btnRegistrarSolicitud").collapse('show');
         }else{
            $("#btnRegistrarSolicitud").collapse('hide');
            alert("No es posible que realices el registro de la solicitud, comunicate con ______________ para revisar tu caso")
         }
      });


      //Actualiza la lista de equipos de acuerdo  a la entidad seleccionada
      $('select[name="categoria"]').on('change', function(){
        var categoriaId = $(this).val();
        if(categoriaId) {
            $.ajax({
                url: "{{url('/categoriaequipo/get')}}/"+categoriaId,
                type:"GET",
                dataType:"json",
                beforeSend: function(){
                    $('#loader').css("visibility", "visible");
                },

                success:function(data) {

                    $('select[name="categoria1"]').empty();

                    $.each(data, function(key, value){


                        $('select[name="categoria1"]').append('<option value="'+ value +'">' + key+ '</option>');

                    });


                },
                complete: function(){
                    $('#loader').css("visibility", "hidden");
                }
            });
        } else {
            $('select[name="categoria1"]').empty();
        }

    });


    $("#categoria1").change(function(){
       if($("#categoria1").val() == 2){
          $("#div_radperia").show();
          $("#categoria1_1").attr('required',true);
       }else{
          $("#div_radperia").hide();
          $("#categoria1_1").attr('required',false);
       }
    });


    $("#anio_fabricacion_equipo").change(function(){
       if($("#anio_fabricacion_equipo").val() >= 2009){
          $("#div_numpermiso").show();
          $("#numero_permiso").attr('required',true);
       }else{
          $("#div_numpermiso").hide();
          $("#numero_permiso").attr('required',false);
       }
    });


    //Actualiza la lista de municipios de acuerdo  a la entidad seleccionada
    $('select[name="depto_entidad"]').on('change', function(){
      var departamentoId = $(this).val();
      if(departamentoId) {
          $.ajax({
              url: "{{url('/municipio/get')}}/"+departamentoId,
              type:"GET",
              dataType:"json",
              beforeSend: function(){
                  $('#loader').css("visibility", "visible");
              },

              success:function(data) {

                  $('select[name="mpio_entidad"]').empty();

                  $.each(data, function(key, value){

                      $('select[name="mpio_entidad"]').append('<option value="'+ value +'">' + key+ '</option>');

                  });


              },
              complete: function(){
                  $('#loader').css("visibility", "hidden");
              }
          });
      } else {
          $('select[name="mpio_entidad"]').empty();
      }

  });

    });
 </script>

@endsection
