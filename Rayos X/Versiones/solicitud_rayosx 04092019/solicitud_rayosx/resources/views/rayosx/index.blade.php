@extends('layouts.layout')

@section('content')


<h2 class="text-blue"><b>Registro y autorización de licencias de Rayos X </b></h2>
@if($idTramiterx==NULL)
<form class="form-inline" id="formInicial" type="submit" name="form_tramite" action="{{ route('rayosx.guardarTramiteRayosx') }}" method="post">
   {{ csrf_field() }}

   <div class="line"></div>

   <!-- PRIMER PASO CREACION DEL TRAMITE Y ASIGNACION DEL PRIMER ESTADO -->
   <div id="paso0" class="row block w-100 newsletter">
      <div class="w-100">
         <div class="subtitle">
            <h3><b>Registro de Información - Crear Trámite Licencia Rayos RX</b></h3>
         </div>

         <div class="form-group ">
            <span class="text-orange"  >•</span><label for="tipo_tramite">Tipo de trámite</label>
            <select id="tipo_tramite" name="tipo_tramite" class="form-control validate[required]">
               <option value="0" @if(isset($tipoTramite)) if($tipoTramite == 0) {{"selected=selected"}} @endif>Seleccione...</option>
               <option value="1" @if(isset($tipoTramite)) if($tipoTramite == 1) {{"selected=selected"}} @endif>Nuevo</option>
               <option value="2" @if(isset($tipoTramite)) if($tipoTramite == 2) {{"selected=selected"}} @endif>Renovación</option>
            </select>
         </div>

      <div class="form-group">
         <span class="text-orange">• </span><label for="visita_previa">¿La IPS cuenta con el talento humano estipulado en el articulo 6 y 7, numeral 7.1?</label> <br/>

         <select id="visita_previa" name="visita_previa" class="form-control validate[required]">
            <option value="0" @if(isset($visitaPrevia)) if($visitaPrevia == 0) {{"selected=selected"}} @endif>Seleccione...</option>
            <option value="1" @if(isset($visitaPrevia)) if($visitaPrevia == 1) {{"selected=selected"}} @endif>SI</option>
            <option value="2" @if(isset($visitaPrevia)) if($visitaPrevia == 2) {{"selected=selected"}} @endif>NO</option>
         </select>
      </div>
       <div id="btnRegistrarSolicitud" class="col-md-12 pt-200 collapse">
         <p align="center">
            <br/>
            <!-- Primer Collapsible - Localizacion Entidad -->
            <button type="submit" class="btn yellow">
               Crear Solicitud
            </button>
         </p>
       </div>
    </div>
   </div>
</form>
@endif

@endsection

@section ("scripts")

 <script languague="javascript">
  $(document).ready(function () {
        //Validaciónes para crear el tramite de rayos x
        $("#tipo_tramite").change(function(){          

           var tipoTramite=$("#tipo_tramite").val();
           if($("#tipo_tramite").val() == 1 && $("#visita_previa").val() == 1){
              $("#btnRegistrarSolicitud").collapse('show');
              alert("este")
           }else{
              $("#btnRegistrarSolicitud").collapse('hide');
           }

        });

        $("#visita_previa").change(function(){

           if($("#visita_previa").val() == 1 && $("#tipo_tramite").val() == 1){
              $("#btnRegistrarSolicitud").collapse('show');
           }else{
              $("#btnRegistrarSolicitud").collapse('hide');
              alert("No es posible que realices el registro de la solicitud, comunicate con ______________ para revisar tu caso")
           }
        });

  });
 </script>

@endsection
