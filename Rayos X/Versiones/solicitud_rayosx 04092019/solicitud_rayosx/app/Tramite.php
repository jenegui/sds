<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tramite extends Model
{
    protected $table = 'tramite';

    protected $primaryKey = 'id';

    protected $fillable = [
        'id',
        'user',
        'tipo_tramite',
        'estado',
        'created_at',
    ];

}
