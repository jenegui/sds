<?php

namespace App\Models\Rayosx;

use Illuminate\Database\Eloquent\Model;

class Rayosx_direccion extends Model
{
    protected $table = 'rayosx_direccion';

    protected $primaryKey = 'id_direccion_rayosx';

    protected $fillable = [
        'id_direccion_rayosx',
        'id_tramite_rayosx',
        'dire_entidad',
        'sede_entidad',
        'sede_entidad',
        'email_entidad',
        'depto_entidad',
        'mpio_entidad',
        'celular_entidad',
        'indicativo_entidad',
        'telefono_entidad',
        'extension_entidad',
        'created_at',
    ];
}
