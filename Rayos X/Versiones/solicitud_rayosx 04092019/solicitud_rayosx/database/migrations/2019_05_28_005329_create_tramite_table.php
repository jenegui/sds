<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTramiteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('tipo_tramite')){
            Schema::create('tipo_tramite', function (Blueprint $table) {            
                $table->increments('id');
                $table->string('descripcion');
                $table->boolean('estado');
                $table->timestamps();
            });
        }

        //Tramite almacena los tramites asociados a un usuario
        if(!Schema::hasTable('tramite')){
            Schema::create('tramite', function (Blueprint $table) {
                $table->bigIncrements('id');            
                $table->unsignedBigInteger('user');
                $table->unsignedInteger('tipo_tramite');
                $table->boolean('estado');
                $table->timestamps();
                
                $table->foreign('user')
                    ->references('id')
                    ->on('users')
                    ->onDelete('cascade');

                $table->foreign('tipo_tramite')
                    ->references('id')
                    ->on('tipo_tramite')
                    ->onDelete('cascade');

            });
        }

        //tramite_flujo almacena el flujo o historia
        if(!Schema::hasTable('tramite_flujo')){
            Schema::create('tramite_flujo', function (Blueprint $table) {
                $table->bigIncrements('id');            
                $table->unsignedBigInteger('tramite_id');
                $table->string('actividad',100);                
                $table->dateTime('fecha_inicio');
                $table->dateTime('fecha_fin')->default(null);
                $table->unsignedBigInteger('user_id');
                $table->unsignedInteger('tipo_tramite');
                $table->text('observaciones');                
                $table->timestamps();
                
                $table->foreign('tramite_id')
                    ->references('id')
                    ->on('tramite');

                $table->foreign('tipo_tramite')
                    ->references('id')
                    ->on('tipo_tramite');

                $table->foreign('user_id')
                    ->references('id')
                    ->on('users')
                    ->onDelete('cascade');

            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tramite');
        Schema::dropIfExists('tramite_flujo');
        Schema::dropIfExists('tipo_tramite');
    }
}
