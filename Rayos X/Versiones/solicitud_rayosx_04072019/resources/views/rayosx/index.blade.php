@extends('layouts.layout')

@section('content')


<h2 class="text-blue"><b>Registro y autorización de licencias de Rayos X <?php if(isset($idTramite)) echo "Tramite #".$idTramite->id; ?></b></h2>

<form class="form-inline collapse" id="formInicial" type="submit" name="form_tramite" action="{{ route('rayosx.create') }}" method="post">
   {{ csrf_field() }}  

   <div class="line"></div> 

   <!-- PRIMER PASO CREACION DEL TRAMITE Y ASIGNACION DEL PRIMER ESTADO -->
   <div id="paso0" class="row block w-100 newsletter">
      <div class="w-100">
         <div class="subtitle">
            <h3><b>Registro de Información <?php echo isset($tipoTramite); ?> </b></h3>
         </div>

         <div class="form-group ">
            <span class="text-orange"  >•</span><label for="tipo_tramite">Tipo de trámite</label>
            <select id="tipo_tramite" name="tipo_tramite" class="form-control validate[required]">
               <option value="0" <?php if(isset($tipoTramite)) if($tipoTramite == 0) echo "selected=selected";?>>Seleccione...</option>
               <option value="1" <?php if(isset($tipoTramite)) if($tipoTramite == 1)  echo "selected=selected";?>>Nuevo</option>
               <option value="2" <?php if(isset($tipoTramite)) if($tipoTramite == 2)  echo "selected=selected";?>>Renovación</option>
            </select>
         </div>

      <div class="form-group">
         <span class="text-orange">• </span><label for="visita_previa">¿La IPS cuenta con el talento humano estipulado en el articulo 6 y 7, numeral 7.1?</label> <br/>                          

         <select id="visita_previa" name="visita_previa" class="form-control validate[required]">
            <option value="0" <?php if(isset($visitaPrevia)) if($visitaPrevia == 0) echo "selected=selected";?>>Seleccione...</option>
            <option value="1" <?php if(isset($visitaPrevia)) if($visitaPrevia == 1) echo "selected=selected";?>>SI</option>
            <option value="2" <?php if(isset($visitaPrevia)) if($visitaPrevia == 2) echo "selected=selected";?>>NO</option>
         </select>                           

         </div>
      </div>
       <div id="btnRegistrarSolicitud" class="col-md-12 pt-200 collapse">
         <p align="center">                       
            <br/>
            <!-- Primer Collapsible - Localizacion Entidad -->       
            <button type="submit" class="btn yellow">
               Crear Solicitud
            </button>
         </p>  
       </div>
   </div>
</form>
<div id="divPasos" class="text-center collapse">
   <div class="registro_triada">
      <a href="" target="_blank"><button class="btn yellow">1 - Localizacíon Entidad</button></a>
      <i class="fa fa-chevron-right"></i>
      <a href="/algo" target="_blank"><button class="btn yellow">2 - Equipos Rayos x</button></a>           
      <i class="fa fa-chevron-right"></i>
      <a href="/algo" target="_blank"><button class="btn yellow">3 - Trabajadores TOE</button></a>
      <i class="fa fa-chevron-right"></i>
      <a href="/algo" target="_blank"><button class="btn yellow">4 - Talento Humano</button></a>
      <i class="fa fa-chevron-right"></i>
      <a href="/algo" target="_blank"><button class="btn yellow">5 - Documentos Adjuntos</button></a>
   </div>
</div>


<!-- localizacion Identidad -->
<form id="formSeccion1" type="submit" name="form_tramite" action="{{route('rayosx.guardarLocalizacion')}}" method="post" class="form-row collapse">
   {{ csrf_field() }}

   <!-- PRIMERA PARTE DEL REGISTRO DE LA SOLICITUD - LOCALIZACION tipoTramite  -->
   <div id="paso1" class="row block w-100 newsletter ">   

      <div class="w-100">
        <div class="subtitle">
            <h3><b>Localizacíon Entidad:</b></h3>
        </div>

        <div class="row">
            <div class="col">
               <span class="text-orange">•</span><label for="depto_entidad">Departamento</label>
               <select id="depto_entidad" name="depto_entidad" class="form-control validate[required]">
                  <option value=""> - Seleccione Departamento -</option>
                  @foreach ($departamentos as $departamento => $value)
                   <option value="{{ $value}}"> {{ $departamento}}</option>   
                   @endforeach
               </select>
            </div>

            <div class="col">
               <span class="text-orange">•</span><label for="mpio_entidad">Municipio</label>
               <select id="mpio_entidad" name="mpio_entidad" class="form-control validate[required]">
                  <option value=""> - Seleccione Municipio - </option>                  
               </select>
               <div class="col-md-2" style="visibility:hidden;"><span id="loader"><i class="fa fa-spinner fa-3x fa-spin"></i></span></div>
            </div>
            
            <!--div class="col">
               <span class="text-orange">•</span><label for="loc_entidad">Localidad</label>
               <select id="loc_entidad" name="loc_entidad" class="form-control validate[required]">
                  <option value="">Seleccione Localidad...</option>
                  <option value = "Prueba">Prueba</option> 
               </select>
            </div>

            <div class="col">
               <span class="text-orange">•</span><label for="barrio_entidad">Barrio</label>
               <select id="barrio_entidad" name="barrio_entidad" class="form-control validate[required]">
                  <option value="">Seleccione Barrio...</option>
                  <option value = "Prueba">Prueba</option> 
               </select>
            </div-->

        </div>
       
        <div class="form-group">
            <span class="text-orange">•</span><label for="tipo_tramite">Dirección de la entidad</label>
            <input id="dire_entidad" name="dire_entidad" placeholder="Dirección de la entidad" class="form-control validate[required]" type="text">
         </div>   

         <div class="form-group">
            <span class="text-orange">•</span><label for="dire_entidad">Sede</label>
            <input id="sede_entidad" name="sede_entidad" placeholder="Sede de la entidad" class="form-control validate[required] input-md" type="text">
         </div>
         <!-- 2 campos en la fila --->
         <div class="row">
            <div class="col">
               <span class="text-orange">•</span><label for="email_entidad">Correo Electrónico</label>
               <input id="email_entidad" name="email_entidad" placeholder="Correo Electrónico" class="form-control validate[required, custom[email]] input-md" type="text">
            </div>      

            <div class="col">
               <span class="text-orange">•</span><label for="celular_entidad">Número celular</label>
               <input id="celular_entidad" name="celular_entidad" placeholder="Número celular" class="form-control validate[required] input-md" type="text">
            </div>
         </div>
         <div class="row">
            <div class="col">
               <span class="text-orange">•</span><label for="indicativo_entidad">Indicativo</label>
               <input id="indicativo_entidad" name="indicativo_entidad" placeholder="Indicativo" class="form-control validate[required] input-md" type="text">
            </div>

            <div class="col col-md-6">
               <span class="text-orange">•</span><label for="telefono_entidad">Número telefónico fijo</label>
               <input id="telefono_entidad" name="telefono_entidad" placeholder="Número telefónico fijo" class="form-control validate[required] input-md" type="text">
            </div>

            <div class="col">
               <span class="text-orange">•</span><label for="extension_entidad">Extensión</label>
               <input id="extension_entidad" name="extension_entidad" placeholder="Extensión" class="form-control validate[required] input-md" type="text">
            </div>
         </div>

          <div id="btnRegistrarGuardarEntidad" class="col-md-12 pt-200">
            <p align="center">                       
               <br/>
               <!-- Primer Collapsible - Localizacion Entidad -->                        
               <button type="submit" class="btn yellow">
                  Guardar y Continuar
               </button>
            </p>  
          </div>
      </div>
   </div>
</form>


<!-- Equipos generadores de radiación ionizante -->
<form id="formSeccion2" type="submit" name="formSeccion2" action="{{route('rayosx.guardarEquiposRayosX')}}" method="post" class="form-row collapse">
   {{ csrf_field() }}
   <!-- PRIMERA PARTE DEL REGISTRO DE LA SOLICITUD - LOCALIZACION tipoTramite  -->
   <div id="paso2" class="row block w-100 newsletter ">   

      <div class="w-100">
        <div class="subtitle">
            <h3><b>Equipos generadores de radiación ionizante:</b></h3>
        </div>
        <div class="row">            

           <div class="col">
               <span class="text-orange">•</span><label for="categoria">Categoría</label>
               <select id="categoria" name="categoria" class="form-control validate[required]">
                  <option value="">Seleccione...</option>
                  <option value="1">Categoria I</option>
                  <option value="2">Categoria II</option>
               </select>
            </div>

            <div class="col">
               <span class="text-orange">•</span><label for="categoria1">Equipos generadores de radicación ionizante</label>            
               <select id="categoria1" name="categoria1" class="form-control validate[required]">
                  <option value="">Seleccione...</option>
                  <option value="1">Radiología odontológica periapical</option>
                  <option value="2">Equipo de RX</option>
               </select>            
            </div>        

         </div>

         <div class="row">

            <div class="col">
                  <span class="text-orange">•</span><label for="categoria2">Equipos generadores de radicación ionizante</label>    
                  <select id="categoria2" name="categoria2" class="form-control validate[required]">
                     <option value="">Seleccione...</option>
                     <option value="1">Radioterapia</option>
                     <option value="2">Radio diagnóstico de alta complejidad</option>
                     <option value="3">Radio diagnóstico de media complejidad</option>
                     <option value="4">Radio diagnóstico de baja complejidad</option>
                     <option value="5">Radiografias odontológicas panóramicas y tomografias orales</option>
                  </select>               
               </div>

            <div class="col">
               <span class="text-orange">•</span><label for="categoria1-1">Radiolog&iacute;a odontológica periapical</label>   
               <select id="categoria1_1" name="categoria1_1" class="form-control validate[required]">
                  <option value="">Seleccione...</option>
                  <option value="1">Equipo de RX odontológico periapical</option>
                  <option value="2">Equipo de RX odontológico periapical portat&iacute;l</option>
               </select>            
            </div>

         </div>

         <div class="row">

            <div class="col">
               <span class="text-orange">•</span><label for="categoria1-2">Equipo de RX</label>    
               <select id="categoria1_2" name="categoria1_2" class="form-control validate[required]">
                  <option value="">Seleccione...</option>
                  <option value="1">Densitómetro óseo</option>
               </select>            
            </div>

            <div class="col">
               <span class="text-orange">•</span><label for="categoria1-2">Tipo de equipo generador de radiación ionizante</label>
               <select id="equipo_generador" name="equipo_generador" class="form-control validate[required]">            
                  <option value="">Seleccione...</option>
                  <?php
                     for($i=0;$i<2;$i++){
                         echo "<option value='".$i."'>".$i."</option>";
                     }
                     
                     ?>
               </select>            
            </div>

         </div>

         <div class="form-group">
            <span class="text-orange">•</span><label for="tipo_visualizacion">Tipo de visualización de la imagen</label>
            <select id="tipo_visualizacion" name="tipo_visualizacion" class="form-control validate[required]">
               <option value="">Seleccione...</option>
               <?php
                  for($i=0;$i<2;$i++){
                      echo "<option value='".$i."'>".$i."</option>";
                  }
                  
                  ?>
            </select>            
         </div>

         <div class="row">

            <div class="col">
               <span class="text-orange">•</span><label for="marca_equipo">Marca equipo</label>
               <input id="marca_equipo" name="marca_equipo" placeholder="Marca equipo" class="form-control validate[required]" type="text">
            </div>

            <div class="col">
               <span class="text-orange">•</span><label for="modelo_equipo">Modelo equipo</label>
               <input id="modelo_equipo" name="modelo_equipo" placeholder="Modelo equipo" class="form-control validate[required]" type="text">
            </div>

            <div class="col">
               <span class="text-orange">•</span><label for="serie_equipo">Serie equipo</label>
               <input id="serie_equipo" name="serie_equipo" placeholder="Serie equipo" class="form-control validate[required]" type="text">
            </div>

            <div class="col">
               <span class="text-orange">•</span><label for="marca_tubo_rx">Marca tubo RX</label>
               <input id="marca_tubo_rx" name="marca_tubo_rx" placeholder="Marca tubo RX" class="form-control validate[required]" type="text">
            </div>

         </div>

         <div class="row">

            <div class="col">
               <span class="text-orange">•</span><label for="modelo_tubo_rx">Modelo tubo RX</label>
               <input id="modelo_tubo_rx" name="modelo_tubo_rx" placeholder="Modelo tubo RX" class="form-control validate[required]" type="text">
            </div>

            <div class="col">
               <span class="text-orange">•</span><label for="serie_tubo_rx">Serie tubo RX</label>
               <input id="serie_tubo_rx" name="serie_tubo_rx" placeholder="Serie tubo RX" class="form-control validate[required]" type="text">
            </div>

            <div class="col">
               <span class="text-orange">•</span><label for="tension_tubo_rx">Tensión máxima tubo RX [kV]</label>
               <input id="tension_tubo_rx" name="tension_tubo_rx" placeholder="Tensión máxima tubo RX [kV]" class="form-control validate[required]" type="text">
            </div>

            <div class="col">
               <span class="text-orange">•</span><label for="contiene_tubo_rx">Cont. Max del tubo RX [mA]</label>
               <input id="contiene_tubo_rx" name="contiene_tubo_rx" placeholder="Contiene máxima del tubo RX [mA]" class="form-control validate[required]" type="text">
            </div>
         </div>

         <div class="row">

            <div class="col">
               <span class="text-orange">•</span><label for="energia_fotones">Energ&iacute;a de fotones [MeV]</label>
               <input id="energia_fotones" name="energia_fotones" placeholder="Energ&iacute;a de fotones [MeV]" class="form-control" type="text">
            </div>

            <div class="col">
               <span class="text-orange">•</span><label for="energia_electrones">Energ&iacute;a de electrones [MeV]</label>
               <input id="energia_electrones" name="energia_electrones" placeholder="Energ&iacute;a de electrones [MeV]" class="form-control" type="text">
            </div>

            <div class="col">
               <span class="text-orange">•</span><label for="carga_trabajo">Carga de trabajo [mA.min/semana]</label>
               <input id="carga_trabajo" name="carga_trabajo" placeholder="Carga de trabajo [mA.min/semana]" class="form-control validate[required]" type="text">
            </div>

         </div>



         <div class="form-group">
            <span class="text-orange">•</span><label for="ubicacion_equipo">Ubicación del equipo de la instalación</label>
            <input id="ubicacion_equipo" name="ubicacion_equipo" placeholder="Ubicación del equipo de la instalación" class="form-control validate[required]" type="text">
         </div>

         <div class="row">

            <div class="col">
               <span class="text-orange">•</span><label for="numero_permiso">Número de permiso de comercialización</label>
               <input id="numero_permiso" name="numero_permiso" placeholder="Número de permiso de comercialización" class="form-control validate[required]" type="text">
            </div>

            <div class="col">
               <span class="text-orange">•</span><label for="anio_fabricacion">A&ntilde;o de fabricación del equipo</label>
               <input id="anio_fabricacion_equipo" name="anio_fabricacion" placeholder="A&ntilde;o de fabricación del equipo" class="form-control validate[required]" type="text">
            </div>

            <div class="col">
               <span class="text-orange">•</span><label for="anio_fabricacion_tubo">A&ntilde;o de fabricación del tubo</label>
               <input id="anio_fabricacion_tubo" name="anio_fabricacion_tubo" placeholder="A&ntilde;o de fabricación del tubo" class="form-control validate[required]" type="text">
            </div>
         </div>              

          <div id="btnGuardarEquipos" class="col-md-12 pt-200">
            <p align="center">                       
               <br/>
               <!-- Primer Collapsible - Localizacion Entidad -->                        
               <button type="submit" class="btn yellow">
                  Guardar y Continuar
               </button>
            </p>  
          </div>
      </div>
   </div>
</form>

<!-- PRIMERA PARTE DEL REGISTRO DE LA SOLICITUD - LOCALIZACION tipoTramite  -->
<div id="paso3" class="row block w-100 newsletter collapse ">   

   <div class="w-100">
     <div class="subtitle">
         <h3><b>Trabajadores ocupacionalmente expuestos - TOE:</b></h3>
     </div>
     <!-- Equipos generadores de radiación ionizante -->
      <form id="formSeccion3-1" type="submit" name="formSeccion3-1" action="{{route('rayosx.guardarOficialTOE')}}" method="post" class="collapse">
      {{ csrf_field() }}
     <h4><b><span class="text-orange">•</span>Oficial de protección radiológica/Encargado de protección Radiológica</b></h4>     

     <div class="row">

         <div class="col">
            <span class="text-orange">•</span><label for="encargado_pnombre">Primer Nombre</label>
            <input id="encargado_pnombre" name="encargado_pnombre" placeholder="Primer Nombre" class="form-control validate[required]" type="text">
         </div>

         <div class="col">
            <span class="text-orange">•</span><label for="encargado_snombre">Segundo Nombre</label>
            <input id="encargado_snombre" name="encargado_snombre" placeholder="Segundo Nombre" class="form-control validate[required]" type="text">
         </div>

         <div class="col">
            <span class="text-orange">•</span><label for="encargado_papellido">Primer Apellido</label>
            <input id="encargado_papellido" name="encargado_papellido" placeholder="Primer Apellido" class="form-control validate[required]" type="text">
         </div>
         <div class="col">
            <span class="text-orange">•</span><label for="encargado_sapellido">Segundo Apellido</label>
            <input id="encargado_sapellido" name="encargado_sapellido" placeholder="Segundo Apellido" class="form-control validate[required]" type="text">
         </div>
      </div>

      <div class="row">          
         <div class="col">
            <span class="text-orange">•</span><label for="encargado_tdocumento">Tipo Documento</label>
            <select id="encargado_tdocumento" name="encargado_tdocumento" class="form-control validate[required]">
               <option value=""> - Seleccione Tipo Documento -</option>
               @foreach ($tiposIdentificacion as $tipoIdentificacion => $value)
               <option value="{{ $value}}"> {{ $tipoIdentificacion}}</option>   
               @endforeach
            </select>
         </div>

         <div class="col">
            <span class="text-orange">•</span><label for="encargado_ndocumento">Número Documento</label>
            <input id="encargado_ndocumento" name="encargado_ndocumento" placeholder="Número Documento" class="form-control validate[required]" type="text">
         </div>

         <div class="col">
            <span class="text-orange">•</span><label for="encargado_lexpedicion">Lugar Expedición</label>
            <input id="encargado_lexpedicion" name="encargado_lexpedicion" placeholder="Lugar Expedición" class="form-control validate[required] input-md" type="text">
         </div>

      </div>

      <div class="form-group">
         <span class="text-orange">•</span><label for="encargado_correo">Correo Electrónico</label>
         <input id="encargado_correo" name="encargado_correo" placeholder="Correo Electrónico" class="form-control validate[required, custom[email]]" type="text">
      </div>

      <div class="row">

         <div class="col">
            <span class="text-orange">•</span><label for="encargado_nivel">Nivel Académico</label>
            <select id="encargado_nivel" name="encargado_nivel" class="form-control validate[required]">
               <option value="">Seleccione...</option>
               @foreach ($prNivelacademico as $nivelAcademico => $value)
                <option value="{{$value}}"> {{ $nivelAcademico}}</option>   
                @endforeach
            </select>
         </div>

         <div class="col">
            <span class="text-orange">•</span><label for="encargado_profesion">Profesión</label>
            <select id="encargado_profesion" name="encargado_profesion" class="form-control validate[required]">
               <option value="">Seleccione...</option>
               @foreach ($prProgramasUniv as $programaAcademico => $value)
                <option value="{{$value}}"> {{ $programaAcademico}}</option>   
                @endforeach
            </select>
         </div>

      </div>

      <div id="btnGuardarOficialTOE" class="col-md-12 pt-200">
         <p align="center">                       
            <br/>
            <!-- Primer Collapsible - Localizacion Entidad -->                        
            <button type="submit" class="btn yellow">
               Guardar Encargado TOE 
            </button>
         </p>  
       </div>

   </form>
   
   <br/>
   <h4><b><span class="text-orange">•</span>TOE - Trabajadores Ocacionalmente Expuestos</b></h4>
   <a id="nuevoTOE" class="btn yellow"> Nuevo Toe</a>

   <!-- Equipos generadores de radiación ionizante -->
   <form id="formSeccion3-2" type="submit" name="formSeccion3-2" action="{{route('rayosx.guardarTemporalTOE')}}" method="post" class="collapse">
   <!-- PRIMERA PARTE DEL REGISTRO DE LA SOLICITUD - LOCALIZACION tipoTramite  -->  
      {{ csrf_field() }}
      <div class="row">

         <div class="col">
            <span class="text-orange">•</span><label for="encargado_pnombre">Primer Nombre</label>
            <input id="encargado_pnombre" name="encargado_pnombre" placeholder="Primer Nombre" class="form-control validate[required]" type="text">
         </div>

         <div class="col">
            <span class="text-orange">•</span><label for="encargado_snombre">Segundo Nombre</label>
            <input id="encargado_snombre" name="encargado_snombre" placeholder="Segundo Nombre" class="form-control validate[required]" type="text">
         </div>

         <div class="col">
            <span class="text-orange">•</span><label for="encargado_papellido">Primer Apellido</label>
            <input id="encargado_papellido" name="encargado_papellido" placeholder="Primer Apellido" class="form-control validate[required]" type="text">
         </div>
         <div class="col">
            <span class="text-orange">•</span><label for="encargado_sapellido">Segundo Apellido</label>
            <input id="encargado_sapellido" name="encargado_sapellido" placeholder="Segundo Apellido" class="form-control validate[required]" type="text">
         </div>
      </div>

      <div class="row">       
         <div class="col">
            <span class="text-orange">•</span><label for="encargado_correo">Correo Electrónico</label>
            <input id="encargado_correo" name="encargado_correo" placeholder="Correo Electrónico" class="form-control validate[required, custom[email]]" type="text">   
         </div>
         <div class="col">
            <span class="text-orange">•</span><label for="encargado_tdocumento">Tipo Documento</label>
            <select id="encargado_tdocumento" name="encargado_tdocumento" class="form-control validate[required]">
               <option value=""> - Seleccione Tipo Documento -</option>
               @foreach ($tiposIdentificacion as $tipoIdentificacion => $value)
               <option value="{{ $value}}"> {{ $tipoIdentificacion}}</option>   
               @endforeach
            </select>
         </div>

         <div class="col">
            <span class="text-orange">•</span><label for="encargado_ndocumento">Número Documento</label>
            <input id="encargado_ndocumento" name="encargado_ndocumento" placeholder="Número Documento" class="form-control validate[required]" type="text">
         </div>

         <div class="col">
            <span class="text-orange">•</span><label for="encargado_lexpedicion">Lugar Expedición</label>
            <input id="encargado_lexpedicion" name="encargado_lexpedicion" placeholder="Lugar Expedición" class="form-control validate[required] input-md" type="text">
         </div>

      </div>

      <div class="row">

         <div class="col">
            <span class="text-orange">•</span><label for="encargado_nivel">Nivel Académico</label>
            <select id="encargado_nivel" name="encargado_nivel" class="form-control validate[required]">
               <option value="">Seleccione...</option>
               @foreach ($prNivelacademico as $nivelAcademico => $value)
                <option value="{{$value}}"> {{ $nivelAcademico}}</option>   
                @endforeach
            </select>
         </div>

         <div class="col">
            <span class="text-orange">•</span><label for="encargado_profesion">Profesión</label>
            <select id="encargado_profesion" name="encargado_profesion" class="form-control validate[required]">
               <option value="">Seleccione...</option>
               @foreach ($prProgramasUniv as $programaAcademico => $value)
                <option value="{{$value}}"> {{ $programaAcademico}}</option>   
                @endforeach
            </select>
         </div>

      </div>

      <div class="row">

         <div class="col">
            <span class="text-orange">•</span><label for="toe_ult_entrenamiento">Fecha del último entrenamiento en protección radiológica</label>
            <input id="toe_ult_entrenamiento" name="toe_ult_entrenamiento" placeholder="Fecha del último entrenamiento en protección radiológica" class="form-control validate[required] input-md" type="text">
         </div>

         <div class="col">
            <span class="text-orange">•</span><label for="toe_pro_entrenamiento">Fecha del próximo entrenamiento en protección radiológica</label>
            <input id="toe_pro_entrenamiento" name="toe_pro_entrenamiento" placeholder="Fecha del próximo entrenamiento en protección radiológica" class="form-control validate[required] input-md" type="text">
         </div>

         <div class="col">
            <span class="text-orange">•</span><label for="toe_registro">Número del registro profesional de salud             </label>
            <input id="toe_registro" name="toe_registro" placeholder="" class="form-control validate[required] input-md" type="text">
         </div>

      </div>


      <div id="btnGuardarOficialTOE" class="col-md-12 pt-200">
         <p align="center">                       
            <br/>
            <!-- Primer Collapsible - Localizacion Entidad -->                        
            <button type="submit" class="btn yellow">
               Guardar Ocacionalmente TOE 
            </button>
         </p>  
       </div>

   </form>

      <table class="display nowrap table table-hover" id="tabla_toe" style="width:100%;">
         <thead>
            <tr>
               <th>ID</th>
               <th>Primer Nombre</th>
               <th>Segundo Nombre</th>
               <th>Primer Apellido</th>
               <th>Segundo Apellido</th>                           
            </tr>
         </thead>
         
         <tbody>            
           @if($temporalTOE->count())  
           @foreach($temporalTOE as $tempTOE)  
           <tr>
             <td>{{$tempTOE->id_toe_rayosx}}</td>
             <td>{{$tempTOE->toe_pnombre}}</td>
             <td>{{$tempTOE->toe_snombre}}</td>
             <td>{{$tempTOE->toe_papellido}}</td>
             <td>{{$tempTOE->toe_sapellido}}</td>                               
            </tr>
            @endforeach 
            @else
            <tr>
             <td colspan="8">0  TOE - Trabajadores Ocacionalmente Expuestos Encontrados</td>
           </tr>
           @endif
         </tbody>
      </table>

      

      <div id="btnContinuarPaso3" class="col-md-12 pt-200">
         <p align="center">                       
            <br/>
            <form id="formSeccion3-3" type="submit" name="formSeccion3-3" action="{{route('rayosx.verificarTOE')}}" method="post" class="collapse">
               {{ csrf_field() }}
               <!-- Continuar con paso 4 - En caso de haber registrado TOE correctamente -->
               <button type="submit" id="button3-3" class="btn yellow">
                  Continuar
               </button>   
            </form>         
         </p>  
       </div>

   </form>

   </div>
</div>





   <!-- PRIMERA PARTE DEL REGISTRO DE LA SOLICITUD - LOCALIZACION tipoTramite  -->
   <div id="paso4" class="row block w-100 newsletter collapse">   

      <div class="w-100">
        <div class="subtitle">
            <h3><b>Talento Humano:</b></h3>
        </div>
        <h4><b><span class="text-orange">•</span>Oficial de protección radiológica/Encargado de protección Radiológica</b></h4>     

        <!--div class="col">
            <span class="text-orange">•</span><label for="tipo_titulo">La IPS cuenta con el talento humano estipulado en el articulo 6 y 7, numeral 7.1?</label>
            <select id="visita_previa" name="visita_previa_th" class="form-control validate[required]">
               <option value="">Seleccione...</option>
               <option value="1">SI</option>
               <option value="2">NO</option>
            </select>
         </div-->

         <h4><b><span class="text-orange">•</span>Director Técnico</b></h4>

         <!-- Equipos generadores de radiación ionizante -->
         <form id="formSeccion4-1" type="submit" name="formSeccion4-1" action="{{route('rayosx.guardarTalentoHumano')}}" method="post">
            {{ csrf_field() }}

         <div class="row">

            <div class="col">
               <span class="text-orange">•</span><label for="talento_pnombre">Primer Nombre</label>
               <input id="talento_pnombre" name="talento_pnombre" placeholder="Primer Nombre" class="form-control validate[required]" type="text">
            </div>

            <div class="col">
               <span class="text-orange">•</span><label for="talento_snombre">Segundo Nombre</label>
               <input id="talento_snombre" name="talento_snombre" placeholder="Segundo Nombre" class="form-control validate[required] input-md" type="text">
            </div>

            <div class="col">
               <span class="text-orange">•</span><label for="talento_papellido">Primer Apellido</label>
               <input id="talento_papellido" name="talento_papellido" placeholder="Primer Apellido" class="form-control validate[required] input-md" type="text">
            </div>

            <div class="col">
               <span class="text-orange">•</span><label for="talento_sapellido">Segundo Apellido</label>
               <input id="talento_sapellido" name="talento_sapellido" placeholder="Segundo Apellido" class="form-control validate[required] input-md" type="text">
            </div>
         </div>

         <div class="row">

            <div class="col">
               <span class="text-orange">•</span><label for="talento_tdocumento">Tipo Documento</label>
               <select id="talento_tdocumento" name="talento_tdocumento" class="form-control validate[required]">
                  <option value=""> - Seleccione Tipo Documento -</option>
                     @foreach ($tiposIdentificacion as $tipoIdentificacion => $value)
                     <option value="{{ $value}}"> {{ $tipoIdentificacion}}</option>   
                     @endforeach
                  </select>
            </div>

            <div class="col">
               <span class="text-orange">•</span><label for="talento_ndocumento">Número Documento</label>
               <input id="talento_ndocumento" name="talento_ndocumento" placeholder="Número Documento" class="form-control validate[required] input-md" type="text">
            </div>

            <div class="col">
               <span class="text-orange">•</span><label for="talento_lexpedicion">Lugar Expedición</label>
               <input id="talento_lexpedicion" name="talento_lexpedicion" placeholder="Lugar Expedición" class="form-control validate[required] input-md" type="text">
            </div>

         </div>

         <div class="col">
            <span class="text-orange">•</span><label for="talento_correo">Correo Electrónico</label>
            <input id="talento_correo" name="talento_correo" placeholder="Correo Electrónico" class="form-control validate[required, custom[email]] input-md" type="text">
         </div>

         <br/>
 
         <h4><b><span class="text-orange">•</span>Idoneidad Profesional</b></h4>

         <div class="row">

            <div class="col">
               <span class="text-orange">•</span><label for="talento_titulo">Titulo de pregrado obtenido</label>
               <input id="talento_titulo" name="talento_titulo" placeholder="Titulo de pregrado obtenido" class="form-control validate[required] input-md" type="text">
            </div>

            <div class="col">
               <span class="text-orange">•</span><label for="talento_universidad">Universidad que otorgó el titulo de pregrado</label>
               <input id="talento_universidad" name="talento_universidad" placeholder="Universidad que otorgó el titulo de pregrado" class="form-control validate[required] input-md" type="text">
            </div>

            <div class="col">
               <span class="text-orange">•</span><label for="talento_libro">Libro del diploma de pregrado</label>
               <input id="talento_libro" name="talento_libro" placeholder="Libro del diploma de pregrado" class="form-control validate[required] input-md" type="text">
            </div>

         </div>

         <div class="row">

            <div class="col">
               <span class="text-orange">•</span><label for="talento_registro">Registro del diploma de pregrado</label>
               <input id="talento_registro" name="talento_registro" placeholder="Registro del diploma de pregrado" class="form-control validate[required] input-md" type="text">
            </div>

            <div class="col">
               <span class="text-orange">•</span><label for="talento_fecha_diploma">Fecha diploma de pregrado</label>
               <input id="talento_fecha_diploma" name="talento_fecha_diploma" placeholder="Fecha diploma de pregrado" class="form-control validate[required] input-md" type="text">
            </div>

            <div class="col">
               <span class="text-orange">•</span><label for="talento_resolucion">Resolución convalidación titulo pregrado</label>
               <input id="talento_resolucion" name="talento_resolucion" placeholder="Resolución convalidación titulo pregrado" class="form-control validate[required] input-md" type="text"> 
            </div>

         </div>

         <div class="row">

            <div class="col">
               <span class="text-orange">•</span><label for="talento_fecha_convalida">Fecha convalidación titulo de pregrado</label>
               <input id="talento_fecha_convalida" name="talento_fecha_convalida" placeholder="Fecha convalidación titulo de pregrado" class="form-control validate[required] input-md" type="text">
            </div>

            <div class="col">
               <span class="text-orange">•</span><label for="talento_nivel">Nivel Académico último posgrado</label>
               <select id="talento_nivel" name="talento_nivel" class="form-control validate[required]">
                  <option value="">Seleccione...</option>
                  <?php
                     for($i=0;$i<2;$i++){
                         echo "<option value='".$i."'>".$i."</option>";
                     }
                     
                     ?>
               </select>
            </div>

            <div class="col">
               <span class="text-orange">•</span><label for="talento_titulo_pos">Título de posgrado obtenido</label>
               <input id="talento_titulo_pos" name="talento_titulo_pos" placeholder="Título de posgrado obtenido" class="form-control validate[required] input-md" type="text">
            </div>

         </div>

         <div class="row">

            <div class="col">
               <span class="text-orange">•</span><label for="talento_universidad_pos">Universidad que otorgó el titulo de posgrado</label>
               <input id="talento_universidad_pos" name="talento_universidad_pos" placeholder="Universidad que otorgó el titulo de posgrado" class="form-control validate[required] input-md" type="text"> 
            </div>

            <div class="col">
               <span class="text-orange">•</span><label for="talento_libro_pos">Libro del diploma de posgrado</label>
               <input id="talento_libro_pos" name="talento_libro_pos" placeholder="Libro del diploma de posgrado" class="form-control validate[required] input-md" type="text">
            </div>

            <div class="col">
               <span class="text-orange">•</span><label for="talento_registro_pos">Registro del diploma de posgrado</label>
               <input id="talento_registro_pos" name="talento_registro_pos" placeholder="Registro del diploma de posgrado" class="form-control validate[required] input-md" type="text">
            </div>
         </div>

         <div class="row">

            <div class="col">
               <span class="text-orange">•</span><label for="talento_fecha_diploma_pos">Fecha diploma de posgrado</label>
               <input id="talento_fecha_diploma_pos" name="talento_fecha_diploma_pos" placeholder="Fecha diploma de posgrado" class="form-control validate[required]" type="text">
            </div>

            <div class="col">
               <span class="text-orange">•</span><label for="talento_resolucion">Resolución convalidación titulo posgrado</label>
               <input id="talento_resolucion_pos" name="talento_resolucion_pos" placeholder="Resolución convalidación titulo posgrado" class="form-control validate[required] input-md" type="text">
            </div>

            <div class="col">
               <span class="text-orange">•</span><label for="talento_fecha_convalida_pos">Fecha convalidación titulo de posgrado</label>
               <input id="talento_fecha_convalida_pos" name="talento_fecha_convalida_pos" placeholder="Fecha convalidación titulo de posgrado" class="form-control validate[required] input-md" type="text">
            </div>
         </div>

         <div id="btnGuardarDirector" class="col-md-12 pt-200">
            <p align="center">                       
               <br/>
               <!-- Primer Collapsible - Localizacion Entidad -->                        
               <button type="submit" class="btn yellow">
                  Guardar Director Técnico
               </button>
            </p>  
          </div>

         </form>
         <h4><b><span class="text-orange">•</span>Equipos u objetos de prueba</b></h4>

         <a id="nuevoEquipo" class="btn yellow"> Agregar Equipo</a>

         <form id="formSeccion4-2" type="submit" name="formSeccion4-2" action="{{route('rayosx.guardarEquipoPrueba')}}" method="post" class="collapse">
            {{ csrf_field() }}

            <div class="row">

               <div class="col">
                  <span class="text-orange">•</span><label for="obj_nombre">Fecha diploma de posgrado</label>
                  <input id="obj_nombre" name="obj_nombre" placeholder="Nombre del Equipo" class="form-control validate[required] input-md" type="text">
               </div>

               <div class="col">
                  <span class="text-orange">•</span><label for="obj_marca">Marca del equipo</label>
                  <input id="obj_marca" name="obj_marca" placeholder="Marca del equipo" class="form-control validate[required] input-md" type="text">
               </div>

               <div class="col">
                  <span class="text-orange">•</span><label for="obj_modelo">Modelo del equipo</label>
                  <input id="obj_modelo" name="obj_modelo" placeholder="Modelo del Equipo" class="form-control validate[required] input-md" type="text">
               </div>
            </div>

            <div class="row">

               <div class="col">
                  <span class="text-orange">•</span><label for="obj_serie">Serie del equipo</label>
                  <input id="obj_serie" name="obj_serie" placeholder="Serie del Equipo" class="form-control validate[required] input-md" type="text">
               </div>

               <div class="col">
                  <span class="text-orange">•</span><label for="obj_calibracion">Calibración</label>
                  <input id="obj_calibracion" name="obj_calibracion" placeholder="Calibración" class="form-control validate[required] input-md" type="text">
               </div>

               <div class="col">
                  <span class="text-orange">•</span><label for="obj_vigencia">Vigencia de calibración</label>
                  <select id="obj_vigencia" name="obj_vigencia" class="form-control validate[required]">
                     <option value="">Seleccione...</option>
                     <option value="1">Un (1) A&ntilde;o</option>
                     <option value="2">Dos (2) A&ntilde;os</option>
                     <option value="3">Otra, definida por el fabricante</option>
                  </select>
               </div>
            </div>

            <div class="row">

               <div class="col">
                  <span class="text-orange">•</span><label for="obj_fecha">Fecha de calibración</label>
                  <input id="obj_fecha" name="obj_fecha" placeholder="Fecha de la calibracion" class="form-control validate[required] input-md" type="text">
               </div>

               <div class="col">
                  <span class="text-orange">•</span><label for="obj_manual">Manual Técnico y ficha Técnica</label>
                  <select id="obj_manual" name="obj_manual" class="form-control validate[required]">
                     <option value="">Seleccione...</option>
                     <option value="1">Posee manual técnico</option>
                     <option value="2">Pose ficha técnica</option>
                  </select>
               </div>

               <div class="col">
                  <span class="text-orange">•</span><label for="obj_uso">Usos</label>
                  <textarea id="obj_uso" name="obj_uso" class="form-control validate[required] input-md"></textarea>
               </div>
            </div>

            <div id="btnGuardarEquipoPrueba" class="col-md-12 pt-200">
               <p align="center">                       
                  <br/>
                  <!-- Primer Collapsible - Localizacion Entidad -->                        
                  <button type="submit" class="btn yellow">
                     Guardar Equipo/Objeto Prueba
                  </button>
               </p>  
             </div>
         
         </form>
         <table class="display nowrap" id="tabla_equiprueba" style="width:100%">
            <thead>
               <tr>
                  <th>ID</th>
                  <th>Nombre del equipo</th>
                  <th>Marca del equipo</th>
                  <th>Modelo del equipo</th>
                  <th>Serie del equipo</th>
                  <th>Calibración</th>
                  <th>Vigencia de calibración</th>
                  <th>Fecha de calibración</th>
                  <th>Manual Técnico y ficha Técnica</th>
                  <th>Usos</th>
               </tr>
            </thead>

            <tbody>            
              @if($objeto->count())  
              @foreach($objeto as $obj)  
              <tr>
                <td>{{$obj->id_obj_rayosx}}</td>
                <td>{{$obj->obj_nombre}}</td>
                <td>{{$obj->obj_marca}}</td>
                <td>{{$obj->obj_modelo}}</td>
                <td>{{$obj->obj_serie}}</td>                               
                <td>{{$obj->obj_calibracion}}</td>
                <td>{{$obj->obj_vigencia}}</td>
                <td>{{$obj->obj_fecha}}</td>
                <td>{{$obj->obj_manual}}</td>
                <td>{{$obj->obj_uso}}</td> 
               </tr>
               @endforeach 
               @else
               <tr>
                <td colspan="8">0  Equipos Encontrados</td>
              </tr>
              @endif
            </tbody>
            
            
         </table>


         <form id="formSeccion4-3" type="submit" name="formSeccion4-3" action="{{route('rayosx.verificarPaso4')}}" method="post">
            {{ csrf_field() }}         
         
            <div id="btnGuardarTalentoHumano" class="col-md-12 pt-200">
               <p align="center">                       
                  <br/>
                  <!-- Primer Collapsible - Localizacion Entidad -->                        
                  <button type="submit" id="button4-3" class="btn yellow">
                     Continuar
                  </button>
               </p>  
             </div>
          </form>


      </div>
   </div>
</form>



<!-- Equipos generadores de radiación ionizante -->
<form id="formSeccion5" type="submit" name="formSeccion5" action="store" method="post" class="form-row collapse">
   {{ csrf_field() }}
   <!-- PRIMERA PARTE DEL REGISTRO DE LA SOLICITUD - LOCALIZACION tipoTramite  -->
   <div id="paso3" class="row block w-100 newsletter ">   

      <div class="w-100">
        <div class="subtitle">
            <h3><b>Documentos Adjuntos:</b></h3>
        </div>
        <h4><b><span class="text-orange">•</span>Documentos Categoría I</b></h4>   

        <table class="table">
            <thead>
               <tr>
                  <th>Descripción</th>
                  <th>Documento</th>
               </tr>
            </thead>
            <tbody>
               <tr>
                  <td>Copia documento identificación del encargado de protección radiológica</td>
                  <td><input id="fi_doc_encargado" name="fi_doc_encargado" type="file" class="archivopdf validate[required]"></td>
               </tr>
               <tr>
                  <td>Copia del diploma del encargado de protección radiológica</td>
                  <td><input id="fi_diploma_encargado" name="fi_diploma_encargado" type="file" class="archivopdf validate[required]"></td>
               </tr>
               <tr>
                  <td>Descripción de los blindajes estructurales o portátiles y el cálculo del blindaje</td>
                  <td><input id="fi_blindajes" name="fi_blindajes" type="file" class="archivopdf validate[required]"></td>
               </tr>
               <tr>
                  <td>Informe sobre los resultados del control de calidad</td>
                  <td><input id="fi_control_calidad" name="fi_control_calidad" type="file" class="archivopdf validate[required]"></td>
               </tr>
               <tr>
                  <td>Registros dosimétricos del último periodo de los trabajadores ocupacionalmente expuestos</td>
                  <td><input id="fi_registro_dosimetrico" name="fi_registro_dosimetrico" type="file" class="archivopdf validate[required]"></td>
               </tr>
               <tr>
                  <td>Registro del cumplimiento de los niveles de referencia para diagnóstico</td>
                  <td><input id="fi_registro_niveles" name="fi_registro_niveles" type="file" class="archivopdf validate[required]"></td>
               </tr>
               <tr>
                  <td>Plano general de las instalaciones</td>
                  <td><input id="fi_plano" name="fi_plano" type="file" class="archivopdf validate[required]"></td>
               </tr>
               <tr>
                  <td>Certificado de la capacitación en protección radiológica de cada trabajador ocupacionalmente expuesto reportado en el formulario</td>
                  <td><input id="fi_certificado_capacitacion" name="fi_certificado_capacitacion" type="file" class="archivopdf validate[required]"></td>
               </tr>
               <tr>
                  <td>Programa de capacitación en protección radiológica</td>
                  <td><input id="fi_programa_capacitacion" name="fi_programa_capacitacion" type="file" class="archivopdf validate[required]"></td>
               </tr>
               <tr>
                  <td>Procedimientos de mantenimiento de conformidad a lo establecido por el fabricante</td>
                  <td><input id="fi_procedimiento_mantenimiento" name="fi_procedimiento_mantenimiento" type="file" class="archivopdf validate[required]"></td>
               </tr>
               <tr>
                  <td>Pruebas iniciales de caracterización de los equipos</td>
                  <td><input id="fi_pruebas_caracterizacion" name="fi_pruebas_caracterizacion" type="file" class="archivopdf validate[required]"></td>
               </tr>
               <tr>
                  <td>Programa de Tecnovigilancia</td>
                  <td><input id="fi_programa_tecno" name="fi_programa_tecno" type="file" class="archivopdf validate[required]"></td>
               </tr>
               <tr>
                  <td>Programa de protección radiológica</td>
                  <td><input id="fi_programa_proteccion" name="fi_programa_proteccion" type="file" class="archivopdf validate[required]"></td>
               </tr>
               <tr>
                  <td>Documentación de soporte de talento humano e infraestructura técnica. En el evento contemplado en el paragrafo 1 del articulo 21</td>
                  <td><input id="fi_soporte_talento" name="fi_soporte_talento" type="file" class="archivopdf validate[required]"></td>
               </tr>
               <tr>
                  <td>Fotocopia de Diploma de Posgrado del Director Técnico</td>
                  <td><input id="fi_diploma_director" name="fi_diploma_director" type="file" class="archivopdf validate[required]"></td>
               </tr>
               <tr>
                  <td>Fotocopia de la Resolución de convalidación del t&iacute;tulo ante el Ministerio de Educación Nacional - MEN del Director Técnico  </td>
                  <td><input id="fi_res_convalida_director" name="fi_res_convalida_director" type="file" class="archivopdf "></td>
               </tr>
               <tr>
                  <td>Fotocopia de Diploma de posgrado del (los) profesional(es) que realiza(n) control de calidad</td>
                  <td><input id="fi_diploma_pos_profe" name="fi_diploma_pos_profe" type="file" class="archivopdf validate[required]"></td>
               </tr>
               <tr>
                  <td>Fotocopia de la Resolución de convalidación del t&iacute;tulo ante el Ministerio de Educación Nacional - MEN del (los) profesional(es) que realiza(n) control de calidad  </td>
                  <td><input id="fi_res_convalida_profe" name="fi_res_convalida_profe" type="file" class="archivopdf "></td>
               </tr>
               <tr>
                  <td>Certificados de calibración con una vigencia superior a seis (6) meses por cada equipo reportado</td>
                  <td><input id="fi_cert_calibracion" name="fi_cert_calibracion" type="file" class="archivopdf validate[required]"></td>
               </tr>
               <tr>
                  <td>Declaraciones de primera parte por cada objeto de prueba reportado</td>
                  <td><input id="fi_declaraciones" name="fi_declaraciones" type="file" class="archivopdf validate[required]"></td>
               </tr>
            </tbody>
         </table>
                                 
        <h4><b><span class="text-orange">•</span>Documentos Categoría II</b></h4>     

        <table class="table">
            <thead>
               <tr>
                  <th>Descripción</th>
                  <th>Documento</th>
               </tr>
            </thead>
            <tbody>
               <tr>
                  <td>Copia documento identificación del oficial de protección radiológica</td>
                  <td><input id="fi_doc_oficial" name="fi_doc_oficial" type="file" class="archivopdf validate[required]"></td>
               </tr>
               <tr>
                  <td>Copia del diploma del oficial de protección radiológica</td>
                  <td><input id="fi_diploma_oficial" name="fi_diploma_oficial" type="file" class="archivopdf validate[required]"></td>
               </tr>
               <tr>
                  <td>Descripción de los blindajes estructurales o portátiles y el cálculo del blindaje</td>
                  <td><input id="fi_blindajes" name="fi_blindajes" type="file" class="archivopdf validate[required]"></td>
               </tr>
               <tr>
                  <td>Informe sobre los resultados del control de calidad</td>
                  <td><input id="fi_control_calidad" name="fi_control_calidad" type="file" class="archivopdf validate[required]"></td>
               </tr>
               <tr>
                  <td>Registros dosimétricos del último periodo de los trabajadores ocupacionalmente expuestos. Para alta complejidad, registros del segundo dosimetro</td>
                  <td><input id="fi_registro_dosimetrico" name="fi_registro_dosimetrico" type="file" class="archivopdf validate[required]"></td>
               </tr>
               <tr>
                  <td>Plano general de las instalaciones</td>
                  <td><input id="fi_plano" name="fi_plano" type="file" class="archivopdf validate[required]"></td>
               </tr>
               <tr>
                  <td>Documentación de soporte de talento humano e infraestructura técnica. En el evento contemplado en el paragrafo del articulo 23</td>
                  <td><input id="fi_soporte_talento" name="fi_soporte_talento" type="file" class="archivopdf validate[required]"></td>
               </tr>
               <tr>
                  <td>Fotocopia de Diploma de Posgrado del Director Técnico</td>
                  <td><input id="fi_diploma_director" name="fi_diploma_director" type="file" class="archivopdf validate[required]"></td>
               </tr>
               <tr>
                  <td>Fotocopia de la Resolución de convalidación del t&iacute;tulo ante el Ministerio de Educación Nacional - MEN del Director Técnico  </td>
                  <td><input id="fi_res_convalida_director" name="fi_res_convalida_director" type="file" class="archivopdf "></td>
               </tr>
               <tr>
                  <td>Fotocopia de Diploma de posgrado del (los) profesional(es) que realiza(n) control de calidad</td>
                  <td><input id="fi_diploma_pos_profe" name="fi_diploma_pos_profe" type="file" class="archivopdf validate[required]"></td>
               </tr>
               <tr>
                  <td>Fotocopia de la Resolución de convalidación del t&iacute;tulo ante el Ministerio de Educación Nacional - MEN del (los) profesional(es) que realiza(n) control de calidad  </td>
                  <td><input id="fi_res_convalida_profe" name="fi_res_convalida_profe" type="file" class="archivopdf "></td>
               </tr>
               <tr>
                  <td>Certificados de calibración con una vigencia superior a seis (6) meses por cada equipo reportado</td>
                  <td><input id="fi_cert_calibracion" name="fi_cert_calibracion" type="file" class="archivopdf validate[required]"></td>
               </tr>
               <tr>
                  <td>Declaraciones de primera parte por cada objeto de prueba reportado</td>
                  <td><input id="fi_declaraciones" name="fi_declaraciones" type="file" class="archivopdf validate[required]"></td>
               </tr>
            </tbody>
         </table>
      </div>
   </div>
</form>
@endsection    

@section ("scripts")

 <script languague="javascript">      

    $(document).ready(function () {
        
      //Inicial
      $("#btnRegistrarSolicitud").collapse('hide');      
      $( "#divPasos" ).collapse('hide');      
      $( "#formSeccion1" ).collapse('hide');
      $( "#formSeccion2" ).collapse('hide');
      $( "#formSeccion3-1" ).collapse('hide');
      $( "#formSeccion3-2" ).collapse('hide');
      $( "#formSeccion3-3" ).collapse('hide');
      $( "#formSeccion4-1" ).collapse('hide');
      $( "#formSeccion4-2" ).collapse('hide');
      $( "#formSeccion5" ).collapse('hide');
      $( "#paso3" ).collapse('hide');
      $( "#paso4" ).collapse('hide');



      

      //Controla la validacion de la creacion del tramite
      var tramiteExiste=<?php if(isset($existeTramite)) echo $existeTramite; else echo 0;?>;
      var paso=<?php if(isset($paso)) echo $paso; else echo 0;?>;
      var idTramite=<?php if(isset($idTramite->id)) echo $idTramite->id; else echo 0;?>;
      //En que paso del registro del formulario se encuentra, esta dividido segun las secciones
      

      var $inputExisteTramite = $('<input/>',{type:'hidden',id:'existeTramite',name:'existeTramite',value:tramiteExiste});

      var $inputIdTramite = $('<input/>',{type:'hidden',id:'idTramite',name:'idTramite',value:idTramite});

      var $inputPaso = $('<input/>',{type:'hidden',id:'paso',name:'paso',value:paso});      

      


      //de accuerdo al paso crea los campos en cada uno      
      if(paso==0){
         $inputExisteTramite.appendTo('#formInicial');
         $inputIdTramite.appendTo('#formInicial');
         $inputPaso.appendTo('#formInicial');
      }
      if(tramiteExiste==1){

         $( "#divPasos" ).collapse('show');
         $( "#tipo_tramite" ).prop( "disabled", true );
         $( "#visita_previa" ).prop( "disabled", true );

         if(paso==1){         
            $inputExisteTramite.appendTo('#formSeccion1');
            $inputIdTramite.appendTo('#formSeccion1');
            $inputPaso.appendTo('#formSeccion1');

            $( "#formSeccion1" ).collapse('show');
            alert("El tramite número "+$( "#idTramite" ).val()+" ha sido creado exitosamente, los campos necesariós han sido habilitados para su registro. El número del tramite servira para continuar el registro de la solicitud o revisar su proceso");
         }
         if(paso==2){
            $inputExisteTramite.appendTo('#formSeccion2');
            $inputIdTramite.appendTo('#formSeccion2');
            $inputPaso.appendTo('#formSeccion2');

            $( "#formSeccion2" ).collapse('show');
         }

         if(paso==3){
            $inputExisteTramite.appendTo('#formSeccion3-1');
            $inputIdTramite.appendTo('#formSeccion3-1');
            $inputPaso.appendTo('#formSeccion3-1');              

            $( "#formSeccion3-1" ).collapse('show'); 
            $( "#formSeccion3-3" ).collapse('show');           
            $( "#paso3" ).collapse('show');
            
         }         

         if(paso==3.5){
            $inputExisteTramite.appendTo('#formSeccion3-1');
            $inputIdTramite.appendTo('#formSeccion3-1');
            $inputPaso.appendTo('#formSeccion3-1');                        
            $( "#formSeccion3-1" ).collapse('show');            
            $( "#formSeccion3-3" ).collapse('show');
            $( "#paso3" ).collapse('show');
            alert("Es obligatorio registrar la información requerida.");            
         }

         if(paso==4){
            $inputExisteTramite.appendTo('#formSeccion4-1');
            $inputIdTramite.appendTo('#formSeccion4-1');
            $inputPaso.appendTo('#formSeccion4-1');            
            $( "#formSeccion4" ).collapse('show');            
            $( "#paso4" ).collapse('show');            
         }

         if(paso==4.5){
            $inputExisteTramite.appendTo('#formSeccion4-1');
            $inputIdTramite.appendTo('#formSeccion4-1');
            $inputPaso.appendTo('#formSeccion4-1');            
            $( "#formSeccion4" ).collapse('show');            
            $( "#paso4" ).collapse('show'); 
            alert("Es obligatorio registrar la información requerida.");            
         }

         if(paso==5){
            $inputExisteTramite.appendTo('#formSeccion5');
            $inputIdTramite.appendTo('#formSeccion5');
            $inputPaso.appendTo('#formSeccion5');            
            $( "#formSeccion5" ).collapse('show');                        
         }


         
      }else{
         $( "#formInicial" ).collapse('show');         
      }


      $("#nuevoTOE").click(function() {         
        $( "#formSeccion3-2" ).collapse('show');
         $inputExisteTramite.appendTo('#formSeccion3-2');
         $inputIdTramite.appendTo('#formSeccion3-2');
         $inputPaso.appendTo('#formSeccion3-2');
      });

      $("#nuevoEquipo").click(function() {         
        $( "#formSeccion4-2" ).collapse('show');
         $inputExisteTramite.appendTo('#formSeccion4-2');
         $inputIdTramite.appendTo('#formSeccion4-2');
         $inputPaso.appendTo('#formSeccion4-2');
      });


      $("#button3-3").click(function() {                       
         $inputExisteTramite.appendTo('#formSeccion3-3');
         $inputIdTramite.appendTo('#formSeccion3-3');
         $inputPaso.appendTo('#formSeccion3-3');
      });

      $("#button4-3").click(function() {                       
         $inputExisteTramite.appendTo('#formSeccion4-3');
         $inputIdTramite.appendTo('#formSeccion4-3');
         $inputPaso.appendTo('#formSeccion4-3');
      });

      
      //Validaciónes para crear el tramite de rayos x
      $("#tipo_tramite").change(function(){

         var tipoTramite=$("#tipo_tramite").val();
         if($("#tipo_tramite").val() == 1 && $("#visita_previa").val() == 1){
            $("#btnRegistrarSolicitud").collapse('show');
         }else{
            $("#btnRegistrarSolicitud").collapse('hide');
            //$(".collapse").collapse('hide');
         }

      });

      $("#visita_previa").change(function(){
         
         if($("#visita_previa").val() == 1 && $("#tipo_tramite").val() == 1){
            $("#btnRegistrarSolicitud").collapse('show');
         }else{
            $("#btnRegistrarSolicitud").collapse('hide');
            alert("No es posible que realices el registro de la solicitud, comunicate con ______________ para revisar tu caso")
         }
      });


      //Actualiza la lista de municipios de acuerdo  a la entidad seleccionada
      $('select[name="depto_entidad"]').on('change', function(){
        var departamentoId = $(this).val();        
        if(departamentoId) {
            $.ajax({
                url: "{{url('/municipio/get')}}/"+departamentoId,
                type:"GET",
                dataType:"json",
                beforeSend: function(){
                    $('#loader').css("visibility", "visible");
                },

                success:function(data) {

                    $('select[name="mpio_entidad"]').empty();

                    $.each(data, function(key, value){

                        $('select[name="mpio_entidad"]').append('<option value="'+ value +'">' + key+ '</option>');

                    });


                },
                complete: function(){
                    $('#loader').css("visibility", "hidden");
                }
            });
        } else {
            $('select[name="mpio_entidad"]').empty();
        }

    });

      
        
    });
 </script>
  
@endsection