<?php

namespace App\Models\Rayosx;

use Illuminate\Database\Eloquent\Model;

class Rayosx_director extends Model
{
    protected $table = 'rayosx_director';

    protected $primaryKey = 'id_director_rayosx';

    protected $fillable = [
        'talento_pnombre',
        'talento_snombre',
        'talento_papellido',
        'talento_sapellido',
        'talento_tdocumento',
        'talento_ndocumento',
        'talento_lexpedicion',
        'talento_correo',
        'talento_titulo',
        'talento_universidad',
        'talento_libro',
        'talento_registro',
        'talento_fecha_diploma',
        'talento_resolucion',
        'talento_fecha_convalida',
        'talento_nivel',
        'talento_titulo_pos',
        'talento_universidad_pos',
        'talento_libro_pos',
        'talento_registro_pos',
        'talento_fecha_diploma_pos',
        'talento_resolucion_pos',
        'talento_fecha_convalida_pos',
    ];
}
