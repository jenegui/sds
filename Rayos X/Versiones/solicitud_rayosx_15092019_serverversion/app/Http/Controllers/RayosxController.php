<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use App\Tramite;
use App\Tipo_Tramite_Flujo;
use App\Tramite_Flujo;
use App\Models\Rayosx\Rayosx_direccion;
use App\Models\Rayosx\Rayosx_director;
use App\Models\Rayosx\Rayosx_documentos;
use App\Models\Rayosx\Rayosx_encargado;
use App\Models\Rayosx\Rayosx_equipos;
use App\Models\Rayosx\Rayosx_objprueba;
use App\Models\Rayosx\Rayosx_toe;
use App\Models\Parametricas\Pr_Tipoidentificacion;
use App\user_has_roles;


class RayosxController extends Controller
{


    private $existe_tramite=0;
    private $idTramite=0;
    private $paso=0;
    private $impedirActualizacion=false;


    /**
     * Get all form details
     * @return blade
     */


     public function index(){

       $iduser = Auth::user()->id;       

       $userRoles=user_has_roles::where("user_id",Auth::id())->get();
       //->pluck('id')
       
       $userRolesArray=collect($userRoles)->map(function($x){return (array) $x->role_id;})->toArray();
       
       //tramites que creo el usuario
      /* $tramitesrx=Tramite::select('tramite.id','tipo_tramite.descripcion','tramite.estado','tramite_flujo.actividad','tramite_flujo.fecha_inicio','tramite_flujo.fecha_fin','tramite_flujo.observaciones','tramite.created_at')
       ->join('tipo_tramite','tipo_tramite.id','=','tramite.tipo_tramite')
       ->join('tramite_flujo','tramite_flujo.tramite_id','=','tramite.id')
       ->where('tramite.user', $iduser)
       ->whereNull('tramite_flujo.fecha_fin')
       ->orderBy('tramite.created_at','DESC');

       //tramites 
       $tramitesrx2=Tramite::select('tramite.id','tipo_tramite.descripcion','tramite.estado','tramite_flujo.actividad','tramite_flujo.fecha_inicio','tramite_flujo.fecha_fin','tramite_flujo.observaciones','tramite.created_at')
       ->join('tipo_tramite','tipo_tramite.id','=','tramite.tipo_tramite')
       ->join('tramite_flujo','tramite_flujo.tramite_id','=','tramite.id')       
       ->whereNull('tramite_flujo.fecha_fin')
       ->whereIn('tramite_flujo.user_rol', array($userRoles))
       ->orderBy('tramite.created_at','DESC')       
       ->union($tramitesrx)
       ->paginate(3);*/

       $json =str_replace(']','',str_replace('[','',json_encode($userRolesArray)));

       

       $query="SELECT tramite.id
                FROM tramite,tipo_tramite,tramite_flujo
                WHERE tramite.tipo_tramite=tipo_tramite.id
                AND tramite_flujo.tramite_id=tramite.id
                AND tramite.user=5
                AND tramite_flujo.fecha_fin IS NULL
                UNION ALL
                SELECT tramite.id
                FROM tramite,tipo_tramite,tramite_flujo
                WHERE tramite.tipo_tramite=tipo_tramite.id
                AND tramite_flujo.tramite_id=tramite.id
                AND tramite_flujo.user_rol in (".$json.") 
                AND tramite_flujo.fecha_fin IS NULL";


      $idTramites=DB::select(DB::raw($query));
      $idTramitesList =str_replace(']','',str_replace('[','',json_encode($idTramites)));
      $idTramitesList =str_replace('{"id":','',str_replace('}','',json_encode($idTramites)));

      $tramitesrx=Tramite::select('tramite.id','tipo_tramite.descripcion','tramite.estado','tramite_flujo.actividad','tramite_flujo.fecha_inicio','tramite_flujo.fecha_fin','tramite_flujo.observaciones','tramite.created_at')
       ->join('tipo_tramite','tipo_tramite.id','=','tramite.tipo_tramite')
       ->join('tramite_flujo','tramite_flujo.tramite_id','=','tramite.id')
       ->whereIn('tramite.id', array($idTramitesList))
       ->orderBy('tramite.created_at','DESC')->paginate(3);;

      
          
       
       return view('rayosx.mistramitesrx',['tramitesrx'=>$tramitesrx]);

     }

    public function index2(){
        try{
            //CARGAR DEPARTAMENTOS
            $departamentos = DB::table('pr_departamento')->pluck("IdDepartamento","Descripcion");
            //CARGAR TIPO IDENTIFICACION
            $tipo_identificacion = DB::table('pr_tipoidentificacion')->pluck("IdTipoIdentificacion","Descripcion");
            //CARGAR PROGRAMAS UNIVERSITARIOS
            $pr_programas_univ  = DB::table('pr_programas_univ')->pluck("id_programa","nombre_programa");
            //CARGAR NIVEL ACADEMICO
            $pr_nivelacademico  = DB::table('pr_nivelacademico')->pluck("IdNivelAcademico","Nombre");

            //CARGAR TOE TRAMITE
            $rayosxOficialToe=Rayosx_toe::where('toe_tipo',1)->where('id_tramite_rayosx',0)->orderBy('id_toe_rayosx','DESC')->first();
            $rayosxTemporalToe=Rayosx_toe::where('toe_tipo',2)->where('id_tramite_rayosx',0)->orderBy('id_toe_rayosx','DESC')->paginate(3);
            $rayosxTalento=Rayosx_director::where('id_tramite_rayosx',0)->orderBy('id_director_rayosx','DESC')->get();
            $rayosxObjprueba=Rayosx_objprueba::where('id_tramite_rayosx',0)->orderBy('id_obj_rayosx','DESC')->get();
            $rayosxDocumentos1=Rayosx_documentos::where('id_tramite_rayosx',0)->get();

            return view('rayosx.index',['departamentos'=>$departamentos,
                                        'tiposIdentificacion'=>$tipo_identificacion,
                                        'prNivelacademico'=>$pr_nivelacademico,
                                        'prProgramasUniv'=>$pr_programas_univ,
                                        'oficialTOE'=>$rayosxOficialToe,
                                        'temporalTOE'=>$rayosxTemporalToe,
                                        'talento'=>$rayosxTalento,
                                        'objeto'=>$rayosxObjprueba,
                                        'existeTramite'=>0,'paso'=>0]);
        }
        catch(Exception $e){
            dd($e);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {


      $tipo_tramite=(int)$request->tipo_tramite;
      $visita_previa=(int)$request->visita_previa;
      $idTramite=(int)$request->idTramite;



      //CARGAR DEPARTAMENTOS
      $departamentos = DB::table('pr_departamento')->pluck("IdDepartamento","Descripcion");
      //CARGAR TIPO IDENTIFICACION
      $tipo_identificacion = DB::table('pr_tipoidentificacion')->pluck("IdTipoIdentificacion","Descripcion");
      //CARGAR PROGRAMAS UNIVERSITARIOS
      $pr_programas_univ  = DB::table('pr_programas_univ')->pluck("id_programa","nombre_programa");
      //CARGAR NIVEL ACADEMICO
      $pr_nivelacademico  = DB::table('pr_nivelacademico')->pluck("IdNivelAcademico","Nombre");
      $rayosxOficialToe=Rayosx_toe::where('toe_tipo',1)->where('id_tramite_rayosx',$idTramite)->orderBy('id_toe_rayosx','DESC')->first();
      $rayosxTemporalToe=Rayosx_toe::where('toe_tipo',2)->where('id_tramite_rayosx',$idTramite)->orderBy('id_toe_rayosx','DESC')->paginate(3);

      $rayosxTalento=Rayosx_director::where('id_tramite_rayosx',$idTramite)->orderBy('id_director_rayosx','DESC')->get();
      $rayosxObjprueba=Rayosx_objprueba::where('id_tramite_rayosx',$idTramite)->orderBy('id_obj_rayosx','DESC')->get();


        //REGISTRA LOS DATOS INICIALES DEL TRAMITE
      if($idTramite==0){
        $tramite = new Tramite();
        $tramite->user=Auth::id();
        $tramite->tipo_tramite=1;
        $tramite->estado=1;
        $tramite->save();

        $idTramite=Tramite::where('user',Auth::id())->orderBy('id','DESC')->first();

        // Consulta del flujo del tramite seleccionado -- tipo_tramite=1 el cual corresponde al tramite de rayos x, consultamos la actividad inicial por medio del filtro de flujo_accion_permitida=inicio
        $tipoTramiteFlujo= Tipo_Tramite_Flujo::where('tipo_tramite_id',1)->where('flujo_accion_permitida', 'like','%inicio%')->first();

        //Se crea el flujo del tramite rayos x o historico
        $tramiteFlujo = new Tramite_Flujo();
        $tramiteFlujo->tramite_id=(int)$idTramite->id;
        $tramiteFlujo->actividad=$tipoTramiteFlujo->flujo_actividad_inicial;
        $tramiteFlujo->fecha_inicio=date('Y-m-d H:i:s');
        $tramiteFlujo->user_id=Auth::id();
        $tramiteFlujo->user_rol=$tipoTramiteFlujo->rol_responsable;
        $tramiteFlujo->observaciones="Creación del tramite RAYOS X";
        $tramiteFlujo->save();
      }else{
          $idTramite=Tramite::find('id',$idTramite);

      }



      /*$id = DB::table('tramite')->insertGetId(
          ['user' => Auth::id(),'tipo_tramite' => 1,'estado' => 1]
      );*/


      return view('rayosx.index', ['idTramite' => $idTramite,
                                    'existeTramite'=>1,
                                    'departamentos'=>$departamentos,
                                    'tiposIdentificacion'=>$tipo_identificacion,
                                    'prNivelacademico'=>$pr_nivelacademico,
                                    'prProgramasUniv'=>$pr_programas_univ,
                                    'oficialTOE'=>$rayosxOficialToe,
                                    'temporalTOE'=>$rayosxTemporalToe,
                                    'talento'=>$rayosxTalento,
                                    'objeto'=>$rayosxObjprueba,
                                    'paso'=>1, //Paso 1 - localizacion entidad
                                    'visitaPrevia'=>$visita_previa]);
    }

    public function crearTramiteRayosx(){
        try{
            //CARGAR DEPARTAMENTOS
            $departamentos = DB::table('pr_departamento')->pluck("IdDepartamento","Descripcion");
            //CARGAR TIPO IDENTIFICACION
            $tipo_identificacion = DB::table('pr_tipoidentificacion')->pluck("IdTipoIdentificacion","Descripcion");
            //CARGAR PROGRAMAS UNIVERSITARIOS
            $pr_programas_univ  = DB::table('pr_programas_univ')->pluck("id_programa","nombre_programa");
            //CARGAR NIVEL ACADEMICO
            $pr_nivelacademico  = DB::table('pr_nivelacademico')->pluck("IdNivelAcademico","Nombre");

            //CARGAR TOE TRAMITE
            $rayosxOficialToe=Rayosx_toe::where('toe_tipo',1)->where('id_tramite_rayosx',0)->orderBy('id_toe_rayosx','DESC')->first();
            $rayosxTemporalToe=Rayosx_toe::where('toe_tipo',2)->where('id_tramite_rayosx',0)->orderBy('id_toe_rayosx','DESC')->paginate(3);
            $rayosxTalento=Rayosx_director::where('id_tramite_rayosx',0)->orderBy('id_director_rayosx','DESC')->get();
            $rayosxObjprueba=Rayosx_objprueba::where('id_tramite_rayosx',0)->orderBy('id_obj_rayosx','DESC')->paginate(3);
            $rayosxDocumentos1=Rayosx_documentos::where('id_tramite_rayosx',0)->get();
            $idTramiterx=NULL;

            return view('rayosx.index',[ 'idTramiterx'=>$idTramiterx,'departamentos'=>$departamentos,
                                          'tiposIdentificacion'=>$tipo_identificacion,
                                          'prNivelacademico'=>$pr_nivelacademico,
                                          'prProgramasUniv'=>$pr_programas_univ]);
        }
        catch(Exception $e){
            dd($e);
        }
    }
    /**
     * Saves the institution's location and shows equipment x-ray form
     *
     * @return \Illuminate\Http\Response
     */

     public function guardarTramiteRayosx(Request $request)
     {


         $input = Input::all();
         $tramite = new Tramite();
         $tramite->user=Auth::id();
         $tramite->tipo_tramite=1;
         $tramite->estado=1;
         $tramite->save();

         $idTramite=Tramite::where('user',Auth::id())->orderBy('id','DESC')->first();
         //CARGAR DEPARTAMENTOS
         $departamentos = DB::table('pr_departamento')->pluck("IdDepartamento","Descripcion");
         //CARGAR TIPO IDENTIFICACION
         $tipo_identificacion = DB::table('pr_tipoidentificacion')->pluck("IdTipoIdentificacion","Descripcion");
         //CARGAR PROGRAMAS UNIVERSITARIOS
         $pr_programas_univ  = DB::table('pr_programas_univ')->pluck("id_programa","nombre_programa");
         //CARGAR NIVEL ACADEMICO
         $pr_nivelacademico  = DB::table('pr_nivelacademico')->pluck("IdNivelAcademico","Nombre");

         /*
         // Consulta del flujo del tramite seleccionado -- tipo_tramite=1 el cual corresponde al tramite de rayos x, consultamos la actividad inicial por medio del filtro de flujo_accion_permitida=inicio*/
         $tipoTramiteFlujo= Tipo_Tramite_Flujo::where('tipo_tramite_id',1)->where('flujo_accion_permitida', 'like','%inicio%')->first();

         //Se crea el flujo del tramite rayos x o historico
         $tramiteFlujo = new Tramite_Flujo();
         $tramiteFlujo->tramite_id=(int)$idTramite->id;
         $tramiteFlujo->tipo_tramite=1;
         $tramiteFlujo->actividad=$tipoTramiteFlujo->flujo_actividad_inicial;
         $tramiteFlujo->fecha_inicio=date('Y-m-d H:i:s');
         $tramiteFlujo->fecha_fin=null;
         $tramiteFlujo->user_id=Auth::id();
         //La estacion inicial debe corresponder al ciudadano
         $tramiteFlujo->user_rol=$tipoTramiteFlujo->rol_responsable;
         $tramiteFlujo->observaciones="Creación del tramite RAYOS X";
         $tramiteFlujo->save();
         



       /*$id = DB::table('tramite')->insertGetId(
           ['user' => Auth::id(),'tipo_tramite' => 1,'estado' => 1]
       );*/


       $iduser = Auth::user()->id;
       $userRoles=user_has_roles::where("user_id",Auth::id())->get();
       
       //tramites que creo el usuario
       $tramitesrx=Tramite::select('tramite.id','tipo_tramite.descripcion','tramite.estado','tramite_flujo.actividad','tramite_flujo.fecha_inicio','tramite_flujo.fecha_fin','tramite_flujo.observaciones','tramite.created_at')
       ->join('tipo_tramite','tipo_tramite.id','=','tramite.tipo_tramite')
       ->join('tramite_flujo','tramite_flujo.tramite_id','=','tramite.id')
       ->where('tramite.user', $iduser)
       ->whereNull('tramite_flujo.fecha_fin')
       ->orderBy('tramite.created_at','DESC');

       //tramites 
       $tramitesrx2=Tramite::select('tramite.id','tipo_tramite.descripcion','tramite.estado','tramite_flujo.actividad','tramite_flujo.fecha_inicio','tramite_flujo.fecha_fin','tramite_flujo.observaciones','tramite.created_at')
       ->join('tipo_tramite','tipo_tramite.id','=','tramite.tipo_tramite')
       ->join('tramite_flujo','tramite_flujo.tramite_id','=','tramite.id')       
       ->whereNull('tramite_flujo.fecha_fin')
       ->whereIn('tramite_flujo.user_rol', array($userRoles))
       ->orderBy('tramite.created_at','DESC')       
       ->union($tramitesrx)
       ->paginate(3);
       //dd($tramitesrx);
       return view('rayosx.mistramitesrx',['tramitesrx'=>$tramitesrx2]);
     }


    public function edit($id_tramite) {

        $idTramite=$id_tramite;

        $puedeEditarSolicitud=FALSE;
        $puedeAvanzarSolicitud=FALSE;


        $actividadInicial=Tramite_Flujo::where("tramite_id",$idTramite)->orderBy('id','ASC')->first();
        $actividadActual=Tramite_Flujo::where("tramite_id",$idTramite)->orderBy('id','DESC')->first();

        //dd($actividadActual);
        
        $rolResponsable=user_has_roles::where("user_id",Auth::id())->where('role_id',$actividadActual->user_rol)->first();
        
        $accionesFlujo=Tipo_Tramite_Flujo::where('flujo_actividad_inicial',$actividadActual->actividad)->where('tipo_tramite',1)->get();



        

        if($actividadInicial != NULL && $actividadActual != NULL){
            if($actividadInicial->id == $actividadActual->id){
                if(Auth::id() == $actividadInicial->user_id){
                    //Se encuentra en la actividad inicial y el usuario autenticado es el encargado
                    $puedeEditarSolicitud=TRUE;
                }
            }

            if($rolResponsable != NULL){
                $puedeAvanzarSolicitud=TRUE;
            }

        }

        //dd($puedeAvanzarSolicitud);
        //dd($puedeEditarSolicitud);


        $idTramiterx=Tramite::where('id',$idTramite)->where('user',Auth::id())->orderBy('id','DESC')->first();
        $rayosxOficialToe=Rayosx_toe::where('toe_tipo',1)->where('id_tramite_rayosx',$idTramite)->orderBy('id_toe_rayosx','DESC')->first();

        $rayosxTemporalToe=Rayosx_toe::where('toe_tipo',2)->where('id_tramite_rayosx',$idTramite)->orderBy('id_toe_rayosx','DESC')->paginate(3);
        $rayosxTalento=Rayosx_director::where('id_tramite_rayosx',$idTramite)->orderBy('id_director_rayosx','DESC')->first();
        $rayosxObjprueba=Rayosx_objprueba::where('id_tramite_rayosx',$idTramite)->orderBy('id_obj_rayosx','DESC')->paginate(3);
        $rayosxDocumentos=Rayosx_documentos::where('id_tramite_rayosx',$idTramite)->first();
        $rayosxDireccion = Rayosx_direccion::where('id_tramite_rayosx',$id_tramite)->first();
        //dd($rayosxDocumentos);

        

        $departamentos = DB::table('pr_departamento')->pluck("IdDepartamento","Descripcion");
        //dd($departamentos);
        $tipo_identificacion = DB::table('pr_tipoidentificacion')->pluck("IdTipoIdentificacion","Descripcion");
        $pr_nivelacademico  = DB::table('pr_nivelacademico')->pluck("IdNivelAcademico","Nombre");
        $pr_programas_univ  = DB::table('pr_programas_univ')->pluck("id_programa","nombre_programa");
        $rayosxOficialToe=Rayosx_toe::where('toe_tipo',1)->where('id_tramite_rayosx',$idTramite)->orderBy('id_toe_rayosx','DESC')->first();

        $rayosxTemporalToe=Rayosx_toe::where('toe_tipo',2)->where('id_tramite_rayosx',$idTramite)->orderBy('id_toe_rayosx','DESC')->paginate(3);

        $rayosxTalento=Rayosx_director::where('id_tramite_rayosx',$idTramite)->orderBy('id_director_rayosx','DESC')->first();
        $rayosxObjprueba=Rayosx_objprueba::where('id_tramite_rayosx',$idTramite)->orderBy('id_obj_rayosx','DESC')->paginate(3);

        $rayosxDireccion = Rayosx_direccion::where('id_tramite_rayosx',$id_tramite)->first();
        $rayosxEquipo = Rayosx_equipos::where('id_tramite_rayosx',$id_tramite)->first();
        $rayosxDocumentos1=Rayosx_documentos::where('categoria','1')->where('id_tramite_rayosx',$id_tramite)->get();
        $rayosxDocumentos2=Rayosx_documentos::where('categoria','2')->where('id_tramite_rayosx',$id_tramite)->get();
        
        return view('rayosx.index2',['idTramiterx'=>$idTramiterx,'departamentos'=>$departamentos,
        'tiposIdentificacion'=>$tipo_identificacion,
        'prNivelacademico'=>$pr_nivelacademico,
        'prProgramasUniv'=>$pr_programas_univ,
        'rayosxOficialToe'=>$rayosxOficialToe,
        'rayosxTemporalToe'=>$rayosxTemporalToe,
        'rayosxTalento'=>$rayosxTalento,
        'rayosxObjprueba'=>$rayosxObjprueba,
        'rayosxDireccion'=>$rayosxDireccion,
        'rayosxEquipo'=>$rayosxEquipo,
        'rayosxDocumentos'=>$rayosxDocumentos,
        'usuarioInicial'=>$actividadInicial->user_id,
        'rolResponsable'=>$actividadActual->user_id,
        'puedeEditarSolicitud'=>$puedeEditarSolicitud,
        'puedeAvanzarSolicitud'=>$puedeAvanzarSolicitud,
        'accionesFlujo'=>$accionesFlujo]);
      
      /*else{
        //CARGAR DEPARTAMENTOS
        $departamentos = DB::table('pr_departamento')->pluck("IdDepartamento","Descripcion");
        //CARGAR TIPO IDENTIFICACION
        $tipo_identificacion = DB::table('pr_tipoidentificacion')->pluck("IdTipoIdentificacion","Descripcion");
        //CARGAR PROGRAMAS UNIVERSITARIOS
        $pr_programas_univ  = DB::table('pr_programas_univ')->pluck("id_programa","nombre_programa");
        //CARGAR NIVEL ACADEMICO
        $pr_nivelacademico  = DB::table('pr_nivelacademico')->pluck("IdNivelAcademico","Nombre");

        //CARGAR TOE TRAMITE

        $rayosxTalento=Rayosx_director::where('id_tramite_rayosx',$idTramite)->orderBy('id_director_rayosx','DESC')->first();

        $rayosxOficialToe=Rayosx_toe::where('toe_tipo',1)->where('id_tramite_rayosx',$id_tramite)->orderBy('id_toe_rayosx','DESC')->first();
        //dd($rayosxOficialToe);
        $rayosxTemporalToe=Rayosx_toe::where('toe_tipo',2)->where('id_tramite_rayosx',$idTramite)->orderBy('id_toe_rayosx','DESC')->paginate(3);
        $rayosxTalento=Rayosx_director::where('id_tramite_rayosx',$idTramite)->orderBy('id_director_rayosx','DESC')->first();
        //dd($rayosxTalento);
        $rayosxObjprueba=Rayosx_objprueba::where('id_tramite_rayosx',$idTramite)->orderBy('id_obj_rayosx','DESC')->paginate(3);
        $rayosxDocumentos=Rayosx_documentos::where('id_tramite_rayosx',$idTramite)->first();
        $idTramiterx=Tramite::where('id',$idTramite)->where('user',Auth::id())->orderBy('id','DESC')->first();
        $rayosxEquipo = Rayosx_equipos::where('id_tramite_rayosx',$idTramite)->first();

        $alert="Pendiente";
        //dd($rayosxEquipo);
        //dd($idTramiterx);
        return view('rayosx.index2',[ 'idTramiterx'=>$idTramiterx,'departamentos'=>$departamentos,
                                      'tiposIdentificacion'=>$tipo_identificacion,
                                      'prNivelacademico'=>$pr_nivelacademico,
                                      'prProgramasUniv'=>$pr_programas_univ,
                                      'rayosxOficialToe'=>$rayosxOficialToe,
                                      'rayosxTemporalToe'=>$rayosxTemporalToe,
                                      'rayosxTalento'=>$rayosxTalento,
                                      'rayosxObjprueba'=>$rayosxObjprueba,
                                      'rayosxDireccion'=>$rayosxDireccion,
                                      'rayosxEquipo'=>$rayosxEquipo,
                                      'rayosxDocumentos'=>$rayosxDocumentos,
                                        'usuarioInicial'=>$actividadInicial->user_id,
                                        'rolResponsable'=>$actividadActual->user_id]);
      }*/
        /*
          if($post != null){
            $users = User::where('id',$post->user_id)->firstorFail();
            $data['post'] = $post;
            $data['comments'] = Comment::where('post_id', $id)->get();
            return view('adminlte::posts.post',$data,['users'=>$users]);
          }*/
    }

    public function editarDireccion() {


      $input = Input::all();
      //dd($input);

         if($input['id_tramite'] == NULL) {
             return redirect()->route('tramites.index');
         }
         else {
         $rayosxDireccion = Rayosx_direccion::where('id_tramite_rayosx',$input['id_tramite'])->firstorFail();

         $rayosxDireccion->depto_entidad = $input['depto_entidad'];
         $rayosxDireccion->mpio_entidad = $input['mpio_entidad'];
         $rayosxDireccion->dire_entidad = $input['dire_entidad'];
         $rayosxDireccion->sede_entidad = $input['sede_entidad'];
         $rayosxDireccion->email_entidad = $input['email_entidad'];
         $rayosxDireccion->celular_entidad = $input['celular_entidad'];
         $rayosxDireccion->telefono_entidad = $input['telefono_entidad'];
         $rayosxDireccion->extension_entidad = $input['extension_entidad'];
         $rayosxDireccion->save(); // Guarda el objeto en la BD



         $departamentos = DB::table('pr_departamento')->pluck("IdDepartamento","Descripcion");
         $tipo_identificacion = DB::table('pr_tipoidentificacion')->pluck("IdTipoIdentificacion","Descripcion");
         $pr_nivelacademico  = DB::table('pr_nivelacademico')->pluck("IdNivelAcademico","Nombre");
         $pr_programas_univ  = DB::table('pr_programas_univ')->pluck("id_programa","nombre_programa");
         $rayosxOficialToe=Rayosx_toe::where('toe_tipo',1)->where('id_tramite_rayosx',$input['id_tramite'])->orderBy('id_toe_rayosx','DESC')->first();

         $rayosxTemporalToe=Rayosx_toe::where('toe_tipo',2)->where('id_tramite_rayosx',$input['id_tramite'])->orderBy('id_toe_rayosx','DESC')->paginate(3);

         $rayosxTalento=Rayosx_director::where('id_tramite_rayosx',$input['id_tramite'])->orderBy('id_director_rayosx','DESC')->firstorFail();
         //dd($rayosxTalento);
         $rayosxObjprueba=Rayosx_objprueba::where('id_tramite_rayosx',$input['id_tramite'])->orderBy('id_obj_rayosx','DESC')->paginate(3);

         $rayosxDireccion = Rayosx_direccion::where('id_tramite_rayosx',$input['id_tramite'])->first();
         $rayosxEquipo = Rayosx_equipos::where('id_tramite_rayosx',$input['id_tramite'])->firstorFail();
         //dd($rayosxEquipo);

         return redirect('/ventanilla/tramites/solicitud_rayosx/tramite/'.$input['id_tramite']);
/*
         return view('rayosx.editartramiterx',['departamentos'=>$departamentos,
         'tiposIdentificacion'=>$tipo_identificacion,
         'prNivelacademico'=>$pr_nivelacademico,
         'prProgramasUniv'=>$pr_programas_univ,
         'rayosxOficialToe'=>$rayosxOficialToe,
         'rayosxTemporalToe'=>$rayosxTemporalToe,
         'rayosxTalento'=>$rayosxTalento,
         'rayosxObjprueba'=>$rayosxObjprueba,
         'rayosxDireccion'=>$rayosxDireccion,
         'rayosxEquipo'=>$rayosxEquipo]);
*/
       }
    }

    public function editarEquipo() {


      $input = Input::all();
      //dd($input);

         if($input['id_tramite'] == NULL) {
             return redirect()->route('tramites.index');
         }
         else {
         $rayosxEquipo = Rayosx_equipos::where('id_tramite_rayosx',$input['id_tramite'])->firstorFail();

         $rayosxEquipo->categoria = $input['categoria'];
         $rayosxEquipo->categoria1 = $input['categoria1'];
         $rayosxEquipo->categoria1_1 = $input['categoria1_1'];
         //$rayosxEquipo->categoria1_2 = $input['categoria1_2'];
         $rayosxEquipo->categoria2 = $input['categoria2'];
         $rayosxEquipo->tipo_visualizacion = $input['tipo_visualizacion'];
         $rayosxEquipo->marca_equipo = $input['marca_equipo'];
         $rayosxEquipo->modelo_equipo = $input['modelo_equipo'];
         $rayosxEquipo->serie_equipo = $input['serie_equipo'];
         $rayosxEquipo->marca_tubo_rx = $input['marca_tubo_rx'];
         $rayosxEquipo->modelo_tubo_rx = $input['modelo_tubo_rx'];
         $rayosxEquipo->serie_tubo_rx = $input['serie_tubo_rx'];
         $rayosxEquipo->tension_tubo_rx = $input['tension_tubo_rx'];
         $rayosxEquipo->contiene_tubo_rx = $input['contiene_tubo_rx'];
         $rayosxEquipo->energia_fotones = $input['energia_fotones'];
         $rayosxEquipo->energia_electrones = $input['energia_electrones'];
         $rayosxEquipo->ubicacion_equipo = $input['ubicacion_equipo'];
         $rayosxEquipo->numero_permiso = $input['numero_permiso'];
         $rayosxEquipo->anio_fabricacion = $input['anio_fabricacion'];
         $rayosxEquipo->anio_fabricacion_tubo = $input['anio_fabricacion_tubo'];
         $rayosxEquipo->estado = 1;
         $rayosxEquipo->save(); // Guarda el objeto en la BD



         $departamentos = DB::table('pr_departamento')->pluck("IdDepartamento","Descripcion");
         $tipo_identificacion = DB::table('pr_tipoidentificacion')->pluck("IdTipoIdentificacion","Descripcion");
         $pr_nivelacademico  = DB::table('pr_nivelacademico')->pluck("IdNivelAcademico","Nombre");
         $pr_programas_univ  = DB::table('pr_programas_univ')->pluck("id_programa","nombre_programa");
         $rayosxOficialToe=Rayosx_toe::where('toe_tipo',1)->where('id_tramite_rayosx',$input['id_tramite'] )->orderBy('id_toe_rayosx','DESC')->first();

         $rayosxTemporalToe=Rayosx_toe::where('toe_tipo',2)->where('id_tramite_rayosx',$input['id_tramite'] )->orderBy('id_toe_rayosx','DESC')->paginate(3);

         $rayosxTalento=Rayosx_director::where('id_tramite_rayosx',$input['id_tramite'] )->orderBy('id_director_rayosx','DESC')->first();
         $rayosxObjprueba=Rayosx_objprueba::where('id_tramite_rayosx',$input['id_tramite'] )->orderBy('id_obj_rayosx','DESC')->paginate(3);

         $rayosxDireccion = Rayosx_direccion::where('id_tramite_rayosx',$input['id_tramite'] )->firstorFail();
         $rayosxEquipo = Rayosx_equipos::where('id_tramite_rayosx',$input['id_tramite'] )->firstorFail();
         //dd($rayosxEquipo);


       return redirect('/ventanilla/tramites/solicitud_rayosx/tramite/'.$input['id_tramite']);
       }
    }

    public function editarOficialToe() {


      $input = Input::all();
      //dd($input);

         if($input['id_tramite'] == NULL) {
             return redirect()->route('tramites.index');
         }
         else {
         $rayosxOficialToe = Rayosx_toe::where('toe_tipo',1)->where('id_tramite_rayosx',$input['id_tramite'] )->firstorFail();

         $rayosxOficialToe->toe_pnombre = $input['encargado_pnombre'];
         $rayosxOficialToe->toe_snombre = $input['encargado_snombre'];
         $rayosxOficialToe->toe_papellido = $input['encargado_papellido'];
         $rayosxOficialToe->toe_sapellido= $input['encargado_sapellido'];
         $rayosxOficialToe->toe_tdocumento = $input['encargado_tdocumento'];
         $rayosxOficialToe->toe_ndocumento = $input['encargado_ndocumento'];
         $rayosxOficialToe->toe_lexpedicion = $input['encargado_lexpedicion'];
         $rayosxOficialToe->toe_correo = $input['encargado_correo'];
         $rayosxOficialToe->toe_profesion = $input['encargado_nivel'];
         $rayosxOficialToe->toe_nivel = $input['encargado_profesion'];
         $rayosxOficialToe->save(); // Guarda el objeto en la BD



         $departamentos = DB::table('pr_departamento')->pluck("IdDepartamento","Descripcion");
         $tipo_identificacion = DB::table('pr_tipoidentificacion')->pluck("IdTipoIdentificacion","Descripcion");
         $pr_nivelacademico  = DB::table('pr_nivelacademico')->pluck("IdNivelAcademico","Nombre");
         $pr_programas_univ  = DB::table('pr_programas_univ')->pluck("id_programa","nombre_programa");
         $rayosxOficialToe=Rayosx_toe::where('toe_tipo',1)->where('id_tramite_rayosx',$input['id_tramite'] )->orderBy('id_toe_rayosx','DESC')->first();

         $rayosxTemporalToe=Rayosx_toe::where('toe_tipo',2)->where('id_tramite_rayosx',$input['id_tramite'] )->orderBy('id_toe_rayosx','DESC')->paginate(3);

         $rayosxTalento=Rayosx_director::where('id_tramite_rayosx',$input['id_tramite'] )->orderBy('id_director_rayosx','DESC')->firstorFail();
         $rayosxObjprueba=Rayosx_objprueba::where('id_tramite_rayosx',$input['id_tramite'] )->orderBy('id_obj_rayosx','DESC')->paginate(3);

         $rayosxDireccion = Rayosx_direccion::where('id_tramite_rayosx',$input['id_tramite'] )->firstorFail();
         $rayosxEquipo = Rayosx_equipos::where('id_tramite_rayosx',$input['id_tramite'] )->firstorFail();
         //dd($rayosxEquipo);


       return redirect('/ventanilla/tramites/solicitud_rayosx/tramite/'.$input['id_tramite']);
       }
    }

    public function editarDirector() {


      $input = Input::all();
      //dd($input);
      $id_tramite = $input['id_tramite'];
         if($input['id_tramite'] == NULL) {
             return redirect()->route('tramites.index');
         }
         else {
         $rayosxTalento = Rayosx_director::where('id_tramite_rayosx',$input['id_tramite'] )->firstorFail();

         $rayosxTalento->talento_pnombre = $input['talento_pnombre'];
         $rayosxTalento->talento_snombre = $input['talento_snombre'];
         $rayosxTalento->talento_papellido = $input['talento_papellido'];
         $rayosxTalento->talento_sapellido= $input['talento_sapellido'];
         $rayosxTalento->talento_tdocumento = $input['talento_tdocumento'];
         $rayosxTalento->talento_ndocumento = $input['talento_ndocumento'];
         $rayosxTalento->talento_lexpedicion = $input['talento_lexpedicion'];
         $rayosxTalento->talento_correo = $input['talento_correo'];
         $rayosxTalento->talento_titulo = $input['talento_titulo'];
         $rayosxTalento->talento_universidad = $input['talento_universidad'];
         $rayosxTalento->talento_libro = $input['talento_libro'];
         $rayosxTalento->talento_registro = $input['talento_registro'];
         $rayosxTalento->talento_fecha_diploma = $input['talento_fecha_diploma'];
         $rayosxTalento->talento_resolucion = $input['talento_resolucion'];
         $rayosxTalento->talento_fecha_convalida = $input['talento_fecha_convalida'];
         $rayosxTalento->talento_nivel = $input['talento_nivel'];
         $rayosxTalento->talento_titulo_pos = $input['talento_titulo_pos'];
         $rayosxTalento->talento_universidad_pos = $input['talento_universidad_pos'];
         $rayosxTalento->talento_libro_pos = $input['talento_libro_pos'];
         $rayosxTalento->talento_registro_pos = $input['talento_registro_pos'];
         $rayosxTalento->talento_fecha_diploma_pos = $input['talento_fecha_diploma_pos'];
         $rayosxTalento->talento_resolucion_pos = $input['talento_resolucion_pos'];
         $rayosxTalento->talento_fecha_convalida_pos = $input['talento_fecha_convalida_pos'];
         $rayosxTalento->save(); // Guarda el objeto en la BD



         $departamentos = DB::table('pr_departamento')->pluck("IdDepartamento","Descripcion");
         $tipo_identificacion = DB::table('pr_tipoidentificacion')->pluck("IdTipoIdentificacion","Descripcion");
         $pr_nivelacademico  = DB::table('pr_nivelacademico')->pluck("IdNivelAcademico","Nombre");
         $pr_programas_univ  = DB::table('pr_programas_univ')->pluck("id_programa","nombre_programa");
         $rayosxOficialToe=Rayosx_toe::where('toe_tipo',1)->where('id_tramite_rayosx',$input['id_tramite'] )->orderBy('id_toe_rayosx','DESC')->first();

         $rayosxTemporalToe=Rayosx_toe::where('toe_tipo',2)->where('id_tramite_rayosx',$input['id_tramite'] )->orderBy('id_toe_rayosx','DESC')->paginate(3);

         $rayosxTalento=Rayosx_director::where('id_tramite_rayosx',$input['id_tramite'] )->orderBy('id_director_rayosx','DESC')->firstorFail();
         $rayosxObjprueba=Rayosx_objprueba::where('id_tramite_rayosx',$input['id_tramite'] )->orderBy('id_obj_rayosx','DESC')->paginate(3);

         $rayosxDireccion = Rayosx_direccion::where('id_tramite_rayosx',$input['id_tramite'] )->firstorFail();
         $rayosxEquipo = Rayosx_equipos::where('id_tramite_rayosx',$input['id_tramite'] )->firstorFail();
         //dd($rayosxEquipo);


       return redirect('/ventanilla/tramites/solicitud_rayosx/tramite/'.$input['id_tramite']);
       }
    }


    public function editarDocumentos1(Request $request)
    {
        $input = Input::all();


        //$rayosxDocumentos2=Rayosx_documentos::where('categoria','2')->where('id_tramite_rayosx',$input['id_tramite'])->get();

        $idrayosxDocumentos=Rayosx_documentos::where('categoria','2')->where('id_tramite_rayosx',$input['id_tramite'])->where('documento','fi_doc_encargado')->first();
        $iddocumento = $idrayosxDocumentos->id_documentos_rayosx;

        if($input['fi_doc_encargado'] != NULL){
          $rayosxdocumentos=Rayosx_documentos::find($iddocumento);
          $path = $input['fi_doc_encargado']->store('public');
          $rayosxdocumentos->id_tramite_rayosx = $input['id_tramite'];
          $rayosxdocumentos->path = $path;
          $rayosxdocumentos->categoria = 1;
          $rayosxdocumentos->documento = 'fi_doc_encargado';
          $rayosxdocumentos->id_archivo = 1;
          $rayosxdocumentos->estado = 1;
          $rayosxdocumentos->save();
        }

        $idrayosxDocumentos=Rayosx_documentos::where('categoria','2')->where('id_tramite_rayosx',$input['id_tramite'])->where('documento','fi_diploma_encargado')->first();
        $iddocumento = $idrayosxDocumentos->id_documentos_rayosx;

        if($input['fi_diploma_encargado'] != NULL){
          $rayosxdocumentos=Rayosx_documentos::find($iddocumento);
          $path = $input['fi_diploma_encargado']->store('public');
          $rayosxdocumentos->id_tramite_rayosx = $input['id_tramite'];
          $rayosxdocumentos->path = $path;
          $rayosxdocumentos->categoria = 1;
          $rayosxdocumentos->documento = 'fi_diploma_encargado';
          $rayosxdocumentos->id_archivo = 1;
          $rayosxdocumentos->estado = 1;
          $rayosxdocumentos->save();
        }

        $idrayosxDocumentos=Rayosx_documentos::where('categoria','2')->where('id_tramite_rayosx',$input['id_tramite'])->where('documento','fi_blindajes')->first();
        $iddocumento = $idrayosxDocumentos->id_documentos_rayosx;

        if($input['fi_blindajes'] != NULL){
          $path =$input['fi_blindajes']->store('public');
          $rayosxdocumentos=Rayosx_documentos::find($iddocumento);
          $rayosxdocumentos->id_tramite_rayosx = $input['id_tramite'];
          $rayosxdocumentos->path = $path;
          $rayosxdocumentos->categoria = 1;
          $rayosxdocumentos->documento = 'fi_blindajes';
          $rayosxdocumentos->id_archivo = 1;
          $rayosxdocumentos->estado = 1;
          $rayosxdocumentos->save();
        }

        $idrayosxDocumentos=Rayosx_documentos::where('categoria','2')->where('id_tramite_rayosx',$input['id_tramite'])->where('documento','fi_control_calidad')->first();
        $iddocumento = $idrayosxDocumentos->id_documentos_rayosx;

        if($input['fi_control_calidad'] != NULL){
          $rayosxdocumentos=Rayosx_documentos::find($iddocumento);
          $path = $input['fi_control_calidad']->store('public');
          $rayosxdocumentos->id_tramite_rayosx = $input['id_tramite'];
          $rayosxdocumentos->path = $path;
          $rayosxdocumentos->categoria = 1;
          $rayosxdocumentos->documento = 'fi_control_calidad';
          $rayosxdocumentos->id_archivo = 1;
          $rayosxdocumentos->estado = 1;
          $rayosxdocumentos->save();
        }

        $idrayosxDocumentos=Rayosx_documentos::where('categoria','2')->where('id_tramite_rayosx',$input['id_tramite'])->where('documento','fi_registro_dosimetrico')->first();
        $iddocumento = $idrayosxDocumentos->id_documentos_rayosx;

        if($input['fi_registro_dosimetrico'] != NULL){
          $rayosxdocumentos=Rayosx_documentos::find($iddocumento);
          $path = $input['fi_registro_dosimetrico']->store('public');
          $rayosxdocumentos->id_tramite_rayosx = $input['id_tramite'];
          $rayosxdocumentos->path = $path;
          $rayosxdocumentos->categoria = 1;
          $rayosxdocumentos->documento = 'fi_registro_dosimetrico';
          $rayosxdocumentos->id_archivo = 1;
          $rayosxdocumentos->estado = 1;
          $rayosxdocumentos->save();
        }

        $idrayosxDocumentos=Rayosx_documentos::where('categoria','2')->where('id_tramite_rayosx',$input['id_tramite'])->where('documento','fi_registro_niveles')->first();
        $iddocumento = $idrayosxDocumentos->id_documentos_rayosx;

        if($input['fi_registro_niveles'] != NULL){
          $rayosxdocumentos=Rayosx_documentos::find($iddocumento);
          $path = $input['fi_registro_niveles']->store('public');
          $rayosxdocumentos->id_tramite_rayosx = $input['id_tramite'];
          $rayosxdocumentos->path = $path;
          $rayosxdocumentos->categoria = 1;
          $rayosxdocumentos->documento = 'fi_registro_niveles';
          $rayosxdocumentos->id_archivo = 1;
          $rayosxdocumentos->estado = 1;
          $rayosxdocumentos->save();
        }

        $idrayosxDocumentos=Rayosx_documentos::where('categoria','2')->where('id_tramite_rayosx',$input['id_tramite'])->where('documento','fi_plano')->first();
        $iddocumento = $idrayosxDocumentos->id_documentos_rayosx;

        if($input['fi_plano'] != NULL){
          $rayosxdocumentos=Rayosx_documentos::find($iddocumento);
          $path = $input['fi_plano']->store('public');
          $rayosxdocumentos->id_tramite_rayosx = $input['id_tramite'];
          $rayosxdocumentos->path = $path;
          $rayosxdocumentos->categoria = 1;
          $rayosxdocumentos->documento = 'fi_plano';
          $rayosxdocumentos->id_archivo = 1;
          $rayosxdocumentos->estado = 1;
          $rayosxdocumentos->save();
        }

        $idrayosxDocumentos=Rayosx_documentos::where('categoria','2')->where('id_tramite_rayosx',$input['id_tramite'])->where('documento','fi_certificado_capacitacion')->first();
        $iddocumento = $idrayosxDocumentos->id_documentos_rayosx;

        if($input['fi_certificado_capacitacion'] != NULL){
          $path = $input['fi_certificado_capacitacion']->store('public');
          $rayosxdocumentos=Rayosx_documentos::find($iddocumento);
          $rayosxdocumentos->id_tramite_rayosx = $input['id_tramite'];
          $rayosxdocumentos->path = $path;
          $rayosxdocumentos->categoria = 1;
          $rayosxdocumentos->documento = 'fi_certificado_capacitacion';
          $rayosxdocumentos->id_archivo = 1;
          $rayosxdocumentos->estado = 1;
          $rayosxdocumentos->save();
        }

        $idrayosxDocumentos=Rayosx_documentos::where('categoria','2')->where('id_tramite_rayosx',$input['id_tramite'])->where('documento','fi_programa_capacitacion')->first();
        $iddocumento = $idrayosxDocumentos->id_documentos_rayosx;

        if($input['fi_programa_capacitacion'] != NULL){
          $path = $input['fi_programa_capacitacion']->store('public');
          $rayosxdocumentos=Rayosx_documentos::find($iddocumento);
          $rayosxdocumentos->id_tramite_rayosx = $input['id_tramite'];
          $rayosxdocumentos->path = $path;
          $rayosxdocumentos->categoria = 1;
          $rayosxdocumentos->documento = 'fi_programa_capacitacion';
          $rayosxdocumentos->id_archivo = 1;
          $rayosxdocumentos->estado = 1;
          $rayosxdocumentos->save();
        }

        $idrayosxDocumentos=Rayosx_documentos::where('categoria','2')->where('id_tramite_rayosx',$input['id_tramite'])->where('documento','fi_procedimiento_mantenimiento')->first();
        $iddocumento = $idrayosxDocumentos->id_documentos_rayosx;

        if($input['fi_procedimiento_mantenimiento'] != NULL){
          $path = $input['fi_procedimiento_mantenimiento']->store('public');
          $rayosxdocumentos=Rayosx_documentos::find($iddocumento);
          $rayosxdocumentos->id_tramite_rayosx = $input['id_tramite'];
          $rayosxdocumentos->path = $path;
          $rayosxdocumentos->categoria = 1;
          $rayosxdocumentos->documento = 'fi_procedimiento_mantenimiento';
          $rayosxdocumentos->id_archivo = 1;
          $rayosxdocumentos->estado = 1;
          $rayosxdocumentos->save();
        }

        $idrayosxDocumentos=Rayosx_documentos::where('categoria','2')->where('id_tramite_rayosx',$input['id_tramite'])->where('documento','fi_pruebas_caracterizacion')->first();
        $iddocumento = $idrayosxDocumentos->id_documentos_rayosx;

        if($input['fi_pruebas_caracterizacion'] != NULL){
          $path = $input['fi_pruebas_caracterizacion']->store('public');
          $rayosxdocumentos=Rayosx_documentos::find($iddocumento);
          $rayosxdocumentos->id_tramite_rayosx = $input['id_tramite'];
          $rayosxdocumentos->path = $path;
          $rayosxdocumentos->categoria = 1;
          $rayosxdocumentos->documento = 'fi_pruebas_caracterizacion';
          $rayosxdocumentos->id_archivo = 1;
          $rayosxdocumentos->estado = 1;
          $rayosxdocumentos->save();
        }

        $idrayosxDocumentos=Rayosx_documentos::where('categoria','2')->where('id_tramite_rayosx',$input['id_tramite'])->where('documento','fi_programa_tecno')->first();
        $iddocumento = $idrayosxDocumentos->id_documentos_rayosx;


        if($input['fi_programa_tecno'] != NULL){
          $path = $input['fi_programa_tecno']->store('public');
          $rayosxdocumentos=Rayosx_documentos::find($iddocumento);
          $rayosxdocumentos->id_tramite_rayosx = $input['id_tramite'];
          $rayosxdocumentos->path = $path;
          $rayosxdocumentos->categoria = 1;
          $rayosxdocumentos->documento = 'fi_programa_tecno';
          $rayosxdocumentos->id_archivo = 1;
          $rayosxdocumentos->estado = 1;
          $rayosxdocumentos->save();
        }

        $idrayosxDocumentos=Rayosx_documentos::where('categoria','2')->where('id_tramite_rayosx',$input['id_tramite'])->where('documento','fi_programa_proteccion')->first();
        $iddocumento = $idrayosxDocumentos->id_documentos_rayosx;

        if($input['fi_programa_proteccion'] != NULL){
          $path = $input['fi_programa_proteccion']->store('public');
          $rayosxdocumentos=Rayosx_documentos::find($iddocumento);
          $rayosxdocumentos->id_tramite_rayosx = $input['id_tramite'];
          $rayosxdocumentos->path = $path;
          $rayosxdocumentos->categoria = 1;
          $rayosxdocumentos->documento = 'fi_programa_proteccion';
          $rayosxdocumentos->id_archivo = 1;
          $rayosxdocumentos->estado = 1;
          $rayosxdocumentos->save();
        }

        $idrayosxDocumentos=Rayosx_documentos::where('categoria','2')->where('id_tramite_rayosx',$input['id_tramite'])->where('documento','fi_soporte_talento')->first();
        $iddocumento = $idrayosxDocumentos->id_documentos_rayosx;

        if($input['fi_soporte_talento'] != NULL){
          $path = $input['fi_soporte_talento']->store('public');
          $rayosxdocumentos=Rayosx_documentos::find($iddocumento);
          $rayosxdocumentos->id_tramite_rayosx = $input['id_tramite'];
          $rayosxdocumentos->path = $path;
          $rayosxdocumentos->categoria = 1;
          $rayosxdocumentos->documento = 'fi_soporte_talento';
          $rayosxdocumentos->id_archivo = 1;
          $rayosxdocumentos->estado = 1;
          $rayosxdocumentos->save();
        }

        $idrayosxDocumentos=Rayosx_documentos::where('categoria','2')->where('id_tramite_rayosx',$input['id_tramite'])->where('documento','fi_diploma_director')->first();
        $iddocumento = $idrayosxDocumentos->id_documentos_rayosx;

        if($input['fi_diploma_director'] != NULL){
          $path = $input['fi_diploma_director']->store('public');
          $rayosxdocumentos=Rayosx_documentos::find($iddocumento);
          $rayosxdocumentos->id_tramite_rayosx = $input['id_tramite'];
          $rayosxdocumentos->path = $path;
          $rayosxdocumentos->categoria = 1;
          $rayosxdocumentos->documento = 'fi_diploma_director';
          $rayosxdocumentos->id_archivo = 1;
          $rayosxdocumentos->estado = 1;
          $rayosxdocumentos->save();
        }

        $idrayosxDocumentos=Rayosx_documentos::where('categoria','2')->where('id_tramite_rayosx',$input['id_tramite'])->where('fi_res_convalida_director','fi_cert_calibracion')->first();
        $iddocumento = $idrayosxDocumentos->id_documentos_rayosx;

        if($input['fi_res_convalida_director'] != NULL){
          $path = $input['fi_res_convalida_director']->store('public');
          $rayosxdocumentos=Rayosx_documentos::find($iddocumento);
          $rayosxdocumentos->id_tramite_rayosx = $input['id_tramite'];
          $rayosxdocumentos->path = $path;
          $rayosxdocumentos->categoria = 1;
          $rayosxdocumentos->documento = 'fi_res_convalida_director';
          $rayosxdocumentos->id_archivo = 1;
          $rayosxdocumentos->estado = 1;
          $rayosxdocumentos->save();
        }

        $idrayosxDocumentos=Rayosx_documentos::where('categoria','2')->where('id_tramite_rayosx',$input['id_tramite'])->where('documento','fi_diploma_pos_profe')->first();
        $iddocumento = $idrayosxDocumentos->id_documentos_rayosx;

        if($input['fi_diploma_pos_profe'] != NULL){
          $path = $input['fi_diploma_pos_profe']->store('public');
          $rayosxdocumentos=Rayosx_documentos::find($iddocumento);
          $rayosxdocumentos->id_tramite_rayosx = $input['id_tramite'];
          $rayosxdocumentos->path = $path;
          $rayosxdocumentos->categoria = 1;
          $rayosxdocumentos->documento = 'fi_diploma_pos_profe';
          $rayosxdocumentos->id_archivo = 1;
          $rayosxdocumentos->estado = 1;
          $rayosxdocumentos->save();
        }

        $idrayosxDocumentos=Rayosx_documentos::where('categoria','2')->where('id_tramite_rayosx',$input['id_tramite'])->where('documento','fi_res_convalida_profe')->first();
        $iddocumento = $idrayosxDocumentos->id_documentos_rayosx;

        if($input['fi_res_convalida_profe'] != NULL){
          $path = $input['fi_res_convalida_profe']->store('public');
          $rayosxdocumentos=Rayosx_documentos::find($iddocumento);
          $rayosxdocumentos->id_tramite_rayosx = $input['id_tramite'];
          $rayosxdocumentos->path = $path;
          $rayosxdocumentos->categoria = 1;
          $rayosxdocumentos->documento = 'fi_res_convalida_profe';
          $rayosxdocumentos->id_archivo = 1;
          $rayosxdocumentos->estado = 1;
          $rayosxdocumentos->save();
        }


        $idrayosxDocumentos=Rayosx_documentos::where('categoria','2')->where('id_tramite_rayosx',$input['id_tramite'])->where('documento','fi_cert_calibracion')->first();
        $iddocumento = $idrayosxDocumentos->id_documentos_rayosx;

        if($input['fi_cert_calibracion'] != NULL){
          $path = $input['fi_cert_calibracion']->store('public');
          $rayosxdocumentos=Rayosx_documentos::find($iddocumento);
          $rayosxdocumentos->id_tramite_rayosx = $input['id_tramite'];
          $rayosxdocumentos->path = $path;
          $rayosxdocumentos->categoria = 1;
          $rayosxdocumentos->documento = 'fi_cert_calibracion';
          $rayosxdocumentos->id_archivo = 1;
          $rayosxdocumentos->estado = 1;
          $rayosxdocumentos->save();
        }

        $idrayosxDocumentos=Rayosx_documentos::where('categoria','2')->where('id_tramite_rayosx',$input['id_tramite'])->where('documento','fi_declaraciones')->first();
        $iddocumento = $idrayosxDocumentos->id_documentos_rayosx;

        if($input['fi_declaraciones'] != NULL){
          $path = $input['fi_declaraciones']->store('public');
          $rayosxdocumentos=Rayosx_documentos::find($iddocumento);
          $rayosxdocumentos->id_tramite_rayosx = $input['id_tramite'];
          $rayosxdocumentos->path = $path;
          $rayosxdocumentos->categoria = 1;
          $rayosxdocumentos->documento = 'fi_declaraciones';
          $rayosxdocumentos->id_archivo = 1;
          $rayosxdocumentos->estado = 1;
          $rayosxdocumentos->save();
        }
       return redirect('/ventanilla/tramites/solicitud_rayosx/tramite/'.$input['id_tramite']);
    }


    public function editarDocumentos2(Request $request)
    {
        $input = Input::all();
        $idtramite = $request->id_tramite;
        //dd($idtramite);
        //dd($input);
        $idrayosxDocumentos=Rayosx_documentos::where('categoria','2')->where('id_tramite_rayosx',$input['id_tramite'])->where('documento','fi_doc_oficial')->first();
        $iddocumento = $idrayosxDocumentos->id_documentos_rayosx;


        if($input['fi_doc_oficial'] != NULL){
          $path = $input['fi_doc_oficial']->store('public');
          $rayosxdocumentos=Rayosx_documentos::find($iddocumento);
          $rayosxdocumentos->path = $path;
          $rayosxdocumentos->save();

        }

        $idrayosxDocumentos=Rayosx_documentos::where('categoria','2')->where('id_tramite_rayosx',$input['id_tramite'])->where('documento','fi_diploma_oficial')->first();
        $iddocumento = $idrayosxDocumentos->id_documentos_rayosx;

        if($input['fi_diploma_oficial'] != NULL){
          $path = $input['fi_diploma_oficial']->store('public');
          $rayosxdocumentos=Rayosx_documentos::find($iddocumento);
          $rayosxdocumentos->id_tramite_rayosx = $input['id_tramite'];
          $rayosxdocumentos->path = $path;
          $rayosxdocumentos->categoria = 2;
          $rayosxdocumentos->documento = 'fi_diploma_oficial';
          $rayosxdocumentos->id_archivo = 1;
          $rayosxdocumentos->estado = 1;
          $rayosxdocumentos->save();
        }

        $idrayosxDocumentos=Rayosx_documentos::where('categoria','2')->where('id_tramite_rayosx',$input['id_tramite'])->where('documento','fi_blindajes')->first();
        $iddocumento = $idrayosxDocumentos->id_documentos_rayosx;

        if($input['fi_blindajes'] != NULL){
          $path =$input['fi_blindajes']->store('public');
          $rayosxdocumentos=Rayosx_documentos::find($iddocumento);
          $rayosxdocumentos->id_tramite_rayosx = $input['id_tramite'];
          $rayosxdocumentos->path = $path;
          $rayosxdocumentos->categoria = 2;
          $rayosxdocumentos->documento = 'fi_blindajes';
          $rayosxdocumentos->id_archivo = 1;
          $rayosxdocumentos->estado = 1;
          $rayosxdocumentos->save();
        }

        $idrayosxDocumentos=Rayosx_documentos::where('categoria','2')->where('id_tramite_rayosx',$input['id_tramite'])->where('documento','fi_control_calidad')->first();
        $iddocumento = $idrayosxDocumentos->id_documentos_rayosx;

        if($input['fi_control_calidad'] != NULL){
          $path = $input['fi_control_calidad']->store('public');
          $rayosxdocumentos=Rayosx_documentos::find($iddocumento);
          $rayosxdocumentos->id_tramite_rayosx = $input['id_tramite'];
          $rayosxdocumentos->path = $path;
          $rayosxdocumentos->categoria = 2;
          $rayosxdocumentos->documento = 'fi_control_calidad';
          $rayosxdocumentos->id_archivo = 1;
          $rayosxdocumentos->estado = 1;
          $rayosxdocumentos->save();
        }

        $idrayosxDocumentos=Rayosx_documentos::where('categoria','2')->where('id_tramite_rayosx',$input['id_tramite'])->where('documento','fi_registro_dosimetrico')->first();
        $iddocumento = $idrayosxDocumentos->id_documentos_rayosx;

        if($input['fi_registro_dosimetrico'] != NULL){
          $path = $input['fi_registro_dosimetrico']->store('public');
          $rayosxdocumentos=Rayosx_documentos::find($iddocumento);
          $rayosxdocumentos->id_tramite_rayosx = $input['id_tramite'];
          $rayosxdocumentos->path = $path;
          $rayosxdocumentos->categoria = 2;
          $rayosxdocumentos->documento = 'fi_registro_dosimetrico';
          $rayosxdocumentos->id_archivo = 1;
          $rayosxdocumentos->estado = 1;
          $rayosxdocumentos->save();
        }

        $idrayosxDocumentos=Rayosx_documentos::where('categoria','2')->where('id_tramite_rayosx',$input['id_tramite'])->where('documento','fi_plano')->first();
        $iddocumento = $idrayosxDocumentos->id_documentos_rayosx;

        if($input['fi_plano'] != NULL){
          $path = $input['fi_plano']->store('public');
          $rayosxdocumentos=Rayosx_documentos::find($iddocumento);
          $rayosxdocumentos->id_tramite_rayosx = $input['id_tramite'];
          $rayosxdocumentos->path = $path;
          $rayosxdocumentos->categoria = 2;
          $rayosxdocumentos->documento = 'fi_plano';
          $rayosxdocumentos->id_archivo = 1;
          $rayosxdocumentos->estado = 1;
          $rayosxdocumentos->save();
        }

        $idrayosxDocumentos=Rayosx_documentos::where('categoria','2')->where('id_tramite_rayosx',$input['id_tramite'])->where('documento','fi_soporte_talento')->first();
        $iddocumento = $idrayosxDocumentos->id_documentos_rayosx;

        if($input['fi_soporte_talento'] != NULL){
          $path = $input['fi_soporte_talento']->store('public');
          $rayosxdocumentos=Rayosx_documentos::find($iddocumento);
          $rayosxdocumentos->id_tramite_rayosx = $input['id_tramite'];
          $rayosxdocumentos->path = $path;
          $rayosxdocumentos->categoria = 2;
          $rayosxdocumentos->documento = 'fi_soporte_talento';
          $rayosxdocumentos->id_archivo = 1;
          $rayosxdocumentos->estado = 1;
          $rayosxdocumentos->save();
        }

        $idrayosxDocumentos=Rayosx_documentos::where('categoria','2')->where('id_tramite_rayosx',$input['id_tramite'])->where('documento','fi_diploma_director')->first();
        $iddocumento = $idrayosxDocumentos->id_documentos_rayosx;

        if($input['fi_diploma_director'] != NULL){
          $path = $input['fi_diploma_director']->store('public');
          $rayosxdocumentos=Rayosx_documentos::find($iddocumento);
          $rayosxdocumentos->id_tramite_rayosx = $input['id_tramite'];
          $rayosxdocumentos->path = $path;
          $rayosxdocumentos->categoria = 2;
          $rayosxdocumentos->documento = 'fi_diploma_director';
          $rayosxdocumentos->id_archivo = 1;
          $rayosxdocumentos->estado = 1;
          $rayosxdocumentos->save();
        }

        $idrayosxDocumentos=Rayosx_documentos::where('categoria','2')->where('id_tramite_rayosx',$input['id_tramite'])->where('documento','fi_res_convalida_director')->first();
        $iddocumento = $idrayosxDocumentos->id_documentos_rayosx;

        if($input['fi_res_convalida_director'] != NULL){
          $path = $input['fi_res_convalida_director']->store('public');
          $rayosxdocumentos=Rayosx_documentos::find($iddocumento);
          $rayosxdocumentos->id_tramite_rayosx = $input['id_tramite'];
          $rayosxdocumentos->path = $path;
          $rayosxdocumentos->categoria = 2;
          $rayosxdocumentos->documento = 'fi_res_convalida_director';
          $rayosxdocumentos->id_archivo = 1;
          $rayosxdocumentos->estado = 1;
          $rayosxdocumentos->save();
        }

        $idrayosxDocumentos=Rayosx_documentos::where('categoria','2')->where('id_tramite_rayosx',$input['id_tramite'])->where('documento','fi_diploma_pos_profe')->first();
        $iddocumento = $idrayosxDocumentos->id_documentos_rayosx;

        if($input['fi_diploma_pos_profe'] != NULL){
          $path = $input['fi_diploma_pos_profe']->store('public');
          $rayosxdocumentos=Rayosx_documentos::find($iddocumento);
          $rayosxdocumentos->id_tramite_rayosx = $input['id_tramite'];
          $rayosxdocumentos->path = $path;
          $rayosxdocumentos->categoria = 2;
          $rayosxdocumentos->documento = 'fi_diploma_pos_profe';
          $rayosxdocumentos->id_archivo = 1;
          $rayosxdocumentos->estado = 1;
          $rayosxdocumentos->save();
        }

        $idrayosxDocumentos=Rayosx_documentos::where('categoria','2')->where('id_tramite_rayosx',$input['id_tramite'])->where('documento','fi_res_convalida_profe')->first();
        $iddocumento = $idrayosxDocumentos->id_documentos_rayosx;

        if($input['fi_res_convalida_profe'] != NULL){
          $path = $input['fi_res_convalida_profe']->store('public');
          $rayosxdocumentos=Rayosx_documentos::find($iddocumento);
          $rayosxdocumentos->id_tramite_rayosx = $input['id_tramite'];
          $rayosxdocumentos->path = $path;
          $rayosxdocumentos->categoria = 2;
          $rayosxdocumentos->documento = 'fi_res_convalida_profe';
          $rayosxdocumentos->id_archivo = 1;
          $rayosxdocumentos->estado = 1;
          $rayosxdocumentos->save();
        }


        $idrayosxDocumentos=Rayosx_documentos::where('categoria','2')->where('id_tramite_rayosx',$input['id_tramite'])->where('documento','fi_cert_calibracion')->first();
        $iddocumento = $idrayosxDocumentos->id_documentos_rayosx;

        if($input['fi_cert_calibracion'] != NULL){
          $path = $input['fi_cert_calibracion']->store('public');
          $rayosxdocumentos=Rayosx_documentos::find($iddocumento);
          $rayosxdocumentos->id_tramite_rayosx = $input['id_tramite'];
          $rayosxdocumentos->path = $path;
          $rayosxdocumentos->categoria = 2;
          $rayosxdocumentos->documento = 'fi_cert_calibracion';
          $rayosxdocumentos->id_archivo = 1;
          $rayosxdocumentos->estado = 1;
          $rayosxdocumentos->save();
        }

        $idrayosxDocumentos=Rayosx_documentos::where('categoria','2')->where('id_tramite_rayosx',$input['id_tramite'])->where('documento','fi_declaraciones')->first();
        $iddocumento = $idrayosxDocumentos->id_documentos_rayosx;

        if($input['fi_declaraciones'] != NULL){
          $path = $input['fi_declaraciones']->store('public');
          $rayosxdocumentos=Rayosx_documentos::find($iddocumento);
          $rayosxdocumentos->id_tramite_rayosx = $input['id_tramite'];
          $rayosxdocumentos->path = $path;
          $rayosxdocumentos->categoria = 2;
          $rayosxdocumentos->documento = 'fi_declaraciones';
          $rayosxdocumentos->id_archivo = 1;
          $rayosxdocumentos->estado = 1;
          $rayosxdocumentos->save();
        }

       return redirect('/ventanilla/tramites/solicitud_rayosx/tramite/'.$input['id_tramite']);
    }

    public function guardarDocumentos1(Request $request)
    {
        $input = Input::all();


        if($input['fi_doc_encargado'] != NULL){
          $path = $input['fi_doc_encargado']->store('public');
          $rayosxdocumentos = new Rayosx_documentos();
          $rayosxdocumentos->id_tramite_rayosx = $input['id_tramite'];
          $rayosxdocumentos->path = $path;
          $rayosxdocumentos->categoria = 1;
          $rayosxdocumentos->documento = 'fi_doc_encargado';
          $rayosxdocumentos->id_archivo = 1;
          $rayosxdocumentos->estado = 1;
          $rayosxdocumentos->save();
        }

        if($input['fi_diploma_encargado'] != NULL){
          $path = $input['fi_diploma_encargado']->store('public');
          $rayosxdocumentos = new Rayosx_documentos();
          $rayosxdocumentos->id_tramite_rayosx = $input['id_tramite'];
          $rayosxdocumentos->path = $path;
          $rayosxdocumentos->categoria = 1;
          $rayosxdocumentos->documento = 'fi_diploma_encargado';
          $rayosxdocumentos->id_archivo = 1;
          $rayosxdocumentos->estado = 1;
          $rayosxdocumentos->save();
        }

        if($input['fi_blindajes'] != NULL){
          $path =$input['fi_blindajes']->store('public');
          $rayosxdocumentos = new Rayosx_documentos();
          $rayosxdocumentos->id_tramite_rayosx = $input['id_tramite'];
          $rayosxdocumentos->path = $path;
          $rayosxdocumentos->categoria = 1;
          $rayosxdocumentos->documento = 'fi_blindajes';
          $rayosxdocumentos->id_archivo = 1;
          $rayosxdocumentos->estado = 1;
          $rayosxdocumentos->save();
        }

        if($input['fi_control_calidad'] != NULL){
          $path = $input['fi_control_calidad']->store('public');
          $rayosxdocumentos = new Rayosx_documentos();
          $rayosxdocumentos->id_tramite_rayosx = $input['id_tramite'];
          $rayosxdocumentos->path = $path;
          $rayosxdocumentos->categoria = 1;
          $rayosxdocumentos->documento = 'fi_control_calidad';
          $rayosxdocumentos->id_archivo = 1;
          $rayosxdocumentos->estado = 1;
          $rayosxdocumentos->save();
        }

        if($input['fi_registro_dosimetrico'] != NULL){
          $path = $input['fi_registro_dosimetrico']->store('public');
          $rayosxdocumentos = new Rayosx_documentos();
          $rayosxdocumentos->id_tramite_rayosx = $input['id_tramite'];
          $rayosxdocumentos->path = $path;
          $rayosxdocumentos->categoria = 1;
          $rayosxdocumentos->documento = 'fi_registro_dosimetrico';
          $rayosxdocumentos->id_archivo = 1;
          $rayosxdocumentos->estado = 1;
          $rayosxdocumentos->save();
        }

        if($input['fi_registro_niveles'] != NULL){
          $path = $input['fi_registro_niveles']->store('public');
          $rayosxdocumentos = new Rayosx_documentos();
          $rayosxdocumentos->id_tramite_rayosx = $input['id_tramite'];
          $rayosxdocumentos->path = $path;
          $rayosxdocumentos->categoria = 1;
          $rayosxdocumentos->documento = 'fi_registro_niveles';
          $rayosxdocumentos->id_archivo = 1;
          $rayosxdocumentos->estado = 1;
          $rayosxdocumentos->save();
        }

        if($input['fi_plano'] != NULL){
          $path = $input['fi_plano']->store('public');
          $rayosxdocumentos = new Rayosx_documentos();
          $rayosxdocumentos->id_tramite_rayosx = $input['id_tramite'];
          $rayosxdocumentos->path = $path;
          $rayosxdocumentos->categoria = 1;
          $rayosxdocumentos->documento = 'fi_plano';
          $rayosxdocumentos->id_archivo = 1;
          $rayosxdocumentos->estado = 1;
          $rayosxdocumentos->save();
        }

        if($input['fi_certificado_capacitacion'] != NULL){
          $path = $input['fi_certificado_capacitacion']->store('public');
          $rayosxdocumentos = new Rayosx_documentos();
          $rayosxdocumentos->id_tramite_rayosx = $input['id_tramite'];
          $rayosxdocumentos->path = $path;
          $rayosxdocumentos->categoria = 1;
          $rayosxdocumentos->documento = 'fi_certificado_capacitacion';
          $rayosxdocumentos->id_archivo = 1;
          $rayosxdocumentos->estado = 1;
          $rayosxdocumentos->save();
        }

        if($input['fi_programa_capacitacion'] != NULL){
          $path = $input['fi_programa_capacitacion']->store('public');
          $rayosxdocumentos = new Rayosx_documentos();
          $rayosxdocumentos->id_tramite_rayosx = $input['id_tramite'];
          $rayosxdocumentos->path = $path;
          $rayosxdocumentos->categoria = 1;
          $rayosxdocumentos->documento = 'fi_programa_capacitacion';
          $rayosxdocumentos->id_archivo = 1;
          $rayosxdocumentos->estado = 1;
          $rayosxdocumentos->save();
        }

        if($input['fi_procedimiento_mantenimiento'] != NULL){
          $path = $input['fi_procedimiento_mantenimiento']->store('public');
          $rayosxdocumentos = new Rayosx_documentos();
          $rayosxdocumentos->id_tramite_rayosx = $input['id_tramite'];
          $rayosxdocumentos->path = $path;
          $rayosxdocumentos->categoria = 1;
          $rayosxdocumentos->documento = 'fi_procedimiento_mantenimiento';
          $rayosxdocumentos->id_archivo = 1;
          $rayosxdocumentos->estado = 1;
          $rayosxdocumentos->save();
        }

        if($input['fi_pruebas_caracterizacion'] != NULL){
          $path = $input['fi_pruebas_caracterizacion']->store('public');
          $rayosxdocumentos = new Rayosx_documentos();
          $rayosxdocumentos->id_tramite_rayosx = $input['id_tramite'];
          $rayosxdocumentos->path = $path;
          $rayosxdocumentos->categoria = 1;
          $rayosxdocumentos->documento = 'fi_pruebas_caracterizacion';
          $rayosxdocumentos->id_archivo = 1;
          $rayosxdocumentos->estado = 1;
          $rayosxdocumentos->save();
        }

        if($input['fi_programa_tecno'] != NULL){
          $path = $input['fi_programa_tecno']->store('public');
          $rayosxdocumentos = new Rayosx_documentos();
          $rayosxdocumentos->id_tramite_rayosx = $input['id_tramite'];
          $rayosxdocumentos->path = $path;
          $rayosxdocumentos->categoria = 1;
          $rayosxdocumentos->documento = 'fi_programa_tecno';
          $rayosxdocumentos->id_archivo = 1;
          $rayosxdocumentos->estado = 1;
          $rayosxdocumentos->save();
        }


        if($input['fi_programa_proteccion'] != NULL){
          $path = $input['fi_programa_proteccion']->store('public');
          $rayosxdocumentos = new Rayosx_documentos();
          $rayosxdocumentos->id_tramite_rayosx = $input['id_tramite'];
          $rayosxdocumentos->path = $path;
          $rayosxdocumentos->categoria = 1;
          $rayosxdocumentos->documento = 'fi_programa_proteccion';
          $rayosxdocumentos->id_archivo = 1;
          $rayosxdocumentos->estado = 1;
          $rayosxdocumentos->save();
        }

        if($input['fi_soporte_talento'] != NULL){
          $path = $input['fi_soporte_talento']->store('public');
          $rayosxdocumentos = new Rayosx_documentos();
          $rayosxdocumentos->id_tramite_rayosx = $input['id_tramite'];
          $rayosxdocumentos->path = $path;
          $rayosxdocumentos->categoria = 1;
          $rayosxdocumentos->documento = 'fi_soporte_talento';
          $rayosxdocumentos->id_archivo = 1;
          $rayosxdocumentos->estado = 1;
          $rayosxdocumentos->save();
        }

        if($input['fi_diploma_director'] != NULL){
          $path = $input['fi_diploma_director']->store('public');
          $rayosxdocumentos = new Rayosx_documentos();
          $rayosxdocumentos->id_tramite_rayosx = $input['id_tramite'];
          $rayosxdocumentos->path = $path;
          $rayosxdocumentos->categoria = 1;
          $rayosxdocumentos->documento = 'fi_diploma_director';
          $rayosxdocumentos->id_archivo = 1;
          $rayosxdocumentos->estado = 1;
          $rayosxdocumentos->save();
        }

        if($input['fi_res_convalida_director'] != NULL){
          $path = $input['fi_res_convalida_director']->store('public');
          $rayosxdocumentos = new Rayosx_documentos();
          $rayosxdocumentos->id_tramite_rayosx = $input['id_tramite'];
          $rayosxdocumentos->path = $path;
          $rayosxdocumentos->categoria = 1;
          $rayosxdocumentos->documento = 'fi_res_convalida_director';
          $rayosxdocumentos->id_archivo = 1;
          $rayosxdocumentos->estado = 1;
          $rayosxdocumentos->save();
        }

        if($input['fi_diploma_pos_profe'] != NULL){
          $path = $input['fi_diploma_pos_profe']->store('public');
          $rayosxdocumentos = new Rayosx_documentos();
          $rayosxdocumentos->id_tramite_rayosx = $input['id_tramite'];
          $rayosxdocumentos->path = $path;
          $rayosxdocumentos->categoria = 1;
          $rayosxdocumentos->documento = 'fi_diploma_pos_profe';
          $rayosxdocumentos->id_archivo = 1;
          $rayosxdocumentos->estado = 1;
          $rayosxdocumentos->save();
        }

        if($input['fi_res_convalida_profe'] != NULL){
          $path = $input['fi_res_convalida_profe']->store('public');
          $rayosxdocumentos = new Rayosx_documentos();
          $rayosxdocumentos->id_tramite_rayosx = $input['id_tramite'];
          $rayosxdocumentos->path = $path;
          $rayosxdocumentos->categoria = 1;
          $rayosxdocumentos->documento = 'fi_res_convalida_profe';
          $rayosxdocumentos->id_archivo = 1;
          $rayosxdocumentos->estado = 1;
          $rayosxdocumentos->save();
        }

        if($input['fi_cert_calibracion'] != NULL){
          $path = $input['fi_cert_calibracion']->store('public');
          $rayosxdocumentos = new Rayosx_documentos();
          $rayosxdocumentos->id_tramite_rayosx = $input['id_tramite'];
          $rayosxdocumentos->path = $path;
          $rayosxdocumentos->categoria = 1;
          $rayosxdocumentos->documento = 'fi_cert_calibracion';
          $rayosxdocumentos->id_archivo = 1;
          $rayosxdocumentos->estado = 1;
          $rayosxdocumentos->save();
        }

        if($input['fi_declaraciones'] != NULL){
          $path = $input['fi_declaraciones']->store('public');
          $rayosxdocumentos = new Rayosx_documentos();
          $rayosxdocumentos->id_tramite_rayosx = $input['id_tramite'];
          $rayosxdocumentos->path = $path;
          $rayosxdocumentos->categoria = 1;
          $rayosxdocumentos->documento = 'fi_declaraciones';
          $rayosxdocumentos->id_archivo = 1;
          $rayosxdocumentos->estado = 1;
          $rayosxdocumentos->save();
        }


       return redirect('/ventanilla/tramites/solicitud_rayosx/tramite/'.$input['id_tramite']);
    }


    public function guardarDocumentos2(Request $request)
    {
        $input = Input::all();


        if($input['fi_doc_oficial'] != NULL){
          $path = $input['fi_doc_oficial']->store('public');
          $rayosxdocumentos = new Rayosx_documentos();
          $rayosxdocumentos->id_tramite_rayosx = $input['id_tramite'];
          $rayosxdocumentos->path = $path;
          $rayosxdocumentos->categoria = 2;
          $rayosxdocumentos->documento = 'fi_doc_oficial';
          $rayosxdocumentos->id_archivo = 1;
          $rayosxdocumentos->estado = 1;
          $rayosxdocumentos->save();
        }

        if($input['fi_diploma_oficial'] != NULL){
          $path = $input['fi_diploma_oficial']->store('public');
          $rayosxdocumentos = new Rayosx_documentos();
          $rayosxdocumentos->id_tramite_rayosx = $input['id_tramite'];
          $rayosxdocumentos->path = $path;
          $rayosxdocumentos->categoria = 2;
          $rayosxdocumentos->documento = 'fi_diploma_oficial';
          $rayosxdocumentos->id_archivo = 1;
          $rayosxdocumentos->estado = 1;
          $rayosxdocumentos->save();
        }

        if($input['fi_blindajes'] != NULL){
          $path =$input['fi_blindajes']->store('public');
          $rayosxdocumentos = new Rayosx_documentos();
          $rayosxdocumentos->id_tramite_rayosx = $input['id_tramite'];
          $rayosxdocumentos->path = $path;
          $rayosxdocumentos->categoria = 2;
          $rayosxdocumentos->documento = 'fi_blindajes';
          $rayosxdocumentos->id_archivo = 1;
          $rayosxdocumentos->estado = 1;
          $rayosxdocumentos->save();
        }

        if($input['fi_control_calidad'] != NULL){
          $path = $input['fi_control_calidad']->store('public');
          $rayosxdocumentos = new Rayosx_documentos();
          $rayosxdocumentos->id_tramite_rayosx = $input['id_tramite'];
          $rayosxdocumentos->path = $path;
          $rayosxdocumentos->categoria = 2;
          $rayosxdocumentos->documento = 'fi_control_calidad';
          $rayosxdocumentos->id_archivo = 1;
          $rayosxdocumentos->estado = 1;
          $rayosxdocumentos->save();
        }

        if($input['fi_registro_dosimetrico'] != NULL){
          $path = $input['fi_registro_dosimetrico']->store('public');
          $rayosxdocumentos = new Rayosx_documentos();
          $rayosxdocumentos->id_tramite_rayosx = $input['id_tramite'];
          $rayosxdocumentos->path = $path;
          $rayosxdocumentos->categoria = 2;
          $rayosxdocumentos->documento = 'fi_registro_dosimetrico';
          $rayosxdocumentos->id_archivo = 1;
          $rayosxdocumentos->estado = 1;
          $rayosxdocumentos->save();
        }

        if($input['fi_plano'] != NULL){
          $path = $input['fi_plano']->store('public');
          $rayosxdocumentos = new Rayosx_documentos();
          $rayosxdocumentos->id_tramite_rayosx = $input['id_tramite'];
          $rayosxdocumentos->path = $path;
          $rayosxdocumentos->categoria = 2;
          $rayosxdocumentos->documento = 'fi_plano';
          $rayosxdocumentos->id_archivo = 1;
          $rayosxdocumentos->estado = 1;
          $rayosxdocumentos->save();
        }

        if($input['fi_soporte_talento'] != NULL){
          $path = $input['fi_soporte_talento']->store('public');
          $rayosxdocumentos = new Rayosx_documentos();
          $rayosxdocumentos->id_tramite_rayosx = $input['id_tramite'];
          $rayosxdocumentos->path = $path;
          $rayosxdocumentos->categoria = 2;
          $rayosxdocumentos->documento = 'fi_soporte_talento';
          $rayosxdocumentos->id_archivo = 1;
          $rayosxdocumentos->estado = 1;
          $rayosxdocumentos->save();
        }

        if($input['fi_diploma_director'] != NULL){
          $path = $input['fi_diploma_director']->store('public');
          $rayosxdocumentos = new Rayosx_documentos();
          $rayosxdocumentos->id_tramite_rayosx = $input['id_tramite'];
          $rayosxdocumentos->path = $path;
          $rayosxdocumentos->categoria = 2;
          $rayosxdocumentos->documento = 'fi_diploma_director';
          $rayosxdocumentos->id_archivo = 1;
          $rayosxdocumentos->estado = 1;
          $rayosxdocumentos->save();
        }

        if($input['fi_res_convalida_director'] != NULL){
          $path = $input['fi_res_convalida_director']->store('public');
          $rayosxdocumentos = new Rayosx_documentos();
          $rayosxdocumentos->id_tramite_rayosx = $input['id_tramite'];
          $rayosxdocumentos->path = $path;
          $rayosxdocumentos->categoria = 2;
          $rayosxdocumentos->documento = 'fi_res_convalida_director';
          $rayosxdocumentos->id_archivo = 1;
          $rayosxdocumentos->estado = 1;
          $rayosxdocumentos->save();
        }

        if($input['fi_diploma_pos_profe'] != NULL){
          $path = $input['fi_diploma_pos_profe']->store('public');
          $rayosxdocumentos = new Rayosx_documentos();
          $rayosxdocumentos->id_tramite_rayosx = $input['id_tramite'];
          $rayosxdocumentos->path = $path;
          $rayosxdocumentos->categoria = 2;
          $rayosxdocumentos->documento = 'fi_diploma_pos_profe';
          $rayosxdocumentos->id_archivo = 1;
          $rayosxdocumentos->estado = 1;
          $rayosxdocumentos->save();
        }

        if($input['fi_res_convalida_profe'] != NULL){
          $path = $input['fi_res_convalida_profe']->store('public');
          $rayosxdocumentos = new Rayosx_documentos();
          $rayosxdocumentos->id_tramite_rayosx = $input['id_tramite'];
          $rayosxdocumentos->path = $path;
          $rayosxdocumentos->categoria = 2;
          $rayosxdocumentos->documento = 'fi_res_convalida_profe';
          $rayosxdocumentos->id_archivo = 1;
          $rayosxdocumentos->estado = 1;
          $rayosxdocumentos->save();
        }


        if($input['fi_cert_calibracion'] != NULL){
          $path = $input['fi_cert_calibracion']->store('public');
          $rayosxdocumentos = new Rayosx_documentos();
          $rayosxdocumentos->id_tramite_rayosx = $input['id_tramite'];
          $rayosxdocumentos->path = $path;
          $rayosxdocumentos->categoria = 2;
          $rayosxdocumentos->documento = 'fi_cert_calibracion';
          $rayosxdocumentos->id_archivo = 1;
          $rayosxdocumentos->estado = 1;
          $rayosxdocumentos->save();
        }

        if($input['fi_declaraciones'] != NULL){
          $path = $input['fi_declaraciones']->store('public');
          $rayosxdocumentos = new Rayosx_documentos();
          $rayosxdocumentos->id_tramite_rayosx = $input['id_tramite'];
          $rayosxdocumentos->path = $path;
          $rayosxdocumentos->categoria = 2;
          $rayosxdocumentos->documento = 'fi_declaraciones';
          $rayosxdocumentos->id_archivo = 1;
          $rayosxdocumentos->estado = 1;
          $rayosxdocumentos->save();
        }

       return redirect('/ventanilla/tramites/solicitud_rayosx/tramite/'.$input['id_tramite']);
    }

    public function guardarLocalizacion(Request $request)
    {
      $input = Input::all();      
      $tramite = $request->input('id_tramite');    

      //CARGAR DEPARTAMENTOS
      $departamentos = DB::table('pr_departamento')->pluck("IdDepartamento","Descripcion");

      //CARGAR TIPO IDENTIFICACION
      $tipo_identificacion = DB::table('pr_tipoidentificacion')->pluck("IdTipoIdentificacion","Descripcion");

      //CARGAR PROGRAMAS UNIVERSITARIOS
      $pr_programas_univ  = DB::table('pr_programas_univ')->pluck("id_programa","nombre_programa");

      //CARGAR NIVEL ACADEMICO
      $pr_nivelacademico  = DB::table('pr_nivelacademico')->pluck("IdNivelAcademico","Nombre");

      $rayosxOficialToe=Rayosx_toe::where('toe_tipo',1)->where('id_tramite_rayosx',$input['id_tramite'])->orderBy('id_toe_rayosx','DESC')->first();
      $rayosxTemporalToe=Rayosx_toe::where('toe_tipo',2)->where('id_tramite_rayosx',$input['id_tramite'])->orderBy('id_toe_rayosx','DESC')->paginate(3);
      $rayosxTalento=Rayosx_director::where('id_tramite_rayosx',$input['id_tramite'])->orderBy('id_director_rayosx','DESC')->first();
      $rayosxObjprueba=Rayosx_objprueba::where('id_tramite_rayosx',$input['id_tramite'])->orderBy('id_obj_rayosx','DESC')->paginate(3);

      $rayosxEquipo = Rayosx_equipos::where('id_tramite_rayosx',$input['id_tramite'] )->first();

      $idTramiterx = $idTramiterx2=Tramite::select('id')->where('id',$input['id_tramite'])->where('user',Auth::id())->orderBy('id','DESC')->firstorFail();;
      $rayosxDireccion = Rayosx_direccion::where('id_tramite_rayosx',$input['id_tramite'])->first();



        if($rayosxDireccion == NULL){                
            $rayosxDireccion = new Rayosx_direccion();
        }

        $rayosxDireccion->id_tramite_rayosx=$input['id_tramite'];
        $rayosxDireccion->dire_entidad=$input['dire_entidad'];
        $rayosxDireccion->sede_entidad=$input['sede_entidad'];
        $rayosxDireccion->email_entidad=$input['email_entidad'];
        $rayosxDireccion->depto_entidad=$input['depto_entidad'];
        $rayosxDireccion->mpio_entidad=$input['mpio_entidad'];
        $rayosxDireccion->celular_entidad=$input['celular_entidad'];
        //$rayosxDireccion->indicativo_entidad=$input['indicativo_entidad'];
        $rayosxDireccion->telefono_entidad=$input['telefono_entidad'];
        $rayosxDireccion->extension_entidad=$input['extension_entidad'];

        $rayosxDireccion->save();

        $rayosxDireccion = Rayosx_direccion::where('id_tramite_rayosx',$input['id_tramite'])->first();        

       return redirect('/ventanilla/tramites/solicitud_rayosx/tramite/'.$input['id_tramite']);
      

    }

    /**
     * Saves the institution's location and shows equipment x-ray form
     *
     * @return \Illuminate\Http\Response
     */
    public function guardarEquiposRayosX(Request $request)
    {
      $input = Input::all();

      
      
      //CARGAR DEPARTAMENTOS
      $departamentos = DB::table('pr_departamento')->pluck("IdDepartamento","Descripcion");
      //CARGAR TIPO IDENTIFICACION
      $tipo_identificacion = DB::table('pr_tipoidentificacion')->pluck("IdTipoIdentificacion","Descripcion");
      //CARGAR PROGRAMAS UNIVERSITARIOS
      $pr_programas_univ  = DB::table('pr_programas_univ')->pluck("id_programa","nombre_programa");
      //CARGAR NIVEL ACADEMICO
      $pr_nivelacademico  = DB::table('pr_nivelacademico')->pluck("IdNivelAcademico","Nombre");


      $rayosxOficialToe=Rayosx_toe::where('toe_tipo',1)->where('id_tramite_rayosx',$input['id_tramite'])->orderBy('id_toe_rayosx','DESC')->first();
      $rayosxTemporalToe=Rayosx_toe::where('toe_tipo',2)->where('id_tramite_rayosx',$input['id_tramite'])->orderBy('id_toe_rayosx','DESC')->paginate(3);
      $rayosxTalento=Rayosx_director::where('id_tramite_rayosx',$input['id_tramite'])->orderBy('id_director_rayosx','DESC')->first();
      $rayosxObjprueba=Rayosx_objprueba::where('id_tramite_rayosx',$input['id_tramite'])->orderBy('id_obj_rayosx','DESC')->paginate(3);

      $idTramiterx = $idTramiterx2=Tramite::select('id')->where('id',$input['id_tramite'])->where('user',Auth::id())->orderBy('id','DESC')->firstorFail();;
      $rayosxDireccion = Rayosx_direccion::where('id_tramite_rayosx',$input['id_tramite'])->first();
      $rayosxEquipo = Rayosx_equipos::where('id_tramite_rayosx',$input['id_tramite'])->first();

        if($rayosxEquipo == NULL){
            $rayosxEquipo = new Rayosx_equipos();
        }
        $rayosxEquipo->id_tramite_rayosx=$input['id_tramite'];
        $rayosxEquipo->categoria=$input['categoria'];
        $rayosxEquipo->categoria1=$input['categoria1'];
        $rayosxEquipo->categoria2=$input['categoria2'];
        $rayosxEquipo->categoria1_1 =$input['categoria1_1'];
        //$rayosxEquipo->categoria1_2 =$input['categoria1_2'];
        //$rayosxEquipo->equipo_generador =$input['equipo_generador'];
        $rayosxEquipo->tipo_visualizacion =$input['tipo_visualizacion'];
        $rayosxEquipo->marca_equipo =$input['marca_equipo'];
        $rayosxEquipo->modelo_equipo =$input['modelo_equipo'];
        $rayosxEquipo->serie_equipo =$input['serie_equipo'];
        $rayosxEquipo->marca_tubo_rx =$input['marca_tubo_rx'];
        $rayosxEquipo->modelo_tubo_rx =$input['modelo_tubo_rx'];
        $rayosxEquipo->serie_tubo_rx =$input['serie_tubo_rx'];
        $rayosxEquipo->tension_tubo_rx =$input['tension_tubo_rx'];
        $rayosxEquipo->contiene_tubo_rx =$input['contiene_tubo_rx'];
        $rayosxEquipo->energia_fotones =$input['energia_fotones'];
        $rayosxEquipo->energia_electrones =$input['energia_electrones'];
        $rayosxEquipo->carga_trabajo =$input['carga_trabajo'];
        $rayosxEquipo->ubicacion_equipo =$input['ubicacion_equipo'];
        $rayosxEquipo->numero_permiso =$input['numero_permiso'];
        $rayosxEquipo->anio_fabricacion =$input['anio_fabricacion'];
        $rayosxEquipo->anio_fabricacion_tubo =$input['anio_fabricacion_tubo'];
        $rayosxEquipo->estado=1;
        $rayosxEquipo->save();

        $rayosxEquipo = Rayosx_equipos::where('id_tramite_rayosx',$input['id_tramite'])->first();
        

        return redirect('/ventanilla/tramites/solicitud_rayosx/tramite/'.$input['id_tramite']);    

    }


    public function getMpoXDpto($id) {
        $municipios = DB::table("pr_municipio")->where("IdDepartamento",$id)->pluck("idMunicipio","Descripcion");

        return json_encode($municipios);

    }

    public function getEquipoXCategoria($id) {
        $equipos = DB::table("pr_equiposrx")->where("categoria",$id)->pluck("id_equipo","nombre_equipo");
        return json_encode($equipos);

    }


    /**
     * Saves the institution's location and shows equipment x-ray form
     *
     * @return \Illuminate\Http\Response
     */
    public function guardarOficialTOE(Request $request)
    {

        $input = Input::all();
        //dd($input);
        //CARGAR DEPARTAMENTOS
        $departamentos = DB::table('pr_departamento')->pluck("IdDepartamento","Descripcion");
        //CARGAR TIPO IDENTIFICACION
        $tipo_identificacion = DB::table('pr_tipoidentificacion')->pluck("IdTipoIdentificacion","Descripcion");
        //CARGAR PROGRAMAS UNIVERSITARIOS
        $pr_programas_univ  = DB::table('pr_programas_univ')->pluck("id_programa","nombre_programa");
        //CARGAR NIVEL ACADEMICO
        $pr_nivelacademico  = DB::table('pr_nivelacademico')->pluck("IdNivelAcademico","Nombre");


        $rayosxOficialToe=Rayosx_toe::where('toe_tipo',1)->where('id_tramite_rayosx',$input['id_tramite'])->orderBy('id_toe_rayosx','DESC')->first();
        $rayosxTemporalToe=Rayosx_toe::where('toe_tipo',2)->where('id_tramite_rayosx',$input['id_tramite'])->orderBy('id_toe_rayosx','DESC')->paginate(3);
        $rayosxTalento=Rayosx_director::where('id_tramite_rayosx',$input['id_tramite'])->orderBy('id_director_rayosx','DESC')->first();
        $rayosxObjprueba=Rayosx_objprueba::where('id_tramite_rayosx',$input['id_tramite'])->orderBy('id_obj_rayosx','DESC')->paginate();

        $idTramiterx = $idTramiterx2=Tramite::select('id')->where('id',$input['id_tramite'])->where('user',Auth::id())->orderBy('id','DESC')->firstorFail();;
        $rayosxDireccion = Rayosx_direccion::where('id_tramite_rayosx',$input['id_tramite'])->first();
        $rayosxEquipo = Rayosx_equipos::where('id_tramite_rayosx',$input['id_tramite'])->first();

        if($rayosxOficialToe == NULL){
            $rayosxOficialToe = new Rayosx_toe();
        }

        $rayosxOficialToe->id_tramite_rayosx=$input['id_tramite'];
        $rayosxOficialToe->toe_pnombre=$input['encargado_pnombre'];
        $rayosxOficialToe->toe_snombre=$input['encargado_snombre'];
        $rayosxOficialToe->toe_papellido=$input['encargado_papellido'];
        $rayosxOficialToe->toe_sapellido=$input['encargado_sapellido'];
        $rayosxOficialToe->toe_tdocumento=$input['encargado_tdocumento'];
        $rayosxOficialToe->toe_ndocumento=$input['encargado_ndocumento'];
        $rayosxOficialToe->toe_lexpedicion=$input['encargado_lexpedicion'];
        $rayosxOficialToe->toe_correo=$input['encargado_correo'];
        $rayosxOficialToe->toe_profesion=$input['encargado_profesion'];
        $rayosxOficialToe->toe_nivel=$input['encargado_nivel'];
        //$rayosxToe->toe_ult_entrenamiento=$request->encargado_ndocumento;
        //$rayosxToe->toe_pro_entrenamiento=$request->encargado_ndocumento;
        $rayosxOficialToe->toe_tipo=1;
        $rayosxOficialToe->save();


        $rayosxOficialToe=Rayosx_toe::where('toe_tipo',1)->where('id_tramite_rayosx',$input['id_tramite'])->orderBy('id_toe_rayosx','DESC')->first();
        //dd($rayosxEquipo);

        return redirect('/ventanilla/tramites/solicitud_rayosx/tramite/'.$input['id_tramite']);

    }

    public function guardarTemporalTOE(Request $request)
    {

      $input = Input::all();

      //dd($input);
      //CARGAR DEPARTAMENTOS
      $departamentos = DB::table('pr_departamento')->pluck("IdDepartamento","Descripcion");
      //CARGAR TIPO IDENTIFICACION
      $tipo_identificacion = DB::table('pr_tipoidentificacion')->pluck("IdTipoIdentificacion","Descripcion");
      //CARGAR PROGRAMAS UNIVERSITARIOS
      $pr_programas_univ  = DB::table('pr_programas_univ')->pluck("id_programa","nombre_programa");
      //CARGAR NIVEL ACADEMICO
      $pr_nivelacademico  = DB::table('pr_nivelacademico')->pluck("IdNivelAcademico","Nombre");


      $rayosxOficialToe=Rayosx_toe::where('toe_tipo',1)->where('id_tramite_rayosx',$input['id_tramite'])->orderBy('id_toe_rayosx','DESC')->first();
      $rayosxTemporalToe=Rayosx_toe::where('toe_tipo',2)->where('id_tramite_rayosx',$input['id_tramite'])->orderBy('id_toe_rayosx','DESC')->get();
      $rayosxTalento=Rayosx_director::where('id_tramite_rayosx',$input['id_tramite'])->orderBy('id_director_rayosx','DESC')->first();
      $rayosxObjprueba=Rayosx_objprueba::where('id_tramite_rayosx',$input['id_tramite'])->orderBy('id_obj_rayosx','DESC')->paginate();

      $idTramiterx = $idTramiterx2=Tramite::select('id')->where('id',$input['id_tramite'])->where('user',Auth::id())->orderBy('id','DESC')->firstorFail();;
      $rayosxDireccion = Rayosx_direccion::where('id_tramite_rayosx',$input['id_tramite'])->first();
      $rayosxEquipo = Rayosx_equipos::where('id_tramite_rayosx',$input['id_tramite'])->first();

             
        $rayosxTemporalToe = new Rayosx_toe();
        

        $rayosxTemporalToe->id_tramite_rayosx=$input['id_tramite'];        
        $rayosxTemporalToe->toe_pnombre=$input['encargado_pnombre'];
        $rayosxTemporalToe->toe_snombre=$input['encargado_snombre'];
        $rayosxTemporalToe->toe_papellido=$input['encargado_papellido'];
        $rayosxTemporalToe->toe_sapellido=$input['encargado_sapellido'];
        $rayosxTemporalToe->toe_tdocumento=$input['encargado_tdocumento'];
        $rayosxTemporalToe->toe_ndocumento=$input['encargado_ndocumento'];
        $rayosxTemporalToe->toe_lexpedicion=$input['encargado_lexpedicion'];
        $rayosxTemporalToe->toe_correo=$input['encargado_correo'];
        $rayosxTemporalToe->toe_profesion=$input['encargado_profesion'];
        $rayosxTemporalToe->toe_nivel=$input['encargado_nivel'];
        $rayosxTemporalToe->toe_ult_entrenamiento=$input['toe_ult_entrenamiento'];
        $rayosxTemporalToe->toe_pro_entrenamiento=$input['toe_pro_entrenamiento'];
        $rayosxTemporalToe->toe_registro=$input['toe_registro'];
        $rayosxTemporalToe->toe_tipo=2;
        $rayosxTemporalToe->save();

        


        $rayosxTemporalToe=Rayosx_toe::where('toe_tipo',2)->where('id_tramite_rayosx',$input['id_tramite'])->orderBy('id_toe_rayosx','DESC')->paginate(3);
        //dd($rayosxEquipo);

       return redirect('/ventanilla/tramites/solicitud_rayosx/tramite/'.$input['id_tramite']);


    }




    public function verificarTOE(Request $request)
    {

      //CARGAR DEPARTAMENTOS
      $departamentos = DB::table('pr_departamento')->pluck("IdDepartamento","Descripcion");
      //CARGAR TIPO IDENTIFICACION
      $tipo_identificacion = DB::table('pr_tipoidentificacion')->pluck("IdTipoIdentificacion","Descripcion");
      //CARGAR PROGRAMAS UNIVERSITARIOS
      $pr_programas_univ  = DB::table('pr_programas_univ')->pluck("id_programa","nombre_programa");
      //CARGAR NIVEL ACADEMICO
      $pr_nivelacademico  = DB::table('pr_nivelacademico')->pluck("IdNivelAcademico","Nombre");


      //Campos Hidden
      $tipo_tramite=1;
      $existe_tramite=(int)$request->existeTramite;
      $id=(int)$request->idTramite;

      if($id>0){
        $idTramite=Tramite::where('user',Auth::id())->where('id',$id)->orderBy('id','DESC')->first();

        $rayosxOficialToe=Rayosx_toe::where('toe_tipo',1)->where('id_tramite_rayosx',$id)->orderBy('id_toe_rayosx','DESC')->first();
        $rayosxTemporalToe=Rayosx_toe::where('toe_tipo',2)->where('id_tramite_rayosx',$id)->orderBy('id_toe_rayosx','DESC')->paginate(3);
        $rayosxTalento=Rayosx_director::where('id_tramite_rayosx',$id)->orderBy('id_director_rayosx','DESC')->firstorFail();
        $rayosxObjprueba=Rayosx_objprueba::where('id_tramite_rayosx',$id)->orderBy('id_obj_rayosx','DESC')->get();

        if($rayosxOficialToe->count() > 0 && $rayosxTemporalToe->count() > 0){

          return view('rayosx.index', ['idTramite' => $idTramite,
                                            'existeTramite'=>1,
                                            'tipoTramite'=>$tipo_tramite,
                                            'departamentos'=>$departamentos,
                                            'tiposIdentificacion'=>$tipo_identificacion,
                                            'prNivelacademico'=>$pr_nivelacademico,
                                            'prProgramasUniv'=>$pr_programas_univ,
                                            'rayosxOficialToe'=>$rayosxOficialToe,
                                            'rayosxTemporalToe'=>$rayosxTemporalToe,
                                            'talento'=>$rayosxTalento,
                                            'objeto'=>$rayosxObjprueba,
                                            'paso'=>4 //Paso 4 - TALENTO HUMANO
                                            ]);
        }

        return view('rayosx.index', ['idTramite' => $idTramite,
                                            'existeTramite'=>1,
                                            'tipoTramite'=>$tipo_tramite,
                                            'departamentos'=>$departamentos,
                                            'tiposIdentificacion'=>$tipo_identificacion,
                                            'prNivelacademico'=>$pr_nivelacademico,
                                            'prProgramasUniv'=>$pr_programas_univ,
                                            'rayosxOficialToe'=>$rayosxOficialToe,
                                            'rayosxTemporalToe'=>$rayosxTemporalToe,
                                            'talento'=>$rayosxTalento,
                                            'objeto'=>$rayosxObjprueba,
                                            'paso'=>3.5 //Paso 3 - Cargar TOE
                                            ]);

      }

    }

    public function guardarTalentoHumano(Request $request)
    {

      $input = Input::all();
      //dd($input);
      //CARGAR DEPARTAMENTOS
      $departamentos = DB::table('pr_departamento')->pluck("IdDepartamento","Descripcion");
      //CARGAR TIPO IDENTIFICACION
      $tipo_identificacion = DB::table('pr_tipoidentificacion')->pluck("IdTipoIdentificacion","Descripcion");
      //CARGAR PROGRAMAS UNIVERSITARIOS
      $pr_programas_univ  = DB::table('pr_programas_univ')->pluck("id_programa","nombre_programa");
      //CARGAR NIVEL ACADEMICO
      $pr_nivelacademico  = DB::table('pr_nivelacademico')->pluck("IdNivelAcademico","Nombre");


      $rayosxOficialToe=Rayosx_toe::where('toe_tipo',1)->where('id_tramite_rayosx',$input['id_tramite'])->orderBy('id_toe_rayosx','DESC')->first();
      $rayosxTemporalToe=Rayosx_toe::where('toe_tipo',2)->where('id_tramite_rayosx',$input['id_tramite'])->orderBy('id_toe_rayosx','DESC')->paginate(3);
      $rayosxTalento=Rayosx_director::where('id_tramite_rayosx',$input['id_tramite'])->orderBy('id_director_rayosx','DESC')->first();
      $rayosxObjprueba=Rayosx_objprueba::where('id_tramite_rayosx',$input['id_tramite'])->orderBy('id_obj_rayosx','DESC')->paginate(3);

      $idTramiterx = $idTramiterx2=Tramite::select('id')->where('id',$input['id_tramite'])->where('user',Auth::id())->orderBy('id','DESC')->firstorFail();;
      $rayosxDireccion = Rayosx_direccion::where('id_tramite_rayosx',$input['id_tramite'])->first();
      $rayosxEquipo = Rayosx_equipos::where('id_tramite_rayosx',$input['id_tramite'])->first();
      //dd($rayosxDireccion);
        if($rayosxTalento == NULL){
            $rayosxTalento = new Rayosx_director();
        }
        $rayosxTalento->id_tramite_rayosx=$input['id_tramite'];
        $rayosxTalento->talento_pnombre= $input['talento_pnombre'];
        $rayosxTalento->talento_snombre = $input['talento_snombre'];
        $rayosxTalento->talento_papellido = $input['talento_papellido'];
        $rayosxTalento->talento_sapellido = $input['talento_sapellido'];
        $rayosxTalento->talento_tdocumento = $input['talento_tdocumento'];
        $rayosxTalento->talento_ndocumento = $input['talento_ndocumento'];
        $rayosxTalento->talento_lexpedicion = $input['talento_lexpedicion'];
        $rayosxTalento->talento_correo = $input['talento_correo'];
        $rayosxTalento->talento_titulo = $input['talento_titulo'];
        $rayosxTalento->talento_universidad = $input['talento_universidad'];
        $rayosxTalento->talento_libro = $input['talento_libro'];
        $rayosxTalento->talento_registro = $input['talento_registro'];
        $rayosxTalento->talento_fecha_diploma = $input['talento_fecha_diploma'];
        $rayosxTalento->talento_resolucion = $input['talento_resolucion'];
        $rayosxTalento->talento_fecha_convalida = $input['talento_fecha_convalida'];
        $rayosxTalento->talento_nivel = $input['talento_nivel'];
        $rayosxTalento->talento_titulo_pos = $input['talento_titulo_pos'];
        $rayosxTalento->talento_universidad_pos = $input['talento_universidad_pos'];
        $rayosxTalento->talento_libro_pos = $input['talento_libro_pos'];
        $rayosxTalento->talento_registro_pos = $input['talento_registro_pos'];
        $rayosxTalento->talento_fecha_diploma_pos = $input['talento_fecha_diploma_pos'];
        $rayosxTalento->talento_resolucion_pos = $input['talento_resolucion_pos'];
        $rayosxTalento->talento_fecha_convalida_pos = $input['talento_fecha_convalida_pos'];

        $rayosxTalento->save();


        $rayosxTalento=Rayosx_director::where('id_tramite_rayosx',$input['id_tramite'])->orderBy('id_director_rayosx','DESC')->first();
        //dd($rayosxEquipo);

       return redirect('/ventanilla/tramites/solicitud_rayosx/tramite/'.$input['id_tramite']);
        


    }



    public function guardarEquipoPrueba(Request $request)
    {

            $input = Input::all();
            //dd($input);
            //CARGAR DEPARTAMENTOS
            $departamentos = DB::table('pr_departamento')->pluck("IdDepartamento","Descripcion");
            //CARGAR TIPO IDENTIFICACION
            $tipo_identificacion = DB::table('pr_tipoidentificacion')->pluck("IdTipoIdentificacion","Descripcion");
            //CARGAR PROGRAMAS UNIVERSITARIOS
            $pr_programas_univ  = DB::table('pr_programas_univ')->pluck("id_programa","nombre_programa");
            //CARGAR NIVEL ACADEMICO
            $pr_nivelacademico  = DB::table('pr_nivelacademico')->pluck("IdNivelAcademico","Nombre");


            $rayosxOficialToe=Rayosx_toe::where('toe_tipo',1)->where('id_tramite_rayosx',$input['id_tramite'])->orderBy('id_toe_rayosx','DESC')->first();
            $rayosxTemporalToe=Rayosx_toe::where('toe_tipo',2)->where('id_tramite_rayosx',$input['id_tramite'])->orderBy('id_toe_rayosx','DESC')->paginate(3);
            $rayosxTalento=Rayosx_director::where('id_tramite_rayosx',$input['id_tramite'])->orderBy('id_director_rayosx','DESC')->first();


            $idTramiterx = $idTramiterx2=Tramite::select('id')->where('id',$input['id_tramite'])->where('user',Auth::id())->orderBy('id','DESC')->firstorFail();;
            $rayosxDireccion = Rayosx_direccion::where('id_tramite_rayosx',$input['id_tramite'])->first();
            $rayosxEquipo = Rayosx_equipos::where('id_tramite_rayosx',$input['id_tramite'])->first();
            //dd($rayosxDireccion);

              $rayosxObjprueba = new Rayosx_objprueba();
              $rayosxObjprueba->id_tramite_rayosx=$input['id_tramite'];
              $rayosxObjprueba->obj_nombre = $input['obj_nombre'];
              $rayosxObjprueba->obj_marca = $input['obj_marca'];
              $rayosxObjprueba->obj_modelo = $input['obj_modelo'];
              $rayosxObjprueba->obj_serie = $input['obj_serie'];
              $rayosxObjprueba->obj_calibracion = $input['obj_calibracion'];
              $rayosxObjprueba->obj_vigencia = $input['obj_vigencia'];
              $rayosxObjprueba->obj_fecha = $input['obj_fecha'];
              $rayosxObjprueba->obj_manual = $input['obj_manual'];
              $rayosxObjprueba->obj_uso = $input['obj_uso'];
              $rayosxObjprueba->save();

              $rayosxObjprueba=Rayosx_objprueba::where('id_tramite_rayosx',$input['id_tramite'])->orderBy('id_obj_rayosx','DESC')->paginate(3);
              //dd($rayosxEquipo);

       return redirect('/ventanilla/tramites/solicitud_rayosx/tramite/'.$input['id_tramite']);



          }
    public function verificarPaso4(Request $request)
    {
      //CARGAR DEPARTAMENTOS
      $departamentos = DB::table('pr_departamento')->pluck("IdDepartamento","Descripcion");
      //CARGAR TIPO IDENTIFICACION
      $tipo_identificacion = DB::table('pr_tipoidentificacion')->pluck("IdTipoIdentificacion","Descripcion");
      //CARGAR PROGRAMAS UNIVERSITARIOS
      $pr_programas_univ  = DB::table('pr_programas_univ')->pluck("id_programa","nombre_programa");
      //CARGAR NIVEL ACADEMICO
      $pr_nivelacademico  = DB::table('pr_nivelacademico')->pluck("IdNivelAcademico","Nombre");


      //Campos Hidden
      $tipo_tramite=1;
      $existe_tramite=(int)$request->existeTramite;
      $id=(int)$request->idTramite;

      if($id>0){
        $idTramite=Tramite::where('user',Auth::id())->where('id',$id)->orderBy('id','DESC')->first();

        $rayosxOficialToe=Rayosx_toe::where('toe_tipo',1)->where('id_tramite_rayosx',$id)->orderBy('id_toe_rayosx','DESC')->first();
        $rayosxTemporalToe=Rayosx_toe::where('toe_tipo',2)->where('id_tramite_rayosx',$id)->orderBy('id_toe_rayosx','DESC')->paginate(3);

        $rayosxTalento=Rayosx_director::where('id_tramite_rayosx',$id)->orderBy('id_director_rayosx','DESC')->get();
        $rayosxObjprueba=Rayosx_objprueba::where('id_tramite_rayosx',$id)->orderBy('id_obj_rayosx','DESC')->get();

        if($rayosxTalento->count() > 0 && $rayosxObjprueba->count() > 0){

          return view('rayosx.index', ['idTramite' => $idTramite,
                                            'existeTramite'=>1,
                                            'tipoTramite'=>$tipo_tramite,
                                            'departamentos'=>$departamentos,
                                            'tiposIdentificacion'=>$tipo_identificacion,
                                            'prNivelacademico'=>$pr_nivelacademico,
                                            'prProgramasUniv'=>$pr_programas_univ,
                                            'oficialTOE'=>$rayosxOficialToe,
                                            'temporalTOE'=>$rayosxTemporalToe,
                                            'talento'=>$rayosxTalento,
                                            'objeto'=>$rayosxObjprueba,
                                            'paso'=>5 //Paso 5 - DOCUMENTOS
                                            ]);
        }

        return view('rayosx.index', ['idTramite' => $idTramite,
                                            'existeTramite'=>1,
                                            'tipoTramite'=>$tipo_tramite,
                                            'departamentos'=>$departamentos,
                                            'tiposIdentificacion'=>$tipo_identificacion,
                                            'prNivelacademico'=>$pr_nivelacademico,
                                            'prProgramasUniv'=>$pr_programas_univ,
                                            'oficialTOE'=>$rayosxOficialToe,
                                            'temporalTOE'=>$rayosxTemporalToe,
                                            'talento'=>$rayosxTalento,
                                            'objeto'=>$rayosxObjprueba,
                                            'paso'=>4.5 //Paso 5 - ERROR
                                            ]);
      }
    }

    public function gestionarFlujoTramiteRayosx(Request $request)
    {

        $input = Input::all();

        

        $idTramite=Tramite::where('id',$input['id_tramite'])->first();

        $salida="";

        //dd($idTramite);


        //$accionEjecutar;
        //$observacionesAccion;

        $estacionActual=Tramite_Flujo::where("tramite_id",$idTramite->id)->where("fecha_fin",NULL)->orderBy('id','DESC')->first();


        //dd($estacionActual);

            if($estacionActual != NULL){               

                $accionesDisponibles= Tipo_Tramite_Flujo::where('flujo_actividad_inicial',$estacionActual->actividad);

                $estacionActual->fecha_fin=date('Y-m-d H:i:s');
                $estacionActual->user_id=Auth::id();
                $estacionActual->observaciones=$estacionActual->observaciones.' --- '.date('Y-m-d H:i:s').' '.$input['observacion'];
                $estacionActual->save();

                //dd($estacionActual);

                $estacionSiguiente=Tipo_Tramite_Flujo::where('flujo_actividad_inicial',$estacionActual->actividad)->where('flujo_accion_permitida', 'like','%'.$input['accion_flujo'].'%')->first();

                //dd($estacionSiguiente);
                
                if($estacionSiguiente != NULL){ 

                    $estacionFinal=Tipo_Tramite_Flujo::where('flujo_actividad_inicial',$estacionSiguiente->flujo_actividad_final)->first();
                    //dd($estacionFinal);
                    //Se avanza
                    $tramiteFlujo = new Tramite_Flujo();
                    $tramiteFlujo->tramite_id=(int)$idTramite->id;
                    $tramiteFlujo->tipo_tramite=1;
                    $tramiteFlujo->actividad=$estacionFinal->flujo_actividad_inicial;
                    $tramiteFlujo->fecha_inicio=date('Y-m-d H:i:s');
                    $tramiteFlujo->user_id=NULL;
                    $tramiteFlujo->user_rol=$estacionFinal->rol_responsable;

                    $tramiteFlujo->observaciones=date('Y-m-d H:i:s')."Se inicia validacion del tramite";
                    $tramiteFlujo->save();
                }else{
                    $salida="Error: El flujo no se encuentra correctamente parametrizado, la actividad no se encuentra";            
                }

                

        }else{
            $salida="Error: El flujo no cuenta con actividad actual";
        }
        
        return redirect('/ventanilla/tramites/solicitud_rayosx/tramite/'.$input['id_tramite']);

    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
