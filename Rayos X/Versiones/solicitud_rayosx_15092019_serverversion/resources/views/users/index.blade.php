@extends('layouts.layout')

@section('title', '| Usuarios')

@section('content')


    <h1><i class="fa fa-users"></i> Gestionar Usuarios<a href="{{ route('roles.index') }}" class="btn btn-primary pull-right">Roles</a>
    <a href="{{ route('permissions.index') }}" class="btn btn-primary pull-right">Privilegios</a></h1>
    <hr>
    <div class="table-responsive">
        <table class="table table-bordered table-striped">

            <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Correo Electronico</th>
                    <th>Fecha/Hora Creado</th>
                    <th>Roles Usuario</th>
                    <th>Operaciones</th>
                </tr>
            </thead>

            <tbody>
                @foreach ($users as $user)
                <tr>

                    <td>{{ $user->name }}</td>
                    <td>{{ $user->email }}</td>
                    <td>{{ $user->created_at->format('F d, Y h:ia') }}</td>
                    <td>{{  $user->roles()->pluck('name')->implode(' ') }}</td>{{-- Retrieve array of roles associated to a user and convert to string --}}

                    <td>
                    <a href="{{ route('users.edit', $user->id) }}" class="btn btn-info pull-left" style="margin-right: 3px;">Edit</a>

                    {!! Form::open(['method' => 'DELETE', 'route' => ['users.destroy', $user->id] ]) !!}
                    {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                    {!! Form::close() !!}

                    </td>
                </tr>
                @endforeach
            </tbody>

        </table>
    </div>

    <a href="{{ route('users.create') }}" class="btn btn-success">Agregar Usuarios</a>



@endsection