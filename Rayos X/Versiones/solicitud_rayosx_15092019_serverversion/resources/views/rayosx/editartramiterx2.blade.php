@extends('layouts.layout')

@section('content')


<h2 class="text-blue"><b>Registro y autorización de licencias de Rayos X </b></h2>


<div id="divPasos" class="text-center">
   <div class="registro_triada">
      <button class="btn yellow" onClick="step1();">1 - Localizacíon Entidad</button>
      <i class="fa fa-chevron-right"></i>
      <button class="btn yellow" onClick="step2();">2 - Equipos Rayos x</button>
      <i class="fa fa-chevron-right"></i>
      <button class="btn yellow" onClick="step3();">3 - Trabajadores TOE</button>
      <i class="fa fa-chevron-right"></i>
      <button class="btn yellow" onClick="step4();">4 - Talento Humano</button>
      <i class="fa fa-chevron-right"></i>
      <button class="btn yellow" onClick="step5();">5 - Documentos Adjuntos</button>
   </div>
</div>


<!-- localizacion Identidad -->
<form id="formSeccion1" type="submit" name="form_tramite" action="{{ route('rayosx.editarDireccion') }}" method="post" class="form-row">
   {{ csrf_field() }}

   <!-- PRIMERA PARTE DEL REGISTRO DE LA SOLICITUD - LOCALIZACION tipoTramite  -->
   <div id="paso1" class="row block w-100 newsletter ">

      <div class="w-100">
        <div class="subtitle">
            <h3><b>Localizacíon Entidad:</b></h3>
        </div>

        <div class="row">
            <div class="col">
               <span class="text-orange">•</span><label for="depto_entidad">Departamento(*)</label>
               <input id="id_tramite" name="id_tramite" type="hidden" value="{{ isset($rayosxDireccion) ? $rayosxDireccion->id_tramite_rayosx : '' }}">
               <select id="depto_entidad" name="depto_entidad" class="form-control validate[required]" required>
                  <option value=""> - Seleccione Departamento -</option>
                  @foreach ($departamentos as $departamento => $value)
                   <option value="{{ $value}}"
                  {{(isset($rayosxDireccion->depto_entidad) && $rayosxDireccion->depto_entidad == $value)  ? 'selected' : '' }}>
                      {{ $departamento}}</option>
                   @endforeach
               </select>
            </div>

            <div class="col">
               <span class="text-orange">•</span><label for="mpio_entidad">Municipio(*)</label>
               <select id="mpio_entidad" name="mpio_entidad" class="form-control validate[required]" required>
                 <option value="{{ $value}}"
                {{(isset($rayosxDireccion->mpio_entidad) && $rayosxDireccion->mpio_entidad == $value)  ? 'selected' : '' }}>
               </select>
               <div class="col-md-2" style="visibility:hidden;"><span id="loader"><i class="fa fa-spinner fa-3x fa-spin"></i></span></div>
            </div>

            <!--div class="col">
               <span class="text-orange">•</span><label for="loc_entidad">Localidad</label>
               <select id="loc_entidad" name="loc_entidad" class="form-control validate[required]">
                  <option value="">Seleccione Localidad...</option>
                  <option value = "Prueba">Prueba</option>
               </select>
            </div>

            <div class="col">
               <span class="text-orange">•</span><label for="barrio_entidad">Barrio</label>
               <select id="barrio_entidad" name="barrio_entidad" class="form-control validate[required]">
                  <option value="">Seleccione Barrio...</option>
                  <option value = "Prueba">Prueba</option>
               </select>
            </div-->

        </div>

        <div class="form-group">
            <span class="text-orange">•</span><label for="tipo_tramite">Dirección de la Instalación(*)</label>
            <input id="dire_entidad" name="dire_entidad" class="form-control input-md validate[required, minSize[4], maxSize[30]]" type="text" required placeholder="Ingresar la Dirección Entidad" value="{{ isset($rayosxDireccion) ? $rayosxDireccion->dire_entidad : '' }}">
         </div>

         <div class="form-group">
            <span class="text-orange">•</span><label for="dire_entidad">Sede de la Instalación(*)</label>
            <input id="sede_entidad" name="sede_entidad" class="form-control input-md validate[required, minSize[4], maxSize[30]]" type="text" required placeholder="Ingresar el Nombre Distintivo de la sede que sera sujeta a inspección" value="{{ isset($rayosxDireccion) ? $rayosxDireccion->sede_entidad : '' }}">
         </div>
         <!-- 2 campos en la fila --->
         <div class="row">
            <div class="col">
               <span class="text-orange">•</span><label for="email_entidad">Correo Electrónico(*)</label>
               <input id="email_entidad" name="email_entidad" class="form-control validate[required, custom[email]] input-md" type="email" required placeholder="Ingresar Correo Electrónico"  value="{{ isset($rayosxDireccion) ? $rayosxDireccion->email_entidad : '' }}">
            </div>

            <div class="col">
               <span class="text-orange">•</span><label for="celular_entidad">Número celular(*)</label>
               <input id="celular_entidad" name="celular_entidad" placeholder="Ingresar Número Celular de contacto en sede de la Instalación" class="form-control validate[required] input-md" type="number" value="{{ isset($rayosxDireccion) ? $rayosxDireccion->celular_entidad : '' }}">
            </div>
         </div>
         <div class="row">
            <div class="col col-md-6">
               <span class="text-orange">•</span><label for="telefono_entidad">Número telefónico fijo</label>
               <input id="telefono_entidad" name="telefono_entidad" placeholder="Ingresar Número telefonico de contacto en sede de la Instalación" class="form-control validate[required] input-md" type="number" value="{{ isset($rayosxDireccion) ? $rayosxDireccion->telefono_entidad : '' }}">
            </div>

            <div class="col">
               <span class="text-orange">•</span><label for="extension_entidad">Extensión</label>
               <input id="extension_entidad" name="extension_entidad" placeholder="Ingresar Extensión telefonica de contacto en sede" class="form-control validate[required] input-md" type="number" value="{{ isset($rayosxDireccion) ? $rayosxDireccion->extension_entidad : '' }}">
            </div>
         </div>

          <div id="btnRegistrarGuardarEntidad" class="col-md-12 pt-200">
            <p align="center">
               <br/>
               <!-- Primer Collapsible - Localizacion Entidad -->
               <button type="submit" class="btn yellow">
                  Guardar y Continuar
               </button>
            </p>
          </div>
      </div>
   </div>
</form>


<!-- Equipos generadores de radiación ionizante -->
<form id="formSeccion2" type="submit" name="formSeccion2" action="{{ route('rayosx.editarEquipo') }}" method="post" class="form-row collapse">
   {{ csrf_field() }}
   <!-- PRIMERA PARTE DEL REGISTRO DE LA SOLICITUD - LOCALIZACION tipoTramite  -->
   <div id="paso2" class="row block w-100 newsletter ">

      <div class="w-100">
        <div class="subtitle">
            <h3><b>Equipos generadores de radiación ionizante:</b></h3>
        </div>
        <div class="row">

           <div class="col">
               <span class="text-orange">•</span><label for="categoria">Categoría</label>
               <input id="id_tramite" name="id_tramite" type="hidden" value="{{ isset($rayosxEquipo) ? $rayosxEquipo->id_tramite_rayosx : '' }}">
               <select id="categoria" name="categoria" class="form-control validate[required]" required>
                  <option value="">Seleccione...</option>
                  <option value="1"
                  {{(isset($rayosxEquipo->categoria) && $rayosxEquipo->categoria == 1)  ? 'selected' : '' }}>
                    Categoria I
                  </option>
                  <option value="2"
                  {{(isset($rayosxEquipo->categoria) && $rayosxEquipo->categoria == 2)  ? 'selected' : '' }}>
                  Categoria II
                  </option>
               </select>
            </div>

            <div class="col">
               <span class="text-orange">•</span><label for="categoria1">Equipos generadores de radicación ionizante</label>
               <select id="categoria1" name="categoria1" class="form-control validate[required]" required>
                  <option value="">Seleccione...</option>
               </select>
            </div>

         </div>

         <div class="row">

            <div class="col">
                  <span class="text-orange">•</span><label for="categoria2">Equipos generadores de radicación ionizante</label>
                  <select id="categoria2" name="categoria2" class="form-control validate[required]" required>
                     <option value="">Seleccione...</option>
                     <option value="1">Radioterapia</option>
                     <option value="2">Radio diagnóstico de alta complejidad</option>
                     <option value="3">Radio diagnóstico de media complejidad</option>
                     <option value="4">Radio diagnóstico de baja complejidad</option>
                     <option value="5">Radiografias odontológicas panóramicas y tomografias orales</option>
                  </select>
               </div>

            <div class="col" id="div_radperia" style="display:none;">
               <span class="text-orange">•</span><label for="categoria1-1">Radiolog&iacute;a odontológica periapical</label>
               <select id="categoria1_1" name="categoria1_1" class="form-control validate[required]">
                  <option value="">Seleccione...</option>
                  <option value="1"
                  {{(isset($rayosxEquipo->categoria1_1) && $rayosxEquipo->categoria1_1 == 1 )  ? 'selected' : '' }}>
                  Equipo de RX odontológico periapical
                  </option>
                  <option value="2"
                  {{(isset($rayosxEquipo->categoria) && $rayosxEquipo->categoria1_1 == 2)  ? 'selected' : '' }}>
                  Equipo de RX odontológico periapical portat&iacute;l
                  </option>
               </select>
            </div>

         </div>

         <div class="row">



         </div>

         <div class="form-group">
            <span class="text-orange">•</span><label for="tipo_visualizacion">Tipo de visualización de la imagen</label>
            <select id="tipo_visualizacion" name="tipo_visualizacion" class="form-control validate[required]" required>
               <option value="1"
               {{(isset($rayosxEquipo->tipo_visualizacion) && $rayosxEquipo->tipo_visualizacion == 1 )  ? 'selected' : '' }}>
                 Digital
               </option>
               <option value="2"
               {{(isset($rayosxEquipo->tipo_visualizacion) && $rayosxEquipo->tipo_visualizacion == 2 )  ? 'selected' : '' }}>
                 Digitalizado
               </option>
               <option value="3"
               {{(isset($rayosxEquipo->tipo_visualizacion) && $rayosxEquipo->tipo_visualizacion == 3 )  ? 'selected' : '' }}>
                 Analogo
               </option>
               <option value="4"
               {{(isset($rayosxEquipo->tipo_visualizacion) && $rayosxEquipo->tipo_visualizacion == 4 )  ? 'selected' : '' }}>
                 Revelado Automatico
               </option>
               <option value="5"
               {{(isset($rayosxEquipo->tipo_visualizacion) && $rayosxEquipo->tipo_visualizacion == 5 )  ? 'selected' : '' }}>
               Revelado Manual
               </option>
               <option value="6"
               {{(isset($rayosxEquipo->tipo_visualizacion) && $rayosxEquipo->tipo_visualizacion == 6 )  ? 'selected' : '' }}>
               Monitor Analogo
               </option>
               <option value="7"
               {{(isset($rayosxEquipo->tipo_visualizacion) && $rayosxEquipo->tipo_visualizacion == 7 )  ? 'selected' : '' }}>
               No Aplica
               </option>
            </select>
         </div>

         <div class="row">

            <div class="col">
               <span class="text-orange">•</span><label for="marca_equipo">Marca equipo</label>
               <input id="marca_equipo" name="marca_equipo" placeholder="Ingresar Marca equipo" class="form-control input-md validate[required, minSize[4], maxSize[30]]"  required type="text"  value="{{ isset($rayosxEquipo) ? $rayosxEquipo->marca_equipo : '' }}">
            </div>

            <div class="col">
               <span class="text-orange">•</span><label for="modelo_equipo">Modelo equipo</label>
               <input id="modelo_equipo" name="modelo_equipo" placeholder="Ingresar Modelo equipo" class="form-control input-md validate[required, minSize[4], maxSize[30]]"  required  type="text"  value="{{ isset($rayosxEquipo) ? $rayosxEquipo->modelo_equipo : '' }}">
            </div>

            <div class="col">
               <span class="text-orange">•</span><label for="serie_equipo">Serie equipo</label>
               <input id="serie_equipo" name="serie_equipo" placeholder="Ingresar Serie equipo" class="form-control input-md validate[required, minSize[4], maxSize[30]]"  required  type="text" value="{{ isset($rayosxEquipo) ? $rayosxEquipo->serie_equipo : '' }}">
            </div>

            <div class="col">
               <span class="text-orange">•</span><label for="marca_tubo_rx">Marca tubo RX</label>
               <input id="marca_tubo_rx" name="marca_tubo_rx" placeholder="Ingresar Marca tubo RX" class="form-control input-md validate[required, minSize[4], maxSize[30]]"  required type="text" value="{{ isset($rayosxEquipo) ? $rayosxEquipo->marca_tubo_rx : '' }}">
            </div>

         </div>

         <div class="row">

            <div class="col">
               <span class="text-orange">•</span><label for="modelo_tubo_rx">Modelo tubo RX</label>
               <input id="modelo_tubo_rx" name="modelo_tubo_rx" placeholder="Ingresar Modelo tubo RX" class="form-control input-md validate[required, minSize[4], maxSize[30]]"  required  type="text" value="{{ isset($rayosxEquipo) ? $rayosxEquipo->modelo_tubo_rx : '' }}">
            </div>

            <div class="col">
               <span class="text-orange">•</span><label for="serie_tubo_rx">Serie tubo RX</label>
               <input id="serie_tubo_rx" name="serie_tubo_rx" placeholder="Ingresar Serie tubo RX" class="form-control input-md validate[required, minSize[4], maxSize[30]]"  required  type="text" value="{{ isset($rayosxEquipo) ? $rayosxEquipo->serie_tubo_rx : '' }}">
            </div>

            <div class="col">
               <span class="text-orange">•</span><label for="tension_tubo_rx">Tensión máxima tubo RX [kV]</label>
               <input id="tension_tubo_rx" name="tension_tubo_rx" placeholder="Ingresar Tensión máxima tubo RX [kV]" class="form-control input-md validate[required, minSize[1], maxSize[3]]"  required type="number" step="any" value="{{ isset($rayosxEquipo) ? $rayosxEquipo->tension_tubo_rx : '' }}">
            </div>

            <div class="col">
               <span class="text-orange">•</span><label for="contiene_tubo_rx">Cont. Max del tubo RX [mA]</label>
               <input id="contiene_tubo_rx" name="contiene_tubo_rx" placeholder="Ingresar Contiene máxima del tubo RX [mA]" class="form-control input-md validate[required, minSize[1], maxSize[3]]" required type="number" step="any" value="{{ isset($rayosxEquipo) ? $rayosxEquipo->contiene_tubo_rx : '' }}">
            </div>
         </div>

         <div class="row">

            <div class="col">
               <span class="text-orange">•</span><label for="energia_fotones">Energ&iacute;a de fotones [MeV]</label>
               <input id="energia_fotones" name="energia_fotones" placeholder="Ingresar Energ&iacute;a de fotones [MeV]" class="form-control input-md validate[required, minSize[1], maxSize[3]]" required type="number" step="any" value="{{ isset($rayosxEquipo) ? $rayosxEquipo->energia_fotones : '' }}">
            </div>

            <div class="col">
               <span class="text-orange">•</span><label for="energia_electrones">Energ&iacute;a de electrones [MeV]</label>
               <input id="energia_electrones" name="energia_electrones" placeholder="Ingresar Energ&iacute;a de electrones [MeV]" class="form-control input-md validate[required, minSize[1], maxSize[3]]" required type="number" step="any" value="{{ isset($rayosxEquipo) ? $rayosxEquipo->energia_fotones : '' }}">
            </div>

            <div class="col">
               <span class="text-orange">•</span><label for="carga_trabajo">Carga de trabajo [mA.min/semana]</label>
               <input id="carga_trabajo" name="carga_trabajo" placeholder="Ingresar Carga de trabajo [mA.min/semana]" class="form-control input-md validate[required, minSize[1], maxSize[3]]" required type="number" step="any" value="{{ isset($rayosxEquipo) ? $rayosxEquipo->carga_trabajo : '' }}">
            </div>

         </div>



         <div class="form-group">
            <span class="text-orange">•</span><label for="ubicacion_equipo">Ubicación del equipo de la instalación</label>
            <input id="ubicacion_equipo" name="ubicacion_equipo" placeholder="Ingresar Ubicación del equipo de la instalación" class="form-control input-md validate[required, minSize[4], maxSize[30]]" required type="text" value="{{ isset($rayosxEquipo) ? $rayosxEquipo->ubicacion_equipo : '' }}">
         </div>

         <div class="row">

            <div class="col">
               <span class="text-orange">•</span><label for="anio_fabricacion">A&ntilde;o de fabricación del equipo</label>
               <input id="anio_fabricacion" name="anio_fabricacion" placeholder="Ingresar A&ntilde;o de fabricación del equipo" class="form-control input-md validate[required, minSize[4], maxSize[4]]" required type="number" value="{{ isset($rayosxEquipo) ? $rayosxEquipo->anio_fabricacion : '' }}">
            </div>

            <div class="col">
               <span class="text-orange">•</span><label for="anio_fabricacion_tubo">A&ntilde;o de fabricación del tubo</label>
               <input id="anio_fabricacion_tubo" name="anio_fabricacion_tubo" placeholder="Ingresar A&ntilde;o de fabricación del tubo" class="form-control input-md validate[required, minSize[4], maxSize[4]]" required type="number" value="{{ isset($rayosxEquipo) ? $rayosxEquipo->anio_fabricacion_tubo : '' }}">
            </div>

            <div class="col" id="div_numpermiso" style="display:none">
               <span class="text-orange">•</span><label for="numero_permiso">Número de permiso de comercialización</label>
               <input id="numero_permiso" name="numero_permiso" placeholder="Ingresar Número de permiso de comercialización" class="form-control input-md validate[required, minSize[4], maxSize[30]]" type="text" value="{{ isset($rayosxEquipo) ? $rayosxEquipo->numero_permiso : '' }}">
            </div>
         </div>

          <div id="btnGuardarEquipos" class="col-md-12 pt-200">
            <p align="center">
               <br/>
               <!-- Primer Collapsible - Localizacion Entidad -->
               <button type="submit" class="btn yellow">
                  Guardar y Continuar
               </button>
            </p>
          </div>
      </div>
   </div>
</form>

<!-- PRIMERA PARTE DEL REGISTRO DE LA SOLICITUD - LOCALIZACION tipoTramite  -->
<!-- Equipos generadores de radiación ionizante -->
      <form id="formSeccion3-1" type="submit" name="formSeccion3-1" action="{{ route('rayosx.editarOficialToe') }}"method="post" >
        <div id="paso3-1" class="row block w-100 newsletter collapse">
           <div class="w-100">
             <div class="subtitle">
                 <h3><b>Trabajadores ocupacionalmente expuestos - TOE:</b></h3>
             </div>
             <div class="row">

                <div class="col">
      {{ csrf_field() }}
     <h4><b><span class="text-orange">•</span>Oficial de protección radiológica/Encargado de protección Radiológica</b></h4>

     <div class="row">

         <div class="col">
            <span class="text-orange">•</span><label for="encargado_pnombre">Primer Nombre</label>
            <input id="id_tramite" name="id_tramite" type="hidden" value="{{ isset($rayosxOficialToe) ? $rayosxOficialToe->id_tramite_rayosx : '' }}">
            <input id="encargado_pnombre" name="encargado_pnombre" placeholder="Ingresar Primer Nombre" class="form-control input-md validate[required, minSize[4], maxSize[30]]" required type="text" value="{{ isset($rayosxOficialToe) ? $rayosxOficialToe->toe_pnombre : '' }}">
         </div>

         <div class="col">
            <span class="text-orange">•</span><label for="encargado_snombre">Segundo Nombre</label>
            <input id="encargado_snombre" name="encargado_snombre" placeholder="Ingresar Segundo Nombre" class="form-control input-md validate[minSize[4], maxSize[30]]"  type="text" value="{{ isset($rayosxOficialToe) ? $rayosxOficialToe->toe_snombre : '' }}">
         </div>

         <div class="col">
            <span class="text-orange">•</span><label for="encargado_papellido">Primer Apellido</label>
            <input id="encargado_papellido" name="encargado_papellido" placeholder="Ingresar Primer Apellido" class="form-control input-md validate[required, minSize[4], maxSize[30]]" required type="text" value="{{ isset($rayosxOficialToe) ? $rayosxOficialToe->toe_papellido : '' }}">
         </div>
         <div class="col">
            <span class="text-orange">•</span><label for="encargado_sapellido">Segundo Apellido</label>
            <input id="encargado_sapellido" name="encargado_sapellido" placeholder="Ingresar Segundo Apellido" class="form-control input-md validate[minSize[4], maxSize[30]]" type="text" value="{{ isset($rayosxOficialToe) ? $rayosxOficialToe->toe_sapellido : '' }}">
         </div>
      </div>

      <div class="row">
         <div class="col">
            <span class="text-orange">•</span><label for="encargado_tdocumento">Tipo Documento</label>
            <select id="encargado_tdocumento" name="encargado_tdocumento" class="form-control validate[required]" required>
               <option value=""> - Seleccione Tipo Documento -</option>
               @foreach ($tiposIdentificacion as $tipoIdentificacion => $value)
               @if($value!=5)
               <option value="{{ $value}}"
               {{(isset($rayosxOficialToe->toe_tdocumento) && $rayosxOficialToe->toe_tdocumento == $value )  ? 'selected' : '' }}>
                  {{ $tipoIdentificacion}}</option>
                @endif
               @endforeach
            </select>
         </div>

         <div class="col">
            <span class="text-orange">•</span><label for="encargado_ndocumento">Número Documento</label>
            <input id="encargado_ndocumento" name="encargado_ndocumento" placeholder="Ingresar Número Documento" class="form-control input-md validate[required, minSize[4], maxSize[15]]" required type="number" value="{{ isset($rayosxOficialToe) ? $rayosxOficialToe->toe_ndocumento : '' }}">
         </div>

         <div class="col">
            <span class="text-orange">•</span><label for="encargado_lexpedicion">Lugar Expedición</label>
            <input id="encargado_lexpedicion" name="encargado_lexpedicion" placeholder="Ingresar Lugar Expedición" required class="form-control input-md validate[required, minSize[4], maxSize[30]]" type="text" value="{{ isset($rayosxOficialToe) ? $rayosxOficialToe->toe_lexpedicion : '' }}">
         </div>

      </div>

      <div class="form-group">
         <span class="text-orange">•</span><label for="encargado_correo">Correo Electrónico</label>
         <input id="encargado_correo" name="encargado_correo" placeholder="Ingresar Correo Electrónico" class="form-control validate[required, custom[email]]" type="email" value="{{ isset($rayosxOficialToe) ? $rayosxOficialToe->toe_correo : '' }}" required>
      </div>

      <div class="row">
         <div class="col">
            <span class="text-orange">•</span><label for="encargado_nivel">Nivel Académico</label>
            <select id="encargado_nivel" name="encargado_nivel" class="form-control validate[required]" required>
               <option value="">Seleccione...</option>
               @foreach ($prNivelacademico as $nivelAcademico => $value)
                <option value="{{$value}}"
                {{(isset($rayosxOficialToe->toe_nivel) && $rayosxOficialToe->toe_nivel == $value )  ? 'selected' : '' }}>
                {{ $nivelAcademico}}
                </option>
                @endforeach
            </select>
         </div>

         <div class="col">
            <span class="text-orange">•</span><label for="encargado_profesion">Profesión</label>
            <select id="encargado_profesion" name="encargado_profesion" class="form-control validate[required]" required>
               <option value="">Seleccione...</option>
               @foreach ($prProgramasUniv as $programaAcademico => $value)
                <option value="{{$value}}"
                {{(isset($rayosxOficialToe->toe_profesion) && $rayosxOficialToe->toe_profesion == $value )  ? 'selected' : '' }}>
                {{ $programaAcademico}}
                </option>
                @endforeach
            </select>
         </div>

      </div>

      <div id="btnGuardarOficialTOE" class="col-md-12 pt-200">
         <p align="center">
            <br/>
            <!-- Primer Collapsible - Localizacion Entidad -->
            <button type="submit" class="btn yellow">
               Guardar Encargado TOE
            </button>
         </p>
       </div>
     </div>
  </div>
  </form>


   <br/>
   <h4><b><span class="text-orange">•</span>TOE - Trabajadores Ocacionalmente Expuestos</b></h4>
   <div id="divSeccion3-2" style="display:none">
   <a id="nuevoTOE" class="btn yellow"> Nuevo Toe</a>

   <!-- Equipos generadores de radiación ionizante -->
   <form id="formSeccion3-2" type="submit" name="formSeccion3-2" action="" method="post" class="collapse">
   <!-- PRIMERA PARTE DEL REGISTRO DE LA SOLICITUD - LOCALIZACION tipoTramite  -->
      {{ csrf_field() }}
      <div class="row">

         <div class="col">
            <span class="text-orange">•</span><label for="encargado_pnombre">Primer Nombre</label>
            <input id="encargado_pnombre" name="encargado_pnombre" placeholder="Ingresar Primer Nombre" class="form-control input-md validate[required, minSize[4], maxSize[30]]"  type="text">
         </div>

         <div class="col">
            <span class="text-orange">•</span><label for="encargado_snombre">Segundo Nombre</label>
            <input id="encargado_snombre" name="encargado_snombre" placeholder="Ingresar Segundo Nombre" class="form-control input-md validate[required, minSize[4], maxSize[30]]"  type="text">
         </div>

         <div class="col">
            <span class="text-orange">•</span><label for="encargado_papellido">Primer Apellido</label>
            <input id="encargado_papellido" name="encargado_papellido" placeholder="Ingresar Primer Apellido" class="form-control input-md validate[required, minSize[4], maxSize[30]]"  type="text">
         </div>
         <div class="col">
            <span class="text-orange">•</span><label for="encargado_sapellido">Segundo Apellido</label>
            <input id="encargado_sapellido" name="encargado_sapellido" placeholder="Ingresar Segundo Apellido" class="form-control input-md validate[required, minSize[4], maxSize[30]]"  type="text">
         </div>
      </div>

      <div class="row">
         <div class="col">
            <span class="text-orange">•</span><label for="encargado_correo">Correo Electrónico</label>
            <input id="encargado_correo" name="encargado_correo" placeholder="Ingresar Correo Electrónico" class="form-control validate[required, custom[email]]" type="email" >
         </div>
         <div class="col">
            <span class="text-orange">•</span><label for="encargado_tdocumento">Tipo Documento</label>
            <select id="encargado_tdocumento" name="encargado_tdocumento" class="form-control validate[required]">
               <option value=""> - Seleccione Tipo Documento -</option>
               @foreach ($tiposIdentificacion as $tipoIdentificacion => $value)
               <option value="{{ $value}}"> {{ $tipoIdentificacion}}</option>
               @endforeach
            </select>
         </div>

         <div class="col">
            <span class="text-orange">•</span><label for="encargado_ndocumento">Número Documento</label>
            <input id="encargado_ndocumento" name="encargado_ndocumento" placeholder="Ingresar Número Documento" class="form-control input-md validate[required, minSize[4], maxSize[15]]"  type="number">
         </div>

         <div class="col">
            <span class="text-orange">•</span><label for="encargado_lexpedicion">Lugar Expedición</label>
            <input id="encargado_lexpedicion" name="encargado_lexpedicion" placeholder="Ingresar Lugar Expedición" class="form-control input-md validate[required, minSize[4], maxSize[15]]"  type="text">
         </div>

      </div>

      <div class="row">

         <div class="col">
            <span class="text-orange">•</span><label for="encargado_nivel">Nivel Académico</label>
            <select id="encargado_nivel" name="encargado_nivel" class="form-control validate[required]" required>
               <option value="">Seleccione...</option>
               @foreach ($prNivelacademico as $nivelAcademico => $value)
                <option value="{{$value}}"> {{ $nivelAcademico}}</option>
                @endforeach
            </select>
         </div>

         <div class="col">
            <span class="text-orange">•</span><label for="encargado_profesion">Profesión</label>
            <select id="encargado_profesion" name="encargado_profesion" class="form-control validate[required]">
               <option value="">Seleccione...</option>
               @foreach ($prProgramasUniv as $programaAcademico => $value)
                <option value="{{$value}}"> {{ $programaAcademico}}</option>
                @endforeach
            </select>
         </div>

      </div>

      <div class="row">

         <div class="col">
            <span class="text-orange">•</span><label for="toe_ult_entrenamiento">Fecha del último entrenamiento en protección radiológica</label>
            <input id="toe_ult_entrenamiento" name="toe_ult_entrenamiento" placeholder="Ingresar Fecha del último entrenamiento en protección radiológica" class="form-control validate[required] input-md" type="date" required>
         </div>

         <div class="col">
            <span class="text-orange">•</span><label for="toe_pro_entrenamiento">Fecha del próximo entrenamiento en protección radiológica</label>
            <input id="toe_pro_entrenamiento" name="toe_pro_entrenamiento" placeholder="Ingresar Fecha del próximo entrenamiento en protección radiológica" class="form-control validate[required] input-md" type="date" required>
         </div>

         <div class="col">
            <span class="text-orange">•</span><label for="toe_registro">Número del registro profesional de salud</label>
            <input id="toe_registro" name="toe_registro" placeholder="Ingresar Número del registro profesional de salud" class="form-control input-md validate[required, minSize[4], maxSize[15]]" type="number">
         </div>

      </div>


      <div id="btnGuardarOficialTOE" class="col-md-12 pt-200">
         <p align="center">
            <br/>
            <!-- Primer Collapsible - Localizacion Entidad -->
            <button type="submit" class="btn yellow">
               Guardar Ocacionalmente TOE
            </button>
         </p>
       </div>

   </form>
 </div>

      <table class="display nowrap table table-hover" id="tabla_toe" style="width:100%;">
         <thead>
            <tr>
               <th>ID</th>
               <th>Primer Nombre</th>
               <th>Segundo Nombre</th>
               <th>Primer Apellido</th>
               <th>Segundo Apellido</th>
               <th>Editar</th>
            </tr>
         </thead>

         <tbody>
           @if($rayosxTemporalToe->count())
           @foreach($rayosxTemporalToe as $rayoxTemporalToe)
           <tr>
             <td>{{$rayoxTemporalToe->id_toe_rayosx}}</td>
             <td>{{$rayoxTemporalToe->toe_pnombre}}</td>
             <td>{{$rayoxTemporalToe->toe_snombre}}</td>
             <td>{{$rayoxTemporalToe->toe_papellido}}</td>
             <td>{{$rayoxTemporalToe->toe_sapellido}}</td>
             <td>{{$rayoxTemporalToe->toe_sapellido}}</td>
            </tr>
            @endforeach
            @else
            <tr>
             <td colspan="8">0  TOE - Trabajadores Ocacionalmente Expuestos Encontrados</td>
           </tr>
           @endif
         </tbody>
      </table>


<!--
      <div id="btnContinuarPaso3" class="col-md-12 pt-200">
         <p align="center">
            <br/>
            <form id="formSeccion3-3" type="submit" name="formSeccion3-3" action="{{route('rayosx.verificarTOE')}}" method="post" class="collapse">
               {{ csrf_field() }}
               <!-- Continuar con paso 4 - En caso de haber registrado TOE correctamente -->
          <!--     <button type="submit" id="button3-3" class="btn yellow">
                  Continuar
               </button>
            </form>
         </p>
       </div>-->

   </form>

   </div>
</div>





   <!-- PRIMERA PARTE DEL REGISTRO DE LA SOLICITUD - LOCALIZACION tipoTramite  -->
         <!-- Equipos generadores de radiación ionizante -->
         <form id="formSeccion4-1" type="submit" name="formSeccion4-1" action="{{route('rayosx.editarDirector')}}" method="post">
            {{ csrf_field() }}
            <div id="paso4-1" class="row block w-100 newsletter collapse">

               <div class="w-100">
                 <div class="subtitle">
                     <h3><b>Talento Humano:</b></h3>
                 </div>
                 <h4><b><span class="text-orange">•</span>Oficial de protección radiológica/Encargado de protección Radiológica</b></h4>

                 <!--div class="col">
                     <span class="text-orange">•</span><label for="tipo_titulo">La IPS cuenta con el talento humano estipulado en el articulo 6 y 7, numeral 7.1?</label>
                     <select id="visita_previa" name="visita_previa_th" class="form-control validate[required]">
                        <option value="">Seleccione...</option>
                        <option value="1">SI</option>
                        <option value="2">NO</option>
                     </select>
                  </div-->

                  <h4><b><span class="text-orange">•</span>Director Técnico</b></h4>

         <div class="row">

            <div class="col">
               <span class="text-orange">•</span><label for="talento_pnombre">Primer Nombre</label>
               <input id="id_tramite" name="id_tramite" type="hidden" value="{{ isset($rayosxTalento) ? $rayosxTalento->id_tramite_rayosx : '' }}">
               <input id="talento_pnombre" name="talento_pnombre" placeholder="Ingresar Primer Nombre" class="form-control input-md validate[required, minSize[4], maxSize[30]]" required type="text" value="{{ isset($rayosxTalento) ? $rayosxTalento->talento_pnombre : '' }}">
            </div>

            <div class="col">
               <span class="text-orange">•</span><label for="talento_snombre">Segundo Nombre</label>
               <input id="talento_snombre" name="talento_snombre" placeholder="Ingresar Segundo Nombre" class="form-control input-md validate[required, minSize[4], maxSize[30]]"  type="text" value="{{ isset($rayosxTalento) ? $rayosxTalento->talento_snombre : '' }}">
            </div>

            <div class="col">
               <span class="text-orange">•</span><label for="talento_papellido">Primer Apellido</label>
               <input id="talento_papellido" name="talento_papellido" placeholder="Ingresar Primer Apellido" class="form-control input-md validate[required, minSize[4], maxSize[30]]" required  type="text" value="{{ isset($rayosxTalento) ? $rayosxTalento->talento_papellido : '' }}">
            </div>

            <div class="col">
               <span class="text-orange">•</span><label for="talento_sapellido">Segundo Apellido</label>
               <input id="talento_sapellido" name="talento_sapellido" placeholder="Ingresar Segundo Apellido"  class="form-control input-md validate[required, minSize[4], maxSize[30]]"  type="text" value="{{ isset($rayosxTalento) ? $rayosxTalento->talento_sapellido : '' }}">
            </div>
         </div>

         <div class="row">

            <div class="col">
               <span class="text-orange">•</span><label for="talento_tdocumento">Tipo Documento</label>
               <select id="talento_tdocumento" name="talento_tdocumento" class="form-control validate[required]" required>
                  <option value=""> - Seleccione Tipo Documento -</option>
                     @foreach ($tiposIdentificacion as $tipoIdentificacion => $value)
                     <option value="{{ $value}}"> {{ $tipoIdentificacion}}</option>
                     @endforeach
                  </select>
            </div>

            <div class="col">
               <span class="text-orange">•</span><label for="talento_ndocumento">Número Documento</label>
               <input id="talento_ndocumento" name="talento_ndocumento" placeholder="Ingresar Número Documento"  class="form-control input-md validate[required, minSize[4], maxSize[15]]" required type="number" value="{{ isset($rayosxTalento) ? $rayosxTalento->talento_ndocumento : '' }}">
            </div>

            <div class="col">
               <span class="text-orange">•</span><label for="talento_lexpedicion">Lugar Expedición</label>
               <input id="talento_lexpedicion" name="talento_lexpedicion" placeholder="Ingresar Lugar Expedición"  class="form-control input-md validate[required, minSize[4], maxSize[30]]" required  type="text" value="{{ isset($rayosxTalento) ? $rayosxTalento->talento_lexpedicion : '' }}">
            </div>

         </div>

         <div class="col">
            <span class="text-orange">•</span><label for="talento_correo">Correo Electrónico</label>
            <input id="talento_correo" name="talento_correo" placeholder="Ingresar Correo Electrónico" class="form-control validate[required, custom[email]] input-md" type="email" required value="{{ isset($rayosxTalento) ? $rayosxTalento->talento_correo : '' }}">
         </div>

         <br/>

         <h4><b><span class="text-orange">•</span>Idoneidad Profesional</b></h4>

         <div class="row">

            <div class="col">
               <span class="text-orange">•</span><label for="talento_titulo">Titulo de pregrado obtenido</label>
               <input id="talento_titulo" name="talento_titulo" placeholder="Titulo de pregrado obtenido"  class="form-control input-md validate[required, minSize[4], maxSize[30]]" required  type="text" value="{{ isset($rayosxTalento) ? $rayosxTalento->talento_titulo : '' }}">
            </div>

            <div class="col">
               <span class="text-orange">•</span><label for="talento_universidad">Universidad que otorgó el titulo de pregrado</label>
               <input id="talento_universidad" name="talento_universidad" placeholder="Universidad que otorgó el titulo de pregrado"  class="form-control input-md validate[required, minSize[4], maxSize[30]]" required  value="{{ isset($rayosxTalento) ? $rayosxTalento->talento_universidad : '' }}">
            </div>

            <div class="col">
               <span class="text-orange">•</span><label for="talento_libro">Libro del diploma de pregrado</label>
               <input id="talento_libro" name="talento_libro" placeholder="Libro del diploma de pregrado"  class="form-control input-md validate[required, minSize[4], maxSize[15]]" type="text" value="{{ isset($rayosxTalento) ? $rayosxTalento->talento_libro : '' }}">
            </div>

         </div>

         <div class="row">

            <div class="col">
               <span class="text-orange">•</span><label for="talento_registro">Registro del diploma de pregrado</label>
               <input id="talento_registro" name="talento_registro" placeholder="Registro del diploma de pregrado"  class="form-control input-md validate[required, minSize[4], maxSize[15]]" required type="text" value="{{ isset($rayosxTalento) ? $rayosxTalento->talento_registro : '' }}">
            </div>

            <div class="col">
               <span class="text-orange">•</span><label for="talento_fecha_diploma">Fecha diploma de pregrado</label>
               <input id="talento_fecha_diploma" name="talento_fecha_diploma" placeholder="Fecha diploma de pregrado" class="form-control input-md validate[required]" required  type="date" value="{{ isset($rayosxTalento) ? $rayosxTalento->talento_fecha_diploma : '' }}">
            </div>

            <div class="col">
               <span class="text-orange">•</span><label for="talento_resolucion">Resolución convalidación titulo pregrado</label>
               <input id="talento_resolucion" name="talento_resolucion" placeholder="Resolución convalidación titulo pregrado"  class="form-control input-md validate[required, minSize[4], maxSize[30]]" required  type="text" value="{{ isset($rayosxTalento) ? $rayosxTalento->talento_resolucion : '' }}">
            </div>

         </div>

         <div class="row">

            <div class="col">
               <span class="text-orange">•</span><label for="talento_fecha_convalida">Fecha convalidación titulo de pregrado</label>
               <input id="talento_fecha_convalida" name="talento_fecha_convalida" placeholder="Fecha convalidación titulo de pregrado" class="form-control validate[required] input-md" type="date" value="{{ isset($rayosxTalento) ? $rayosxTalento->talento_fecha_convalida : '' }}">
            </div>

            <div class="col">
               <span class="text-orange">•</span><label for="talento_nivel">Nivel Académico último posgrado</label>
               <select id="talento_nivel" name="talento_nivel" class="form-control validate[required]" required>
                  <option value="">Seleccione...</option>
                  @foreach ($prNivelacademico as $nivelAcademico => $value)
                  <option value="{{$value}}"> {{ $nivelAcademico}}</option>
                  @endforeach
               </select>
            </div>

            <div class="col">
               <span class="text-orange">•</span><label for="talento_titulo_pos">Título de posgrado obtenido</label>
               <input id="talento_titulo_pos" name="talento_titulo_pos" placeholder="Título de posgrado obtenido"  class="form-control input-md validate[required, minSize[4], maxSize[30]]"  type="text" value="{{ isset($rayosxTalento) ? $rayosxTalento->talento_nivel : '' }}">
            </div>

         </div>

         <div class="row">

            <div class="col">
               <span class="text-orange">•</span><label for="talento_universidad_pos">Universidad que otorgó el titulo de posgrado</label>
               <input id="talento_universidad_pos" name="talento_universidad_pos" placeholder="Universidad que otorgó el titulo de posgrado"  class="form-control input-md validate[required, minSize[4], maxSize[30]]"  type="text" value="{{ isset($rayosxTalento) ? $rayosxTalento->talento_universidad_pos : '' }}">
            </div>

            <div class="col">
               <span class="text-orange">•</span><label for="talento_libro_pos">Libro del diploma de posgrado</label>
               <input id="talento_libro_pos" name="talento_libro_pos" placeholder="Libro del diploma de posgrado"class="form-control input-md validate[required, minSize[4], maxSize[15]]"  type="text" value="{{ isset($rayosxTalento) ? $rayosxTalento->talento_libro_pos : '' }}">
            </div>

            <div class="col">
               <span class="text-orange">•</span><label for="talento_registro_pos">Registro del diploma de posgrado</label>
               <input id="talento_registro_pos" name="talento_registro_pos" placeholder="Registro del diploma de posgrado" class="form-control input-md validate[required, minSize[4], maxSize[15]]"  type="text" value="{{ isset($rayosxTalento) ? $rayosxTalento->talento_registro_pos : '' }}">
            </div>
         </div>

         <div class="row">

            <div class="col">
               <span class="text-orange">•</span><label for="talento_fecha_diploma_pos">Fecha diploma de posgrado</label>
               <input id="talento_fecha_diploma_pos" name="talento_fecha_diploma_pos" placeholder="Fecha diploma de posgrado" class="form-control validate[required]" type="date" value="{{ isset($rayosxTalento) ? $rayosxTalento->talento_fecha_diploma_pos : '' }}">
            </div>

            <div class="col">
               <span class="text-orange">•</span><label for="talento_resolucion">Resolución convalidación titulo posgrado</label>
               <input id="talento_resolucion_pos" name="talento_resolucion_pos" placeholder="Resolución convalidación titulo posgrado" class="form-control input-md validate[required, minSize[4], maxSize[15]]"  type="text" value="{{ isset($rayosxTalento) ? $rayosxTalento->talento_resolucion_pos : '' }}">
            </div>

            <div class="col">
               <span class="text-orange">•</span><label for="talento_fecha_convalida_pos">Fecha convalidación titulo de posgrado</label>
               <input id="talento_fecha_convalida_pos" name="talento_fecha_convalida_pos" placeholder="Fecha convalidación titulo de posgrado" class="form-control validate[required] input-md" type="date" value="{{ isset($rayosxTalento) ? $rayosxTalento->talento_fecha_convalida_pos : '' }}">
            </div>
         </div>

         <div id="btnGuardarDirector" class="col-md-12 pt-200">
            <p align="center">
               <br/>
               <!-- Primer Collapsible - Localizacion Entidad -->
               <button type="submit" class="btn yellow">
                  Guardar Director Técnico
               </button>
            </p>
          </div>

       </form>

       <br/>
       <h4><b><span class="text-orange">•</span>Equipos u objetos de prueba</b></h4>
       <div id="divSeccion4-2" style="display:none">
         <form id="formSeccion4-2" type="submit" name="formSeccion4-2" action="{{route('rayosx.guardarEquipoPrueba')}}" method="post">
            {{ csrf_field() }}

            <div class="row">
              <div class="col">
                <a id="nuevoEquipo" class="btn yellow"> Agregar Equipo</a>
              </div>

               <div class="col">
                  <span class="text-orange">•</span><label for="obj_nombre">Fecha diploma de posgrado</label>
                  <input id="obj_nombre" name="obj_nombre" placeholder="Nombre del Equipo" class="form-control validate[required] input-md" type="date">
               </div>

               <div class="col">
                  <span class="text-orange">•</span><label for="obj_marca">Marca del equipo</label>
                  <input id="obj_marca" name="obj_marca" placeholder="Marca del equipo" class="form-control validate[required] input-md" type="text">
               </div>

               <div class="col">
                  <span class="text-orange">•</span><label for="obj_modelo">Modelo del equipo</label>
                  <input id="obj_modelo" name="obj_modelo" placeholder="Modelo del Equipo" class="form-control validate[required] input-md" type="text">
               </div>
            </div>

            <div class="row">

               <div class="col">
                  <span class="text-orange">•</span><label for="obj_serie">Serie del equipo</label>
                  <input id="obj_serie" name="obj_serie" placeholder="Serie del Equipo" class="form-control validate[required] input-md" type="text">
               </div>

               <div class="col">
                  <span class="text-orange">•</span><label for="obj_calibracion">Calibración</label>
                  <input id="obj_calibracion" name="obj_calibracion" placeholder="Calibración" class="form-control validate[required] input-md" type="text">
               </div>

               <div class="col">
                  <span class="text-orange">•</span><label for="obj_vigencia">Vigencia de calibración</label>
                  <select id="obj_vigencia" name="obj_vigencia" class="form-control validate[required]">
                     <option value="">Seleccione...</option>
                     <option value="1">Un (1) A&ntilde;o</option>
                     <option value="2">Dos (2) A&ntilde;os</option>
                     <option value="3">Otra, definida por el fabricante</option>
                  </select>
               </div>
            </div>

            <div class="row">

               <div class="col">
                  <span class="text-orange">•</span><label for="obj_fecha">Fecha de calibración</label>
                  <input id="obj_fecha" name="obj_fecha" placeholder="Fecha de la calibracion" class="form-control validate[required] input-md" type="date">
               </div>

               <div class="col">
                  <span class="text-orange">•</span><label for="obj_manual">Manual Técnico y ficha Técnica</label>
                  <select id="obj_manual" name="obj_manual" class="form-control validate[required]">
                     <option value="">Seleccione...</option>
                     <option value="1">Posee manual técnico</option>
                     <option value="2">Pose ficha técnica</option>
                  </select>
               </div>

               <div class="col">
                  <span class="text-orange">•</span><label for="obj_uso">Usos</label>
                  <textarea id="obj_uso" name="obj_uso" class="form-control validate[required] input-md"></textarea>
               </div>
            </div>

            <div id="btnGuardarEquipoPrueba" class="col-md-12 pt-200">
               <p align="center">
                  <br/>
                  <!-- Primer Collapsible - Localizacion Entidad -->
                  <button type="submit" class="btn yellow">
                     Guardar Equipo/Objeto Prueba
                  </button>
               </p>
             </div>
         </form>
       </div>

         <table class="display nowrap" id="tabla_equiprueba" style="width:100%">
            <thead>
               <tr>
                  <th>ID</th>
                  <th>Nombre del equipo</th>
                  <th>Marca del equipo</th>
                  <th>Modelo del equipo</th>
                  <th>Serie del equipo</th>
                  <th>Calibración</th>
                  <th>Vigencia de calibración</th>
                  <th>Fecha de calibración</th>
                  <th>Manual Técnico y ficha Técnica</th>
                  <th>Usos</th>
               </tr>
            </thead>

            <tbody>
              @if($rayosxObjprueba->count())
              @foreach($rayosxObjprueba as $rayoxObjprueba)
              <tr>
                <td>{{$rayoxObjprueba->id_obj_rayosx}}</td>
                <td>{{$rayoxObjprueba->obj_nombre}}</td>
                <td>{{$rayoxObjprueba->obj_marca}}</td>
                <td>{{$rayoxObjprueba->obj_modelo}}</td>
                <td>{{$rayoxObjprueba->obj_serie}}</td>
                <td>{{$rayoxObjprueba->obj_calibracion}}</td>
                <td>{{$rayoxObjprueba->obj_vigencia}}</td>
                <td>{{$rayoxObjprueba->obj_fecha}}</td>
                <td>{{$rayoxObjprueba->obj_manual}}</td>
                <td>{{$rayoxObjprueba->obj_uso}}</td>
               </tr>
               @endforeach
               @else
               <tr>
                <td colspan="8">0  Equipos Encontrados</td>
              </tr>
              @endif
            </tbody>


         </table>

</form>

</div>
</div>


<!-- Equipos generadores de radiación ionizante -->
<form id="formSeccion5" type="submit" name="formSeccion5" action="{{route('rayosx.guardarDocumentos')}}" method="post" class="form-row">
   {{ csrf_field() }}
   <!-- PRIMERA PARTE DEL REGISTRO DE LA SOLICITUD - LOCALIZACION tipoTramite  -->
   <div id="paso5" class="row block w-100 newsletter collapse ">

      <div class="w-100">
        <div class="subtitle">
            <h3><b>Documentos Adjuntos:</b></h3>
        </div>
        <h4><b><span class="text-orange">•</span>Documentos Categoría I</b></h4>

        <table class="table">
            <thead>
               <tr>
                  <th>Descripción</th>
                  <th>Documento</th>
               </tr>
            </thead>
            <tbody>
               <tr>
                  <td>Copia documento identificación del encargado de protección radiológica</td>
                  <td><input id="fi_doc_encargado" name="fi_doc_encargado" type="file" class="archivopdf validate[required]"></td>
               </tr>
               <tr>
                  <td>Copia del diploma del encargado de protección radiológica</td>
                  <td><input id="fi_diploma_encargado" name="fi_diploma_encargado" type="file" class="archivopdf validate[required]"></td>
               </tr>
               <tr>
                  <td>Descripción de los blindajes estructurales o portátiles y el cálculo del blindaje</td>
                  <td><input id="fi_blindajes" name="fi_blindajes" type="file" class="archivopdf validate[required]"></td>
               </tr>
               <tr>
                  <td>Informe sobre los resultados del control de calidad</td>
                  <td><input id="fi_control_calidad" name="fi_control_calidad" type="file" class="archivopdf validate[required]"></td>
               </tr>
               <tr>
                  <td>Registros dosimétricos del último periodo de los trabajadores ocupacionalmente expuestos</td>
                  <td><input id="fi_registro_dosimetrico" name="fi_registro_dosimetrico" type="file" class="archivopdf validate[required]"></td>
               </tr>
               <tr>
                  <td>Registro del cumplimiento de los niveles de referencia para diagnóstico</td>
                  <td><input id="fi_registro_niveles" name="fi_registro_niveles" type="file" class="archivopdf validate[required]"></td>
               </tr>
               <tr>
                  <td>Plano general de las instalaciones</td>
                  <td><input id="fi_plano" name="fi_plano" type="file" class="archivopdf validate[required]"></td>
               </tr>
               <tr>
                  <td>Certificado de la capacitación en protección radiológica de cada trabajador ocupacionalmente expuesto reportado en el formulario</td>
                  <td><input id="fi_certificado_capacitacion" name="fi_certificado_capacitacion" type="file" class="archivopdf validate[required]"></td>
               </tr>
               <tr>
                  <td>Programa de capacitación en protección radiológica</td>
                  <td><input id="fi_programa_capacitacion" name="fi_programa_capacitacion" type="file" class="archivopdf validate[required]"></td>
               </tr>
               <tr>
                  <td>Procedimientos de mantenimiento de conformidad a lo establecido por el fabricante</td>
                  <td><input id="fi_procedimiento_mantenimiento" name="fi_procedimiento_mantenimiento" type="file" class="archivopdf validate[required]"></td>
               </tr>
               <tr>
                  <td>Pruebas iniciales de caracterización de los equipos</td>
                  <td><input id="fi_pruebas_caracterizacion" name="fi_pruebas_caracterizacion" type="file" class="archivopdf validate[required]"></td>
               </tr>
               <tr>
                  <td>Programa de Tecnovigilancia</td>
                  <td><input id="fi_programa_tecno" name="fi_programa_tecno" type="file" class="archivopdf validate[required]"></td>
               </tr>
               <tr>
                  <td>Programa de protección radiológica</td>
                  <td><input id="fi_programa_proteccion" name="fi_programa_proteccion" type="file" class="archivopdf validate[required]"></td>
               </tr>
               <tr>
                  <td>Documentación de soporte de talento humano e infraestructura técnica. En el evento contemplado en el paragrafo 1 del articulo 21</td>
                  <td><input id="fi_soporte_talento" name="fi_soporte_talento" type="file" class="archivopdf validate[required]"></td>
               </tr>
               <tr>
                  <td>Fotocopia de Diploma de Posgrado del Director Técnico</td>
                  <td><input id="fi_diploma_director" name="fi_diploma_director" type="file" class="archivopdf validate[required]"></td>
               </tr>
               <tr>
                  <td>Fotocopia de la Resolución de convalidación del t&iacute;tulo ante el Ministerio de Educación Nacional - MEN del Director Técnico  </td>
                  <td><input id="fi_res_convalida_director" name="fi_res_convalida_director" type="file" class="archivopdf "></td>
               </tr>
               <tr>
                  <td>Fotocopia de Diploma de posgrado del (los) profesional(es) que realiza(n) control de calidad</td>
                  <td><input id="fi_diploma_pos_profe" name="fi_diploma_pos_profe" type="file" class="archivopdf validate[required]"></td>
               </tr>
               <tr>
                  <td>Fotocopia de la Resolución de convalidación del t&iacute;tulo ante el Ministerio de Educación Nacional - MEN del (los) profesional(es) que realiza(n) control de calidad  </td>
                  <td><input id="fi_res_convalida_profe" name="fi_res_convalida_profe" type="file" class="archivopdf "></td>
               </tr>
               <tr>
                  <td>Certificados de calibración con una vigencia superior a seis (6) meses por cada equipo reportado</td>
                  <td><input id="fi_cert_calibracion" name="fi_cert_calibracion" type="file" class="archivopdf validate[required]"></td>
               </tr>
               <tr>
                  <td>Declaraciones de primera parte por cada objeto de prueba reportado</td>
                  <td><input id="fi_declaraciones" name="fi_declaraciones" type="file" class="archivopdf validate[required]"></td>
               </tr>
            </tbody>
         </table>

        <h4><b><span class="text-orange">•</span>Documentos Categoría II</b></h4>

        <table class="table">
            <thead>
               <tr>
                  <th>Descripción</th>
                  <th>Documento</th>
               </tr>
            </thead>
            <tbody>
               <tr>
                  <td>Copia documento identificación del oficial de protección radiológica</td>
                  <td><input id="fi_doc_oficial" name="fi_doc_oficial" type="file" class="archivopdf validate[required]"></td>
               </tr>
               <tr>
                  <td>Copia del diploma del oficial de protección radiológica</td>
                  <td><input id="fi_diploma_oficial" name="fi_diploma_oficial" type="file" class="archivopdf validate[required]"></td>
               </tr>
               <tr>
                  <td>Descripción de los blindajes estructurales o portátiles y el cálculo del blindaje</td>
                  <td><input id="fi_blindajes" name="fi_blindajes" type="file" class="archivopdf validate[required]"></td>
               </tr>
               <tr>
                  <td>Informe sobre los resultados del control de calidad</td>
                  <td><input id="fi_control_calidad" name="fi_control_calidad" type="file" class="archivopdf validate[required]"></td>
               </tr>
               <tr>
                  <td>Registros dosimétricos del último periodo de los trabajadores ocupacionalmente expuestos. Para alta complejidad, registros del segundo dosimetro</td>
                  <td><input id="fi_registro_dosimetrico" name="fi_registro_dosimetrico" type="file" class="archivopdf validate[required]"></td>
               </tr>
               <tr>
                  <td>Plano general de las instalaciones</td>
                  <td><input id="fi_plano" name="fi_plano" type="file" class="archivopdf validate[required]"></td>
               </tr>
               <tr>
                  <td>Documentación de soporte de talento humano e infraestructura técnica. En el evento contemplado en el paragrafo del articulo 23</td>
                  <td><input id="fi_soporte_talento" name="fi_soporte_talento" type="file" class="archivopdf validate[required]"></td>
               </tr>
               <tr>
                  <td>Fotocopia de Diploma de Posgrado del Director Técnico</td>
                  <td><input id="fi_diploma_director" name="fi_diploma_director" type="file" class="archivopdf validate[required]"></td>
               </tr>
               <tr>
                  <td>Fotocopia de la Resolución de convalidación del t&iacute;tulo ante el Ministerio de Educación Nacional - MEN del Director Técnico  </td>
                  <td><input id="fi_res_convalida_director" name="fi_res_convalida_director" type="file" class="archivopdf "></td>
               </tr>
               <tr>
                  <td>Fotocopia de Diploma de posgrado del (los) profesional(es) que realiza(n) control de calidad</td>
                  <td><input id="fi_diploma_pos_profe" name="fi_diploma_pos_profe" type="file" class="archivopdf validate[required]"></td>
               </tr>
               <tr>
                  <td>Fotocopia de la Resolución de convalidación del t&iacute;tulo ante el Ministerio de Educación Nacional - MEN del (los) profesional(es) que realiza(n) control de calidad  </td>
                  <td><input id="fi_res_convalida_profe" name="fi_res_convalida_profe" type="file" class="archivopdf "></td>
               </tr>
               <tr>
                  <td>Certificados de calibración con una vigencia superior a seis (6) meses por cada equipo reportado</td>
                  <td><input id="fi_cert_calibracion" name="fi_cert_calibracion" type="file" class="archivopdf validate[required]"></td>
               </tr>
               <tr>
                  <td>Declaraciones de primera parte por cada objeto de prueba reportado</td>
                  <td><input id="fi_declaraciones" name="fi_declaraciones" type="file" class="archivopdf validate[required]"></td>
               </tr>
            </tbody>
         </table>
      </div>
   </div>
</form>
@endsection

@section ("scripts")

 <script languague="javascript">

 function step1() {
   $( "#paso1" ).collapse('show');
   $( "#paso2" ).collapse('hide');
   $( "#paso3-1" ).collapse('hide');
   $( "#paso3-2" ).collapse('hide');
   $( "#paso4-1" ).collapse('hide');
   $( "#paso4-2" ).collapse('hide');
   $( "#paso5" ).collapse('hide');
   $( "#formSeccion1" ).collapse('show');
   $( "#formSeccion2" ).collapse('hide');
   $( "#formSeccion3-1" ).collapse('hide');
   $( "#formSeccion3-2" ).collapse('hide');
   $( "#formSeccion3-3" ).collapse('hide');
   $( "#formSeccion4-1" ).collapse('hide');
   $( "#formSeccion4-2" ).collapse('hide');
   $( "#formSeccion5" ).collapse('hide');
 }

 function step2() {
   $( "#paso1" ).collapse('hide');
   $( "#paso2" ).collapse('show');
   $( "#paso3" ).collapse('hide');
   $( "#paso3-1" ).collapse('hide');
   $( "#paso3-2" ).collapse('hide');
   $( "#paso4-1" ).collapse('hide');
   $( "#paso4-2" ).collapse('hide');
   $( "#paso5" ).collapse('hide');
   $( "#formSeccion1" ).collapse('hide');
   $( "#formSeccion2" ).collapse('show');
   $( "#formSeccion3-1" ).collapse('hide');
   $( "#formSeccion3-2" ).collapse('hide');
   $( "#formSeccion3-3" ).collapse('hide');
   $( "#formSeccion4-1" ).collapse('hide');
   $( "#formSeccion4-2" ).collapse('hide');
   $( "#formSeccion5" ).collapse('hide');
 }

 function step3() {
   $( "#paso1" ).collapse('hide');
   $( "#paso2" ).collapse('hide');
   $( "#paso3-1" ).collapse('show');
   $( "#paso3-2" ).collapse('show');
   $( "#paso4-1" ).collapse('hide');
   $( "#paso4-2" ).collapse('hide');
   $( "#paso5" ).collapse('hide');
   $( "#formSeccion1" ).collapse('hide');
   $( "#formSeccion2" ).collapse('hide');
   $( "#formSeccion3-1" ).collapse('hide');
   $( "#formSeccion3-2" ).collapse('hide');
   $( "#formSeccion3-3" ).collapse('hide');
   $( "#formSeccion4-1" ).collapse('show');
   $( "#formSeccion4-2" ).collapse('show');
   $( "#formSeccion5" ).collapse('hide');
 }

 function step4() {
   $( "#paso1" ).collapse('hide');
   $( "#paso2" ).collapse('hide');
   $( "#paso3-1" ).collapse('hide');
   $( "#paso3-2" ).collapse('hide');
   $( "#paso4-1" ).collapse('show');
   $( "#paso4-2" ).collapse('show');
   $( "#paso5" ).collapse('hide');
   $( "#formSeccion1" ).collapse('hide');
   $( "#formSeccion2" ).collapse('hide');
   $( "#formSeccion3-1" ).collapse('hide');
   $( "#formSeccion3-2" ).collapse('hide');
   $( "#formSeccion3-3" ).collapse('hide');
   $( "#formSeccion4-1" ).collapse('show');
   $( "#formSeccion4-2" ).collapse('show');
   $( "#formSeccion5" ).collapse('hide');
 }

 function step5() {
   $( "#paso1" ).collapse('hide');
   $( "#paso2" ).collapse('hide');
   $( "#paso3-1" ).collapse('hide');
   $( "#paso3-2" ).collapse('hide');
   $( "#paso4-1" ).collapse('hide');
   $( "#paso4-2" ).collapse('hide');
   $( "#paso5" ).collapse('show');
   $( "#formSeccion1" ).collapse('hide');
   $( "#formSeccion2" ).collapse('hide');
   $( "#formSeccion3-1" ).collapse('hide');
   $( "#formSeccion3-2" ).collapse('hide');
   $( "#formSeccion3-3" ).collapse('hide');
   $( "#formSeccion4-1" ).collapse('show');
   $( "#formSeccion4-2" ).collapse('show');
   $( "#formSeccion5" ).collapse('hide');
 }
    $(document).ready(function () {

      //Inicial
      $("#btnRegistrarSolicitud").collapse('hide');
      $( "#divPasos" ).collapse('hide');
      $( "#formSeccion1" ).collapse('hide');
      $( "#formSeccion2" ).collapse('hide');
      $( "#formSeccion3-1" ).collapse('hide');
      $( "#formSeccion3-2" ).collapse('hide');
      $( "#formSeccion3-3" ).collapse('hide');
      $( "#formSeccion4-1" ).collapse('hide');
      $( "#formSeccion4-2" ).collapse('hide');
      $( "#formSeccion5" ).collapse('hide');
      $( "#paso3" ).collapse('hide');
      $( "#paso4" ).collapse('hide');

      if($("#anio_fabricacion_equipo").val() <= 2009){
         $("#div_numpermiso").show();
         $("#numero_permiso").attr('required',true);
      }else{
         $("#div_numpermiso").hide();
         $("#numero_permiso").attr('required',false);
      }



      //Controla la validacion de la creacion del tramite
      var tramiteExiste=<?php if(isset($existeTramite)) echo $existeTramite; else echo 0;?>;
      var paso=<?php if(isset($paso)) echo $paso; else echo 0;?>;
      var idTramite=<?php if(isset($idTramite->id)) echo $idTramite->id; else echo 0;?>;
      //En que paso del registro del formulario se encuentra, esta dividido segun las secciones


      var $inputExisteTramite = $('<input/>',{type:'hidden',id:'existeTramite',name:'existeTramite',value:tramiteExiste});

      var $inputIdTramite = $('<input/>',{type:'hidden',id:'idTramite',name:'idTramite',value:idTramite});

      var $inputPaso = $('<input/>',{type:'hidden',id:'paso',name:'paso',value:paso});




      //de accuerdo al paso crea los campos en cada uno
      if(paso==0){
         $inputExisteTramite.appendTo('#formInicial');
         $inputIdTramite.appendTo('#formInicial');
         $inputPaso.appendTo('#formInicial');
      }
      if(tramiteExiste==1){

         $( "#divPasos" ).collapse('show');
         $( "#tipo_tramite" ).prop( "disabled", true );
         $( "#visita_previa" ).prop( "disabled", true );

         if(paso==1){
            $inputExisteTramite.appendTo('#formSeccion1');
            $inputIdTramite.appendTo('#formSeccion1');
            $inputPaso.appendTo('#formSeccion1');

            $( "#formSeccion1" ).collapse('show');
            alert("El tramite número "+$( "#idTramite" ).val()+" ha sido creado exitosamente, los campos necesariós han sido habilitados para su registro. El número del tramite servira para continuar el registro de la solicitud o revisar su proceso");
         }
         if(paso==2){
            $inputExisteTramite.appendTo('#formSeccion2');
            $inputIdTramite.appendTo('#formSeccion2');
            $inputPaso.appendTo('#formSeccion2');

            $( "#formSeccion2" ).collapse('show');
         }

         if(paso==3){
            $inputExisteTramite.appendTo('#formSeccion3-1');
            $inputIdTramite.appendTo('#formSeccion3-1');
            $inputPaso.appendTo('#formSeccion3-1');

            $( "#formSeccion3-1" ).collapse('show');
            $( "#formSeccion3-3" ).collapse('show');
            $( "#paso3" ).collapse('show');

         }

         if(paso==3.5){
            $inputExisteTramite.appendTo('#formSeccion3-1');
            $inputIdTramite.appendTo('#formSeccion3-1');
            $inputPaso.appendTo('#formSeccion3-1');
            $( "#formSeccion3-1" ).collapse('show');
            $( "#formSeccion3-3" ).collapse('show');
            $( "#paso3" ).collapse('show');
            alert("Es obligatorio registrar la información requerida.");
         }

         if(paso==4){
            $inputExisteTramite.appendTo('#formSeccion4-1');
            $inputIdTramite.appendTo('#formSeccion4-1');
            $inputPaso.appendTo('#formSeccion4-1');
            $( "#formSeccion4" ).collapse('show');
            $( "#paso4" ).collapse('show');
         }

         if(paso==4.5){
            $inputExisteTramite.appendTo('#formSeccion4-1');
            $inputIdTramite.appendTo('#formSeccion4-1');
            $inputPaso.appendTo('#formSeccion4-1');
            $( "#formSeccion4" ).collapse('show');
            $( "#paso4" ).collapse('show');
            alert("Es obligatorio registrar la información requerida.");
         }

         if(paso==5){
            $inputExisteTramite.appendTo('#formSeccion5');
            $inputIdTramite.appendTo('#formSeccion5');
            $inputPaso.appendTo('#formSeccion5');
            $( "#formSeccion5" ).collapse('show');
         }



      }else{
         $( "#formInicial" ).collapse('show');
      }


      $("#nuevoTOE").click(function() {
        $( "#formSeccion3-2" ).collapse('show');
         $inputExisteTramite.appendTo('#formSeccion3-2');
         $inputIdTramite.appendTo('#formSeccion3-2');
         $inputPaso.appendTo('#formSeccion3-2');
      });

      $("#nuevoEquipo").click(function() {
        $( "#formSeccion4-2" ).collapse('show');
         $inputExisteTramite.appendTo('#formSeccion4-2');
         $inputIdTramite.appendTo('#formSeccion4-2');
         $inputPaso.appendTo('#formSeccion4-2');
      });


      $("#button3-3").click(function() {
         $inputExisteTramite.appendTo('#formSeccion3-3');
         $inputIdTramite.appendTo('#formSeccion3-3');
         $inputPaso.appendTo('#formSeccion3-3');
      });

      $("#button4-3").click(function() {
         $inputExisteTramite.appendTo('#formSeccion4-3');
         $inputIdTramite.appendTo('#formSeccion4-3');
         $inputPaso.appendTo('#formSeccion4-3');
      });


      //Validaciónes para crear el tramite de rayos x
      $("#tipo_tramite").change(function(){

         var tipoTramite=$("#tipo_tramite").val();
         if($("#tipo_tramite").val() == 1 && $("#visita_previa").val() == 1){
            $("#btnRegistrarSolicitud").collapse('show');
         }else{
            $("#btnRegistrarSolicitud").collapse('hide');
            //$(".collapse").collapse('hide');
         }

      });

      $("#visita_previa").change(function(){

         if($("#visita_previa").val() == 1 && $("#tipo_tramite").val() == 1){
            $("#btnRegistrarSolicitud").collapse('show');
         }else{
            $("#btnRegistrarSolicitud").collapse('hide');
            alert("No es posible que realices el registro de la solicitud, comunicate con ______________ para revisar tu caso")
         }
      });


      //Actualiza la lista de equipos de acuerdo  a la entidad seleccionada
      $('select[name="categoria"]').on('change', function(){
        var categoriaId = $(this).val();
        if(categoriaId) {
            $.ajax({
                url: "{{url('/categoriaequipo/get')}}/"+categoriaId,
                type:"GET",
                dataType:"json",
                beforeSend: function(){
                    $('#loader').css("visibility", "visible");
                },

                success:function(data) {

                    $('select[name="categoria1"]').empty();

                    $.each(data, function(key, value){


                        $('select[name="categoria1"]').append('<option value="'+ value +'">' + key+ '</option>');

                    });


                },
                complete: function(){
                    $('#loader').css("visibility", "hidden");
                }
            });
        } else {
            $('select[name="categoria1"]').empty();
        }

    });


    $("#categoria1").change(function(){
       if($("#categoria1").val() == 2){
          $("#div_radperia").show();
          $("#categoria1_1").attr('required',true);
       }else{
          $("#div_radperia").hide();
          $("#categoria1_1").attr('required',false);
       }
    });


    $("#anio_fabricacion_equipo").change(function(){
       if($("#anio_fabricacion_equipo").val() >= 2009){
          $("#div_numpermiso").show();
          $("#numero_permiso").attr('required',true);
       }else{
          $("#div_numpermiso").hide();
          $("#numero_permiso").attr('required',false);
       }
    });


    //Actualiza la lista de municipios de acuerdo  a la entidad seleccionada
    $('select[name="depto_entidad"]').on('change', function(){
      var departamentoId = $(this).val();
      if(departamentoId) {
          $.ajax({
              url: "{{url('/municipio/get')}}/"+departamentoId,
              type:"GET",
              dataType:"json",
              beforeSend: function(){
                  $('#loader').css("visibility", "visible");
              },

              success:function(data) {

                  $('select[name="mpio_entidad"]').empty();

                  $.each(data, function(key, value){

                      $('select[name="mpio_entidad"]').append('<option value="'+ value +'">' + key+ '</option>');

                  });


              },
              complete: function(){
                  $('#loader').css("visibility", "hidden");
              }
          });
      } else {
          $('select[name="mpio_entidad"]').empty();
      }

  });

    });
 </script>

@endsection
