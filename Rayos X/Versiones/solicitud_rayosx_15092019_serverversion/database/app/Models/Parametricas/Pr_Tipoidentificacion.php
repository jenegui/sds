<?php

namespace App\Models\Pr_Tipoidentificacion;

use Illuminate\Database\Eloquent\Model;

class Pr_Tipoidentificacion extends Model
{
    protected $table = 'pr_tipoidentificacion';
}
