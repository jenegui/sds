<?php

namespace App\Models\Rayosx;

use Illuminate\Database\Eloquent\Model;

class Rayosx_documentos extends Model
{
    protected $table = 'rayosx_documentos';

    protected $primaryKey = 'id_documentos_rayosx';

    protected $fillable = [
        'id_tramite_rayosx',
        'documento',
        'path',
        'categoria',
        'id_archivo',
        'estado',
    ];
}
