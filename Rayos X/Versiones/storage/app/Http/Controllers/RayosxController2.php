<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use App\Tramite;
use App\Tipo_Tramite_Flujo;
use App\Tramite_Flujo;
use App\Models\Rayosx\Rayosx_direccion;
use App\Models\Rayosx\Rayosx_director;
use App\Models\Rayosx\Rayosx_documentos;
use App\Models\Rayosx\Rayosx_encargado;
use App\Models\Rayosx\Rayosx_equipos;
use App\Models\Rayosx\Rayosx_objprueba;
use App\Models\Rayosx\Rayosx_toe;
use App\Models\Parametricas\Pr_Tipoidentificacion;


class RayosxController extends Controller
{


    private $existe_tramite=0;
    private $idTramite=0;
    private $paso=0;
    private $impedirActualizacion=false;


    /**
     * Get all form details
     * @return blade
     */


     public function index(){
       $iduser = Auth::user()->id;
       $tramitesrx=Tramite::select('tramite.id','tipo_tramite.descripcion','tramite.estado','tramite.created_at')->join('tipo_tramite','tipo_tramite.id','=','tramite.tipo_tramite')->where('tramite.user', $iduser)->orderBy('tramite.created_at','DESC')->paginate(3);
       //dd($tramitesrx);
       return view('rayosx.mistramitesrx',['tramitesrx'=>$tramitesrx]);

     }

    public function index2(){
        try{
            //CARGAR DEPARTAMENTOS
            $departamentos = DB::table('pr_departamento')->pluck("IdDepartamento","Descripcion");
            //CARGAR TIPO IDENTIFICACION
            $tipo_identificacion = DB::table('pr_tipoidentificacion')->pluck("IdTipoIdentificacion","Descripcion");
            //CARGAR PROGRAMAS UNIVERSITARIOS
            $pr_programas_univ  = DB::table('pr_programas_univ')->pluck("id_programa","nombre_programa");
            //CARGAR NIVEL ACADEMICO
            $pr_nivelacademico  = DB::table('pr_nivelacademico')->pluck("IdNivelAcademico","Nombre");

            //CARGAR TOE TRAMITE
            $rayosxOficialToe=Rayosx_toe::where('toe_tipo',1)->where('id_tramite_rayosx',0)->orderBy('id_toe_rayosx','DESC')->first();
            $rayosxTemporalToe=Rayosx_toe::where('toe_tipo',2)->where('id_tramite_rayosx',0)->orderBy('id_toe_rayosx','DESC')->get();
            $rayosxTalento=Rayosx_director::where('id_tramite_rayosx',0)->orderBy('id_director_rayosx','DESC')->get();
            $rayosxObjprueba=Rayosx_objprueba::where('id_tramite_rayosx',0)->orderBy('id_obj_rayosx','DESC')->get();

            return view('rayosx.index',['departamentos'=>$departamentos,
                                        'tiposIdentificacion'=>$tipo_identificacion,
                                        'prNivelacademico'=>$pr_nivelacademico,
                                        'prProgramasUniv'=>$pr_programas_univ,
                                        'oficialTOE'=>$rayosxOficialToe,
                                        'temporalTOE'=>$rayosxTemporalToe,
                                        'talento'=>$rayosxTalento,
                                        'objeto'=>$rayosxObjprueba,
                                        'existeTramite'=>0,'paso'=>0]);
        }
        catch(Exception $e){
            dd($e);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {


      $tipo_tramite=(int)$request->tipo_tramite;
      $visita_previa=(int)$request->visita_previa;
      $idTramite=(int)$request->idTramite;



      //CARGAR DEPARTAMENTOS
      $departamentos = DB::table('pr_departamento')->pluck("IdDepartamento","Descripcion");
      //CARGAR TIPO IDENTIFICACION
      $tipo_identificacion = DB::table('pr_tipoidentificacion')->pluck("IdTipoIdentificacion","Descripcion");
      //CARGAR PROGRAMAS UNIVERSITARIOS
      $pr_programas_univ  = DB::table('pr_programas_univ')->pluck("id_programa","nombre_programa");
      //CARGAR NIVEL ACADEMICO
      $pr_nivelacademico  = DB::table('pr_nivelacademico')->pluck("IdNivelAcademico","Nombre");
      $rayosxOficialToe=Rayosx_toe::where('toe_tipo',1)->where('id_tramite_rayosx',$idTramite)->orderBy('id_toe_rayosx','DESC')->first();
      $rayosxTemporalToe=Rayosx_toe::where('toe_tipo',2)->where('id_tramite_rayosx',$idTramite)->orderBy('id_toe_rayosx','DESC')->get();

      $rayosxTalento=Rayosx_director::where('id_tramite_rayosx',$idTramite)->orderBy('id_director_rayosx','DESC')->get();
      $rayosxObjprueba=Rayosx_objprueba::where('id_tramite_rayosx',$idTramite)->orderBy('id_obj_rayosx','DESC')->get();


        //REGISTRA LOS DATOS INICIALES DEL TRAMITE
      if($idTramite==0){
        $tramite = new Tramite();
        $tramite->user=Auth::id();
        $tramite->tipo_tramite=1;
        $tramite->estado=1;
        $tramite->save();

        $idTramite=Tramite::where('user',Auth::id())->orderBy('id','DESC')->first();

        // Consulta del flujo del tramite seleccionado -- tipo_tramite=1 el cual corresponde al tramite de rayos x, consultamos la actividad inicial por medio del filtro de flujo_accion_permitida=inicio
        $tipoTramiteFlujo= Tipo_Tramite_Flujo::where('tipo_tramite_id',1)->where('flujo_accion_permitida', 'like','%inicio%')->first();

        //Se crea el flujo del tramite rayos x o historico
        $tramiteFlujo = new Tramite_Flujo();
        $tramiteFlujo->tramite_id=(int)$idTramite->id;
        $tramiteFlujo->actividad=$tipoTramiteFlujo->flujo_actividad_inicial;
        $tramiteFlujo->fecha_inicio=date('Y-m-d H:i:s');
        $tramiteFlujo->user_id=Auth::id();
        $tramiteFlujo->observaciones="Creación del tramite RAYOS X";
        $tramiteFlujo->save();
      }else{
          $idTramite=Tramite::find('id',$idTramite);

      }



      /*$id = DB::table('tramite')->insertGetId(
          ['user' => Auth::id(),'tipo_tramite' => 1,'estado' => 1]
      );*/


      return view('rayosx.index', ['idTramite' => $idTramite,
                                    'existeTramite'=>1,
                                    'departamentos'=>$departamentos,
                                    'tiposIdentificacion'=>$tipo_identificacion,
                                    'prNivelacademico'=>$pr_nivelacademico,
                                    'prProgramasUniv'=>$pr_programas_univ,
                                    'oficialTOE'=>$rayosxOficialToe,
                                    'temporalTOE'=>$rayosxTemporalToe,
                                    'talento'=>$rayosxTalento,
                                    'objeto'=>$rayosxObjprueba,
                                    'paso'=>1, //Paso 1 - localizacion entidad
                                    'visitaPrevia'=>$visita_previa]);
    }


    /**
     * Saves the institution's location and shows equipment x-ray form
     *
     * @return \Illuminate\Http\Response
     */


    public function edit($id_tramite) {

    $idTramite = $id_tramite;
    $departamentos = DB::table('pr_departamento')->pluck("IdDepartamento","Descripcion");
    $tipo_identificacion = DB::table('pr_tipoidentificacion')->pluck("IdTipoIdentificacion","Descripcion");
    $pr_nivelacademico  = DB::table('pr_nivelacademico')->pluck("IdNivelAcademico","Nombre");
    $pr_programas_univ  = DB::table('pr_programas_univ')->pluck("id_programa","nombre_programa");
    $rayosxOficialToe=Rayosx_toe::where('toe_tipo',1)->where('id_tramite_rayosx',$idTramite)->orderBy('id_toe_rayosx','DESC')->first();

    $rayosxTemporalToe=Rayosx_toe::where('toe_tipo',2)->where('id_tramite_rayosx',$idTramite)->orderBy('id_toe_rayosx','DESC')->get();

    $rayosxTalento=Rayosx_director::where('id_tramite_rayosx',$idTramite)->orderBy('id_director_rayosx','DESC')->get();
    $rayosxObjprueba=Rayosx_objprueba::where('id_tramite_rayosx',$idTramite)->orderBy('id_obj_rayosx','DESC')->get();

    $rayosxDireccion = Rayosx_direccion::where('id_tramite_rayosx',$id_tramite)->firstorFail();
    $rayosxEquipo = Rayosx_equipos::where('id_tramite_rayosx',$id_tramite)->firstorFail();
    //dd($rayosxEquipo);


    return view('rayosx.editartramiterx',['departamentos'=>$departamentos,
    'tiposIdentificacion'=>$tipo_identificacion,
    'prNivelacademico'=>$pr_nivelacademico,
    'prProgramasUniv'=>$pr_programas_univ,
    'rayosxOficialToe'=>$rayosxOficialToe,
    'rayosxTemporalToe'=>$rayosxTemporalToe,
    'talento'=>$rayosxTalento,
    'objeto'=>$rayosxObjprueba,
    'rayosxDireccion'=>$rayosxDireccion,
    'rayosxEquipo'=>$rayosxEquipo]);
    /*
      if($post != null){
        $users = User::where('id',$post->user_id)->firstorFail();
        $data['post'] = $post;
        $data['comments'] = Comment::where('post_id', $id)->get();
        return view('adminlte::posts.post',$data,['users'=>$users]);
      }*/
    }

    public function editarDireccion(Request $request) {

      $iduser = Auth::user()->id;
      $tramites=Tramite::where('user', $iduser)->orderBy('id','DESC')->paginate(3);

      return view('tramites.index',compact('tramites'));
      /*
      $input = Input::all();
      dd($request);

         if($input['id_tramite'] == NULL) {
             return redirect()->route('tramites.index');
         }
         else {
         $rayosxDireccion = Rayosx_direccion::where('id_tramite_rayosx',$input['id_tramite'])->firstorFail();

         $rayosxDireccion->depto_entidad = $input['depto_entidad'];
         $rayosxDireccion->mpio_entidad = $input['mpio_entidad'];
         $rayosxDireccion->dire_entidad = $input['dire_entidad'];
         $rayosxDireccion->sede_entidad = $input['sede_entidad'];
         $rayosxDireccion->email_entidad = $input['email_entidad'];
         $rayosxDireccion->celular_entidad = $input['celular_entidad'];
         $rayosxDireccion->telefono_entidad = $input['telefono_entidad'];
         $rayosxDireccion->extension_entidad = $input['extension_entidad'];
         $rayosxDireccion->save(); // Guarda el objeto en la BD


         $idTramite = $id_tramite;
         $departamentos = DB::table('pr_departamento')->pluck("IdDepartamento","Descripcion");
         $tipo_identificacion = DB::table('pr_tipoidentificacion')->pluck("IdTipoIdentificacion","Descripcion");
         $pr_nivelacademico  = DB::table('pr_nivelacademico')->pluck("IdNivelAcademico","Nombre");
         $pr_programas_univ  = DB::table('pr_programas_univ')->pluck("id_programa","nombre_programa");
         $rayosxOficialToe=Rayosx_toe::where('toe_tipo',1)->where('id_tramite_rayosx',$idTramite)->orderBy('id_toe_rayosx','DESC')->first();

         $rayosxTemporalToe=Rayosx_toe::where('toe_tipo',2)->where('id_tramite_rayosx',$idTramite)->orderBy('id_toe_rayosx','DESC')->get();

         $rayosxTalento=Rayosx_director::where('id_tramite_rayosx',$idTramite)->orderBy('id_director_rayosx','DESC')->get();
         $rayosxObjprueba=Rayosx_objprueba::where('id_tramite_rayosx',$idTramite)->orderBy('id_obj_rayosx','DESC')->get();

         $rayosxDireccion = Rayosx_direccion::where('id_tramite_rayosx',$id_tramite)->firstorFail();
         $rayosxEquipo = Rayosx_equipos::where('id_tramite_rayosx',$id_tramite)->firstorFail();
         //dd($rayosxEquipo);


         return view('rayosx.editartramiterx',['departamentos'=>$departamentos,
         'tiposIdentificacion'=>$tipo_identificacion,
         'prNivelacademico'=>$pr_nivelacademico,
         'prProgramasUniv'=>$pr_programas_univ,
         'rayosxOficialToe'=>$rayosxOficialToe,
         'rayosxTemporalToe'=>$rayosxTemporalToe,
         'talento'=>$rayosxTalento,
         'objeto'=>$rayosxObjprueba,
         'rayosxDireccion'=>$rayosxDireccion,
         'rayosxEquipo'=>$rayosxEquipo]);
       }*/
    }

    public function guardarLocalizacion(Request $request)
    {
      //CARGAR DEPARTAMENTOS
      $departamentos = DB::table('pr_departamento')->pluck("IdDepartamento","Descripcion");
      //CARGAR TIPO IDENTIFICACION
      $tipo_identificacion = DB::table('pr_tipoidentificacion')->pluck("IdTipoIdentificacion","Descripcion");
      //CARGAR PROGRAMAS UNIVERSITARIOS
      $pr_programas_univ  = DB::table('pr_programas_univ')->pluck("id_programa","nombre_programa");
      //CARGAR NIVEL ACADEMICO
      $pr_nivelacademico  = DB::table('pr_nivelacademico')->pluck("IdNivelAcademico","Nombre");

      //Campos Hidden
      $tipo_tramite=1;
      $existe_tramite=(int)$request->existeTramite;
      $id=(int)$request->idTramite;
      $paso=(int)$request->paso;

      $rayosxOficialToe=Rayosx_toe::where('toe_tipo',1)->where('id_tramite_rayosx',$id)->orderBy('id_toe_rayosx','DESC')->first();
      $rayosxTemporalToe=Rayosx_toe::where('toe_tipo',2)->where('id_tramite_rayosx',$id)->orderBy('id_toe_rayosx','DESC')->get();
      $rayosxTalento=Rayosx_director::where('id_tramite_rayosx',$id)->orderBy('id_director_rayosx','DESC')->get();
      $rayosxObjprueba=Rayosx_objprueba::where('id_tramite_rayosx',$id)->orderBy('id_obj_rayosx','DESC')->get();




      $idTramite=Tramite::where('user',Auth::id())->where('id',$id)->orderBy('id','DESC')->first();
      if($id!=0){
        //Consutlar datos de su tramite


        $rayosxDireccion = new Rayosx_direccion();
        $rayosxDireccion->id_tramite_rayosx=$id;
        $rayosxDireccion->dire_entidad=$request->dire_entidad;
        $rayosxDireccion->sede_entidad=$request->sede_entidad;
        $rayosxDireccion->email_entidad=$request->email_entidad;
        $rayosxDireccion->depto_entidad=$request->depto_entidad;
        $rayosxDireccion->mpio_entidad=$request->mpio_entidad;
        $rayosxDireccion->celular_entidad=$request->celular_entidad;
        $rayosxDireccion->indicativo_entidad=$request->indicativo_entidad;
        $rayosxDireccion->telefono_entidad=$request->telefono_entidad;
        $rayosxDireccion->extension_entidad=$request->extension_entidad;

        $rayosxDireccion->save();


        return view('rayosx.index', ['idTramite' => $idTramite,
                                      'existeTramite'=>1,
                                      'tipoTramite'=>$tipo_tramite,
                                      'departamentos'=>$departamentos,
                                      'tiposIdentificacion'=>$tipo_identificacion,
                                      'prNivelacademico'=>$pr_nivelacademico,
                                      'prProgramasUniv'=>$pr_programas_univ,
                                      'oficialTOE'=>$rayosxOficialToe,
                                      'temporalTOE'=>$rayosxTemporalToe,
                                      'talento'=>$rayosxTalento,
                                      'objeto'=>$rayosxObjprueba,
                                      'paso'=>2 //Paso 2 - Cargar Formulario Equipos rayos x
                                      ]);
      }

      return view('rayosx.index', ['idTramite' => $idTramite,
                                    'existeTramite'=>1,
                                    'departamentos'=>$departamentos,
                                    'tiposIdentificacion'=>$tipo_identificacion,
                                    'prNivelacademico'=>$pr_nivelacademico,
                                    'prProgramasUniv'=>$pr_programas_univ,
                                    'oficialTOE'=>$rayosxOficialToe,
                                    'temporalTOE'=>$rayosxTemporalToe,
                                    'talento'=>$rayosxTalento,
                                    'objeto'=>$rayosxObjprueba,
                                    'paso'=>1 //Paso 1 - localizacion entidad
                                    ]);
    }

    /**
     * Saves the institution's location and shows equipment x-ray form
     *
     * @return \Illuminate\Http\Response
     */
    public function guardarEquiposRayosX(Request $request)
    {

      //CARGAR DEPARTAMENTOS
      $departamentos = DB::table('pr_departamento')->pluck("IdDepartamento","Descripcion");
      //CARGAR TIPO IDENTIFICACION
      $tipo_identificacion = DB::table('pr_tipoidentificacion')->pluck("IdTipoIdentificacion","Descripcion");
      //CARGAR PROGRAMAS UNIVERSITARIOS
      $pr_programas_univ  = DB::table('pr_programas_univ')->pluck("id_programa","nombre_programa");
      //CARGAR NIVEL ACADEMICO
      $pr_nivelacademico  = DB::table('pr_nivelacademico')->pluck("IdNivelAcademico","Nombre");

      //Campos Hidden
      $tipo_tramite=1;
      $existe_tramite=(int)$request->existeTramite;
      $id=(int)$request->idTramite;
      $rayosxOficialToe=Rayosx_toe::where('toe_tipo',1)->where('id_tramite_rayosx',$id)->orderBy('id_toe_rayosx','DESC')->first();
      $rayosxTemporalToe=Rayosx_toe::where('toe_tipo',2)->where('id_tramite_rayosx',$id)->orderBy('id_toe_rayosx','DESC')->get();
      $rayosxTalento=Rayosx_director::where('id_tramite_rayosx',$id)->orderBy('id_director_rayosx','DESC')->get();
      $rayosxObjprueba=Rayosx_objprueba::where('id_tramite_rayosx',$id)->orderBy('id_obj_rayosx','DESC')->get();
      $idTramite=Tramite::where('user',Auth::id())->where('id',$id)->orderBy('id','DESC')->first();
      if($id!=0){
        $rayosxEquipos = new Rayosx_equipos();
        $rayosxEquipos->id_tramite_rayosx=$id;
        $rayosxEquipos->categoria=$request->categoria;
        $rayosxEquipos->categoria1=$request->categoria1;
        $rayosxEquipos->categoria2=$request->categoria2;
        $rayosxEquipos->categoria1_1 =$request->categoria1_1;
        $rayosxEquipos->categoria1_2 =$request->categoria1_2;
        $rayosxEquipos->equipo_generador =$request->equipo_generador;
        $rayosxEquipos->tipo_visualizacion =$request->tipo_visualizacion;
        $rayosxEquipos->marca_equipo =$request->marca_equipo;
        $rayosxEquipos->modelo_equipo =$request->modelo_equipo;
        $rayosxEquipos->serie_equipo =$request->serie_equipo;
        $rayosxEquipos->marca_tubo_rx =$request->marca_tubo_rx;
        $rayosxEquipos->modelo_tubo_rx =$request->modelo_tubo_rx;
        $rayosxEquipos->serie_tubo_rx =$request->serie_tubo_rx;
        $rayosxEquipos->tension_tubo_rx =$request->tension_tubo_rx;
        $rayosxEquipos->contiene_tubo_rx =$request->contiene_tubo_rx;
        $rayosxEquipos->energia_fotones =$request->energia_fotones;
        $rayosxEquipos->energia_electrones =$request->energia_electrones;
        $rayosxEquipos->carga_trabajo =$request->carga_trabajo;
        $rayosxEquipos->ubicacion_equipo =$request->ubicacion_equipo;
        $rayosxEquipos->numero_permiso =$request->numero_permiso;
        $rayosxEquipos->anio_fabricacion =$request->anio_fabricacion;
        $rayosxEquipos->anio_fabricacion_tubo =$request->anio_fabricacion_tubo;
        $rayosxEquipos->estado=1;
        $rayosxEquipos->save();


        return view('rayosx.index', ['idTramite' => $idTramite,
                                        'existeTramite'=>1,
                                        'tipoTramite'=>$tipo_tramite,
                                        'departamentos'=>$departamentos,
                                        'tiposIdentificacion'=>$tipo_identificacion,
                                        'prNivelacademico'=>$pr_nivelacademico,
                                        'prProgramasUniv'=>$pr_programas_univ,
                                        'oficialTOE'=>$rayosxOficialToe,
                                        'temporalTOE'=>$rayosxTemporalToe,
                                        'talento'=>$rayosxTalento,
                                        'objeto'=>$rayosxObjprueba,
                                        'paso'=>3 //Paso 2 - Cargar Formulario Equipos rayos x
                                        ]);
      }

      return view('rayosx.index', ['idTramite' => $idTramite,
                                    'existeTramite'=>1,
                                    'departamentos'=>$departamentos,
                                    'tiposIdentificacion'=>$tipo_identificacion,
                                    'prNivelacademico'=>$pr_nivelacademico,
                                    'prProgramasUniv'=>$pr_programas_univ,
                                    'oficialTOE'=>$rayosxOficialToe,
                                    'temporalTOE'=>$rayosxTemporalToe,
                                    'talento'=>$rayosxTalento,
                                    'objeto'=>$rayosxObjprueba,
                                    'paso'=>2 //Paso 1 - localizacion entidad
                                    ]);
    }


    public function getMpoXDpto($id) {
        $municipios = DB::table("pr_municipio")->where("IdDepartamento",$id)->pluck("idMunicipio","Descripcion");

        return json_encode($municipios);

    }

    public function getEquipoXCategoria($id) {
        $equipos = DB::table("pr_equiposrx")->where("categoria",$id)->pluck("id_equipo","nombre_equipo");

        return json_encode($equipos);

    }


    /**
     * Saves the institution's location and shows equipment x-ray form
     *
     * @return \Illuminate\Http\Response
     */
    public function guardarOficialTOE(Request $request)
    {

      //CARGAR DEPARTAMENTOS
      $departamentos = DB::table('pr_departamento')->pluck("IdDepartamento","Descripcion");
      //CARGAR TIPO IDENTIFICACION
      $tipo_identificacion = DB::table('pr_tipoidentificacion')->pluck("IdTipoIdentificacion","Descripcion");
      //CARGAR PROGRAMAS UNIVERSITARIOS
      $pr_programas_univ  = DB::table('pr_programas_univ')->pluck("id_programa","nombre_programa");
      //CARGAR NIVEL ACADEMICO
      $pr_nivelacademico  = DB::table('pr_nivelacademico')->pluck("IdNivelAcademico","Nombre");


      //Campos Hidden
      $tipo_tramite=1;
      $existe_tramite=(int)$request->existeTramite;
      $id=(int)$request->idTramite;

      $idTramite=Tramite::where('user',Auth::id())->where('id',$id)->orderBy('id','DESC')->first();

      if($id!=0){

        $rayosxToe=Rayosx_toe::where('toe_tipo',1)->where('id_tramite_rayosx',$id)->orderBy('id_toe_rayosx','DESC')->first();

        if(!isset($rayosxToe)){
          $rayosxToe = new Rayosx_toe();
        }

        $rayosxToe->id_tramite_rayosx=$id;
        $rayosxToe->toe_pnombre=$request->encargado_pnombre;
        $rayosxToe->toe_snombre=$request->encargado_snombre;
        $rayosxToe->toe_papellido=$request->encargado_papellido;
        $rayosxToe->toe_sapellido=$request->encargado_sapellido;
        $rayosxToe->toe_tdocumento=$request->encargado_tdocumento;
        $rayosxToe->toe_ndocumento=$request->encargado_ndocumento;
        $rayosxToe->toe_lexpedicion=$request->encargado_lexpedicion;
        $rayosxToe->toe_correo=$request->encargado_correo;
        $rayosxToe->toe_profesion=$request->encargado_profesion;
        $rayosxToe->toe_nivel=$request->encargado_nivel;
        //$rayosxToe->toe_ult_entrenamiento=$request->encargado_ndocumento;
        //$rayosxToe->toe_pro_entrenamiento=$request->encargado_ndocumento;
        $rayosxToe->toe_registro=$request->encargado_ndocumento;
        $rayosxToe->toe_tipo=1;


        $rayosxToe->save();


      }

      $rayosxOficialToe=Rayosx_toe::where('toe_tipo',1)->where('id_tramite_rayosx',$id)->orderBy('id_toe_rayosx','DESC')->first();
      $rayosxTemporalToe=Rayosx_toe::where('toe_tipo',2)->where('id_tramite_rayosx',$id)->orderBy('id_toe_rayosx','DESC')->get();
      $rayosxTalento=Rayosx_director::where('id_tramite_rayosx',$id)->orderBy('id_director_rayosx','DESC')->get();
      $rayosxObjprueba=Rayosx_objprueba::where('id_tramite_rayosx',$id)->orderBy('id_obj_rayosx','DESC')->get();

      return view('rayosx.index', ['idTramite' => $idTramite,
                                        'existeTramite'=>1,
                                        'tipoTramite'=>$tipo_tramite,
                                        'departamentos'=>$departamentos,
                                        'tiposIdentificacion'=>$tipo_identificacion,
                                        'prNivelacademico'=>$pr_nivelacademico,
                                        'prProgramasUniv'=>$pr_programas_univ,
                                        'oficialTOE'=>$rayosxOficialToe,
                                        'temporalTOE'=>$rayosxTemporalToe,
                                        'talento'=>$rayosxTalento,
                                        'objeto'=>$rayosxObjprueba,
                                        'paso'=>3 //Paso 3 - Cargar TOE
                                        ]);

    }

    public function guardarTemporalTOE(Request $request)
    {

      //CARGAR DEPARTAMENTOS
      $departamentos = DB::table('pr_departamento')->pluck("IdDepartamento","Descripcion");
      //CARGAR TIPO IDENTIFICACION
      $tipo_identificacion = DB::table('pr_tipoidentificacion')->pluck("IdTipoIdentificacion","Descripcion");
      //CARGAR PROGRAMAS UNIVERSITARIOS
      $pr_programas_univ  = DB::table('pr_programas_univ')->pluck("id_programa","nombre_programa");
      //CARGAR NIVEL ACADEMICO
      $pr_nivelacademico  = DB::table('pr_nivelacademico')->pluck("IdNivelAcademico","Nombre");


      //Campos Hidden
      $tipo_tramite=1;
      $existe_tramite=(int)$request->existeTramite;
      $id=(int)$request->idTramite;

      $idTramite=Tramite::where('user',Auth::id())->where('id',$id)->orderBy('id','DESC')->first();

      if($id!=0){
        $rayosxToe = new Rayosx_toe();
        $rayosxToe->id_tramite_rayosx=$id;
        $rayosxToe->toe_pnombre=$request->encargado_pnombre;
        $rayosxToe->toe_snombre=$request->encargado_snombre;
        $rayosxToe->toe_papellido=$request->encargado_papellido;
        $rayosxToe->toe_sapellido=$request->encargado_sapellido;
        $rayosxToe->toe_tdocumento=$request->encargado_tdocumento;
        $rayosxToe->toe_ndocumento=$request->encargado_ndocumento;
        $rayosxToe->toe_lexpedicion=$request->encargado_lexpedicion;
        $rayosxToe->toe_correo=$request->encargado_correo;
        $rayosxToe->toe_profesion=$request->encargado_profesion;
        $rayosxToe->toe_nivel=$request->encargado_nivel;
        $rayosxToe->toe_ult_entrenamiento=$request->encargado_ndocumento;
        $rayosxToe->toe_pro_entrenamiento=$request->encargado_ndocumento;
        $rayosxToe->toe_registro=$request->encargado_ndocumento;
        $rayosxToe->toe_tipo=2;
        $rayosxToe->save();


      }

      $rayosxOficialToe=Rayosx_toe::where('toe_tipo',1)->where('id_tramite_rayosx',$id)->orderBy('id_toe_rayosx','DESC')->first();
      $rayosxTemporalToe=Rayosx_toe::where('toe_tipo',2)->where('id_tramite_rayosx',$id)->orderBy('id_toe_rayosx','DESC')->get();
      $rayosxTalento=Rayosx_director::where('id_tramite_rayosx',$id)->orderBy('id_director_rayosx','DESC')->get();
      $rayosxObjprueba=Rayosx_objprueba::where('id_tramite_rayosx',$id)->orderBy('id_obj_rayosx','DESC')->get();

      return view('rayosx.index', ['idTramite' => $idTramite,
                                        'existeTramite'=>1,
                                        'tipoTramite'=>$tipo_tramite,
                                        'departamentos'=>$departamentos,
                                        'tiposIdentificacion'=>$tipo_identificacion,
                                        'prNivelacademico'=>$pr_nivelacademico,
                                        'prProgramasUniv'=>$pr_programas_univ,
                                        'oficialTOE'=>$rayosxOficialToe,
                                        'temporalTOE'=>$rayosxTemporalToe,
                                        'talento'=>$rayosxTalento,
                                        'objeto'=>$rayosxObjprueba,
                                        'paso'=>3 //Paso 3 - Cargar TOE
                                        ]);

    }



    public function verificarTOE(Request $request)
    {

      //CARGAR DEPARTAMENTOS
      $departamentos = DB::table('pr_departamento')->pluck("IdDepartamento","Descripcion");
      //CARGAR TIPO IDENTIFICACION
      $tipo_identificacion = DB::table('pr_tipoidentificacion')->pluck("IdTipoIdentificacion","Descripcion");
      //CARGAR PROGRAMAS UNIVERSITARIOS
      $pr_programas_univ  = DB::table('pr_programas_univ')->pluck("id_programa","nombre_programa");
      //CARGAR NIVEL ACADEMICO
      $pr_nivelacademico  = DB::table('pr_nivelacademico')->pluck("IdNivelAcademico","Nombre");


      //Campos Hidden
      $tipo_tramite=1;
      $existe_tramite=(int)$request->existeTramite;
      $id=(int)$request->idTramite;

      if($id>0){
        $idTramite=Tramite::where('user',Auth::id())->where('id',$id)->orderBy('id','DESC')->first();

        $rayosxOficialToe=Rayosx_toe::where('toe_tipo',1)->where('id_tramite_rayosx',$id)->orderBy('id_toe_rayosx','DESC')->first();
        $rayosxTemporalToe=Rayosx_toe::where('toe_tipo',2)->where('id_tramite_rayosx',$id)->orderBy('id_toe_rayosx','DESC')->get();
        $rayosxTalento=Rayosx_director::where('id_tramite_rayosx',$id)->orderBy('id_director_rayosx','DESC')->get();
        $rayosxObjprueba=Rayosx_objprueba::where('id_tramite_rayosx',$id)->orderBy('id_obj_rayosx','DESC')->get();

        if($rayosxOficialToe->count() > 0 && $rayosxTemporalToe->count() > 0){

          return view('rayosx.index', ['idTramite' => $idTramite,
                                            'existeTramite'=>1,
                                            'tipoTramite'=>$tipo_tramite,
                                            'departamentos'=>$departamentos,
                                            'tiposIdentificacion'=>$tipo_identificacion,
                                            'prNivelacademico'=>$pr_nivelacademico,
                                            'prProgramasUniv'=>$pr_programas_univ,
                                            'oficialTOE'=>$rayosxOficialToe,
                                            'temporalTOE'=>$rayosxTemporalToe,
                                            'talento'=>$rayosxTalento,
                                            'objeto'=>$rayosxObjprueba,
                                            'paso'=>4 //Paso 4 - TALENTO HUMANO
                                            ]);
        }

        return view('rayosx.index', ['idTramite' => $idTramite,
                                            'existeTramite'=>1,
                                            'tipoTramite'=>$tipo_tramite,
                                            'departamentos'=>$departamentos,
                                            'tiposIdentificacion'=>$tipo_identificacion,
                                            'prNivelacademico'=>$pr_nivelacademico,
                                            'prProgramasUniv'=>$pr_programas_univ,
                                            'oficialTOE'=>$rayosxOficialToe,
                                            'temporalTOE'=>$rayosxTemporalToe,
                                            'talento'=>$rayosxTalento,
                                            'objeto'=>$rayosxObjprueba,
                                            'paso'=>3.5 //Paso 3 - Cargar TOE
                                            ]);

      }

    }

    public function guardarTalentoHumano(Request $request)
    {

      //CARGAR DEPARTAMENTOS
      $departamentos = DB::table('pr_departamento')->pluck("IdDepartamento","Descripcion");
      //CARGAR TIPO IDENTIFICACION
      $tipo_identificacion = DB::table('pr_tipoidentificacion')->pluck("IdTipoIdentificacion","Descripcion");
      //CARGAR PROGRAMAS UNIVERSITARIOS
      $pr_programas_univ  = DB::table('pr_programas_univ')->pluck("id_programa","nombre_programa");
      //CARGAR NIVEL ACADEMICO
      $pr_nivelacademico  = DB::table('pr_nivelacademico')->pluck("IdNivelAcademico","Nombre");


      //Campos Hidden
      $tipo_tramite=1;
      $existe_tramite=(int)$request->existeTramite;
      $id=(int)$request->idTramite;

      $idTramite=Tramite::where('user',Auth::id())->where('id',$id)->orderBy('id','DESC')->first();

      if($id!=0){
        $rayosxDirector = new Rayosx_director();
        $rayosxDirector->id_tramite_rayosx=$id;
        $rayosxDirector->talento_pnombre= $request->talento_pnombre;
        $rayosxDirector->talento_snombre = $request->talento_snombre;
        $rayosxDirector->talento_papellido = $request->talento_papellido;
        $rayosxDirector->talento_sapellido = $request->talento_sapellido;
        $rayosxDirector->talento_tdocumento = $request->talento_tdocumento;
        $rayosxDirector->talento_ndocumento = $request->talento_ndocumento;
        $rayosxDirector->talento_lexpedicion = $request->talento_lexpedicion;
        $rayosxDirector->talento_correo = $request->talento_correo;
        $rayosxDirector->talento_titulo = $request->talento_titulo;
        $rayosxDirector->talento_universidad = $request->talento_universidad;
        $rayosxDirector->talento_libro = $request->talento_libro;
        $rayosxDirector->talento_registro = $request->talento_registro;
        $rayosxDirector->talento_fecha_diploma = $request->talento_fecha_diploma;
        $rayosxDirector->talento_resolucion = $request->talento_resolucion;
        $rayosxDirector->talento_fecha_convalida = $request->talento_fecha_convalida;
        $rayosxDirector->talento_nivel = $request->talento_nivel;
        $rayosxDirector->talento_titulo_pos = $request->talento_titulo_pos;
        $rayosxDirector->talento_universidad_pos = $request->talento_universidad_pos;
        $rayosxDirector->talento_libro_pos = $request->talento_libro_pos;
        $rayosxDirector->talento_registro_pos = $request->talento_registro_pos;
        $rayosxDirector->talento_fecha_diploma_pos = $request->talento_fecha_diploma_pos;
        $rayosxDirector->talento_resolucion_pos = $request->talento_resolucion_pos;
        $rayosxDirector->talento_fecha_convalida_pos = $request->talento_fecha_convalida_pos;

        $rayosxDirector->save();


      }

      $rayosxOficialToe=Rayosx_toe::where('toe_tipo',1)->where('id_tramite_rayosx',$id)->orderBy('id_toe_rayosx','DESC')->first();
      $rayosxTemporalToe=Rayosx_toe::where('toe_tipo',2)->where('id_tramite_rayosx',$id)->orderBy('id_toe_rayosx','DESC')->get();
      $rayosxTalento=Rayosx_director::where('id_tramite_rayosx',$id)->orderBy('id_director_rayosx','DESC')->get();
      $rayosxObjprueba=Rayosx_objprueba::where('id_tramite_rayosx',$id)->orderBy('id_obj_rayosx','DESC')->get();

      return view('rayosx.index', ['idTramite' => $idTramite,
                                        'existeTramite'=>1,
                                        'tipoTramite'=>$tipo_tramite,
                                        'departamentos'=>$departamentos,
                                        'tiposIdentificacion'=>$tipo_identificacion,
                                        'prNivelacademico'=>$pr_nivelacademico,
                                        'prProgramasUniv'=>$pr_programas_univ,
                                        'oficialTOE'=>$rayosxOficialToe,
                                        'temporalTOE'=>$rayosxTemporalToe,
                                        'talento'=>$rayosxTalento,
                                        'objeto'=>$rayosxObjprueba,
                                        'paso'=>4 //Paso 4 - TALENTO HUMANO DIRECTOR TECNICO
                                        ]);

    }



    public function guardarEquipoPrueba(Request $request)
    {
      //CARGAR DEPARTAMENTOS
      $departamentos = DB::table('pr_departamento')->pluck("IdDepartamento","Descripcion");
      //CARGAR TIPO IDENTIFICACION
      $tipo_identificacion = DB::table('pr_tipoidentificacion')->pluck("IdTipoIdentificacion","Descripcion");
      //CARGAR PROGRAMAS UNIVERSITARIOS
      $pr_programas_univ  = DB::table('pr_programas_univ')->pluck("id_programa","nombre_programa");
      //CARGAR NIVEL ACADEMICO
      $pr_nivelacademico  = DB::table('pr_nivelacademico')->pluck("IdNivelAcademico","Nombre");


      //Campos Hidden
      $tipo_tramite=1;
      $existe_tramite=(int)$request->existeTramite;
      $id=(int)$request->idTramite;

      $idTramite=Tramite::where('user',Auth::id())->where('id',$id)->orderBy('id','DESC')->first();

      if($id!=0){
        $rayosxObjprueba = new Rayosx_objprueba();
        $rayosxObjprueba->id_tramite_rayosx=$id;
        $rayosxObjprueba->obj_nombre = $request->obj_nombre;
        $rayosxObjprueba->obj_marca = $request->obj_marca;
        $rayosxObjprueba->obj_modelo = $request->obj_modelo;
        $rayosxObjprueba->obj_serie = $request->obj_serie;
        $rayosxObjprueba->obj_calibracion = $request->obj_calibracion;
        $rayosxObjprueba->obj_vigencia = $request->obj_vigencia;
        $rayosxObjprueba->obj_fecha = $request->obj_fecha;
        $rayosxObjprueba->obj_manual = $request->obj_manual;
        $rayosxObjprueba->obj_uso = $request->obj_uso;

        $rayosxObjprueba->save();


      }

      $rayosxOficialToe=Rayosx_toe::where('toe_tipo',1)->where('id_tramite_rayosx',$id)->orderBy('id_toe_rayosx','DESC')->first();
      $rayosxTemporalToe=Rayosx_toe::where('toe_tipo',2)->where('id_tramite_rayosx',$id)->orderBy('id_toe_rayosx','DESC')->get();
      $rayosxTalento=Rayosx_director::where('id_tramite_rayosx',$id)->orderBy('id_director_rayosx','DESC')->get();
      $rayosxObjprueba=Rayosx_objprueba::where('id_tramite_rayosx',$id)->orderBy('id_obj_rayosx','DESC')->get();

      return view('rayosx.index', ['idTramite' => $idTramite,
                                        'existeTramite'=>1,
                                        'tipoTramite'=>$tipo_tramite,
                                        'departamentos'=>$departamentos,
                                        'tiposIdentificacion'=>$tipo_identificacion,
                                        'prNivelacademico'=>$pr_nivelacademico,
                                        'prProgramasUniv'=>$pr_programas_univ,
                                        'oficialTOE'=>$rayosxOficialToe,
                                        'temporalTOE'=>$rayosxTemporalToe,
                                        'talento'=>$rayosxTalento,
                                        'objeto'=>$rayosxObjprueba,
                                        'paso'=>4 //Paso 3 - Cargar TOE
                                        ]);
    }

    public function verificarPaso4(Request $request)
    {
      //CARGAR DEPARTAMENTOS
      $departamentos = DB::table('pr_departamento')->pluck("IdDepartamento","Descripcion");
      //CARGAR TIPO IDENTIFICACION
      $tipo_identificacion = DB::table('pr_tipoidentificacion')->pluck("IdTipoIdentificacion","Descripcion");
      //CARGAR PROGRAMAS UNIVERSITARIOS
      $pr_programas_univ  = DB::table('pr_programas_univ')->pluck("id_programa","nombre_programa");
      //CARGAR NIVEL ACADEMICO
      $pr_nivelacademico  = DB::table('pr_nivelacademico')->pluck("IdNivelAcademico","Nombre");


      //Campos Hidden
      $tipo_tramite=1;
      $existe_tramite=(int)$request->existeTramite;
      $id=(int)$request->idTramite;

      if($id>0){
        $idTramite=Tramite::where('user',Auth::id())->where('id',$id)->orderBy('id','DESC')->first();

        $rayosxOficialToe=Rayosx_toe::where('toe_tipo',1)->where('id_tramite_rayosx',$id)->orderBy('id_toe_rayosx','DESC')->first();
        $rayosxTemporalToe=Rayosx_toe::where('toe_tipo',2)->where('id_tramite_rayosx',$id)->orderBy('id_toe_rayosx','DESC')->get();

        $rayosxTalento=Rayosx_director::where('id_tramite_rayosx',$id)->orderBy('id_director_rayosx','DESC')->get();
        $rayosxObjprueba=Rayosx_objprueba::where('id_tramite_rayosx',$id)->orderBy('id_obj_rayosx','DESC')->get();

        if($rayosxTalento->count() > 0 && $rayosxObjprueba->count() > 0){

          return view('rayosx.index', ['idTramite' => $idTramite,
                                            'existeTramite'=>1,
                                            'tipoTramite'=>$tipo_tramite,
                                            'departamentos'=>$departamentos,
                                            'tiposIdentificacion'=>$tipo_identificacion,
                                            'prNivelacademico'=>$pr_nivelacademico,
                                            'prProgramasUniv'=>$pr_programas_univ,
                                            'oficialTOE'=>$rayosxOficialToe,
                                            'temporalTOE'=>$rayosxTemporalToe,
                                            'talento'=>$rayosxTalento,
                                            'objeto'=>$rayosxObjprueba,
                                            'paso'=>5 //Paso 5 - DOCUMENTOS
                                            ]);
        }

        return view('rayosx.index', ['idTramite' => $idTramite,
                                            'existeTramite'=>1,
                                            'tipoTramite'=>$tipo_tramite,
                                            'departamentos'=>$departamentos,
                                            'tiposIdentificacion'=>$tipo_identificacion,
                                            'prNivelacademico'=>$pr_nivelacademico,
                                            'prProgramasUniv'=>$pr_programas_univ,
                                            'oficialTOE'=>$rayosxOficialToe,
                                            'temporalTOE'=>$rayosxTemporalToe,
                                            'talento'=>$rayosxTalento,
                                            'objeto'=>$rayosxObjprueba,
                                            'paso'=>4.5 //Paso 5 - ERROR
                                            ]);
      }
    }




    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function store(Request $request)
    {
      echo $tipo_tramite = $request->tipo_tramite."</br>";
      echo $dire_entidad = $request->dire_entidad."</br>";
      echo $sede_entidad = $request->sede_entidad."</br>";
      echo $email_entidad = $request->email_entidad."</br>";
      echo $depto_entidad = $request->depto_entidad."</br>";
      echo $mpio_entidad = $request->mpio_entidad."</br>";
      echo $celular_entidad = $request->celular_entidad."</br>";
      echo $indicativo_entidad = $request->indicativo_entidad."</br>";
      echo $telefono_entidad = $request->telefono_entidad."</br>";
      echo $extension_entidad = $request->extension_entidad."</br>";

      echo $categoria = $request->categoria."</br>";
      echo $categoria1 = $request->categoria1." </br>";
      echo $categoria2 = $request->categoria2."</br>";
      echo $categoria1_1 = $request->categoria1_1."</br>";
      echo $categoria1_2 = $request->categoria1_2."</br>";
      echo $equipo_generador = $request->equipo_generador."</br>";
      echo $tipo_visualizacion = $request->tipo_visualizacion."</br>";
      echo $marca_equipo = $request->marca_equipo."</br>";
      echo $modelo_equipo = $request->modelo_equipo."</br>";
      echo $serie_equipo = $request->serie_equipo."</br>";
      echo $marca_tubo_rx = $request->marca_tubo_rx."</br>";
      echo $modelo_tubo_rx = $request->modelo_tubo_rx."</br>";
      echo $serie_tubo_rx = $request->serie_tubo_rx."</br>";
      echo $tension_tubo_rx = $request->tension_tubo_rx."</br>";
      echo $contiene_tubo_rx = $request->contiene_tubo_rx."</br>";
      echo $energia_fotones = $request->energia_fotones."</br>";
      echo $energia_electrones = $request->energia_electrones."</br>";
      echo $carga_trabajo = $request->carga_trabajo."</br>";
      echo $ubicacion_equipo = $request->ubicacion_equipo."</br>";
      echo $numero_permiso = $request->numero_permiso."</br>";
      echo $anio_fabricacion = $request->anio_fabricacion."</br>";
      echo $anio_fabricacion_tubo = $request->anio_fabricacion_tubo."</br>";
      echo $encargado_pnombre = $request->encargado_pnombre."</br>";
      echo $encargado_snombre = $request->encargado_snombre."</br>";
      echo $encargado_papellido = $request->encargado_papellido."</br>";
      echo $encargado_sapellido = $request->encargado_sapellido."</br>";
       $encargado_tdocumento = $request->encargado_tdocumento;
      print_r($encargado_tdocumento)."</br>";
      echo $encargado_ndocumento = $request->encargado_ndocumento."</br>";
      echo $encargado_lexpedicion = $request->encargado_lexpedicion."</br>";
      echo $encargado_correo = $request->encargado_correo."</br>";
      echo $encargado_profesion = $request->encargado_profesion."</br>";
      echo $encargado_nivel = $request->encargado_nivel."</br>";

      $toe_pnombre = $request->toe_pnombre;
      print_r($toe_pnombre)."</br>";
      $toe_snombre = $request->toe_snombre;
      print_r($toe_snombre)."</br>";
      $toe_papellido = $request->toe_papellido;
      print_r($toe_papellido)."</br>";
      $toe_sapellido = $request->toe_sapellido;
      print_r($toe_sapellido)."</br>";
      $toe_tdocumento = $request->toe_tdocumento;
      print_r($toe_tdocumento)."</br>";
      $toe_ndocumento = $request->toe_ndocumento;
      print_r($toe_ndocumento)."</br>";
      $toe_lexpedicion = $request->toe_lexpedicion;
      print_r($toe_lexpedicion)."</br>";
      $toe_profesion = $request->toe_profesion;
      print_r($toe_profesion)."</br>";
      $toe_nivel = $request->toe_nivel;
      print_r($toe_nivel)."</br>";
      $toe_ult_entrenamiento = $request->toe_ult_entrenamiento;
      print_r($toe_ult_entrenamiento)."</br>";
      $toe_pro_entrenamiento = $request->toe_pro_entrenamiento;
      print_r($toe_pro_entrenamiento)."</br>";
      $toe_registro = $request->toe_registro;
      print_r($toe_registro)."</br>";
      echo $visita_previa = $request->visita_previa."</br>";
      echo $talento_pnombre = $request->talento_pnombre."</br>";
      echo $talento_snombre = $request->talento_snombre."</br>";
      echo $talento_papellido = $request->talento_papellido."</br>";
      echo $talento_sapellido = $request->talento_sapellido."</br>";
       $talento_tdocumento = $request->talento_tdocumento;
      print_r($talento_tdocumento);
      echo $talento_ndocumento = $request->talento_ndocumento."</br>";
      echo $talento_lexpedicion = $request->talento_lexpedicion."</br>";
      echo $talento_correo = $request->talento_correo."</br>";
      echo $talento_titulo = $request->talento_titulo."</br>";
      echo $talento_universidad = $request->talento_universidad."</br>";
      echo $talento_libro = $request->talento_libro."</br>";
      echo $talento_registro = $request->talento_registro."</br>";
      echo $talento_fecha_diploma = $request->talento_fecha_diploma."</br>";
      echo $talento_resolucion = $request->talento_resolucion."</br>";
      echo $talento_fecha_convalida = $request->talento_fecha_convalida."</br>";
      echo $talento_nivel = $request->talento_nivel."</br>";
      echo $talento_titulo_pos = $request->talento_titulo_pos."</br>";
      echo $talento_universidad_pos = $request->talento_universidad_pos."</br>";
      echo $talento_libro_pos = $request->talento_libro_pos."</br>";
      echo $talento_registro_pos = $request->talento_registro_pos."</br>";
      echo $talento_fecha_diploma_pos = $request->talento_fecha_diploma_pos."</br>";
      echo $talento_resolucion_pos = $request->talento_resolucion_pos."</br>";
      echo $talento_fecha_convalida_pos = $request->talento_fecha_convalida_pos."</br>";
      $obj_nombre = $request->obj_nombre;
      print_r($obj_nombre);
      $obj_marca = $request->obj_marca;
      print_r($obj_marca);
      $obj_modelo = $request->obj_modelo;
      print_r($obj_modelo);
      $obj_serie = $request->obj_serie;
      print_r($obj_serie);
      $obj_calibracion = $request->obj_calibracion;
      print_r($obj_calibracion);
      $obj_vigencia = $request->obj_vigencia;
      print_r($obj_vigencia);
      $obj_fecha = $request->obj_fecha;
      print_r($obj_fecha);
      $obj_manual = $request->obj_manual;
      print_r($obj_manual);
      $obj_uso = $request->obj_uso;
      print_r($obj_uso);
      echo $fi_doc_encargado = $request->fi_doc_encargado."</br>";
      echo $fi_diploma_encargado = $request->fi_diploma_encargado."</br>";
      echo $fi_blindajes = $request->fi_blindajes."</br>";
      echo $fi_control_calidad = $request->fi_control_calidad."</br>";
      echo $fi_registro_dosimetrico = $request->fi_registro_dosimetrico."</br>";
      echo $fi_registro_niveles = $request->fi_registro_niveles."</br>";
      echo $fi_plano = $request->fi_plano."</br>";
      echo $fi_certificado_capacitacion = $request->fi_certificado_capacitacion."</br>";
      echo $fi_programa_capacitacion = $request->fi_programa_capacitacion."</br>";
      echo $fi_procedimiento_mantenimiento = $request->fi_procedimiento_mantenimiento."</br>";
      echo $fi_pruebas_caracterizacion = $request->fi_pruebas_caracterizacion."</br>";
      echo $fi_programa_tecno = $request->fi_programa_tecno."</br>";
      echo $fi_programa_proteccion = $request->fi_programa_proteccion."</br>";
      echo $fi_soporte_talento = $request->fi_soporte_talento."</br>";
      echo $fi_diploma_director = $request->fi_diploma_director."</br>";
      echo $fi_res_convalida_director = $request->fi_res_convalida_director."</br>";
      echo $fi_diploma_pos_profe = $request->fi_diploma_pos_profe."</br>";
      echo $fi_res_convalida_profe = $request->fi_res_convalida_profe."</br>";
      echo $fi_cert_calibracion = $request->fi_cert_calibracion."</br>";
      echo $fi_declaraciones = $request->fi_declaraciones."</br>";
      echo $fi_doc_oficial = $request->fi_doc_oficial."</br>";
      echo $fi_diploma_oficial = $request->fi_diploma_oficial."</br>";
    }




    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
