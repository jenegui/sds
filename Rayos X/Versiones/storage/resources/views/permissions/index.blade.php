@extends('layouts.layout')

@section('title', '| Privilegios')

@section('content')

    <h1><i class="fa fa-key"></i>Privilegios Disponibles</h1>

    <a href="{{ route('users.index') }}" class="btn btn-primary pull-right">Usuarios</a>
    <a href="{{ route('roles.index') }}" class="btn btn-primary pull-right">Roles</a></h1>
    <hr>
    <div class="table-responsive">
        <table class="table table-bordered table-striped">

            <thead>
                <tr>
                    <th>Privilegios</th>
                    <th>Operaciones</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($permissions as $permission)
                <tr>
                    <td>{{ $permission->name }}</td> 
                    <td>
                    <a href="{{ URL::to('permissions/'.$permission->id.'/edit') }}" class="btn btn-info pull-left" style="margin-right: 3px;">Editar</a>

                    {!! Form::open(['method' => 'DELETE', 'route' => ['permissions.destroy', $permission->id] ]) !!}
                    {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                    {!! Form::close() !!}

                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    <a href="{{ URL::to('permissions/create') }}" class="btn btn-success">Agregar Privilegios</a>



@endsection