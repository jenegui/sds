<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0,shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Salud</title>



    <link rel="apple-touch-icon" sizes="57x57" href="{{url('/favicon/apple-icon-57x57.png')}}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{url('/favicon/apple-icon-60x60.png')}}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{url('/favicon/apple-icon-72x72.png')}}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{url('/favicon/apple-icon-76x76.png')}}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{url('/favicon/apple-icon-114x114.png')}}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{url('/favicon/apple-icon-120x120.png')}}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{url('/favicon/apple-icon-144x144.png')}}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{url('/favicon/apple-icon-152x152.png')}}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{url('/favicon/apple-icon-180x180.png')}}">
    <link rel="icon" type="image/png" sizes="192x192"  href="{{url('/favicon/android-icon-192x192.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{url('/favicon/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{url('/favicon/favicon-96x96.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{url('/favicon/favicon-16x16.png')}}">
    <link rel="manifest" href="{{url('/favicon/manifest.json')}}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="{{url('favicon/ms-icon-144x144.png')}}">
    <meta name="theme-color" content="#ffffff">




    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=M+PLUS+Rounded+1c:400,800" rel="stylesheet">
    <link href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css">
    <link rel="stylesheet" type="text/css" href="{{asset('css/validationEngine.jquery.css')}}">    
    <link rel="stylesheet" type="text/css" href="{{asset('css/jquery-ui.css')}}">        
    <link rel="stylesheet" href="{{asset('css/styles.css')}}">
    @isset($access)
    <link rel="stylesheet" href="{{asset('css/accesibilidad.css')}}">
    @endisset
</head>
<body>

    <header>
        <div class="container">
            <div class="logos">
                <div class="row">
                    <div class="col-12 col-md-6">
                        <img class="logo1" src="{{url('img/logos/home_alcaldia.svg')}}" alt="">
                    </div>
                    <div class="col-12 col-md-6 text-right">
                        <div style="color:white;">
                            <h2>Ventanilla Ùnica digital de Trámites y Servicios</h2>
                        </div>

                    </div>
                </div>
            </div>

            <nav class="navbar navbar-expand-md mt-2">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse"
                    aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarCollapse">
                    <div class="mr-2">
                        <div class="locator mb-2">
                            <div class="point"><span></span></div>
                        </div>
                        <ul class="navbar-nav">
                            <li class="nav-item">
                                <a class="nav-link" href="{{route('tramites.index')}}">Ventanilla Ùnica</a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" href="{{url('/tramiteRayosX')}}">Licencia Rayos X</a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" href="#">Autorización de Títulos</a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" href="#">Soporte y Ayuda</a>
                            </li>

                            @guest
                                <li class="nav-item"><a class="nav-link" href="#registro">Iniciar o Registrarse</a></li>
                            @else
                                <li class="nav-item">
                                <div class="dropdown">
                                    <a href="#" class="nav-link">Mi Cuenta</a>
                                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                      <a class="dropdown-item" href="{{ route('tramites.index') }}">
                                          Portal Ventanilla Unica
                                      </a>
                                      <a class="dropdown-item" href="{{ route('home') }}">
                                          Mi Perfil
                                      </a>
                                      <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                          Cerrar Sesión
                                      </a>
                                      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                         {{ csrf_field() }}
                                      </form>
                                    </div>
                                 </div>
                                </li>
                            @endguest

                        </ul>
                    </div>
                    <div class="right-content">
                            <form action="{{url('/busqueda')}}" method="post">
                                <input name="term" class="form-control mr-sm-2" type="text" placeholder="Buscar" aria-label="Buscar">
                                <button class="buscar"></button>
                            </form>
                        <div class="dropdown text-center">
                            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                                Accesibilidad
                            </button>
                            <div class="dropdown-menu " aria-labelledby="dropdownMenuButton">

                                <a class="dropdown-item" href="{{url('/accesibilidad')}}">
                                </a>
                            </div>
                        </div>
                    </div>

                </div>
            </nav>
            @yield('barnav')

        </div>

    </header>


    <main class="container">

        @yield('content')

    </main>

    @yield("redes")


    <div class="container enlaces">
        <div class="row">
            <div class="col-12 col-md-4">
                <div class="title">Entes de Control</div>
                <ul>
                    <li><a target="_blank" href="http://www.personeriabogota.gov.co/">Personería de Bogotá</a></li>
                    <li><a target="_blank" href="https://www.procuraduria.gov.co/portal/">Procuraduría General de la Nación</a></li>
                    <li><a target="_blank" href="https://www.contraloria.gov.co/">Contraloría General de la Nación</a></li>
                    <li><a target="_blank" href="http://concejodebogota.gov.co/cbogota/site/edic/base/port/inicio.php">Concejo de Bogotá</a></li>
                    <li><a target="_blank" href="http://www.veeduriadistrital.gov.co/">Veeduría Distrital</a></li>
                    <li><a target="_blank" href="https://www.contratacionbogota.gov.co/es/web/cav3/ciudadano">Portal de Contratación a la Vista</a></li>
                    <li><a target="_blank" href="https://www.contratos.gov.co/puc/buscador.html">Portal de Contratación - SECOP</a></li>
                </ul>
            </div>
            <div class="col-12 col-md-4">
                <div class="title">Entes del Gobierno</div>
                <ul>
                    <li><a target="_blank" href="https://www.minsalud.gov.co/Paginas/default.aspx">Ministerio de Salud y Protección Social</a></li>
                    <li><a target="_blank" href="http://estrategia.gobiernoenlinea.gov.co/623/w3-channel.html">Gobierno Digital</a></li>
                    <li><a target="_blank" href="https://www.nomasfilas.gov.co/">No más filas</a></li>
                </ul>
            </div>
            <div class="col-12 col-md-4">
                <div class="title">Secretaría Distrital de Salud</div>
                <ul>
                    <li><a target="_blank" href="https://www.google.com/maps/place/Secretar%C3%ADa+Distrital+de+Salud/@4.6161635,-74.0947824,17.83z/data=!4m5!3m4!1s0x8e3f996f336f8a7b:0x183024e16c3df506!8m2!3d4.6155823!4d-74.0941572">Cra 32# 12-81 Bogotá, Colombia</a></li>
                    <li><a href="tel:57-1-3649090">Teléfono: (571) 3649090</a></li>
                    <li>Código Postal: 0571</li>
                    <li><a href="mailto:contactenos@saludcapital.gov.co">contactenos@saludcapital.gov.co</a></li>
                </ul>
            </div>
        </div>
    </div>

    <footer>
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-4"></div>
                <div class="col-12 col-md-4 text-md-center">
                    <p>2019. @ Todos los derechos reservados</p>
                    <p><a target="_blank" href="http://www.saludcapital.gov.co/Documents/Politica_Proteccion_Datos_P.pdf">*Habeas data</a></p>
                    <p><a href="http://www.saludcapital.gov.co/Documents/Politicas_Sitios_Web.pdf" target="_blank">*Terminos y condiciones</a></p>
                </div>
                <div class="col-12 col-md-4 text-md-right">
                    <img src="img/logos/home_footer.svg" alt="">
                </div>
            </div>
        </div>
    </footer>


    <script src="http://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script type="text/javascript" src="{{asset('js/jquery.validationEngine.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/jquery.validationEngine-es.js')}}"></script>
    <script src="{{asset('js/jquery.nicescroll.min.js')}}"></script>
    <script src="{{asset('js/jquery.autocomplete.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
    <script src="{{asset('js/bootstrap.bundle.min.js')}}"></script>
    <script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    
    



    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="{{asset('js/code.js')}}"></script>

    @yield("scripts")

</body>
</html>
