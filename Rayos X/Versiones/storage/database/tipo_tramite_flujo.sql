-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 25-08-2019 a las 23:02:24
-- Versión del servidor: 10.1.40-MariaDB
-- Versión de PHP: 7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `pruebas_solicitud_rayosx`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_tramite_flujo`
--

CREATE TABLE `tipo_tramite_flujo` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tipo_tramite` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tipo_tramite_id` int(10) UNSIGNED NOT NULL,
  `flujo_actividad_inicial` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `flujo_actividad_final` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `flujo_accion_permitida` set('avanzar','devolver','volver_inicio','inicio','fin') COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_responsable` bigint(20) UNSIGNED NOT NULL,
  `tipo_estado` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tipo_tramite_flujo`
--

INSERT INTO `tipo_tramite_flujo` (`id`, `tipo_tramite`, `tipo_tramite_id`, `flujo_actividad_inicial`, `flujo_actividad_final`, `flujo_accion_permitida`, `user_responsable`, `tipo_estado`) VALUES
(2, '1', 1, 'CREAR SOLICITUD', 'REVISAR SOLICITUD', 'avanzar,inicio', 1, 1),
(3, '1', 1, 'CREAR SOLICITUD', 'REVISAR SOLICITUD', 'avanzar,inicio', 1, 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `tipo_tramite_flujo`
--
ALTER TABLE `tipo_tramite_flujo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tipo_tramite_flujo_tipo_tramite_id_foreign` (`tipo_tramite_id`),
  ADD KEY `tipo_tramite_flujo_user_responsable_foreign` (`user_responsable`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `tipo_tramite_flujo`
--
ALTER TABLE `tipo_tramite_flujo`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `tipo_tramite_flujo`
--
ALTER TABLE `tipo_tramite_flujo`
  ADD CONSTRAINT `tipo_tramite_flujo_tipo_tramite_id_foreign` FOREIGN KEY (`tipo_tramite_id`) REFERENCES `tipo_tramite` (`id`),
  ADD CONSTRAINT `tipo_tramite_flujo_user_responsable_foreign` FOREIGN KEY (`user_responsable`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
