-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 26-08-2019 a las 11:03:13
-- Versión del servidor: 10.1.40-MariaDB
-- Versión de PHP: 7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `pruebas_solicitud_rayosx`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rayosx_direccion`
--

CREATE TABLE `rayosx_direccion` (
  `id_direccion_rayosx` int(11) NOT NULL,
  `id_tramite_rayosx` int(11) NOT NULL,
  `dire_entidad` varchar(250) NOT NULL,
  `sede_entidad` varchar(250) NOT NULL,
  `email_entidad` varchar(250) NOT NULL,
  `depto_entidad` int(11) NOT NULL,
  `mpio_entidad` int(11) NOT NULL,
  `celular_entidad` varchar(20) NOT NULL,
  `indicativo_entidad` varchar(5) DEFAULT NULL,
  `telefono_entidad` varchar(15) NOT NULL,
  `extension_entidad` varchar(5) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Tabla direccion entidad Rayos X';

--
-- Volcado de datos para la tabla `rayosx_direccion`
--

INSERT INTO `rayosx_direccion` (`id_direccion_rayosx`, `id_tramite_rayosx`, `dire_entidad`, `sede_entidad`, `email_entidad`, `depto_entidad`, `mpio_entidad`, `celular_entidad`, `indicativo_entidad`, `telefono_entidad`, `extension_entidad`, `created_at`, `updated_at`) VALUES
(1, 232, 'Carrera 104 No 75c-12', 'Alamos Norte2', 'mario@mario.com', 1, 1, '3012388303', '031', '2272270', '9870', '2019-08-10 00:07:15', '2019-08-10 05:07:15'),
(2, 238, 'akaskaksasasska', 'Alamos Nasasorte67', 'asas@asasaa.vom', 10, 429, '1234512121212', NULL, '12345123434', '12345', '2019-08-12 22:46:53', '2019-08-13 03:46:53'),
(3, 237, 'akaskakska', 'Alamos Norte2', 'asas@asa.vom', 2, 126, '123', NULL, '123', '123', '2019-08-10 20:18:43', '2019-08-10 20:18:43'),
(4, 236, 'akaskakska', 'Alamos Norte2', 'asas@asa.vom', 11, 459, '12212', NULL, '121212', '1212', '2019-08-11 03:29:06', '2019-08-11 03:29:06'),
(5, 235, 'akaskakska', 'Alamos Norte2', 'asas@asa.vom', 3, 149, '213123', NULL, '1323122', '3321', '2019-08-11 03:40:59', '2019-08-11 03:40:59'),
(6, 239, 'AKASKAKSKA', 'ALAMOS NORTE2', 'ASAS@ASA.VOM', 1, 1, '232312312313123', NULL, '321312313123123', '12312', '2019-08-12 18:36:04', '2019-08-12 18:36:04'),
(7, 240, 'AKASKAKSKA', 'ALAMOS NORTE2', 'ASAS@ASA.VOM', 1, 1, '121312121211212', NULL, '121212112', '12121', '2019-08-13 02:30:16', '2019-08-13 02:30:16'),
(8, 241, 'AKASKAKSKA', 'ALAMOS NASASORTE2', 'ASAS@ASA.VOM', 3, 149, '1243435311', NULL, '12233567', '12345', '2019-08-13 03:37:56', '2019-08-13 03:37:56');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rayosx_director`
--

CREATE TABLE `rayosx_director` (
  `id_director_rayosx` int(11) NOT NULL,
  `id_tramite_rayosx` int(11) NOT NULL,
  `talento_pnombre` varchar(150) NOT NULL,
  `talento_snombre` varchar(150) NOT NULL,
  `talento_papellido` varchar(150) NOT NULL,
  `talento_sapellido` varchar(150) NOT NULL,
  `talento_tdocumento` int(11) NOT NULL,
  `talento_ndocumento` varchar(20) NOT NULL,
  `talento_lexpedicion` varchar(150) NOT NULL,
  `talento_correo` varchar(150) NOT NULL,
  `talento_titulo` varchar(150) NOT NULL,
  `talento_universidad` varchar(150) NOT NULL,
  `talento_libro` varchar(150) NOT NULL,
  `talento_registro` varchar(150) NOT NULL,
  `talento_fecha_diploma` varchar(150) NOT NULL,
  `talento_resolucion` varchar(150) NOT NULL,
  `talento_fecha_convalida` varchar(150) NOT NULL,
  `talento_nivel` varchar(150) NOT NULL,
  `talento_titulo_pos` varchar(150) NOT NULL,
  `talento_universidad_pos` varchar(150) NOT NULL,
  `talento_libro_pos` varchar(150) DEFAULT NULL,
  `talento_registro_pos` varchar(150) DEFAULT NULL,
  `talento_fecha_diploma_pos` varchar(150) DEFAULT NULL,
  `talento_resolucion_pos` varchar(150) DEFAULT NULL,
  `talento_fecha_convalida_pos` varchar(150) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Tabla director tecnico rayos x';

--
-- Volcado de datos para la tabla `rayosx_director`
--

INSERT INTO `rayosx_director` (`id_director_rayosx`, `id_tramite_rayosx`, `talento_pnombre`, `talento_snombre`, `talento_papellido`, `talento_sapellido`, `talento_tdocumento`, `talento_ndocumento`, `talento_lexpedicion`, `talento_correo`, `talento_titulo`, `talento_universidad`, `talento_libro`, `talento_registro`, `talento_fecha_diploma`, `talento_resolucion`, `talento_fecha_convalida`, `talento_nivel`, `talento_titulo_pos`, `talento_universidad_pos`, `talento_libro_pos`, `talento_registro_pos`, `talento_fecha_diploma_pos`, `talento_resolucion_pos`, `talento_fecha_convalida_pos`, `created_at`, `updated_at`) VALUES
(1, 232, 'LILIANA', 'LILIANA', 'ESPINOSA', 'ESPINOSA', 1, '3456789', 'ewrtyuio', 'ertyuio@hgjkl.com', 'PROFESIONAL EN INFORMATICA 2', 'dfghjkl', '310', 'ghjkll', '2019-08-02', '301', '2019-08-09', '1', '1', 'gfhjkl', '910', '310', '2019-08-09', '6755', '2019-08-07', '2019-08-10 00:33:58', '2019-08-10 05:33:58'),
(2, 238, 'LILIANA', 'LILIANA', 'ESPINOSA', 'ESPINOSA', 1, '1231212', '12121', 'ertyuio@hgjkl.com', '121212', '121212', '12121', '121212', '2019-08-10', '12121', '2019-08-10', '1', '1212121', '1121212', '12121', '12121', '2019-08-10', '121212', '2019-08-10', '2019-08-10 20:53:21', '2019-08-10 20:53:21'),
(3, 237, 'LILIAN', 'espinosa', 'espinosa', 'espinosa', 1, '647474', 'ewrtyuio', 'ertyuio@hgjkl.com', 'PROFESIONAL EN INFORMATICA', 'dfghjkl', 'fghjkl', 'ghjkll', '2019-08-10', '301', '2019-08-10', '1', 'asasas', '1121212', '910', '310', '2019-08-10', 'fghjkl', '2019-08-10', '2019-08-11 02:23:14', '2019-08-11 02:23:14'),
(4, 236, 'LILIAN', 'LILIANA', 'ESPINOSA', 'espinosa', 1, '231123', 'ewrtyuio', 'sasa@asas.com', 'dfghj', 'dfghjkl', 'fghjkl', 'ghjkll', '2019-08-10', '301', '2019-08-10', '1', '1212121', '1121212', '12121', '12121', '2019-08-10', 'fghjkl', '2019-08-10', '2019-08-11 02:38:47', '2019-08-11 02:38:47');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rayosx_documentos`
--

CREATE TABLE `rayosx_documentos` (
  `id_documentos_rayosx` int(11) NOT NULL,
  `id_tramite_rayosx` int(11) NOT NULL,
  `documento` varchar(60) NOT NULL,
  `path` varchar(255) NOT NULL,
  `categoria` int(15) NOT NULL,
  `id_archivo` int(11) NOT NULL,
  `estado` char(2) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Tabla documentos adjuntos';

--
-- Volcado de datos para la tabla `rayosx_documentos`
--

INSERT INTO `rayosx_documentos` (`id_documentos_rayosx`, `id_tramite_rayosx`, `documento`, `path`, `categoria`, `id_archivo`, `estado`, `created_at`, `updated_at`) VALUES
(1, 232, 'fi_doc_oficial', 'public/XTs7qM9Wov6KaFp77nGG8lH5yedAwxEuDZL3kFnh.png', 2, 1, '1', '2019-08-11 05:48:30', '2019-08-11 10:48:30'),
(2, 232, 'fi_diploma_oficial', 'public/YaZnZjC4y7uHIcaAdnLVbXcWsiP3xjvexmlahybb.png', 2, 1, '1', '2019-08-11 05:48:30', '2019-08-11 10:48:30'),
(3, 232, 'fi_blindajes', 'public/bl9GiX2VQzruHArEdEX8L8vFmH9eU29KKYk6vCGm.png', 2, 1, '1', '2019-08-11 05:48:30', '2019-08-11 10:48:30'),
(4, 232, 'fi_control_calidad', 'public/9fjRFEHN08zXhczI3gxSCLHqhv6tq9PKGhHJZHgP.png', 2, 1, '1', '2019-08-11 05:48:30', '2019-08-11 10:48:30'),
(5, 232, 'fi_registro_dosimetrico', 'public/HG1tbZQSYC6NXcDcpb6uwtDcoMquxXEbTrIoQBjt.png', 2, 1, '1', '2019-08-11 05:48:30', '2019-08-11 10:48:30'),
(6, 232, 'fi_plano', 'public/E8Ny31HglPiRFeNTocDYTW2cdiEFCAFFiMQe3Zhs.png', 2, 1, '1', '2019-08-11 05:48:30', '2019-08-11 10:48:30'),
(7, 232, 'fi_soporte_talento', 'public/aM9vBziCtLY8ygJ73zxpl5soSS9JKEyI0GXfMQqa.png', 2, 1, '1', '2019-08-11 05:48:30', '2019-08-11 10:48:30'),
(8, 232, 'fi_diploma_director', 'public/Ba6hwZWVtKtnMlkrR5MigdEaCV7GKswJfrpm5RL3.png', 2, 1, '1', '2019-08-11 05:48:30', '2019-08-11 10:48:30'),
(9, 232, 'fi_res_convalida_director', 'public/QFVd6EmudqAlhN0gPN2QLGNgsChd8o3PCjJjz9KB.png', 2, 1, '1', '2019-08-11 05:48:30', '2019-08-11 10:48:30'),
(10, 232, 'fi_diploma_pos_profe', 'public/88AzSfgi7ccdesFIAq4NsB1B4oNFpJmzc1qP2Dek.png', 2, 1, '1', '2019-08-11 05:53:10', '2019-08-11 05:53:10'),
(11, 232, 'fi_res_convalida_profe', 'public/yKg7m9WqDRzh3zquQfM7myTZtwwoeENVdTO8wwHl.png', 2, 1, '1', '2019-08-10 09:44:51', '2019-08-10 09:44:51'),
(12, 232, 'fi_cert_calibracion', 'public/sDJ0hxJcKRRuaRfBiILSntGFBCQCc2ZO0ZshhIqq.png', 2, 1, '1', '2019-08-11 05:48:30', '2019-08-11 10:48:30'),
(13, 232, 'fi_declaraciones', 'public/OgxUalsvOvFYdSvbCi18m5kpqPOxC03UNXCAsI5D.png', 2, 1, '1', '2019-08-11 05:48:30', '2019-08-11 10:48:30'),
(37, 238, 'fi_pruebas_caracterizacion', 'public/9CljaoPoedJELNp9s63jGOrk26CU4rXCpeuiUDZj.png', 1, 1, '1', '2019-08-11 11:07:25', '2019-08-11 11:07:25'),
(36, 238, 'fi_procedimiento_mantenimiento', 'public/gmKxSPPJHaicFjiUbmXXP5uDr7JQjHoTI1tYoV8l.png', 1, 1, '1', '2019-08-11 11:07:25', '2019-08-11 11:07:25'),
(35, 238, 'fi_programa_capacitacion', 'public/g6VpJV0MtayyaB6uNTWqnKqwGxqViiIIhv0Cchzr.png', 1, 1, '1', '2019-08-11 11:07:25', '2019-08-11 11:07:25'),
(34, 238, 'fi_certificado_capacitacion', 'public/uGrnan6XRks6hE3rFmaDfUYDz7gPvJB9c6K3n547.png', 1, 1, '1', '2019-08-11 11:07:25', '2019-08-11 11:07:25'),
(33, 238, 'fi_plano', 'public/VmJlq9uhPe8BJk8tMOLt3NdVte1rBknrDrT5UIcg.png', 1, 1, '1', '2019-08-11 11:07:25', '2019-08-11 11:07:25'),
(32, 238, 'fi_registro_niveles', 'public/D2FshVI1yHPIhkZsApIp3P0zucoOu5G2k21gzbWE.png', 1, 1, '1', '2019-08-11 11:07:25', '2019-08-11 11:07:25'),
(31, 238, 'fi_registro_dosimetrico', 'public/eJS5LcCRS1qlDr6ie99fRDvezTS7X83xiEZU09bu.png', 1, 1, '1', '2019-08-11 11:07:25', '2019-08-11 11:07:25'),
(30, 238, 'fi_control_calidad', 'public/8tUJT9bVJBUeE9GoISkBzdfKWl7enYfUYX1LL838.png', 1, 1, '1', '2019-08-11 11:07:25', '2019-08-11 11:07:25'),
(29, 238, 'fi_blindajes', 'public/EoMPMXbBi4kMLu8a2S4uXpeaPczRiCuFdGipnA5X.png', 1, 1, '1', '2019-08-11 11:07:25', '2019-08-11 11:07:25'),
(28, 238, 'fi_diploma_encargado', 'public/ZFV7D99PTJkiIi4Ubv1AashFCJjefcv8odkuituw.png', 1, 1, '1', '2019-08-11 11:07:25', '2019-08-11 11:07:25'),
(27, 238, 'fi_doc_encargado', 'public/AotdtI6YrXdZzEATn3oPJ9NKpGGTJJBgMMCeDEM0.png', 1, 1, '1', '2019-08-11 11:07:25', '2019-08-11 11:07:25'),
(38, 238, 'fi_programa_tecno', 'public/plWn5oBe679xSOVwqPz8FIElKjdvPG289v97FfuD.png', 1, 1, '1', '2019-08-11 11:07:25', '2019-08-11 11:07:25'),
(39, 238, 'fi_programa_proteccion', 'public/4q7bJsh9U6ENtUIUYuVjHPef6JZpOYaCcX9JPFO7.png', 1, 1, '1', '2019-08-11 11:07:25', '2019-08-11 11:07:25'),
(40, 238, 'fi_soporte_talento', 'public/94xDn9yXNqc4dmxessvGOC7aFJCgytGZEAciYjII.png', 1, 1, '1', '2019-08-11 11:07:25', '2019-08-11 11:07:25'),
(41, 238, 'fi_diploma_director', 'public/e6ppyMyR7vfl5QotSMmsRaXskJ7PFXtXDHWi6QAu.png', 1, 1, '1', '2019-08-11 11:07:25', '2019-08-11 11:07:25'),
(42, 238, 'fi_res_convalida_director', 'public/9i6WAi1sJP8Bk9oKl9CtKiJl1hGR9OZdbrBvDS8I.png', 1, 1, '1', '2019-08-11 11:07:25', '2019-08-11 11:07:25'),
(43, 238, 'fi_diploma_pos_profe', 'public/Jh3YzlzKSXMiJnRhHBAPeLteTUqv23dzVw8ZQwv0.png', 1, 1, '1', '2019-08-11 11:07:25', '2019-08-11 11:07:25'),
(44, 238, 'fi_res_convalida_profe', 'public/WKwAqkzpsuoxfL6QkPfLXLpVpw1opMkAp2gbv2q5.png', 1, 1, '1', '2019-08-11 11:07:25', '2019-08-11 11:07:25'),
(45, 238, 'fi_cert_calibracion', 'public/Pz82TdRKi47Rq5Fa2btPSSxvztGaPI4ajNkTHZbL.png', 1, 1, '1', '2019-08-11 11:07:25', '2019-08-11 11:07:25'),
(46, 238, 'fi_declaraciones', 'public/cliqdv1obcfWADUIn3AG0LHeQjHvQtm00rGGYmVN.png', 1, 1, '1', '2019-08-11 11:07:25', '2019-08-11 11:07:25'),
(47, 237, 'fi_doc_encargado', 'public/wf9ehuMaKoT2At3ljBe6MjLJjfCQcDrjYm5CEYdZ.png', 1, 1, '1', '2019-08-11 11:32:37', '2019-08-11 11:32:37'),
(48, 237, 'fi_diploma_encargado', 'public/MmHnyeXAhxioeP8DeC8Zs91ajm8xX24PpqIxhrWQ.png', 1, 1, '1', '2019-08-11 11:32:37', '2019-08-11 11:32:37'),
(49, 237, 'fi_blindajes', 'public/anoXvHmYq31E5oSnJFJqlhjbdBXleQGVqMndsLxD.png', 1, 1, '1', '2019-08-11 11:32:37', '2019-08-11 11:32:37'),
(50, 237, 'fi_control_calidad', 'public/2djGPz6gfjPUzhgTSzENisFHBU9geBe6TYIPYe2u.png', 1, 1, '1', '2019-08-11 11:32:37', '2019-08-11 11:32:37'),
(51, 237, 'fi_registro_dosimetrico', 'public/qO7Tl3d93knqgPBCMJisIbZ6ZcA2xjvOytxll6mr.png', 1, 1, '1', '2019-08-11 11:32:37', '2019-08-11 11:32:37'),
(52, 237, 'fi_registro_niveles', 'public/HqkHveb7TW8uvDh4fcZWpXOKdU9EBejqwE8QLIFF.png', 1, 1, '1', '2019-08-11 11:32:37', '2019-08-11 11:32:37'),
(53, 237, 'fi_plano', 'public/aJ21BXVwVBlhDOxUBsCrKwGy19wPs0ccvSoKiPhs.png', 1, 1, '1', '2019-08-11 11:32:37', '2019-08-11 11:32:37'),
(54, 237, 'fi_certificado_capacitacion', 'public/RqSwHks05IRk8bdgiGDLXrvVWrgOS1ajgrfnf7wG.png', 1, 1, '1', '2019-08-11 11:32:37', '2019-08-11 11:32:37'),
(55, 237, 'fi_programa_capacitacion', 'public/1Zk35Bog7JORrFnyhC7muHON9O5BPOs5MTWCr8gF.png', 1, 1, '1', '2019-08-11 11:32:37', '2019-08-11 11:32:37'),
(56, 237, 'fi_procedimiento_mantenimiento', 'public/n0J8ldD1O1GIBcNDMU3li1C3APa3gRoXkxafbDUc.png', 1, 1, '1', '2019-08-11 11:32:37', '2019-08-11 11:32:37'),
(57, 237, 'fi_pruebas_caracterizacion', 'public/FS1BbQmGuQm4aRbECZ0UvInXKgEX9Q34MYM9pUfu.png', 1, 1, '1', '2019-08-11 11:32:37', '2019-08-11 11:32:37'),
(58, 237, 'fi_programa_tecno', 'public/Uxh9001qF3r7auqH7fqD2PX8e745Ny9YMelI9YNc.png', 1, 1, '1', '2019-08-11 11:32:37', '2019-08-11 11:32:37'),
(59, 237, 'fi_programa_proteccion', 'public/zkWLYFBrVLCHNaX9ahr150NQUvTyTXZjJ49zhiHE.png', 1, 1, '1', '2019-08-11 11:32:37', '2019-08-11 11:32:37'),
(60, 237, 'fi_soporte_talento', 'public/DYH07O86xW1HumVcvR99dRmApMaKMwhnu195fzoE.png', 1, 1, '1', '2019-08-11 11:32:37', '2019-08-11 11:32:37'),
(61, 237, 'fi_diploma_director', 'public/rZJ6tdDbQNxKAsFDLlPHfaRMkW4XsIcOA3gbObtF.png', 1, 1, '1', '2019-08-11 11:32:37', '2019-08-11 11:32:37'),
(62, 237, 'fi_res_convalida_director', 'public/Urm7fytNtz7xHwVjvqfyxlgM4oofzhGXGe5Vi3PL.png', 1, 1, '1', '2019-08-11 11:32:37', '2019-08-11 11:32:37'),
(63, 237, 'fi_diploma_pos_profe', 'public/qvsMPKgaWkGyioDoTuVSWdDDugNK6AjVgopO1R99.png', 1, 1, '1', '2019-08-11 11:32:37', '2019-08-11 11:32:37'),
(64, 237, 'fi_res_convalida_profe', 'public/jEXX7npiA8MkFMZemZbKKyWepTQRBIaIEKNkJIYS.png', 1, 1, '1', '2019-08-11 11:32:37', '2019-08-11 11:32:37'),
(65, 237, 'fi_cert_calibracion', 'public/6TregXMpvcDDblmMcMG994ugjaPAwCupq4DWkZSn.png', 1, 1, '1', '2019-08-11 11:32:37', '2019-08-11 11:32:37'),
(66, 237, 'fi_declaraciones', 'public/q1kmvD90m0HoPwWJ3JMcRQsnERhwWLUaRlr9o57p.png', 1, 1, '1', '2019-08-11 11:32:37', '2019-08-11 11:32:37');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rayosx_encargado`
--

CREATE TABLE `rayosx_encargado` (
  `id_encargado_rayosx` int(11) NOT NULL,
  `id_tramite_rayosx` int(11) NOT NULL,
  `encargado_pnombre` varchar(150) NOT NULL,
  `encargado_snombre` varchar(150) NOT NULL,
  `encargado_papellido` varchar(150) NOT NULL,
  `encargado_sapellido` varchar(150) NOT NULL,
  `encargado_tdocumento` int(11) NOT NULL,
  `encargado_ndocumento` varchar(20) NOT NULL,
  `encargado_lexpedicion` varchar(150) NOT NULL,
  `encargado_correo` varchar(150) NOT NULL,
  `encargado_profesion` int(11) NOT NULL,
  `encargado_nivel` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Tabla encargado de proteccion';

--
-- Volcado de datos para la tabla `rayosx_encargado`
--

INSERT INTO `rayosx_encargado` (`id_encargado_rayosx`, `id_tramite_rayosx`, `encargado_pnombre`, `encargado_snombre`, `encargado_papellido`, `encargado_sapellido`, `encargado_tdocumento`, `encargado_ndocumento`, `encargado_lexpedicion`, `encargado_correo`, `encargado_profesion`, `encargado_nivel`) VALUES
(1, 238, 'Javier', 'Andres', 'Morales', 'Rivera', 1, '1035487796', 'Bogota', 'qwe@qwe.com', 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rayosx_equipos`
--

CREATE TABLE `rayosx_equipos` (
  `id_equipo_rayosx` int(11) NOT NULL,
  `id_tramite_rayosx` int(11) NOT NULL,
  `categoria` int(11) NOT NULL,
  `categoria1` int(11) DEFAULT NULL,
  `categoria1_1` int(11) DEFAULT NULL,
  `categoria1_2` int(11) DEFAULT NULL,
  `categoria2` int(11) DEFAULT NULL,
  `equipo_generador` int(11) DEFAULT NULL,
  `tipo_visualizacion` int(11) NOT NULL,
  `marca_equipo` varchar(250) NOT NULL,
  `modelo_equipo` varchar(250) NOT NULL,
  `serie_equipo` varchar(250) NOT NULL,
  `marca_tubo_rx` varchar(250) NOT NULL,
  `modelo_tubo_rx` varchar(250) NOT NULL,
  `serie_tubo_rx` varchar(250) NOT NULL,
  `tension_tubo_rx` float NOT NULL,
  `contiene_tubo_rx` float NOT NULL,
  `energia_fotones` float NOT NULL,
  `energia_electrones` float NOT NULL,
  `carga_trabajo` float NOT NULL,
  `ubicacion_equipo` varchar(250) NOT NULL,
  `numero_permiso` varchar(250) DEFAULT NULL,
  `anio_fabricacion` varchar(250) NOT NULL,
  `anio_fabricacion_tubo` varchar(250) NOT NULL,
  `estado` int(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Tabla equipos de rayos X';

--
-- Volcado de datos para la tabla `rayosx_equipos`
--

INSERT INTO `rayosx_equipos` (`id_equipo_rayosx`, `id_tramite_rayosx`, `categoria`, `categoria1`, `categoria1_1`, `categoria1_2`, `categoria2`, `equipo_generador`, `tipo_visualizacion`, `marca_equipo`, `modelo_equipo`, `serie_equipo`, `marca_tubo_rx`, `modelo_tubo_rx`, `serie_tubo_rx`, `tension_tubo_rx`, `contiene_tubo_rx`, `energia_fotones`, `energia_electrones`, `carga_trabajo`, `ubicacion_equipo`, `numero_permiso`, `anio_fabricacion`, `anio_fabricacion_tubo`, `estado`, `created_at`, `updated_at`) VALUES
(1, 232, 2, 3, 1, NULL, 2, 0, 3, 'wert', 'rtyu', 'tyui', 'yuio', 'fghj', 'ghjk', 789, 987, 157, 157, 456, 'CENTROS SONRIA CLINICA DENTAL 3', '45', '2010', '567', 1, '2019-08-10 00:31:46', '2019-08-10 05:31:46'),
(26, 238, 1, 2, 2, NULL, 3, NULL, 4, 'wert', 'rtyu', 'tyui', 'yuio', 'fghj', 'ghjk', 123, 123, 123, 123, 123, 'CENTROS SONRIA CLINICA DENTAL', NULL, '2019', '2019', 1, '2019-08-10 19:06:56', '2019-08-10 19:06:56'),
(27, 237, 1, 2, 2, NULL, 2, NULL, 3, 'wert', 'rtyu', 'tyui', 'yuio', 'fghj', 'ghjk', 1231, 123, 123, 123, 123, 'CENTROS SONRIA CLINICA DENTAL 3', NULL, '1232', '1234', 1, '2019-08-10 20:19:24', '2019-08-10 20:19:24'),
(28, 236, 1, 2, 2, NULL, 2, NULL, 2, 'wert', 'rtyu', 'tyui', 'yuio', 'fghj', 'ghjk', 231, 213, 231, 23123, 321, 'CENTROS SONRIA CLINICA DENTAL', NULL, '2019', '2019', 1, '2019-08-11 03:32:33', '2019-08-11 03:32:33'),
(29, 235, 1, 2, 2, NULL, 2, NULL, 1, 'ASASSASASSASASSASASSASASSASASS', 'AA', 'AA', 'AAA', 'AAA', 'AAA', 1, 1, 1, 1, 1, '111', NULL, '111', '111', 1, '2019-08-11 21:12:55', '2019-08-11 21:12:55'),
(30, 239, 1, 2, 1, NULL, 2, NULL, 3, 'ASASSASASSASASSASASSASASSASASS', 'ASASSASASSASASSASASSASASSASASS', 'ASASSASASSASASSASASSASASSASASS', 'ASASSASASSASASSASASSASASSASASS', 'ASASSASASSASASSASASSASASSASASS', 'ASASSASASSASASSASASSASASSASASS', 9, 9, 9, 9, 9, 'CENTROS SONRIA CLINICA DENTAL', NULL, '2019', '2019', 1, '2019-08-12 18:41:53', '2019-08-12 18:41:53');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rayosx_objprueba`
--

CREATE TABLE `rayosx_objprueba` (
  `id_obj_rayosx` int(11) NOT NULL,
  `id_tramite_rayosx` int(11) NOT NULL,
  `obj_nombre` varchar(150) NOT NULL,
  `obj_marca` varchar(150) NOT NULL,
  `obj_modelo` varchar(150) NOT NULL,
  `obj_serie` varchar(150) NOT NULL,
  `obj_calibracion` varchar(150) NOT NULL,
  `obj_vigencia` int(11) NOT NULL,
  `obj_fecha` varchar(150) NOT NULL,
  `obj_manual` int(11) NOT NULL,
  `obj_uso` varchar(150) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `rayosx_objprueba`
--

INSERT INTO `rayosx_objprueba` (`id_obj_rayosx`, `id_tramite_rayosx`, `obj_nombre`, `obj_marca`, `obj_modelo`, `obj_serie`, `obj_calibracion`, `obj_vigencia`, `obj_fecha`, `obj_manual`, `obj_uso`, `created_at`, `updated_at`) VALUES
(1, 238, 'sdfghjk', 'fghjkl', 'gfhjkl', 'fghjkl', 'gfhjkl', 1, 'tytui', 1, 'retyui', '2019-08-11 22:56:23', '2019-08-11 22:56:23'),
(5, 236, 'mamsma', 'amamsma', 'amsmams', 'masmams', 'masmamsmas', 1, '2019-08-10', 1, 'sdadds', '2019-08-11 02:51:52', '2019-08-11 02:51:52'),
(6, 236, 'mamsma', 'amamsma', 'amsmams', 'masmams', 'masmamsmas', 1, '2019-08-10', 2, 'dfsaasd', '2019-08-11 03:39:42', '2019-08-11 03:39:42');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rayosx_toe`
--

CREATE TABLE `rayosx_toe` (
  `id_toe_rayosx` int(11) NOT NULL,
  `id_tramite_rayosx` int(11) NOT NULL,
  `toe_pnombre` varchar(150) NOT NULL,
  `toe_snombre` varchar(150) NOT NULL,
  `toe_papellido` varchar(150) NOT NULL,
  `toe_sapellido` varchar(150) NOT NULL,
  `toe_tdocumento` int(11) NOT NULL,
  `toe_ndocumento` varchar(20) NOT NULL,
  `toe_lexpedicion` varchar(150) NOT NULL,
  `toe_correo` varchar(150) NOT NULL,
  `toe_profesion` int(11) NOT NULL,
  `toe_nivel` int(11) NOT NULL,
  `toe_ult_entrenamiento` varchar(150) DEFAULT NULL,
  `toe_pro_entrenamiento` varchar(150) DEFAULT NULL,
  `toe_registro` varchar(150) DEFAULT NULL,
  `toe_tipo` int(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Tabla TOE rayos x';

--
-- Volcado de datos para la tabla `rayosx_toe`
--

INSERT INTO `rayosx_toe` (`id_toe_rayosx`, `id_tramite_rayosx`, `toe_pnombre`, `toe_snombre`, `toe_papellido`, `toe_sapellido`, `toe_tdocumento`, `toe_ndocumento`, `toe_lexpedicion`, `toe_correo`, `toe_profesion`, `toe_nivel`, `toe_ult_entrenamiento`, `toe_pro_entrenamiento`, `toe_registro`, `toe_tipo`, `created_at`, `updated_at`) VALUES
(1, 232, 'MARIO', 'EDWIN', 'BELTRAN', 'SARMIENTO', 3, '80856839', 'rtyui', 'rtyuio@hgjkl.com', 3223, 1, NULL, NULL, '56789', 1, '2019-08-11 04:00:19', '2019-08-11 04:00:19'),
(2, 232, 'rtyui', 'ytuio', 'yuio', 'tyuio', 1, 'tuyi', 'FGHJK', 'yuiop@hjkl.com', 3223, 1, NULL, NULL, 'tuyi', 2, '2019-08-09 03:20:25', '2019-08-09 03:20:25'),
(3, 232, 'rtytui', 'ytuio', 'tyuio', 'uyio', 1, '567689', 'rtyu', 'rtyuio@ghjkl.com', 3223, 1, NULL, NULL, '567689', 2, '2019-08-09 03:20:31', '2019-08-09 03:20:31'),
(15, 238, 'MARIO', 'EDWIN', 'BELTRAN', 'SARMIENTO', 1, '123456789', 'rtyui', 'rtyuio@hgjkl.com', 17806, 1, NULL, NULL, NULL, 1, '2019-08-10 20:06:26', '2019-08-10 20:06:26'),
(16, 237, 'MARIO', 'EDWIN', 'BELTRAN', 'SARMIENTO', 1, '12345', 'rtyui', 'rtyuio@hgjkl.com', 5251, 1, NULL, NULL, NULL, 1, '2019-08-10 20:21:13', '2019-08-10 20:21:13'),
(17, 236, 'MARIO', 'EDWIN', 'BELTRAN', 'SARMIENTO', 1, '986969', 'rtyui', 'rtyuio@hgjkl.com', 17806, 1, NULL, NULL, NULL, 1, '2019-08-11 03:38:42', '2019-08-11 03:38:42'),
(18, 235, 'MARIO', 'EDWIN', 'BELTRAN', 'SARMIENTO', 1, '2113132', 'rtyui', 'rtyuio@hgjkl.com', 14306, 1, '2019-08-10', '2019-08-10', '232132', 2, '2019-08-11 04:59:46', '2019-08-11 04:59:46'),
(19, 239, 'MARIO', 'EDWIN', 'BELTRAN', 'SARMIENTO', 1, '80856839', 'BOGOTA', 'meb@meb.com', 17806, 1, NULL, NULL, NULL, 1, '2019-08-12 18:42:35', '2019-08-12 18:42:35'),
(20, 239, 'MARIO', 'EDWIN', 'BELTRAN', 'SARMIENTO', 1, '121213121', 'BOGOTA', 'rtyuio@hgjkl.com', 52188, 1, '2019-08-09', '2019-08-15', '12121', 2, '2019-08-12 19:40:25', '2019-08-12 19:40:25'),
(21, 241, 'MARIO', 'EDWIN', 'BELTRAN', 'SARMIENTO', 1, '123456789', 'BOGOTA', 'rtyuio@hgjkl.com', 17806, 1, NULL, NULL, NULL, 1, '2019-08-13 03:43:30', '2019-08-13 03:43:30'),
(22, 241, 'MARIO', 'EDWIN', 'BELTRAN', 'SARMIENTO', 1, '12345678', 'BOGOTA', 'meb@meb.com', 17806, 1, '2019-08-23', '2019-08-23', '12345', 2, '2019-08-13 03:44:17', '2019-08-13 03:44:17');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `rayosx_direccion`
--
ALTER TABLE `rayosx_direccion`
  ADD PRIMARY KEY (`id_direccion_rayosx`);

--
-- Indices de la tabla `rayosx_director`
--
ALTER TABLE `rayosx_director`
  ADD PRIMARY KEY (`id_director_rayosx`);

--
-- Indices de la tabla `rayosx_documentos`
--
ALTER TABLE `rayosx_documentos`
  ADD PRIMARY KEY (`id_documentos_rayosx`);

--
-- Indices de la tabla `rayosx_encargado`
--
ALTER TABLE `rayosx_encargado`
  ADD PRIMARY KEY (`id_encargado_rayosx`);

--
-- Indices de la tabla `rayosx_equipos`
--
ALTER TABLE `rayosx_equipos`
  ADD PRIMARY KEY (`id_equipo_rayosx`);

--
-- Indices de la tabla `rayosx_objprueba`
--
ALTER TABLE `rayosx_objprueba`
  ADD PRIMARY KEY (`id_obj_rayosx`);

--
-- Indices de la tabla `rayosx_toe`
--
ALTER TABLE `rayosx_toe`
  ADD PRIMARY KEY (`id_toe_rayosx`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `rayosx_direccion`
--
ALTER TABLE `rayosx_direccion`
  MODIFY `id_direccion_rayosx` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `rayosx_director`
--
ALTER TABLE `rayosx_director`
  MODIFY `id_director_rayosx` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `rayosx_documentos`
--
ALTER TABLE `rayosx_documentos`
  MODIFY `id_documentos_rayosx` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;

--
-- AUTO_INCREMENT de la tabla `rayosx_encargado`
--
ALTER TABLE `rayosx_encargado`
  MODIFY `id_encargado_rayosx` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `rayosx_equipos`
--
ALTER TABLE `rayosx_equipos`
  MODIFY `id_equipo_rayosx` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT de la tabla `rayosx_objprueba`
--
ALTER TABLE `rayosx_objprueba`
  MODIFY `id_obj_rayosx` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `rayosx_toe`
--
ALTER TABLE `rayosx_toe`
  MODIFY `id_toe_rayosx` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
