<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearDepartamentoMunicipio extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared( file_get_contents( "database/parametricas.sql" ) );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pr_barrio');
        Schema::dropIfExists('pr_localidad');
        Schema::dropIfExists('pr_municipio');
        Schema::dropIfExists('pr_departamento');        
        Schema::dropIfExists('pr_equiposrx');
        Schema::dropIfExists('pr_nivelacademico');
        Schema::dropIfExists('pr_programas_univ');
        Schema::dropIfExists('pr_tipoidentificacion');
    }
}
