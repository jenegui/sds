<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Persona extends Model
{
    protected $table = 'Persona';

    protected $primaryKey = 'id_persona';

    protected $fillable = [
        'id_persona',        
        'tipo_identificacion',
        'nume_identificacion',
        'p_nombre',
        's_nombre',
        'p_apellido',
        's_apellido',
        'email',
        'telefono_fijo',
        'telefono_celular',
        'nacionalidad',
        'departamento',
        'ciudad_nacimiento',
        'ciudad_nacimiento_otro',
        'depa_resi',
        'ciudad_resi',
        'dire_resi',
        'zona',
        'localidad',
        'upz',
        'barrio',
        'fecha_nacimiento',
        'edad',
        'sexo', 
        'genero',
        'orientacion',
        'etnia',
        'estado_civil',
        'nivel_educativo'

    ];

}
