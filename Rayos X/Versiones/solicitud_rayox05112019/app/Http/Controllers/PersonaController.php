<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

use Auth;
use App\Persona;
use App\User;
use Session;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Mail\SendMail;
use Illuminate\Support\Facades\Mail;

class PersonaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function cargarPersonaNatural()
    {
        $roles = Role::all();
        $pais = DB::table('pr_pais')->pluck("IdPais","Nombre");
        $departamentos = DB::table('pr_departamento')->pluck("IdDepartamento","Descripcion");
        $tipo_identificacion = DB::table('pr_tipoidentificacion')->pluck("IdTipoIdentificacion","Descripcion");



         return view('auth.registerPersonaNatural',['paises'=>$pais,
                                                    'departamentos'=>$departamentos,
                                                    'tipo_identificacion'=>$tipo_identificacion,
                                                    'roles'=>$roles]);      

        
    }

    public function cargarPersonaJuridica()
    {
        $roles = Role::all();
        $pais = DB::table('pr_pais')->pluck("IdPais","Nombre");
        $departamentos = DB::table('pr_departamento')->pluck("IdDepartamento","Descripcion");
        $tipo_identificacion = DB::table('pr_tipoidentificacion')->pluck("IdTipoIdentificacion","Descripcion");



         return view('auth.registerPersonaJuridica',['paises'=>$pais,
                                                    'departamentos'=>$departamentos,
                                                    'tipo_identificacion'=>$tipo_identificacion,
                                                    'roles'=>$roles]);      

        
    }


    public function registrarPersonaNatural(Request $request)
    {
        $input = Input::all();      

        

        $personaCreada =Persona::where('nume_identificacion',$input['nume_documento'])->where('tipo_identificacion',$input['tipo_identificacion'])->first();

        

        if(!isset($personaCreada)){

        

        

        $persona = new Persona();

        $persona->tipo_identificacion=$input['tipo_identificacion'];
        $persona->nume_identificacion=$input['nume_documento'];
        $persona->p_nombre=$input['p_nombre'];
        $persona->s_nombre=$input['s_nombre'];
        $persona->p_apellido=$input['p_apellido'];
        $persona->s_apellido=$input['s_apellido'];
        $persona->email=$input['email'];
        $persona->telefono_fijo=$input['telefono_fijo'];
        $persona->telefono_celular=$input['telefono_celular'];
        $persona->nacionalidad=$input['nacionalidad'];
        $persona->departamento=$input['departamento'];
        $persona->ciudad_nacimiento=$input['ciudad_nacimiento'];
        $persona->depa_resi=$input['depa_resi'];
        $persona->ciudad_resi=$input['ciudad_resi'];
        $persona->dire_resi=$input['dire_resi'];
        $persona->zona=isset($input['subred']) ? $input['subred'] : NULL;
        $persona->localidad=isset($input['localidad']) ? $input['localidad'] : NULL;
        $persona->upz=isset($input['upz']) ? $input['upz'] : NULL;
        $persona->barrio=isset($input['barrio']) ? $input['barrio'] : NULL;
        $persona->fecha_nacimiento=$input['fecha_nacimiento'];
        $persona->edad=$this->CalculaEdad($input['fecha_nacimiento']);
        $persona->sexo=$input['sexo'];
        $persona->genero=$input['genero'];
        $persona->orientacion=$input['orientacion'];
        $persona->etnia=$input['etnia'];
        $persona->estado_civil=$input['estado_civil'];
        $persona->nivel_educativo=$input['nivel_educativo'];         

        $result =$persona->save();

        $personaCreada =Persona::where('nume_identificacion',$input['nume_documento'])->where('tipo_identificacion',$input['tipo_identificacion'])->firstOrFail();

        //dd($personaCreada);            

        if($result){

            $user = new User();
            $user->email=$input['email'];
            $user->name=$input['p_nombre']." ".$input['s_nombre']." ".$input['p_apellido']." ".$input['s_apellido'];
            $user->password=Hash::needsRehash($input['password']) ? Hash::make($input['password']) : $input['password'];
            $user->persona_id=$personaCreada->id_persona;
            $user->save();


            $role_r = Role::where('id',7)->firstOrFail();            
            $user->assignRole($role_r);

             $data = array(
            'name'      =>  "Su registro ha sido exitoso",
            'message'   =>   "Bienvenido ".$input['p_nombre']." ".$input['s_nombre']." ".$input['p_apellido']." ".$input['s_apellido']
            );

            Mail::to($input['email'])->send(new SendMail($data));

        }



        }else{
            dd("usuario existente");
        }
        return redirect()->route('/');
           
    }

    public function registrarPersonaJuridica(Request $request)
    {
        $input = Input::all();      

        //dd($input);

        $personaCreada =Persona::where('nume_identificacion',$input['nume_documento'])->where('tipo_identificacion',$input['tipo_identificacion'])->first();        

        if(!isset($personaCreada)){

        $persona = new Persona();


        $persona->tipo_identificacion=$input['tipo_identificacion'];
        $persona->nume_identificacion=$input['nume_documento'];
        $persona->p_nombre=$input['p_nombre'];
        $persona->nombre_rp=$input['nombre_rp'];                      
        $persona->s_nombre=$input['s_nombre'];
        $persona->p_apellido=$input['p_apellido'];
        $persona->s_apellido=$input['s_apellido'];
        $persona->tipo_iden_rp=$input['tipo_iden_rp'];
        $persona->nume_iden_rp=$input['nume_iden_rp'];
        $persona->email=$input['email'];
        $persona->telefono_fijo=$input['telefono_fijo'];
        $persona->telefono_celular=$input['telefono_celular'];       


        
        $persona->save();

        $personaCreada =Persona::where('nume_identificacion',$input['nume_documento'])->where('tipo_identificacion',$input['tipo_identificacion'])->firstOrFail();

        //dd($personaCreada);            

        if($result){

            $user = new User();
            $user->email=$input['email'];
            $user->name=$input['p_nombre']." ".$input['s_nombre']." ".$input['p_apellido']." ".$input['s_apellido'];
            $user->password=Hash::needsRehash($input['password']) ? Hash::make($input['password']) : $input['password'];
            $user->persona_id=$personaCreada->id_persona;
            $user->save();


            $role_r = Role::where('id',7)->firstOrFail();            
            $user->assignRole($role_r);

        }

        }else{
            dd("El usuario ya existe");
        }

        return redirect()->route('/');              
    }





    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $permissions = Permission::all();

        return view('roles.create', ['permissions'=>$permissions]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'=>'required|unique:roles|max:10',
            'permissions' =>'required',
            ]
        );

        $name = $request['name'];
        $role = new Role();
        $role->name = $name;

        $permissions = $request['permissions'];

        $role->save();

        foreach ($permissions as $permission) {
            $p = Permission::where('id', '=', $permission)->firstOrFail();
            $role = Role::where('name', '=', $name)->first();
            $role->givePermissionTo($p);
        }

        return redirect()->route('roles.index')
            ->with('flash_message',
             'Role'. $role->name.' added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect('roles');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = Role::findOrFail($id);
        $permissions = Permission::all();

        return view('roles.edit', compact('role', 'permissions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $role = Role::findOrFail($id);
        $this->validate($request, [
            'name'=>'required|max:10|unique:roles,name,'.$id,
            'permissions' =>'required',
        ]);

        $input = $request->except(['permissions']);
        $permissions = $request['permissions'];
        $role->fill($input)->save();
        $p_all = Permission::all();

        foreach ($p_all as $p) {
            $role->revokePermissionTo($p);
        }

        foreach ($permissions as $permission) {
            $p = Permission::where('id', '=', $permission)->firstOrFail(); //Get corresponding form permission in db
            $role->givePermissionTo($p);  
        }

        return redirect()->route('roles.index')
            ->with('flash_message',
             'Role'. $role->name.' updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $role = Role::findOrFail($id);
        $role->delete();

        return redirect()->route('roles.index')
            ->with('flash_message',
             'Role deleted!');
    }

    function CalculaEdad( $fecha ) {
        list($Y,$m,$d) = explode("-",$fecha);
        return( date("md") < $m.$d ? date("Y")-$Y-1 : date("Y")-$Y );
    }
}
