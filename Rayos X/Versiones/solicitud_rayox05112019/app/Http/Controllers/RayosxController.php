<?php
namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use App\Tramite;
use App\Tipo_Tramite_Flujo;
use App\Tramite_Flujo;
use App\Models\Rayosx\Rayosx_direccion;
use App\Models\Rayosx\Rayosx_director;
use App\Models\Rayosx\Rayosx_documentos;
use App\Models\Rayosx\Rayosx_encargado;
use App\Models\Rayosx\Rayosx_equipos;
use App\Models\Rayosx\Rayosx_objprueba;
use App\Models\Rayosx\Rayosx_toe;
use App\Models\Parametricas\Pr_Tipoidentificacion;
use App\user_has_roles;
use DataTables;
use App\rayosx_tipo_documento;
use App\tipo_tramite_tipo_doc;
use App\tipo_tramite_tipo_doc_campos;
use App\tramite_documento_campos;
use App\Mail\SendFlujoTramite;
use App\Persona;
use App\User;

class RayosxController extends Controller
{


    private $existe_tramite=0;
    private $idTramite=0;
    private $paso=0;
    private $impedirActualizacion=false;


    /**
     * Get all form details
     * @return blade
     */


     public function index(){

       $iduser = Auth::user()->id;       

       $userRoles=user_has_roles::where("user_id",Auth::id())->get();
       //->pluck('id')
       
       $userRolesArray=collect($userRoles)->map(function($x){return (array) $x->role_id;})->toArray();
       

       $json =str_replace(']','',str_replace('[','',json_encode($userRolesArray)));

       

       $query="SELECT tramite.id
                FROM tramite,tipo_tramite,tramite_flujo
                WHERE tramite.tipo_tramite=tipo_tramite.id
                AND tramite_flujo.tramite_id=tramite.id
                AND tramite.user=".Auth::id()."
                AND tramite_flujo.fecha_fin IS NULL
                UNION ALL
                SELECT tramite.id
                FROM tramite,tipo_tramite,tramite_flujo
                WHERE tramite.tipo_tramite=tipo_tramite.id
                AND tramite_flujo.tramite_id=tramite.id
                AND tramite_flujo.user_rol in (".$json.") 
                AND tramite_flujo.fecha_fin IS NULL";

      //dd($query);
      $idTramites=DB::select(DB::raw($query));
      $idTramitesList =str_replace(']','',str_replace('[','',json_encode($idTramites)));          
      $idTramitesList =str_replace('{"id":','',str_replace('}','',$idTramitesList));      
      $idTramitesList =str_replace('"','',$idTramitesList);     
      //dd($idTramitesList);
      $idTramitesList = explode(',',$idTramitesList); 
      //dd($idTramitesList);
      //dd(array($idTramitesList));

      $tramitesrx=Tramite::distinct()->select('tramite.id','tipo_tramite.descripcion','tramite.estado','tramite_flujo.actividad','tramite_flujo.fecha_inicio','tramite_flujo.fecha_fin','tramite_flujo.observaciones','tramite.created_at','tipo_tramite_flujo.duracion_maxima_dias')
       ->join('tipo_tramite','tipo_tramite.id','=','tramite.tipo_tramite')       
       ->join('tramite_flujo','tramite_flujo.tramite_id','=','tramite.id')
       ->join('tipo_tramite_flujo','tipo_tramite_flujo.flujo_actividad_inicial','=','tramite_flujo.actividad')
       ->whereIn('tramite.id', $idTramitesList)     
       ->whereNull('tramite_flujo.fecha_fin')  
       ->orderBy('tramite_flujo.created_at','DESC')
       //->toSql();
       ->paginate(2);

       //dd($tramitesrx);

      
          
       
       return view('rayosx.mistramitesrx',['tramitesrx'=>$tramitesrx]);

     }

    public function index2(){
        try{
            //CARGAR DEPARTAMENTOS
            $departamentos = DB::table('pr_departamento')->pluck("IdDepartamento","Descripcion");
            //CARGAR TIPO IDENTIFICACION
            $tipo_identificacion = DB::table('pr_tipoidentificacion')->pluck("IdTipoIdentificacion","Descripcion");
            //CARGAR PROGRAMAS UNIVERSITARIOS
            $pr_programas_univ  = DB::table('pr_programas_univ')->pluck("id_programa","nombre_programa");
            //CARGAR NIVEL ACADEMICO
            $pr_nivelacademico  = DB::table('pr_nivelacademico')->pluck("IdNivelAcademico","Nombre");

            //CARGAR TOE TRAMITE
            $rayosxOficialToe=Rayosx_toe::where('toe_tipo',1)->where('id_tramite_rayosx',0)->orderBy('id_toe_rayosx','DESC')->first();

            $rayosxTemporalToe=Rayosx_toe::where('toe_tipo',2)->where('id_tramite_rayosx',0)->orderBy('id_toe_rayosx','DESC')->paginate(3);

            $rayosxTalento=Rayosx_director::where('id_tramite_rayosx',0)->orderBy('id_director_rayosx','DESC')->get();
            $rayosxObjprueba=Rayosx_objprueba::where('id_tramite_rayosx',0)->orderBy('id_obj_rayosx','DESC')->get();
            $rayosxDocumentos1=Rayosx_documentos::where('id_tramite_rayosx',0)->get();

            return view('rayosx.index',['departamentos'=>$departamentos,
                                        'tiposIdentificacion'=>$tipo_identificacion,
                                        'prNivelacademico'=>$pr_nivelacademico,
                                        'prProgramasUniv'=>$pr_programas_univ,
                                        'oficialTOE'=>$rayosxOficialToe,
                                        'temporalTOE'=>$rayosxTemporalToe,
                                        'talento'=>$rayosxTalento,
                                        'objeto'=>$rayosxObjprueba,
                                        'existeTramite'=>0,'paso'=>0]);
        }
        catch(Exception $e){
            dd($e);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {


      $tipo_tramite=(int)$request->tipo_tramite;
      $visita_previa=(int)$request->visita_previa;
      $idTramite=(int)$request->idTramite;


      
      //CARGAR DEPARTAMENTOS
      $departamentos = DB::table('pr_departamento')->pluck("IdDepartamento","Descripcion");
      //CARGAR TIPO IDENTIFICACION
      $tipo_identificacion = DB::table('pr_tipoidentificacion')->pluck("IdTipoIdentificacion","Descripcion");
      //CARGAR PROGRAMAS UNIVERSITARIOS
      $pr_programas_univ  = DB::table('pr_programas_univ')->pluck("id_programa","nombre_programa");
      //CARGAR NIVEL ACADEMICO
      $pr_nivelacademico  = DB::table('pr_nivelacademico')->pluck("IdNivelAcademico","Nombre");
      $rayosxOficialToe=Rayosx_toe::where('toe_tipo',1)->where('id_tramite_rayosx',$idTramite)->orderBy('id_toe_rayosx','DESC')->first();
      $rayosxTemporalToe=Rayosx_toe::where('toe_tipo',2)->where('id_tramite_rayosx',$idTramite)->orderBy('id_toe_rayosx','DESC')->paginate(3);

      $rayosxTalento=Rayosx_director::where('id_tramite_rayosx',$idTramite)->orderBy('id_director_rayosx','DESC')->get();
      $rayosxObjprueba=Rayosx_objprueba::where('id_tramite_rayosx',$idTramite)->orderBy('id_obj_rayosx','DESC')->get();


        //REGISTRA LOS DATOS INICIALES DEL TRAMITE
      if($idTramite==0){
        $tramite = new Tramite();
        $tramite->user=Auth::id();
        $tramite->tipo_tramite=1;
        $tramite->estado=1;
        $tramite->save();

        $idTramite=Tramite::where('user',Auth::id())->orderBy('id','DESC')->first();

        // Consulta del flujo del tramite seleccionado -- tipo_tramite=1 el cual corresponde al tramite de rayos x, consultamos la actividad inicial por medio del filtro de flujo_accion_permitida=inicio
        $tipoTramiteFlujo= Tipo_Tramite_Flujo::where('tipo_tramite_id',1)->where('flujo_accion_permitida', 'like','%inicio%')->first();

        //Se crea el flujo del tramite rayos x o historico
        $tramiteFlujo = new Tramite_Flujo();
        $tramiteFlujo->tramite_id=(int)$idTramite->id;
        $tramiteFlujo->actividad=$tipoTramiteFlujo->flujo_actividad_inicial;
        $tramiteFlujo->fecha_inicio=date('Y-m-d H:i:s');
        $tramiteFlujo->user_id=Auth::id();
        $tramiteFlujo->user_rol=NULL;
        $tramiteFlujo->observaciones="Creación del tramite RAYOS X";
        $tramiteFlujo->save();
      }else{
          $idTramite=Tramite::find('id',$idTramite);

      }



      /*$id = DB::table('tramite')->insertGetId(
          ['user' => Auth::id(),'tipo_tramite' => 1,'estado' => 1]
      );*/


      return view('rayosx.index', ['idTramite' => $idTramite,
                                    'existeTramite'=>1,
                                    'departamentos'=>$departamentos,
                                    'tiposIdentificacion'=>$tipo_identificacion,
                                    'prNivelacademico'=>$pr_nivelacademico,
                                    'prProgramasUniv'=>$pr_programas_univ,
                                    'oficialTOE'=>$rayosxOficialToe,
                                    'temporalTOE'=>$rayosxTemporalToe,
                                    'talento'=>$rayosxTalento,
                                    'objeto'=>$rayosxObjprueba,
                                    'paso'=>1, //Paso 1 - localizacion entidad
                                    'visitaPrevia'=>$visita_previa]);
    }

    public function crearTramiteRayosx(){
        try{
            //CARGAR DEPARTAMENTOS
            $departamentos = DB::table('pr_departamento')->pluck("IdDepartamento","Descripcion");
            //CARGAR TIPO IDENTIFICACION
            $tipo_identificacion = DB::table('pr_tipoidentificacion')->pluck("IdTipoIdentificacion","Descripcion");
            //CARGAR PROGRAMAS UNIVERSITARIOS
            $pr_programas_univ  = DB::table('pr_programas_univ')->pluck("id_programa","nombre_programa");
            //CARGAR NIVEL ACADEMICO
            $pr_nivelacademico  = DB::table('pr_nivelacademico')->pluck("IdNivelAcademico","Nombre");

            //CARGAR TOE TRAMITE
            $rayosxOficialToe=Rayosx_toe::where('toe_tipo',1)->where('id_tramite_rayosx',0)->orderBy('id_toe_rayosx','DESC')->first();
            $rayosxTemporalToe=Rayosx_toe::where('toe_tipo',2)->where('id_tramite_rayosx',0)->orderBy('id_toe_rayosx','DESC')->paginate(3);
            $rayosxTalento=Rayosx_director::where('id_tramite_rayosx',0)->orderBy('id_director_rayosx','DESC')->get();
            $rayosxObjprueba=Rayosx_objprueba::where('id_tramite_rayosx',0)->orderBy('id_obj_rayosx','DESC')->paginate(3);
            $rayosxDocumentos1=Rayosx_documentos::where('id_tramite_rayosx',0)->get();
            $idTramiterx=NULL;

            return view('rayosx.index',[ 'idTramiterx'=>$idTramiterx,'departamentos'=>$departamentos,
                                          'tiposIdentificacion'=>$tipo_identificacion,
                                          'prNivelacademico'=>$pr_nivelacademico,
                                          'prProgramasUniv'=>$pr_programas_univ]);
        }
        catch(Exception $e){
            dd($e);
        }
    }
    /**
     * Saves the institution's location and shows equipment x-ray form
     *
     * @return \Illuminate\Http\Response
     */

     public function guardarTramiteRayosx(Request $request)
     {


         $input = Input::all();
         $tramite = new Tramite();
         $tramite->user=Auth::id();
         $tramite->tipo_tramite=1;
         $tramite->estado=1;
         $tramite->save();

         $idTramite=Tramite::where('user',Auth::id())->orderBy('id','DESC')->first();
         //CARGAR DEPARTAMENTOS
         $departamentos = DB::table('pr_departamento')->pluck("IdDepartamento","Descripcion");
         //CARGAR TIPO IDENTIFICACION
         $tipo_identificacion = DB::table('pr_tipoidentificacion')->pluck("IdTipoIdentificacion","Descripcion");
         //CARGAR PROGRAMAS UNIVERSITARIOS
         $pr_programas_univ  = DB::table('pr_programas_univ')->pluck("id_programa","nombre_programa");
         //CARGAR NIVEL ACADEMICO
         $pr_nivelacademico  = DB::table('pr_nivelacademico')->pluck("IdNivelAcademico","Nombre");

         /*
         // Consulta del flujo del tramite seleccionado -- tipo_tramite=1 el cual corresponde al tramite de rayos x, consultamos la actividad inicial por medio del filtro de flujo_accion_permitida=inicio*/
         $tipoTramiteFlujo= Tipo_Tramite_Flujo::where('tipo_tramite_id',1)->where('flujo_accion_permitida', 'like','%inicio%')->first();

         //Se crea el flujo del tramite rayos x o historico
         $tramiteFlujo = new Tramite_Flujo();
         $tramiteFlujo->tramite_id=(int)$idTramite->id;
         $tramiteFlujo->tipo_tramite=1;
         $tramiteFlujo->actividad=$tipoTramiteFlujo->flujo_actividad_inicial;
         $tramiteFlujo->fecha_inicio=date('Y-m-d H:i:s');
         $tramiteFlujo->fecha_fin=null;
         $tramiteFlujo->user_id=Auth::id();
         //La estacion inicial debe corresponder al ciudadano
         $tramiteFlujo->user_rol=null;//se envia en null para que no le presente el tramite a otros usuarios que tienen el mismo rol
         $tramiteFlujo->observaciones="Creación del tramite RAYOS X";
         $tramiteFlujo->save();
         



       /*$id = DB::table('tramite')->insertGetId(
           ['user' => Auth::id(),'tipo_tramite' => 1,'estado' => 1]
       );*/


       $iduser = Auth::user()->id;       

       $userRoles=user_has_roles::where("user_id",Auth::id())->get();
       //->pluck('id')
       
       $userRolesArray=collect($userRoles)->map(function($x){return (array) $x->role_id;})->toArray();
       

       $json =str_replace(']','',str_replace('[','',json_encode($userRolesArray)));

       

       $query="SELECT tramite.id
                FROM tramite,tipo_tramite,tramite_flujo
                WHERE tramite.tipo_tramite=tipo_tramite.id
                AND tramite_flujo.tramite_id=tramite.id
                AND tramite.user=".Auth::id()."
                AND tramite_flujo.fecha_fin IS NULL
                UNION ALL
                SELECT tramite.id
                FROM tramite,tipo_tramite,tramite_flujo
                WHERE tramite.tipo_tramite=tipo_tramite.id
                AND tramite_flujo.tramite_id=tramite.id
                AND tramite_flujo.user_rol in (".$json.") 
                AND tramite_flujo.fecha_fin IS NULL";


      $idTramites=DB::select(DB::raw($query));
      $idTramitesList =str_replace(']','',str_replace('[','',json_encode($idTramites)));          
      $idTramitesList =str_replace('{"id":','',str_replace('}','',$idTramitesList));      
      $idTramitesList =str_replace('"','',$idTramitesList);     
      //dd($idTramitesList);
      $idTramitesList = explode(',',$idTramitesList); 
      //dd($idTramitesList);
      //dd(array($idTramitesList));

      $tramitesrx=Tramite::select('tramite.id','tipo_tramite.descripcion','tramite.estado','tramite_flujo.actividad','tramite_flujo.fecha_inicio','tramite_flujo.fecha_fin','tramite_flujo.observaciones','tramite.created_at')
       ->join('tipo_tramite','tipo_tramite.id','=','tramite.tipo_tramite')
       ->join('tramite_flujo','tramite_flujo.tramite_id','=','tramite.id')
       ->whereIn('tramite.id', $idTramitesList)     
       ->whereNull('tramite_flujo.fecha_fin')  
       ->orderBy('tramite_flujo.created_at','DESC')
       //->toSql();
       ->paginate(5);

       //dd($tramitesrx);
       //dd($tramitesrx);
       return view('rayosx.mistramitesrx',['tramitesrx'=>$tramitesrx]);
     }

      

     


    public function edit($id_tramite) {

        $idTramite=$id_tramite;

        $puedeEditarSolicitud=FALSE;
        $puedeAvanzarSolicitud=FALSE;


        $actividadInicial=Tramite_Flujo::where("tramite_id",$idTramite)->orderBy('id','ASC')->first();
        $actividadActual=Tramite_Flujo::where("tramite_id",$idTramite)->where("fecha_fin",NULL)->orderBy('id','DESC')->first();
        

        //dd($actividadActual);


        $rolResponsable=user_has_roles::where("user_id",Auth::id())->where('role_id',$actividadActual->user_rol)->first();

        //dd($rolResponsable);
        /*if($actividadActual->actividad == 'COMPLETAR DOCUMENTACIÓN'){
            $rolResponsable=user_has_roles::where("user_id",Auth::id())->first();        
        }*/
        
        $accionesFlujo=Tipo_Tramite_Flujo::where('tipo_tramite',1)
        //->where('flujo_actividad_inicial',$actividadActual->actividad)
        ->where('flujo_actividad_inicial', 'like', '%' . $actividadActual->actividad. '%')
        ->get();

              

        if($actividadInicial != NULL && $actividadActual != NULL){
            if($actividadInicial->id == $actividadActual->id && Auth::id() == $actividadInicial->user_id){                
                    //Se encuentra en la actividad inicial y el usuario autenticado es el encargado

                    $puedeEditarSolicitud=TRUE;                
                    $puedeAvanzarSolicitud=TRUE;
            }

            if($actividadActual->actividad == 'COMPLETAR DOCUMENTACIÓN'){
                if(Auth::id() == $actividadActual->user_id){
                    //Se encuentra en la actividad inicial y el usuario autenticado es el encargado
                    $puedeEditarSolicitud=TRUE;
                    $puedeAvanzarSolicitud=TRUE;

                }
            }

            if($rolResponsable != null){
                $puedeAvanzarSolicitud=TRUE;                
            }

        }
        
        //dd($puedeAvanzarSolicitud);
        //dd($puedeEditarSolicitud);


        $idTramiterx=Tramite::where('id',$idTramite)->where('user',Auth::id())->orderBy('id','DESC')->first();
        $rayosxOficialToe=Rayosx_toe::where('toe_tipo',1)->where('id_tramite_rayosx',$idTramite)->orderBy('id_toe_rayosx','DESC')->first();

        $rayosxTemporalToe=Rayosx_toe::where('toe_tipo',2)->where('id_tramite_rayosx',$idTramite)->orderBy('id_toe_rayosx','DESC')->paginate(3);
        $rayosxTalento=Rayosx_director::where('id_tramite_rayosx',$idTramite)->orderBy('id_director_rayosx','DESC')->first();
        $rayosxObjprueba=Rayosx_objprueba::where('id_tramite_rayosx',$idTramite)->orderBy('id_obj_rayosx','DESC')->paginate(3);
        $rayosxDocumentos=Rayosx_documentos::where('id_tramite_rayosx',$idTramite)->first();
        $rayosxDireccion = Rayosx_direccion::where('id_tramite_rayosx',$id_tramite)->first();
        //dd($rayosxDocumentos);

        

        $departamentos = DB::table('pr_departamento')->pluck("IdDepartamento","Descripcion");
        //dd($departamentos);
        $tipo_identificacion = DB::table('pr_tipoidentificacion')->pluck("IdTipoIdentificacion","Descripcion");
        $pr_nivelacademico  = DB::table('pr_nivelacademico')->pluck("IdNivelAcademico","Nombre");
        $pr_programas_univ  = DB::table('pr_programas_univ')->pluck("id_programa","nombre_programa");
        $rayosxOficialToe=Rayosx_toe::where('toe_tipo',1)->where('id_tramite_rayosx',$idTramite)->orderBy('id_toe_rayosx','DESC')->first();

        $rayosxTemporalToe=Rayosx_toe::where('toe_tipo',2)->where('id_tramite_rayosx',$idTramite)->orderBy('id_toe_rayosx','DESC')->paginate(3);

        //dd($rayosxTemporalToe);

        $rayosxTalento=Rayosx_director::where('id_tramite_rayosx',$idTramite)->orderBy('id_director_rayosx','DESC')->first();
        $rayosxObjprueba=Rayosx_objprueba::where('id_tramite_rayosx',$idTramite)->orderBy('id_obj_rayosx','DESC')->paginate(3);

        $rayosxDireccion = Rayosx_direccion::where('id_tramite_rayosx',$id_tramite)->first();
        //$rayosxEquipo = Rayosx_equipos::where('id_tramite_rayosx',$id_tramite)->first();
        $rayosxEquipo = NULL;
        $rayosxEquipos = Rayosx_equipos::where('id_tramite_rayosx',$id_tramite)->paginate(5);
        $rayosxEquipo1 = Rayosx_equipos::where('id_tramite_rayosx',$id_tramite)->first();        
        $rayosxDocumentos1=NULL;
        $rayosxDocumentos2=NULL;
        $rayosxDocumentos=NULL;
        $tipoDocumento=NULL;
        $categoria=1;  

        $resolucion_campos=tipo_tramite_tipo_doc_campos::where('estado',1)->get();


        if(isset($rayosxEquipo1)){          

          $categoria=$rayosxEquipo1->categoria;

          if($rayosxEquipo1->categoria == '1'){
            
            $rayosxDocumentos=Rayosx_documentos::where('categoria','1')->where('id_tramite_rayosx',$id_tramite)->get();

            $tipoDocumento=rayosx_tipo_documento::where('categoria','1')->get();

          }else if($rayosxEquipo1->categoria == '2'){

            $rayosxDocumentos=Rayosx_documentos::where('categoria','2')->where('id_tramite_rayosx',$id_tramite)->get();  
            $tipoDocumento=rayosx_tipo_documento::where('categoria','2')->get();
          }else{
            $tipoDocumento=rayosx_tipo_documento::where('categoria','1')->get();
          }
        }else{          
          $tipoDocumento=rayosx_tipo_documento::where('categoria','1')->get();
        }

        $idTramiterx=Tramite::where('id',$id_tramite)->first();
        
        //dd($rayosxDireccion); 
        //dd($rayosxEquipos);
        //dd($rayosxObjprueba);

        //dd($rayosxDocumentos);
        //dd($tipoDocumento);
        //dd($accionesFlujo);
        //dd($puedeEditarSolicitud);
        //dd($puedeAvanzarSolicitud);
        //dd($rayosxTemporalToe);
        //dd($idTramiterx);
        
        return view('rayosx.index2',['idTramiterx'=>$idTramiterx,'departamentos'=>$departamentos,
        'tiposIdentificacion'=>$tipo_identificacion,
        'prNivelacademico'=>$pr_nivelacademico,
        'prProgramasUniv'=>$pr_programas_univ,
        'rayosxOficialToe'=>$rayosxOficialToe,
        'rayosxTemporalToe'=>$rayosxTemporalToe,
        'rayosxTalento'=>$rayosxTalento,
        'rayosxObjprueba'=>$rayosxObjprueba,
        'rayosxDireccion'=>$rayosxDireccion,
        'rayosxEquipo'=>$rayosxEquipo,
        'rayosxEquipos'=>$rayosxEquipos,
        'rayosxDocumentos'=>$rayosxDocumentos,
        'usuarioInicial'=>$actividadInicial->user_id,
        'rolResponsable'=>$actividadActual->user_id,
        'puedeEditarSolicitud'=>$puedeEditarSolicitud,
        'puedeAvanzarSolicitud'=>$puedeAvanzarSolicitud,
        'tipoDocumento'=>$tipoDocumento,
        'rayosxDocumentos'=>$rayosxDocumentos,
        'categoria'=>$categoria,
        'resolucionCampos'=>$resolucion_campos,        
        'accionesFlujo'=>$accionesFlujo]);
      
    }

    public function editarDireccion() {


      $input = Input::all();
      //dd($input);

         if($input['id_tramite'] == NULL) {
             return redirect()->route('tramites.index');
         }
         else {
         $rayosxDireccion = Rayosx_direccion::where('id_tramite_rayosx',$input['id_tramite'])->firstorFail();

         $rayosxDireccion->depto_entidad = $input['depto_entidad'];
         $rayosxDireccion->mpio_entidad = $input['mpio_entidad'];
         $rayosxDireccion->dire_entidad = $input['dire_entidad'];
         $rayosxDireccion->sede_entidad = $input['sede_entidad'];
         $rayosxDireccion->email_entidad = $input['email_entidad'];
         $rayosxDireccion->celular_entidad = $input['celular_entidad'];
         $rayosxDireccion->telefono_entidad = $input['telefono_entidad'];
         $rayosxDireccion->extension_entidad = $input['extension_entidad'];
         $rayosxDireccion->save(); // Guarda el objeto en la BD



         $departamentos = DB::table('pr_departamento')->pluck("IdDepartamento","Descripcion");
         $tipo_identificacion = DB::table('pr_tipoidentificacion')->pluck("IdTipoIdentificacion","Descripcion");
         $pr_nivelacademico  = DB::table('pr_nivelacademico')->pluck("IdNivelAcademico","Nombre");
         $pr_programas_univ  = DB::table('pr_programas_univ')->pluck("id_programa","nombre_programa");
         $rayosxOficialToe=Rayosx_toe::where('toe_tipo',1)->where('id_tramite_rayosx',$input['id_tramite'])->orderBy('id_toe_rayosx','DESC')->first();

         $rayosxTemporalToe=Rayosx_toe::where('toe_tipo',2)->where('id_tramite_rayosx',$input['id_tramite'])->orderBy('id_toe_rayosx','DESC')->paginate(3);

         $rayosxTalento=Rayosx_director::where('id_tramite_rayosx',$input['id_tramite'])->orderBy('id_director_rayosx','DESC')->firstorFail();
         //dd($rayosxTalento);
         $rayosxObjprueba=Rayosx_objprueba::where('id_tramite_rayosx',$input['id_tramite'])->orderBy('id_obj_rayosx','DESC')->paginate(3);

         $rayosxDireccion = Rayosx_direccion::where('id_tramite_rayosx',$input['id_tramite'])->first();
         $rayosxEquipo = Rayosx_equipos::where('id_tramite_rayosx',$input['id_tramite'])->firstorFail();
         //dd($rayosxEquipo);

         return redirect('/ventanilla/tramites/solicitud_rayosx/tramite/'.$input['id_tramite']);
/*
         return view('rayosx.editartramiterx',['departamentos'=>$departamentos,
         'tiposIdentificacion'=>$tipo_identificacion,
         'prNivelacademico'=>$pr_nivelacademico,
         'prProgramasUniv'=>$pr_programas_univ,
         'rayosxOficialToe'=>$rayosxOficialToe,
         'rayosxTemporalToe'=>$rayosxTemporalToe,
         'rayosxTalento'=>$rayosxTalento,
         'rayosxObjprueba'=>$rayosxObjprueba,
         'rayosxDireccion'=>$rayosxDireccion,
         'rayosxEquipo'=>$rayosxEquipo]);
*/
       }
    }

    public function editarEquipo() {


      $input = Input::all();
      //dd($input);

         if($input['id_tramite'] == NULL) {
             return redirect()->route('tramites.index');
         }
         else {
         $rayosxEquipo = Rayosx_equipos::where('id_tramite_rayosx',$input['id_tramite'])->firstorFail();

         $rayosxEquipo->categoria = $input['categoria'];
         $rayosxEquipo->categoria1 = $input['categoria1'];
         $rayosxEquipo->categoria1_1 = $input['categoria1_1'];
         //$rayosxEquipo->categoria1_2 = $input['categoria1_2'];
         $rayosxEquipo->categoria2 = $input['categoria2'];
         $rayosxEquipo->tipo_visualizacion = $input['tipo_visualizacion'];
         $rayosxEquipo->marca_equipo = $input['marca_equipo'];
         $rayosxEquipo->modelo_equipo = $input['modelo_equipo'];
         $rayosxEquipo->serie_equipo = $input['serie_equipo'];
         $rayosxEquipo->marca_tubo_rx = $input['marca_tubo_rx'];
         $rayosxEquipo->modelo_tubo_rx = $input['modelo_tubo_rx'];
         $rayosxEquipo->serie_tubo_rx = $input['serie_tubo_rx'];
         $rayosxEquipo->tension_tubo_rx = $input['tension_tubo_rx'];
         $rayosxEquipo->contiene_tubo_rx = $input['contiene_tubo_rx'];
         $rayosxEquipo->energia_fotones = $input['energia_fotones'];
         $rayosxEquipo->energia_electrones = $input['energia_electrones'];
         $rayosxEquipo->ubicacion_equipo = $input['ubicacion_equipo'];
         $rayosxEquipo->numero_permiso = $input['numero_permiso'];
         $rayosxEquipo->anio_fabricacion = $input['anio_fabricacion'];
         $rayosxEquipo->anio_fabricacion_tubo = $input['anio_fabricacion_tubo'];
         $rayosxEquipo->estado = 1;
         $rayosxEquipo->save(); // Guarda el objeto en la BD



         $departamentos = DB::table('pr_departamento')->pluck("IdDepartamento","Descripcion");
         $tipo_identificacion = DB::table('pr_tipoidentificacion')->pluck("IdTipoIdentificacion","Descripcion");
         $pr_nivelacademico  = DB::table('pr_nivelacademico')->pluck("IdNivelAcademico","Nombre");
         $pr_programas_univ  = DB::table('pr_programas_univ')->pluck("id_programa","nombre_programa");
         $rayosxOficialToe=Rayosx_toe::where('toe_tipo',1)->where('id_tramite_rayosx',$input['id_tramite'] )->orderBy('id_toe_rayosx','DESC')->first();

         $rayosxTemporalToe=Rayosx_toe::where('toe_tipo',2)->where('id_tramite_rayosx',$input['id_tramite'] )->orderBy('id_toe_rayosx','DESC')->paginate(3);

         $rayosxTalento=Rayosx_director::where('id_tramite_rayosx',$input['id_tramite'] )->orderBy('id_director_rayosx','DESC')->first();
         $rayosxObjprueba=Rayosx_objprueba::where('id_tramite_rayosx',$input['id_tramite'] )->orderBy('id_obj_rayosx','DESC')->paginate(3);

         $rayosxDireccion = Rayosx_direccion::where('id_tramite_rayosx',$input['id_tramite'] )->firstorFail();
         $rayosxEquipo = Rayosx_equipos::where('id_tramite_rayosx',$input['id_tramite'] )->firstorFail();
         //dd($rayosxEquipo);


       return redirect('/ventanilla/tramites/solicitud_rayosx/tramite/'.$input['id_tramite']);
       }
    }

    public function editarOficialToe() {


      $input = Input::all();
      //dd($input);

         if($input['id_tramite'] == NULL) {
             return redirect()->route('tramites.index');
         }
         else {
         $rayosxOficialToe = Rayosx_toe::where('toe_tipo',1)->where('id_tramite_rayosx',$input['id_tramite'] )->firstorFail();

         $rayosxOficialToe->toe_pnombre = $input['encargado_pnombre'];
         $rayosxOficialToe->toe_snombre = $input['encargado_snombre'];
         $rayosxOficialToe->toe_papellido = $input['encargado_papellido'];
         $rayosxOficialToe->toe_sapellido= $input['encargado_sapellido'];
         $rayosxOficialToe->toe_tdocumento = $input['encargado_tdocumento'];
         $rayosxOficialToe->toe_ndocumento = $input['encargado_ndocumento'];
         $rayosxOficialToe->toe_lexpedicion = $input['encargado_lexpedicion'];
         $rayosxOficialToe->toe_correo = $input['encargado_correo'];
         $rayosxOficialToe->toe_profesion = $input['encargado_nivel'];
         $rayosxOficialToe->toe_nivel = $input['encargado_profesion'];
         $rayosxOficialToe->save(); // Guarda el objeto en la BD



         $departamentos = DB::table('pr_departamento')->pluck("IdDepartamento","Descripcion");
         $tipo_identificacion = DB::table('pr_tipoidentificacion')->pluck("IdTipoIdentificacion","Descripcion");
         $pr_nivelacademico  = DB::table('pr_nivelacademico')->pluck("IdNivelAcademico","Nombre");
         $pr_programas_univ  = DB::table('pr_programas_univ')->pluck("id_programa","nombre_programa");
         $rayosxOficialToe=Rayosx_toe::where('toe_tipo',1)->where('id_tramite_rayosx',$input['id_tramite'] )->orderBy('id_toe_rayosx','DESC')->first();

         $rayosxTemporalToe=Rayosx_toe::where('toe_tipo',2)->where('id_tramite_rayosx',$input['id_tramite'] )->orderBy('id_toe_rayosx','DESC')->paginate(3);

         $rayosxTalento=Rayosx_director::where('id_tramite_rayosx',$input['id_tramite'] )->orderBy('id_director_rayosx','DESC')->firstorFail();
         $rayosxObjprueba=Rayosx_objprueba::where('id_tramite_rayosx',$input['id_tramite'] )->orderBy('id_obj_rayosx','DESC')->paginate(3);

         $rayosxDireccion = Rayosx_direccion::where('id_tramite_rayosx',$input['id_tramite'] )->firstorFail();
         $rayosxEquipo = Rayosx_equipos::where('id_tramite_rayosx',$input['id_tramite'] )->firstorFail();
         //dd($rayosxEquipo);


       return redirect('/ventanilla/tramites/solicitud_rayosx/tramite/'.$input['id_tramite']);
       }
    }

    public function editarDirector() {


      $input = Input::all();
      //dd($input);
      $id_tramite = $input['id_tramite'];
         if($input['id_tramite'] == NULL) {
             return redirect()->route('tramites.index');
         }
         else {
         $rayosxTalento = Rayosx_director::where('id_tramite_rayosx',$input['id_tramite'] )->firstorFail();

         $rayosxTalento->talento_pnombre = $input['talento_pnombre'];
         $rayosxTalento->talento_snombre = $input['talento_snombre'];
         $rayosxTalento->talento_papellido = $input['talento_papellido'];
         $rayosxTalento->talento_sapellido= $input['talento_sapellido'];
         $rayosxTalento->talento_tdocumento = $input['talento_tdocumento'];
         $rayosxTalento->talento_ndocumento = $input['talento_ndocumento'];
         $rayosxTalento->talento_lexpedicion = $input['talento_lexpedicion'];
         $rayosxTalento->talento_correo = $input['talento_correo'];
         $rayosxTalento->talento_titulo = $input['talento_titulo'];
         $rayosxTalento->talento_universidad = $input['talento_universidad'];
         $rayosxTalento->talento_libro = $input['talento_libro'];
         $rayosxTalento->talento_registro = $input['talento_registro'];
         $rayosxTalento->talento_fecha_diploma = $input['talento_fecha_diploma'];
         $rayosxTalento->talento_resolucion = $input['talento_resolucion'];
         $rayosxTalento->talento_fecha_convalida = $input['talento_fecha_convalida'];
         $rayosxTalento->talento_nivel = $input['talento_nivel'];
         $rayosxTalento->talento_titulo_pos = $input['talento_titulo_pos'];
         $rayosxTalento->talento_universidad_pos = $input['talento_universidad_pos'];
         $rayosxTalento->talento_libro_pos = $input['talento_libro_pos'];
         $rayosxTalento->talento_registro_pos = $input['talento_registro_pos'];
         $rayosxTalento->talento_fecha_diploma_pos = $input['talento_fecha_diploma_pos'];
         $rayosxTalento->talento_resolucion_pos = $input['talento_resolucion_pos'];
         $rayosxTalento->talento_fecha_convalida_pos = $input['talento_fecha_convalida_pos'];
         $rayosxTalento->save(); // Guarda el objeto en la BD



         $departamentos = DB::table('pr_departamento')->pluck("IdDepartamento","Descripcion");
         $tipo_identificacion = DB::table('pr_tipoidentificacion')->pluck("IdTipoIdentificacion","Descripcion");
         $pr_nivelacademico  = DB::table('pr_nivelacademico')->pluck("IdNivelAcademico","Nombre");
         $pr_programas_univ  = DB::table('pr_programas_univ')->pluck("id_programa","nombre_programa");
         $rayosxOficialToe=Rayosx_toe::where('toe_tipo',1)->where('id_tramite_rayosx',$input['id_tramite'] )->orderBy('id_toe_rayosx','DESC')->first();

         $rayosxTemporalToe=Rayosx_toe::where('toe_tipo',2)->where('id_tramite_rayosx',$input['id_tramite'] )->orderBy('id_toe_rayosx','DESC')->paginate(3);

         $rayosxTalento=Rayosx_director::where('id_tramite_rayosx',$input['id_tramite'] )->orderBy('id_director_rayosx','DESC')->firstorFail();
         $rayosxObjprueba=Rayosx_objprueba::where('id_tramite_rayosx',$input['id_tramite'] )->orderBy('id_obj_rayosx','DESC')->paginate(3);

         $rayosxDireccion = Rayosx_direccion::where('id_tramite_rayosx',$input['id_tramite'] )->firstorFail();
         $rayosxEquipo = Rayosx_equipos::where('id_tramite_rayosx',$input['id_tramite'] )->firstorFail();
         //dd($rayosxEquipo);


       return redirect('/ventanilla/tramites/solicitud_rayosx/tramite/'.$input['id_tramite']);
       }
    }

    public function guardarDocumentos(Request $request)
    {
      $input = Input::all();

      $rayosxDocumentos=Rayosx_documentos::where('id_tramite_rayosx',$input['id_tramite'])->get();
      $tipoDocumento=rayosx_tipo_documento::where('categoria',$input['categoria'])->get();

      //En caso de cambiar la categoria se eliminan los datos de la categoria anterior
      if($input['categoria'] == '1'){
        $rayosxDocumentos=Rayosx_documentos::where('id_tramite_rayosx',$input['id_tramite'])->where('categoria','2')->get();
        foreach ($rayosxDocumentos as $documento) {
          Storage::delete($documento->path);
        }
        Rayosx_documentos::where('id_tramite_rayosx',$input['id_tramite'])->where('categoria','2')->delete();
      }else{
        $rayosxDocumentos=Rayosx_documentos::where('id_tramite_rayosx',$input['id_tramite'])->where('categoria','1')->get();
        foreach ($rayosxDocumentos as $documento) {
          Storage::delete($documento->path);
        }
        Rayosx_documentos::where('id_tramite_rayosx',$input['id_tramite'])->where('categoria','1')->delete();
      }


      foreach($tipoDocumento as $tipoDoc){        

        if($input[''.$tipoDoc->documento] != NULL){
          
          $rayosxdocumentos=Rayosx_documentos::where('id_tramite_rayosx',$input['id_tramite'])->where('documento',$tipoDoc->documento)->first();
          
          if(!isset($rayosxdocumentos)){
            $rayosxdocumentos = new Rayosx_documentos();            
          }
          //dd($rayosxdocumentos);
          $path = $input[''.$tipoDoc->documento]->store('public');
          //dd($path);
          $rayosxdocumentos->id_tramite_rayosx = $input['id_tramite'];
          $rayosxdocumentos->path = $path;
          $rayosxdocumentos->categoria = $input['categoria'];
          $rayosxdocumentos->documento = $tipoDoc->documento;
          $rayosxdocumentos->id_archivo = 1;
          $rayosxdocumentos->estado = 1;
          $rayosxdocumentos->save();
          }
        }

        return redirect('/ventanilla/tramites/solicitud_rayosx/tramite/'.$input['id_tramite']);
      }



    public function guardarLocalizacion(Request $request)
    {
      $input = Input::all();      
      $tramite = $request->input('id_tramite');    

      //CARGAR DEPARTAMENTOS
      $departamentos = DB::table('pr_departamento')->pluck("IdDepartamento","Descripcion");

      //CARGAR TIPO IDENTIFICACION
      $tipo_identificacion = DB::table('pr_tipoidentificacion')->pluck("IdTipoIdentificacion","Descripcion");

      //CARGAR PROGRAMAS UNIVERSITARIOS
      $pr_programas_univ  = DB::table('pr_programas_univ')->pluck("id_programa","nombre_programa");

      //CARGAR NIVEL ACADEMICO
      $pr_nivelacademico  = DB::table('pr_nivelacademico')->pluck("IdNivelAcademico","Nombre");

      $rayosxOficialToe=Rayosx_toe::where('toe_tipo',1)->where('id_tramite_rayosx',$input['id_tramite'])->orderBy('id_toe_rayosx','DESC')->first();
      $rayosxTemporalToe=Rayosx_toe::where('toe_tipo',2)->where('id_tramite_rayosx',$input['id_tramite'])->orderBy('id_toe_rayosx','DESC')->paginate(3);
      $rayosxTalento=Rayosx_director::where('id_tramite_rayosx',$input['id_tramite'])->orderBy('id_director_rayosx','DESC')->first();
      $rayosxObjprueba=Rayosx_objprueba::where('id_tramite_rayosx',$input['id_tramite'])->orderBy('id_obj_rayosx','DESC')->paginate(3);

      $rayosxEquipo = Rayosx_equipos::where('id_tramite_rayosx',$input['id_tramite'] )->first();

      $idTramiterx = $idTramiterx2=Tramite::select('id')->where('id',$input['id_tramite'])->where('user',Auth::id())->orderBy('id','DESC')->firstorFail();;
      $rayosxDireccion = Rayosx_direccion::where('id_tramite_rayosx',$input['id_tramite'])->first();



        if($rayosxDireccion == NULL){                
            $rayosxDireccion = new Rayosx_direccion();
        }

        $rayosxDireccion->id_tramite_rayosx=$input['id_tramite'];
        $rayosxDireccion->dire_entidad=$input['dire_entidad'];
        $rayosxDireccion->sede_entidad=$input['sede_entidad'];
        $rayosxDireccion->email_entidad=$input['email_entidad'];
        $rayosxDireccion->depto_entidad=$input['depto_entidad'];
        $rayosxDireccion->mpio_entidad=$input['mpio_entidad'];
        $rayosxDireccion->celular_entidad=$input['celular_entidad'];
        //$rayosxDireccion->indicativo_entidad=$input['indicativo_entidad'];
        $rayosxDireccion->telefono_entidad=$input['telefono_entidad'];
        $rayosxDireccion->extension_entidad=$input['extension_entidad'];

        $rayosxDireccion->save();

        $rayosxDireccion = Rayosx_direccion::where('id_tramite_rayosx',$input['id_tramite'])->first();        

       return redirect('/ventanilla/tramites/solicitud_rayosx/tramite/'.$input['id_tramite']);
      

    }

    /**
     * Saves the institution's location and shows equipment x-ray form
     *
     * @return \Illuminate\Http\Response
     */
    public function guardarEquiposRayosX(Request $request)
    {
      $input = Input::all();

      
      
      //CARGAR DEPARTAMENTOS
      $departamentos = DB::table('pr_departamento')->pluck("IdDepartamento","Descripcion");
      //CARGAR TIPO IDENTIFICACION
      $tipo_identificacion = DB::table('pr_tipoidentificacion')->pluck("IdTipoIdentificacion","Descripcion");
      //CARGAR PROGRAMAS UNIVERSITARIOS
      $pr_programas_univ  = DB::table('pr_programas_univ')->pluck("id_programa","nombre_programa");
      //CARGAR NIVEL ACADEMICO
      $pr_nivelacademico  = DB::table('pr_nivelacademico')->pluck("IdNivelAcademico","Nombre");


      $rayosxOficialToe=Rayosx_toe::where('toe_tipo',1)->where('id_tramite_rayosx',$input['id_tramite'])->orderBy('id_toe_rayosx','DESC')->first();
      $rayosxTemporalToe=Rayosx_toe::where('toe_tipo',2)->where('id_tramite_rayosx',$input['id_tramite'])->orderBy('id_toe_rayosx','DESC')->paginate(3);
      $rayosxTalento=Rayosx_director::where('id_tramite_rayosx',$input['id_tramite'])->orderBy('id_director_rayosx','DESC')->first();
      $rayosxObjprueba=Rayosx_objprueba::where('id_tramite_rayosx',$input['id_tramite'])->orderBy('id_obj_rayosx','DESC')->paginate(3);

      $idTramiterx = $idTramiterx2=Tramite::select('id')->where('id',$input['id_tramite'])->where('user',Auth::id())->orderBy('id','DESC')->firstorFail();;
      $rayosxDireccion = Rayosx_direccion::where('id_tramite_rayosx',$input['id_tramite'])->first();
      
      $rayosxEquipos = Rayosx_equipos::where('id_tramite_rayosx',$input['id_tramite'])->get();
      $equiposCount = $rayosxEquipos->count();

      if($equiposCount<=20){

        $rayosxEquipo = new Rayosx_equipos();      
        $rayosxEquipo->id_tramite_rayosx=$input['id_tramite'];
        $rayosxEquipo->categoria=$input['categoria'];
        $rayosxEquipo->categoria1=$input['categoria1'];
        $rayosxEquipo->categoria2=$input['categoria2'];
        $rayosxEquipo->categoria1_1 =$input['categoria1_1'];
        //$rayosxEquipo->categoria1_2 =$input['categoria1_2'];
        //$rayosxEquipo->equipo_generador =$input['equipo_generador'];
        $rayosxEquipo->tipo_visualizacion =$input['tipo_visualizacion'];
        $rayosxEquipo->marca_equipo =$input['marca_equipo'];
        $rayosxEquipo->modelo_equipo =$input['modelo_equipo'];
        $rayosxEquipo->serie_equipo =$input['serie_equipo'];
        $rayosxEquipo->marca_tubo_rx =$input['marca_tubo_rx'];
        $rayosxEquipo->modelo_tubo_rx =$input['modelo_tubo_rx'];
        $rayosxEquipo->serie_tubo_rx =$input['serie_tubo_rx'];
        $rayosxEquipo->tension_tubo_rx =$input['tension_tubo_rx'];
        $rayosxEquipo->contiene_tubo_rx =$input['contiene_tubo_rx'];
        $rayosxEquipo->energia_fotones =$input['energia_fotones'];
        $rayosxEquipo->energia_electrones =$input['energia_electrones'];
        $rayosxEquipo->carga_trabajo =$input['carga_trabajo'];
        $rayosxEquipo->ubicacion_equipo =$input['ubicacion_equipo'];
        $rayosxEquipo->numero_permiso =$input['numero_permiso'];
        $rayosxEquipo->anio_fabricacion =$input['anio_fabricacion'];
        $rayosxEquipo->anio_fabricacion_tubo =$input['anio_fabricacion_tubo'];
        $rayosxEquipo->estado=1;
        $rayosxEquipo->save();
      }

        

        return redirect('/ventanilla/tramites/solicitud_rayosx/tramite/'.$input['id_tramite']);    

    }

    public function getDptoXPais($id) {
        $departamento = DB::table("pr_departamento")->where("IdPais",$id)->pluck("IdDepartamento","Descripcion");
        return json_encode($departamento);
    }


    public function getMpoXDpto($id) {
        $municipios = DB::table("pr_municipio")->where("IdDepartamento",$id)->pluck("idMunicipio","Descripcion");
        return json_encode($municipios);
    }

    public function getSubred() {
        $subred = DB::table("pr_subred")->pluck("idSubRed","Nombre");
        return json_encode($subred);
    }

    public function getLocalidadXSubred($id) {
        $localidad = DB::table("pr_localidad")->where("id_subred",$id)->pluck("idLocalidad","Nombre");
        return json_encode($localidad);
    }

    public function getUpzXLocalidad($id) {
        $localidad = DB::table("pr_upz")->where("id_localidad",$id)->pluck("id_upz","nom_upz");
        return json_encode($localidad);
    }


    public function getBarrioXUpz($id) {
        $barrio = DB::table("pr_barrio")->where("id_upz",$id)->pluck("id_barrio","nombre_barrio");
        return json_encode($barrio);
    }



    public function getEquipoXCategoria($id) {
        $equipos = DB::table("pr_equiposrx")->where("categoria",$id)->pluck("id_equipo","nombre_equipo");
        return json_encode($equipos);

    }

    public function getCamposDocumentos($id) {
        $documento = tipo_tramite_tipo_doc_campos::where("tipo_tramite_tipo_doc_id",$id)->orderBy('orden','ASC')->get();

        return json_encode($documento);

    }


    /**
     * Saves the institution's location and shows equipment x-ray form
     *
     * @return \Illuminate\Http\Response
     */
    public function guardarOficialTOE(Request $request)
    {

        $input = Input::all();
        //dd($input);
        //CARGAR DEPARTAMENTOS
        $departamentos = DB::table('pr_departamento')->pluck("IdDepartamento","Descripcion");
        //CARGAR TIPO IDENTIFICACION
        $tipo_identificacion = DB::table('pr_tipoidentificacion')->pluck("IdTipoIdentificacion","Descripcion");
        //CARGAR PROGRAMAS UNIVERSITARIOS
        $pr_programas_univ  = DB::table('pr_programas_univ')->pluck("id_programa","nombre_programa");
        //CARGAR NIVEL ACADEMICO
        $pr_nivelacademico  = DB::table('pr_nivelacademico')->pluck("IdNivelAcademico","Nombre");


        $rayosxOficialToe=Rayosx_toe::where('toe_tipo',1)->where('id_tramite_rayosx',$input['id_tramite'])->orderBy('id_toe_rayosx','DESC')->first();
        $rayosxTemporalToe=Rayosx_toe::where('toe_tipo',2)->where('id_tramite_rayosx',$input['id_tramite'])->orderBy('id_toe_rayosx','DESC')->paginate(3);
        $rayosxTalento=Rayosx_director::where('id_tramite_rayosx',$input['id_tramite'])->orderBy('id_director_rayosx','DESC')->first();
        $rayosxObjprueba=Rayosx_objprueba::where('id_tramite_rayosx',$input['id_tramite'])->orderBy('id_obj_rayosx','DESC')->paginate();

        $idTramiterx = $idTramiterx2=Tramite::select('id')->where('id',$input['id_tramite'])->where('user',Auth::id())->orderBy('id','DESC')->firstorFail();;
        $rayosxDireccion = Rayosx_direccion::where('id_tramite_rayosx',$input['id_tramite'])->first();
        $rayosxEquipo = Rayosx_equipos::where('id_tramite_rayosx',$input['id_tramite'])->first();

        if($rayosxOficialToe == NULL){
            $rayosxOficialToe = new Rayosx_toe();
        }

        $rayosxOficialToe->id_tramite_rayosx=$input['id_tramite'];
        $rayosxOficialToe->toe_pnombre=$input['encargado_pnombre'];
        $rayosxOficialToe->toe_snombre=$input['encargado_snombre'];
        $rayosxOficialToe->toe_papellido=$input['encargado_papellido'];
        $rayosxOficialToe->toe_sapellido=$input['encargado_sapellido'];
        $rayosxOficialToe->toe_tdocumento=$input['encargado_tdocumento'];
        $rayosxOficialToe->toe_ndocumento=$input['encargado_ndocumento'];
        $rayosxOficialToe->toe_lexpedicion=$input['encargado_lexpedicion'];
        $rayosxOficialToe->toe_correo=$input['encargado_correo'];
        $rayosxOficialToe->toe_profesion=$input['encargado_profesion'];
        $rayosxOficialToe->toe_nivel=$input['encargado_nivel'];
        //$rayosxToe->toe_ult_entrenamiento=$request->encargado_ndocumento;
        //$rayosxToe->toe_pro_entrenamiento=$request->encargado_ndocumento;
        $rayosxOficialToe->toe_tipo=1;
        $rayosxOficialToe->save();


        $rayosxOficialToe=Rayosx_toe::where('toe_tipo',1)->where('id_tramite_rayosx',$input['id_tramite'])->orderBy('id_toe_rayosx','DESC')->first();
        //dd($rayosxEquipo);

        return redirect('/ventanilla/tramites/solicitud_rayosx/tramite/'.$input['id_tramite']);

    }

    public function guardarTemporalTOE(Request $request)
    {

      $input = Input::all();

      //dd($input);
      //CARGAR DEPARTAMENTOS
      $departamentos = DB::table('pr_departamento')->pluck("IdDepartamento","Descripcion");
      //CARGAR TIPO IDENTIFICACION
      $tipo_identificacion = DB::table('pr_tipoidentificacion')->pluck("IdTipoIdentificacion","Descripcion");
      //CARGAR PROGRAMAS UNIVERSITARIOS
      $pr_programas_univ  = DB::table('pr_programas_univ')->pluck("id_programa","nombre_programa");
      //CARGAR NIVEL ACADEMICO
      $pr_nivelacademico  = DB::table('pr_nivelacademico')->pluck("IdNivelAcademico","Nombre");


      $rayosxOficialToe=Rayosx_toe::where('toe_tipo',1)->where('id_tramite_rayosx',$input['id_tramite'])->orderBy('id_toe_rayosx','DESC')->first();
      $rayosxTemporalToe=Rayosx_toe::where('toe_tipo',2)->where('id_tramite_rayosx',$input['id_tramite'])->orderBy('id_toe_rayosx','DESC')->get();
      $rayosxTalento=Rayosx_director::where('id_tramite_rayosx',$input['id_tramite'])->orderBy('id_director_rayosx','DESC')->first();
      $rayosxObjprueba=Rayosx_objprueba::where('id_tramite_rayosx',$input['id_tramite'])->orderBy('id_obj_rayosx','DESC')->paginate();

      $idTramiterx = $idTramiterx2=Tramite::select('id')->where('id',$input['id_tramite'])->where('user',Auth::id())->orderBy('id','DESC')->firstorFail();;
      $rayosxDireccion = Rayosx_direccion::where('id_tramite_rayosx',$input['id_tramite'])->first();
      $rayosxEquipo = Rayosx_equipos::where('id_tramite_rayosx',$input['id_tramite'])->first();

             
        $rayosxTemporalToe = new Rayosx_toe();
        

        $rayosxTemporalToe->id_tramite_rayosx=$input['id_tramite'];        
        $rayosxTemporalToe->toe_pnombre=$input['encargado_pnombre'];
        $rayosxTemporalToe->toe_snombre=$input['encargado_snombre'];
        $rayosxTemporalToe->toe_papellido=$input['encargado_papellido'];
        $rayosxTemporalToe->toe_sapellido=$input['encargado_sapellido'];
        $rayosxTemporalToe->toe_tdocumento=$input['encargado_tdocumento'];
        $rayosxTemporalToe->toe_ndocumento=$input['encargado_ndocumento'];
        $rayosxTemporalToe->toe_lexpedicion=$input['encargado_lexpedicion'];
        $rayosxTemporalToe->toe_correo=$input['encargado_correo'];
        $rayosxTemporalToe->toe_profesion=$input['encargado_profesion'];
        $rayosxTemporalToe->toe_nivel=$input['encargado_nivel'];
        $rayosxTemporalToe->toe_ult_entrenamiento=$input['toe_ult_entrenamiento'];
        $rayosxTemporalToe->toe_pro_entrenamiento=$input['toe_pro_entrenamiento'];
        $rayosxTemporalToe->toe_registro=$input['toe_registro'];
        $rayosxTemporalToe->toe_tipo=2;
        $rayosxTemporalToe->save();

        


        $rayosxTemporalToe=Rayosx_toe::where('toe_tipo',2)->where('id_tramite_rayosx',$input['id_tramite'])->orderBy('id_toe_rayosx','DESC')->paginate(3);
        //dd($rayosxEquipo);

       return redirect('/ventanilla/tramites/solicitud_rayosx/tramite/'.$input['id_tramite']);


    }




    public function verificarTOE(Request $request)
    {

      //CARGAR DEPARTAMENTOS
      $departamentos = DB::table('pr_departamento')->pluck("IdDepartamento","Descripcion");
      //CARGAR TIPO IDENTIFICACION
      $tipo_identificacion = DB::table('pr_tipoidentificacion')->pluck("IdTipoIdentificacion","Descripcion");
      //CARGAR PROGRAMAS UNIVERSITARIOS
      $pr_programas_univ  = DB::table('pr_programas_univ')->pluck("id_programa","nombre_programa");
      //CARGAR NIVEL ACADEMICO
      $pr_nivelacademico  = DB::table('pr_nivelacademico')->pluck("IdNivelAcademico","Nombre");


      //Campos Hidden
      $tipo_tramite=1;
      $existe_tramite=(int)$request->existeTramite;
      $id=(int)$request->idTramite;

      if($id>0){
        $idTramite=Tramite::where('user',Auth::id())->where('id',$id)->orderBy('id','DESC')->first();

        $rayosxOficialToe=Rayosx_toe::where('toe_tipo',1)->where('id_tramite_rayosx',$id)->orderBy('id_toe_rayosx','DESC')->first();
        $rayosxTemporalToe=Rayosx_toe::where('toe_tipo',2)->where('id_tramite_rayosx',$id)->orderBy('id_toe_rayosx','DESC')->paginate(3);
        $rayosxTalento=Rayosx_director::where('id_tramite_rayosx',$id)->orderBy('id_director_rayosx','DESC')->firstorFail();
        $rayosxObjprueba=Rayosx_objprueba::where('id_tramite_rayosx',$id)->orderBy('id_obj_rayosx','DESC')->get();

        if($rayosxOficialToe->count() > 0 && $rayosxTemporalToe->count() > 0){

          return view('rayosx.index', ['idTramite' => $idTramite,
                                            'existeTramite'=>1,
                                            'tipoTramite'=>$tipo_tramite,
                                            'departamentos'=>$departamentos,
                                            'tiposIdentificacion'=>$tipo_identificacion,
                                            'prNivelacademico'=>$pr_nivelacademico,
                                            'prProgramasUniv'=>$pr_programas_univ,
                                            'rayosxOficialToe'=>$rayosxOficialToe,
                                            'rayosxTemporalToe'=>$rayosxTemporalToe,
                                            'talento'=>$rayosxTalento,
                                            'objeto'=>$rayosxObjprueba,
                                            'paso'=>4 //Paso 4 - TALENTO HUMANO
                                            ]);
        }

        return view('rayosx.index', ['idTramite' => $idTramite,
                                            'existeTramite'=>1,
                                            'tipoTramite'=>$tipo_tramite,
                                            'departamentos'=>$departamentos,
                                            'tiposIdentificacion'=>$tipo_identificacion,
                                            'prNivelacademico'=>$pr_nivelacademico,
                                            'prProgramasUniv'=>$pr_programas_univ,
                                            'rayosxOficialToe'=>$rayosxOficialToe,
                                            'rayosxTemporalToe'=>$rayosxTemporalToe,
                                            'talento'=>$rayosxTalento,
                                            'objeto'=>$rayosxObjprueba,
                                            'paso'=>3.5 //Paso 3 - Cargar TOE
                                            ]);

      }

    }

    public function guardarTalentoHumano(Request $request)
    {

      $input = Input::all();
      //dd($input);
      //CARGAR DEPARTAMENTOS
      $departamentos = DB::table('pr_departamento')->pluck("IdDepartamento","Descripcion");
      //CARGAR TIPO IDENTIFICACION
      $tipo_identificacion = DB::table('pr_tipoidentificacion')->pluck("IdTipoIdentificacion","Descripcion");
      //CARGAR PROGRAMAS UNIVERSITARIOS
      $pr_programas_univ  = DB::table('pr_programas_univ')->pluck("id_programa","nombre_programa");
      //CARGAR NIVEL ACADEMICO
      $pr_nivelacademico  = DB::table('pr_nivelacademico')->pluck("IdNivelAcademico","Nombre");


      $rayosxOficialToe=Rayosx_toe::where('toe_tipo',1)->where('id_tramite_rayosx',$input['id_tramite'])->orderBy('id_toe_rayosx','DESC')->first();
      $rayosxTemporalToe=Rayosx_toe::where('toe_tipo',2)->where('id_tramite_rayosx',$input['id_tramite'])->orderBy('id_toe_rayosx','DESC')->paginate(3);
      $rayosxTalento=Rayosx_director::where('id_tramite_rayosx',$input['id_tramite'])->orderBy('id_director_rayosx','DESC')->first();
      $rayosxObjprueba=Rayosx_objprueba::where('id_tramite_rayosx',$input['id_tramite'])->orderBy('id_obj_rayosx','DESC')->paginate(3);

      $idTramiterx = $idTramiterx2=Tramite::select('id')->where('id',$input['id_tramite'])->where('user',Auth::id())->orderBy('id','DESC')->firstorFail();;
      $rayosxDireccion = Rayosx_direccion::where('id_tramite_rayosx',$input['id_tramite'])->first();
      $rayosxEquipo = Rayosx_equipos::where('id_tramite_rayosx',$input['id_tramite'])->first();
      //dd($rayosxDireccion);
        if($rayosxTalento == NULL){
            $rayosxTalento = new Rayosx_director();
        }
        $rayosxTalento->id_tramite_rayosx=$input['id_tramite'];
        $rayosxTalento->talento_pnombre= $input['talento_pnombre'];
        $rayosxTalento->talento_snombre = $input['talento_snombre'];
        $rayosxTalento->talento_papellido = $input['talento_papellido'];
        $rayosxTalento->talento_sapellido = $input['talento_sapellido'];
        $rayosxTalento->talento_tdocumento = $input['talento_tdocumento'];
        $rayosxTalento->talento_ndocumento = $input['talento_ndocumento'];
        $rayosxTalento->talento_lexpedicion = $input['talento_lexpedicion'];
        $rayosxTalento->talento_correo = $input['talento_correo'];
        $rayosxTalento->talento_titulo = $input['talento_titulo'];
        $rayosxTalento->talento_universidad = $input['talento_universidad'];
        $rayosxTalento->talento_libro = $input['talento_libro'];
        $rayosxTalento->talento_registro = $input['talento_registro'];
        $rayosxTalento->talento_fecha_diploma = $input['talento_fecha_diploma'];
        $rayosxTalento->talento_resolucion = $input['talento_resolucion'];
        $rayosxTalento->talento_fecha_convalida = $input['talento_fecha_convalida'];
        $rayosxTalento->talento_nivel = $input['talento_nivel'];
        $rayosxTalento->talento_titulo_pos = $input['talento_titulo_pos'];
        $rayosxTalento->talento_universidad_pos = $input['talento_universidad_pos'];
        $rayosxTalento->talento_libro_pos = $input['talento_libro_pos'];
        $rayosxTalento->talento_registro_pos = $input['talento_registro_pos'];
        $rayosxTalento->talento_fecha_diploma_pos = $input['talento_fecha_diploma_pos'];
        $rayosxTalento->talento_resolucion_pos = $input['talento_resolucion_pos'];
        $rayosxTalento->talento_fecha_convalida_pos = $input['talento_fecha_convalida_pos'];

        $rayosxTalento->save();


        $rayosxTalento=Rayosx_director::where('id_tramite_rayosx',$input['id_tramite'])->orderBy('id_director_rayosx','DESC')->first();
        //dd($rayosxEquipo);

       return redirect('/ventanilla/tramites/solicitud_rayosx/tramite/'.$input['id_tramite']);
        


    }



    public function guardarEquipoPrueba(Request $request)
    {

            $input = Input::all();
            //dd($input);
            //CARGAR DEPARTAMENTOS
            $departamentos = DB::table('pr_departamento')->pluck("IdDepartamento","Descripcion");
            //CARGAR TIPO IDENTIFICACION
            $tipo_identificacion = DB::table('pr_tipoidentificacion')->pluck("IdTipoIdentificacion","Descripcion");
            //CARGAR PROGRAMAS UNIVERSITARIOS
            $pr_programas_univ  = DB::table('pr_programas_univ')->pluck("id_programa","nombre_programa");
            //CARGAR NIVEL ACADEMICO
            $pr_nivelacademico  = DB::table('pr_nivelacademico')->pluck("IdNivelAcademico","Nombre");


            $rayosxOficialToe=Rayosx_toe::where('toe_tipo',1)->where('id_tramite_rayosx',$input['id_tramite'])->orderBy('id_toe_rayosx','DESC')->first();
            $rayosxTemporalToe=Rayosx_toe::where('toe_tipo',2)->where('id_tramite_rayosx',$input['id_tramite'])->orderBy('id_toe_rayosx','DESC')->paginate(3);
            $rayosxTalento=Rayosx_director::where('id_tramite_rayosx',$input['id_tramite'])->orderBy('id_director_rayosx','DESC')->first();


            $idTramiterx = $idTramiterx2=Tramite::select('id')->where('id',$input['id_tramite'])->where('user',Auth::id())->orderBy('id','DESC')->firstorFail();;
            $rayosxDireccion = Rayosx_direccion::where('id_tramite_rayosx',$input['id_tramite'])->first();
            $rayosxEquipo = Rayosx_equipos::where('id_tramite_rayosx',$input['id_tramite'])->first();
            //dd($rayosxDireccion);

              $rayosxObjprueba = new Rayosx_objprueba();
              $rayosxObjprueba->id_tramite_rayosx=$input['id_tramite'];
              $rayosxObjprueba->obj_nombre = $input['obj_nombre'];
              $rayosxObjprueba->obj_marca = $input['obj_marca'];
              $rayosxObjprueba->obj_modelo = $input['obj_modelo'];
              $rayosxObjprueba->obj_serie = $input['obj_serie'];
              $rayosxObjprueba->obj_calibracion = $input['obj_calibracion'];
              $rayosxObjprueba->obj_vigencia = $input['obj_vigencia'];
              $rayosxObjprueba->obj_fecha = $input['obj_fecha'];
              $rayosxObjprueba->obj_manual = $input['obj_manual'];
              $rayosxObjprueba->obj_uso = $input['obj_uso'];
              $rayosxObjprueba->save();

              $rayosxObjprueba=Rayosx_objprueba::where('id_tramite_rayosx',$input['id_tramite'])->orderBy('id_obj_rayosx','DESC')->paginate(3);
              //dd($rayosxEquipo);

       return redirect('/ventanilla/tramites/solicitud_rayosx/tramite/'.$input['id_tramite']);



          }
    public function verificarPaso4(Request $request)
    {
      //CARGAR DEPARTAMENTOS
      $departamentos = DB::table('pr_departamento')->pluck("IdDepartamento","Descripcion");
      //CARGAR TIPO IDENTIFICACION
      $tipo_identificacion = DB::table('pr_tipoidentificacion')->pluck("IdTipoIdentificacion","Descripcion");
      //CARGAR PROGRAMAS UNIVERSITARIOS
      $pr_programas_univ  = DB::table('pr_programas_univ')->pluck("id_programa","nombre_programa");
      //CARGAR NIVEL ACADEMICO
      $pr_nivelacademico  = DB::table('pr_nivelacademico')->pluck("IdNivelAcademico","Nombre");


      //Campos Hidden
      $tipo_tramite=1;
      $existe_tramite=(int)$request->existeTramite;
      $id=(int)$request->idTramite;

      if($id>0){
        $idTramite=Tramite::where('user',Auth::id())->where('id',$id)->orderBy('id','DESC')->first();

        $rayosxOficialToe=Rayosx_toe::where('toe_tipo',1)->where('id_tramite_rayosx',$id)->orderBy('id_toe_rayosx','DESC')->first();
        $rayosxTemporalToe=Rayosx_toe::where('toe_tipo',2)->where('id_tramite_rayosx',$id)->orderBy('id_toe_rayosx','DESC')->paginate(3);

        $rayosxTalento=Rayosx_director::where('id_tramite_rayosx',$id)->orderBy('id_director_rayosx','DESC')->get();
        $rayosxObjprueba=Rayosx_objprueba::where('id_tramite_rayosx',$id)->orderBy('id_obj_rayosx','DESC')->get();

        if($rayosxTalento->count() > 0 && $rayosxObjprueba->count() > 0){

          return view('rayosx.index', ['idTramite' => $idTramite,
                                            'existeTramite'=>1,
                                            'tipoTramite'=>$tipo_tramite,
                                            'departamentos'=>$departamentos,
                                            'tiposIdentificacion'=>$tipo_identificacion,
                                            'prNivelacademico'=>$pr_nivelacademico,
                                            'prProgramasUniv'=>$pr_programas_univ,
                                            'oficialTOE'=>$rayosxOficialToe,
                                            'temporalTOE'=>$rayosxTemporalToe,
                                            'talento'=>$rayosxTalento,
                                            'objeto'=>$rayosxObjprueba,
                                            'paso'=>5 //Paso 5 - DOCUMENTOS
                                            ]);
        }

        return view('rayosx.index', ['idTramite' => $idTramite,
                                            'existeTramite'=>1,
                                            'tipoTramite'=>$tipo_tramite,
                                            'departamentos'=>$departamentos,
                                            'tiposIdentificacion'=>$tipo_identificacion,
                                            'prNivelacademico'=>$pr_nivelacademico,
                                            'prProgramasUniv'=>$pr_programas_univ,
                                            'oficialTOE'=>$rayosxOficialToe,
                                            'temporalTOE'=>$rayosxTemporalToe,
                                            'talento'=>$rayosxTalento,
                                            'objeto'=>$rayosxObjprueba,
                                            'paso'=>4.5 //Paso 5 - ERROR
                                            ]);
      }
    }

    public function gestionarFlujoTramiteRayosx(Request $request)
    {

        $input = Input::all();

        

        $idTramite=Tramite::where('id',$input['id_tramite'])->first();

        $salida="";

        //dd($idTramite);


        //$accionEjecutar;
        //$observacionesAccion;

        $actividadInicial=Tramite_Flujo::where("tramite_id",$idTramite->id)->orderBy('id','ASC')->first();

        $estacionActual=Tramite_Flujo::where("tramite_id",$idTramite->id)->where("fecha_fin",NULL)->orderBy('id','DESC')->first();


        //dd($estacionActual);
         if($actividadInicial != NULL && $estacionActual != NULL){
            if($actividadInicial->id == $estacionActual->id && Auth::id() == $actividadInicial->user_id){ 
              $accion='avanzar'; 

            }elseif($estacionActual->actividad == "COMPLETAR DOCUMENTACIÓN" && Auth::id() == $actividadInicial->user_id){  
              $accion='avanzar'; 
            }else{
              $accion=json_decode($input['accion_flujo'])->{'flujo_accion_permitida'};
            }
          }
        
        //dd($accion);


            if($estacionActual != NULL){               

                $accionesDisponibles= Tipo_Tramite_Flujo::where('flujo_actividad_inicial',$estacionActual->actividad);

                

                //dd($estacionActual);

                $estacionSiguiente=Tipo_Tramite_Flujo::where('flujo_actividad_inicial',$estacionActual->actividad)->where('flujo_accion_permitida', 'like','%'.$accion.'%')->first();

                $estacionInicial=Tramite_Flujo::where('tramite_id',$input['id_tramite'])->orderBy('id','ASC')->first();

                //dd($estacionSiguiente);
                
                if($estacionSiguiente != NULL){ 

                  //actualizando la accion acual

                  $estacionActual->fecha_fin=date('Y-m-d H:i:s');
                  $estacionActual->user_id=Auth::id();
                  $observaciones=$estacionActual->observaciones.' --- '.date('Y-m-d H:i:s').' '.$input['observacion'];
                  $estacionActual->save();



                    $estacionFinal=Tipo_Tramite_Flujo::where('flujo_actividad_inicial',$estacionSiguiente->flujo_actividad_final)->first();
                    //dd($estacionFinal);
                    //Se avanza
                    $tramiteFlujo = new Tramite_Flujo();
                    $tramiteFlujo->tramite_id=(int)$idTramite->id;
                    $tramiteFlujo->tipo_tramite=1;
                    $tramiteFlujo->actividad=$estacionFinal->flujo_actividad_inicial;
                    $tramiteFlujo->fecha_inicio=date('Y-m-d H:i:s');
                    if($estacionFinal->flujo_actividad_inicial == 'COMPLETAR DOCUMENTACIÓN'){
                      $tramiteFlujo->user_id=$estacionInicial->user_id;  
                      $tramiteFlujo->user_rol=NULL;
                      $para=User::select('email')->where('id',$estacionInicial->user_id)->pluck('email');
                      $titulo="Estimado usuario, el tramite".$idTramite->id." para expedir licencia de rayos x ha sido revisado y requiero de actualizarion por parte suya.";
                      $mensaje="observaciones revision: ".$input['observacion']."<br/> Por favor realice la actualizacion de su  tramite de acuerdo a las observaciones realizadas, una vez estas se encuentren subsanadas registre la solicitud para ser revisada nuevamente, para esta accion usted cuenta con: 20 dias ";


                    }else{
                      $tramiteFlujo->user_id=NULL;
                      $tramiteFlujo->user_rol=$estacionFinal->rol_responsable;
                      $para=User::select('email')->join('user_has_roles','user_has_roles.user_id','=','user.id')->where('user_has_roles.role_id',$estacionFinal->rol_responsable)->pluck('email');

                      $titulo="Estimado usuario, el tramite #".$idTramite->id." ha ingresado a la actividad de ".$estacionFinal->flujo_actividad_inicial.", cuenta con maximo ".$estacionFinal->duracion_maxima_dias." dias para realizar accion";
                      $mensaje="";

                    }
                    

                    $tramiteFlujo->observaciones=$observaciones;

                    //dd($tramiteFlujo);
                    $result=$tramiteFlujo->save();

                    if($result){
                      $this->enviarAsignaciónTramite($para,$titulo,$mensaje);
                    }


                }else{
                    $salida="Error: El flujo no se encuentra correctamente parametrizado, la actividad no se encuentra";            
                }

                

        }else{
            $salida="Error: El flujo no cuenta con actividad actual";
        }
        
        return redirect('/ventanilla/tramites/solicitud_rayosx/tramite/'.$input['id_tramite']);

    }

    private function enviarAsignaciónTramite($to,$titulo,$mensaje){

      $userRoles=user_has_roles::where("user_id",Auth::id())->get();
       $data = array(
            'name'      =>  $titulo,
            'message'   =>  $mensaje
            );

        Mail::to($to)->send(new SendFlujoTramite($data));

    }

    public function getEquipos($id_tramite) {

      $rayosxEquipo = Rayosx_equipos::where('id_tramite_rayosx',$id_tramite)->get();   
      return Datatables::of($rayosxEquipo)->make(true);
    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
