<?php

namespace App\Http\Controllers;

use PDF;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\DB;
use App\Tramite;
use App\Tipo_Tramite_Flujo;
use App\Tramite_Flujo;
use App\Models\Rayosx\Rayosx_direccion;
use App\Models\Rayosx\Rayosx_director;
use App\Models\Rayosx\Rayosx_documentos;
use App\Models\Rayosx\Rayosx_encargado;
use App\Models\Rayosx\Rayosx_equipos;
use App\Models\Rayosx\Rayosx_objprueba;
use App\Models\Rayosx\Rayosx_toe;
use App\Models\Parametricas\Pr_Tipoidentificacion;


use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\user_has_roles;
use DataTables;
use App\rayosx_tipo_documento;
use App\tipo_tramite_tipo_doc;
use App\tipo_tramite_tipo_doc_campos;
use App\tramite_documento_campos;


class PdfController extends Controller
{


public function proyectarResolucionAprobacion($id_tramite) {

        $idTramite=$id_tramite;

        $puedeEditarSolicitud=FALSE;
        $puedeAvanzarSolicitud=FALSE;


        $actividadInicial=Tramite_Flujo::where("tramite_id",$idTramite)->orderBy('id','ASC')->first();
        $actividadActual=Tramite_Flujo::where("tramite_id",$idTramite)->where("fecha_fin",NULL)->orderBy('id','DESC')->first();
        

        //dd($actividadActual);


        $rolResponsable=user_has_roles::where("user_id",Auth::id())->where('role_id',$actividadActual->user_rol)->first();

        //dd($rolResponsable);
        /*if($actividadActual->actividad == 'COMPLETAR DOCUMENTACIÓN'){
            $rolResponsable=user_has_roles::where("user_id",Auth::id())->first();        
        }*/
        
        $accionesFlujo=Tipo_Tramite_Flujo::where('tipo_tramite',1)
        //->where('flujo_actividad_inicial',$actividadActual->actividad)
        ->where('flujo_actividad_inicial', 'like', '%' . $actividadActual->actividad. '%')
        ->get();

              

        if($actividadInicial != NULL && $actividadActual != NULL){
            if($actividadInicial->id == $actividadActual->id && Auth::id() == $actividadInicial->user_id){                
                    //Se encuentra en la actividad inicial y el usuario autenticado es el encargado

                    $puedeEditarSolicitud=TRUE;                
                    $puedeAvanzarSolicitud=TRUE;
            }

            if($actividadActual->actividad == 'COMPLETAR DOCUMENTACIÓN'){
                if(Auth::id() == $actividadActual->user_id){
                    //Se encuentra en la actividad inicial y el usuario autenticado es el encargado
                    $puedeEditarSolicitud=TRUE;
                    $puedeAvanzarSolicitud=TRUE;

                }
            }

            if($rolResponsable != null){
                $puedeAvanzarSolicitud=TRUE;                
            }

        }
        
        //dd($puedeAvanzarSolicitud);
        //dd($puedeEditarSolicitud);


        $idTramiterx=Tramite::where('id',$idTramite)->where('user',Auth::id())->orderBy('id','DESC')->first();
        $rayosxOficialToe=Rayosx_toe::where('toe_tipo',1)->where('id_tramite_rayosx',$idTramite)->orderBy('id_toe_rayosx','DESC')->first();

        $rayosxTemporalToe=Rayosx_toe::where('toe_tipo',2)->where('id_tramite_rayosx',$idTramite)->orderBy('id_toe_rayosx','DESC')->paginate(3);
        $rayosxTalento=Rayosx_director::where('id_tramite_rayosx',$idTramite)->orderBy('id_director_rayosx','DESC')->first();
        $rayosxObjprueba=Rayosx_objprueba::where('id_tramite_rayosx',$idTramite)->orderBy('id_obj_rayosx','DESC')->paginate(3);
        $rayosxDocumentos=Rayosx_documentos::where('id_tramite_rayosx',$idTramite)->first();
        $rayosxDireccion = Rayosx_direccion::where('id_tramite_rayosx',$id_tramite)->first();
        //dd($rayosxDocumentos);

        

        $departamentos = DB::table('pr_departamento')->pluck("IdDepartamento","Descripcion");
        //dd($departamentos);
        $tipo_identificacion = DB::table('pr_tipoidentificacion')->pluck("IdTipoIdentificacion","Descripcion");
        $pr_nivelacademico  = DB::table('pr_nivelacademico')->pluck("IdNivelAcademico","Nombre");
        $pr_programas_univ  = DB::table('pr_programas_univ')->pluck("id_programa","nombre_programa");
        $rayosxOficialToe=Rayosx_toe::where('toe_tipo',1)->where('id_tramite_rayosx',$idTramite)->orderBy('id_toe_rayosx','DESC')->first();

        $rayosxTemporalToe=Rayosx_toe::where('toe_tipo',2)->where('id_tramite_rayosx',$idTramite)->orderBy('id_toe_rayosx','DESC')->paginate(3);

        //dd($rayosxTemporalToe);

        $rayosxTalento=Rayosx_director::where('id_tramite_rayosx',$idTramite)->orderBy('id_director_rayosx','DESC')->first();
        $rayosxObjprueba=Rayosx_objprueba::where('id_tramite_rayosx',$idTramite)->orderBy('id_obj_rayosx','DESC')->paginate(3);

        $rayosxDireccion = Rayosx_direccion::where('id_tramite_rayosx',$id_tramite)->first();
        //$rayosxEquipo = Rayosx_equipos::where('id_tramite_rayosx',$id_tramite)->first();
        $rayosxEquipo = NULL;
        $rayosxEquipos = Rayosx_equipos::where('id_tramite_rayosx',$id_tramite)->paginate(5);
        $rayosxEquipo1 = Rayosx_equipos::where('id_tramite_rayosx',$id_tramite)->first();        
        $rayosxDocumentos1=NULL;
        $rayosxDocumentos2=NULL;
        $rayosxDocumentos=NULL;
        $tipoDocumento=NULL;
        $categoria=1;  

        $resolucion_campos=tipo_tramite_tipo_doc_campos::where('estado',1)->get();

         if(isset($rayosxEquipo1)){          

          $categoria=$rayosxEquipo1->categoria;

          if($rayosxEquipo1->categoria == '1'){
            
            $rayosxDocumentos=Rayosx_documentos::where('categoria','1')->where('id_tramite_rayosx',$id_tramite)->get();

            $tipoDocumento=rayosx_tipo_documento::where('categoria','1')->get();

          }else if($rayosxEquipo1->categoria == '2'){

            $rayosxDocumentos=Rayosx_documentos::where('categoria','2')->where('id_tramite_rayosx',$id_tramite)->get();  
            $tipoDocumento=rayosx_tipo_documento::where('categoria','2')->get();
          }else{
            $tipoDocumento=rayosx_tipo_documento::where('categoria','1')->get();
          }
        }else{          
          $tipoDocumento=rayosx_tipo_documento::where('categoria','1')->get();
        }

        $idTramiterx=Tramite::where('id',$id_tramite)->first();
        
        //dd($rayosxDireccion); 
        //dd($rayosxEquipos);
        //dd($rayosxObjprueba);

        //dd($rayosxDocumentos);
        //dd($tipoDocumento);
        //dd($accionesFlujo);
        //dd($puedeEditarSolicitud);
        //dd($puedeAvanzarSolicitud);
        //dd($rayosxTemporalToe);
        //dd($idTramiterx);
        
        return view('rayosx.resolucionAprobacionProyectar',['idTramiterx'=>$idTramiterx,'departamentos'=>$departamentos,
        'tiposIdentificacion'=>$tipo_identificacion,
        'prNivelacademico'=>$pr_nivelacademico,
        'prProgramasUniv'=>$pr_programas_univ,
        'rayosxOficialToe'=>$rayosxOficialToe,
        'rayosxTemporalToe'=>$rayosxTemporalToe,
        'rayosxTalento'=>$rayosxTalento,
        'rayosxObjprueba'=>$rayosxObjprueba,
        'rayosxDireccion'=>$rayosxDireccion,
        'rayosxEquipo'=>$rayosxEquipo,
        'rayosxEquipos'=>$rayosxEquipos,
        'rayosxDocumentos'=>$rayosxDocumentos,
        'usuarioInicial'=>$actividadInicial->user_id,
        'rolResponsable'=>$actividadActual->user_id,
        'puedeEditarSolicitud'=>$puedeEditarSolicitud,
        'puedeAvanzarSolicitud'=>$puedeAvanzarSolicitud,
        'tipoDocumento'=>$tipoDocumento,
        'rayosxDocumentos'=>$rayosxDocumentos,
        'categoria'=>$categoria,
        'resolucionCampos'=>$resolucion_campos,        
        'accionesFlujo'=>$accionesFlujo]);
      
    }




  public function resolucion1($id_tramite){

  $iduser = Auth::user()->id;
  $idTramite = $id_tramite;  

  $tramitesrx=Tramite::select('tramite.id','tipo_tramite.descripcion','tramite.estado','tramite_flujo.actividad','tramite_flujo.fecha_inicio','tramite_flujo.fecha_fin','tramite_flujo.observaciones','tramite.created_at')
       ->join('tipo_tramite','tipo_tramite.id','=','tramite.tipo_tramite')
       ->join('tramite_flujo','tramite_flujo.tramite_id','=','tramite.id')
       ->where('tramite.id', $idTramite)     
       ->whereNull('tramite_flujo.fecha_fin')  
       ->orderBy('tramite_flujo.created_at','DESC')
       //->toSql();
       ->first();


/*
  $tramitesrx=Tramite::select('tramite.id','tipo_tramite.descripcion','users.name','rayosx_equipos.categoria'
  ,'rayosx_equipos.categoria2','rayosx_equipos.categoria1_1','rayosx_equipos.marca_equipo','rayosx_equipos.marca_equipo'
  ,'rayosx_equipos.modelo_equipo','rayosx_equipos.serie_equipo','rayosx_equipos.marca_tubo_rx','rayosx_equipos.modelo_tubo_rx'
  ,'rayosx_equipos.serie_tubo_rx','rayosx_equipos.tension_tubo_rx','rayosx_equipos.contiene_tubo_rx','rayosx_equipos.energia_fotones'
  ,'rayosx_equipos.energia_electrones','rayosx_equipos.carga_trabajo'
  ,'rayosx_direccion.dire_entidad','rayosx_toe.toe_pnombre','rayosx_toe.toe_snombre'
  ,'rayosx_toe.toe_papellido','rayosx_toe.toe_sapellido','rayosx_toe.toe_tdocumento','rayosx_toe.toe_ndocumento'
  ,'rayosx_toe.toe_profesion'
  ,'tramite.estado','tramite.created_at')
  ->join('tipo_tramite','tipo_tramite.id','=','tramite.tipo_tramite')
  ->join('rayosx_direccion','rayosx_direccion.id_tramite_rayosx','=','tramite.id')
  ->join('rayosx_director','rayosx_director.id_tramite_rayosx','=','tramite.id')
  ->join('rayosx_documentos','rayosx_documentos.id_tramite_rayosx','=','tramite.id')
  ->join('rayosx_encargado','rayosx_encargado.id_tramite_rayosx','=','tramite.id')
  ->join('rayosx_equipos','rayosx_equipos.id_tramite_rayosx','=','tramite.id')
  ->join('rayosx_objprueba','rayosx_objprueba.id_tramite_rayosx','=','tramite.id')
  ->join('rayosx_toe','rayosx_toe.id_tramite_rayosx','=','tramite.id')
  ->join('users','users.id','=','tramite.user')  
  ->where('tramite.id', $idTramite)
  ->where('rayosx_toe.toe_tipo', 1)
  ->orderBy('tramite.created_at','DESC')
  ->toSql();
  //->first();*/

  //dd($tramitesrx);

  if($tramitesrx != NULL){
  $pdf = PDF::loadView('rayosx.pdftramiterxres1', compact('tramitesrx'));
  return $pdf->stream('analisis-intro');
  }
  else{
    return redirect()->route('rayosx.index');
  }

  }

  public function resolucion2($id_tramite){

  $iduser = Auth::user()->id;
  $idTramite = $id_tramite;

  $tramitesrx=Tramite::select('tramite.id_tramite','tipo_tramite.descripcion','users.name','rayosx_equipos.categoria'
  ,'rayosx_equipos.categoria2','rayosx_equipos.categoria1_1','rayosx_equipos.marca_equipo','rayosx_equipos.marca_equipo'
  ,'rayosx_equipos.modelo_equipo','rayosx_equipos.serie_equipo','rayosx_equipos.marca_tubo_rx','rayosx_equipos.modelo_tubo_rx'
  ,'rayosx_equipos.serie_tubo_rx','rayosx_equipos.tension_tubo_rx','rayosx_equipos.contiene_tubo_rx','rayosx_equipos.energia_fotones'
  ,'rayosx_equipos.energia_electrones','rayosx_equipos.carga_trabajo'
  ,'rayosx_direccion.dire_entidad','rayosx_toe.toe_pnombre','rayosx_toe.toe_snombre'
  ,'rayosx_toe.toe_papellido','rayosx_toe.toe_sapellido','rayosx_toe.toe_tdocumento','rayosx_toe.toe_ndocumento'
  ,'rayosx_toe.toe_profesion'
  ,'tramite.estado','tramite.created_at')
  ->join('tipo_tramite','tipo_tramite.id','=','tramite.tipo_tramite')
  ->join('rayosx_direccion','rayosx_direccion.id_tramite_rayosx','=','tramite.id_tramite')
  ->join('rayosx_director','rayosx_director.id_tramite_rayosx','=','tramite.id_tramite')
  ->join('rayosx_documentos','rayosx_documentos.id_tramite_rayosx','=','tramite.id_tramite')
  ->join('rayosx_encargado','rayosx_encargado.id_tramite_rayosx','=','tramite.id_tramite')
  ->join('rayosx_equipos','rayosx_equipos.id_tramite_rayosx','=','tramite.id_tramite')
  ->join('rayosx_objprueba','rayosx_objprueba.id_tramite_rayosx','=','tramite.id_tramite')
  ->join('rayosx_toe','rayosx_toe.id_tramite_rayosx','=','tramite.id_tramite')
  ->join('users','users.id','=','tramite.user')
  ->where('tramite.user', $iduser)
  ->where('tramite.id_tramite', $idTramite)
  ->where('rayosx_toe.toe_tipo', 1)
  ->orderBy('tramite.created_at','DESC')->first();

  if($tramitesrx != NULL){
  $pdf = PDF::loadView('rayosx.pdftramiterxres2', compact('tramitesrx'));
  return $pdf->stream('analisis-intro');
  }
  else{
    return redirect()->route('rayosx.index');
  }

  }

  public function resolucion3($id_tramite){

  $iduser = Auth::user()->id;
  $idTramite = $id_tramite;

  $tramitesrx=Tramite::select('tramite.id_tramite','tipo_tramite.descripcion','users.name','rayosx_equipos.categoria'
  ,'rayosx_equipos.categoria2','rayosx_equipos.categoria1_1','rayosx_equipos.marca_equipo','rayosx_equipos.marca_equipo'
  ,'rayosx_equipos.modelo_equipo','rayosx_equipos.serie_equipo','rayosx_equipos.marca_tubo_rx','rayosx_equipos.modelo_tubo_rx'
  ,'rayosx_equipos.serie_tubo_rx','rayosx_equipos.tension_tubo_rx','rayosx_equipos.contiene_tubo_rx','rayosx_equipos.energia_fotones'
  ,'rayosx_equipos.energia_electrones','rayosx_equipos.carga_trabajo'
  ,'rayosx_direccion.dire_entidad','rayosx_toe.toe_pnombre','rayosx_toe.toe_snombre'
  ,'rayosx_toe.toe_papellido','rayosx_toe.toe_sapellido','rayosx_toe.toe_tdocumento','rayosx_toe.toe_ndocumento'
  ,'rayosx_toe.toe_profesion'
  ,'tramite.estado','tramite.created_at')
  ->join('tipo_tramite','tipo_tramite.id','=','tramite.tipo_tramite')
  ->join('rayosx_direccion','rayosx_direccion.id_tramite_rayosx','=','tramite.id_tramite')
  ->join('rayosx_director','rayosx_director.id_tramite_rayosx','=','tramite.id_tramite')
  ->join('rayosx_documentos','rayosx_documentos.id_tramite_rayosx','=','tramite.id_tramite')
  ->join('rayosx_encargado','rayosx_encargado.id_tramite_rayosx','=','tramite.id_tramite')
  ->join('rayosx_equipos','rayosx_equipos.id_tramite_rayosx','=','tramite.id_tramite')
  ->join('rayosx_objprueba','rayosx_objprueba.id_tramite_rayosx','=','tramite.id_tramite')
  ->join('rayosx_toe','rayosx_toe.id_tramite_rayosx','=','tramite.id_tramite')
  ->join('users','users.id','=','tramite.user')
  ->where('tramite.user', $iduser)
  ->where('tramite.id_tramite', $idTramite)
  ->where('rayosx_toe.toe_tipo', 1)
  ->orderBy('tramite.created_at','DESC')->first();

  if($tramitesrx != NULL){
  $pdf = PDF::loadView('rayosx.pdftramiterxres3', compact('tramitesrx'));
  return $pdf->stream('analisis-intro');
  }
  else{
    return redirect()->route('rayosx.index');
  }

  }
}
