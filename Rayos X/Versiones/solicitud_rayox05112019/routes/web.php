<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/tramiteRayosX', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'RayosxController@index')->name('home');
Route::get('notes', 'NotesController@index');


Route::group(['middleware' => ['permission:destroy_notes']], function () {
    Route::get('notes/{id}/destroy', 'NotesController@destroy')->name('notes.destroy');
});

//Listado de tramites inicial
Route::any('/ventanilla/tramites', 'TramitesController@index')->middleware('auth')->name('tramites.index');

Route::any('/ventanilla/tramites/edit', 'TramitesController@edit')->middleware('auth')->name('tramites.edit');

//				--		SOLICITUD RAYOS X 				--

//Descripcion del tramite


Route::get('/ventanilla/tramites/solicitud_rayosx', 'RayosxController@index')->name('rayosx.index')->middleware('auth');

//Crear nuevo rayos x
Route::any('/ventanilla/tramites/solicitud_rayosx/crear', 'RayosxController@create')->name('rayosx.create')->middleware('auth');

Route::any('/tramites/creartramiterayosx', 'RayosxController@crearTramiteRayosx')->name('rayosx.crearTramiteRayosx')->middleware('auth');

Route::any('/tramites/guardartramiterayosx', 'RayosxController@guardarTramiteRayosx')->name('rayosx.guardarTramiteRayosx')->middleware('auth');

Route::post('/tramite/editardireccion/', 'RayosxController@editarDireccion')->name('rayosx.editarDireccion')->middleware('auth');

Route::post('/tramite/editarequipo/', 'RayosxController@editarEquipo')->name('rayosx.editarEquipo')->middleware('auth');

Route::post('/tramite/editaroficialtoe/', 'RayosxController@editarOficialToe')->name('rayosx.editarOficialToe')->middleware('auth');

Route::post('/tramite/editardirector/', 'RayosxController@editarDirector')->name('rayosx.editarDirector')->middleware('auth');

Route::post('/tramite/editardocumentos1/', 'RayosxController@editarDocumentos1')->name('rayosx.editarDocumentos1')->middleware('auth');

Route::post('/tramite/editardocumentos2/', 'RayosxController@editarDocumentos2')->name('rayosx.editarDocumentos2')->middleware('auth');

Route::post('/tramite/guardardocumentos1/', 'RayosxController@guardarDocumentos1')->name('rayosx.guardarDocumentos1')->middleware('auth');

Route::post('/tramite/guardardocumentos2/', 'RayosxController@guardarDocumentos2')->name('rayosx.guardarDocumentos2')->middleware('auth');

Route::post('/tramite/guardardocumentos/', 'RayosxController@guardarDocumentos')->name('rayosx.guardarDocumentos')->middleware('auth');
//Editar  rayos x
Route::any('/ventanilla/tramites/solicitud_rayosx/tramite/{idtramite}', 'RayosxController@edit')->middleware('auth');

Route::any('/ventanilla/tramites/solicitud_rayosx/resolucion1/{idtramite}', 'PdfController@resolucion1')->middleware('auth');

Route::any('/ventanilla/tramites/solicitud_rayosx/resolucion2/{idtramite}', 'PdfController@resolucion2')->middleware('auth');

Route::any('/ventanilla/tramites/solicitud_rayosx/resolucion3/{idtramite}', 'PdfController@resolucion3')->middleware('auth');

Route::any('/ventanilla/tramites/solicitud_rayosx/gestionarFlujo', 'RayosxController@gestionarFlujoTramiteRayosx')->name('rayosx.gestionarFlujoTramiteRayosx')->middleware('auth');

//Editar  rayos x equipo
Route::any('/ventanilla/tramites/solicitud_rayosx/tramite/editardireccion/', 'RayosxController@editardireccion')->name('rayosx.editardireccion')->middleware('auth');

//RAYOS X - Guardar localizacion entidad
Route::any('/tramites/guardarlocalizacion', 'RayosxController@guardarLocalizacion')->name('rayosx.guardarLocalizacion')->middleware('auth');

//RAYOS X - Guardar equipos de rayosX
Route::any('/ventanilla/tramites/solicitud_rayosx/formSeccion2', 'RayosxController@guardarEquiposRayosX')->name('rayosx.guardarEquiposRayosX')->middleware('auth');

//RAYOS X - Guardar oficial TOE
Route::any('/ventanilla/tramites/solicitud_rayosx/formSeccion3-1', 'RayosxController@guardarOficialTOE')->name('rayosx.guardarOficialTOE')->middleware('auth');

//RAYOS X - Guardar oficial TOE
Route::any('/ventanilla/tramites/solicitud_rayosx/formSeccion3-2', 'RayosxController@guardarTemporalTOE')->name('rayosx.guardarTemporalTOE')->middleware('auth');

//RAYOS X - VERIFICAR TOE y PASO 4S
Route::any('/ventanilla/tramites/solicitud_rayosx/formSeccion4', 'RayosxController@verificarTOE')->name('rayosx.verificarTOE')->middleware('auth');

//RAYOS X - Guardar oficial TOE
Route::any('/ventanilla/tramites/solicitud_rayosx/formSeccion4-1', 'RayosxController@guardarTalentoHumano')->name('rayosx.guardarTalentoHumano')->middleware('auth');

Route::any('/ventanilla/tramites/solicitud_rayosx/formSeccion4-2', 'RayosxController@guardarEquipoPrueba')->name('rayosx.guardarEquipoPrueba')->middleware('auth');

Route::any('/ventanilla/tramites/solicitud_rayosx/formSeccion5', 'RayosxController@verificarPaso4')->name('rayosx.verificarPaso4')->middleware('auth');

//guardar rayos x
Route::get('/ventanilla/tramites/solicitud_rayosx/store', 'RayosxController@store')->middleware('auth');


Route::any('/ventanilla/tramites/solicitud_rayosx/equipos/{idtramite}', 'RayosxController@getEquipos')->middleware('auth');


//consultar rayos x
//Avanzar tramite
//Devolver tramite


Route::get('/departamento/get/{id}', 'RayosxController@getDptoXPais');
Route::get('/municipio/get/{id}', 'RayosxController@getMpoXDpto');
Route::get('/subred/get', 'RayosxController@getSubred');

Route::get('/categoriaequipo/get/{id}', 'RayosxController@getEquipoXCategoria');
Route::get('/localidad/get/{id}', 'RayosxController@getLocalidadXSubred');
Route::get('/upz/get/{id}', 'RayosxController@getUpzXLocalidad');
Route::get('/barrio/get/{id}', 'RayosxController@getBarrioXUpz');

Route::get('/documento/get/{id}', 'RayosxController@getCamposDocumentos');

//ADMINISTRADOR
//guardar rayos x
Route::resource('users', 'UserController');
Route::resource('roles', 'RoleController');
Route::resource('permissions', 'PermissionController');

//RESOLUCIONES

Route::any('/ventanilla/tramites/solicitud_rayosx/aprobacionProyectar/{idtramite}', 'PdfController@proyectarResolucionAprobacion')->middleware('auth');


//REGISTRAR PERSONA NATURAL

Route::any('/cargar/personaNatural', 'PersonaController@cargarPersonaNatural')->name('cargar.personanatural');
Route::any('/registrar/personaNatural', 'PersonaController@registrarPersonaNatural')->name('registrar.personanatural');
Route::any('/cargar/personaJuridica', 'PersonaController@cargarPersonaJuridica')->name('cargar.personajuridica');
Route::any('/registrar/personaJuridica', 'PersonaController@registrarPersonaJuridica')->name('registrar.personajuridica');

