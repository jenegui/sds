@extends('layouts.layout')



@section('content')

<div class="breadcrumb">
    <ul>
        <li><a href="{{url('/')}}">Inicio</a></li>
    </ul>
</div>

<div id="paso3" class="row block">
    <div class="col-7 col-lg-7">
        <div class="subtitle">
            <h2><b>Ventanilla Única de Trámites y Servicios</b>
            </h2>
        </div>
        <p style="text-align: justify;">
           La Secretaria Distrital de Salud, en concordancia con la Política de Gobierno Digital, ha dispuesto para la ciudadanía, la ventanilla única de trámites en línea, con el fin de hacer más ágil y efectiva la interacción de nuestra institución con los ciudadanos.
           A través de esta ventanilla, cualquier ciudadano o institución podrá igualmente consultar la validez y veracidad de los actos administrativos que se generen por cada trámite, respaldando la gestión de la SDS bajo los principios de seguridad de la información.
           Tenga en cuenta, que para realizar de nuestros trámites en línea, es obligatorio diligenciar previamente el REGISTRO DEL CIUDADANO (persona natural o jurídica), el cual servirá para la realización de trámites posteriores ante la Secretaria Distrital de Salud.
           Cualquier información adicional, consulta o dificultad frente a la realización de sus trámites en línea, podrá escribirnos al correo electrónico contactenos@saludcapital.gov.co
         </p>
    </div>
    <div class="col-md-5">
      <p align="center">
        <img src="{{url('img/autorregula/a3.png')}}" alt="">
      </p>
    </div>
</div>

<div class="row block">
  <div class="col-12 col-lg-12 text-center">
    <div class="title">
        <h2><b><a name="registro">Inicio Sesión y Registro</a></b></h2>
    </div>
  </div>
  <hr class="new1">
  <div class="col-md-6">
    <div class="subtitle">
        <h2><b>Ingreso de usuarios</b>
        </h2>
    </div>
    <div class="col-md-12">
      <form class="text-center" method="post" action="{{ url('/login') }}">
        {{ csrf_field() }}
        <div class="form-newsletter">
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="inputIconBox"><i class="fas fa-user"></i></span>
            </div>
              <input name="email" type="text" class="form-control" placeholder="Ingrese Nombre Usuario"  id="email" required="">
              <br>
              @if ($errors->has('email'))
                  <span class="help-block">
                      <strong>{{ $errors->first('username') }}</strong>
                  </span>
              @endif
          </div>
          <br><br>
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="inputIconBox"><i class="fas fa-key"></i></span>
            </div>
              <input type="password" class="form-control" placeholder="Ingrese su Contraseña" id="password" name="password" required="">
          </div>
          <br><br>
          <div class="input-group">
            <button class="btn yellow w-100 py-2" type="submit">Ingresar</button>
          </div>
          <div>
            <p aling="center">
              <br>
              <a class="btn btn-info" href="">¿Olvidaste tu contraseña?</a>
            </p>
          </div>
        </div>
      </form>
    </div>
  </div>

  <div class="col-md-6">
    <div class="subtitle">
        <h2><b>Registro Ventanilla Única</b>
        </h2>
    </div>
    <div class="col-md-12">
      <form class="text-center" method="post" action="">
        <div class="form-newsletter">
          <div class="input-group">
              <a href="{{url('cargar/personaNatural')}}"  class="btn blue w-100 py-2">Registro Persona Natural</a>
          </div>
          <br><br>
          <div class="input-group">
            <a href="{{url('cargar/personaJuridica')}}"  class="btn blue w-100 py-2">Registro Persona Jurídica</a>
          </div>
      </div>
      </form>
    </div>
  </div>

</div>


<div class="row block">
  <div class="col-12 col-lg-12 text-center">
    <div class="title">
        <h2><b>Validación de Documentos</b></h2>
    </div>
  </div>
  <hr class="new1">
  <div class="col-md-12">
    <p>
      En esta sección puede validar la autenticidad del documento emitido por esta entidad. Por favor digite el código de verificación que viene anexo en el documento
    </p>
      <form class="text-center" method="post" action="">
        <div class="form-newsletter">
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="inputIconBox"><i class="fas fa-search-plus"></i></i></span>
            </div>
              <input name="codever" type="text" class="form-control" placeholder="Ingrese Código de Verificación"  id="codever" required="">
          </div>
          <br><br>
          <div class="input-group">
            <button class="btn yellow w-100 py-2" type="submit">Consultar</button>
          </div>
        </div>
      </form>
  </div>
</div>

<div class="row mt-4">
  <div class="col-12 col-md-6">
    <div class="subtitle">
      <h2><b>Preguntas Frecuentes</b></h2>
    </div>
    <div class="faqs-btn form-buscar mt-2 mt-md-0">
      <input id="autocomplete" class="form-control mr-sm-2 buscar" type="text" placeholder="Buscar" aria-label="Buscar">
      <button class="buscar"></button>
    </div>
    <div class="faqs">
      <div class="faq_wrapper">
        <div id="faq_accordion">
          <div class="card">
            <div class="card-header" id="heading">
              <button class="btn btn-link" data-toggle="collapse" data-target="#collapse" aria-expanded="false" aria-controls="collapse">
              </button>
            </div>
            <div id="collapse" class="collapse" aria-labelledby="heading" data-parent="#faq_accordion">
              <div class="card-body">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="col-12 col-md-6">
    <div class="subtitle">
      <h2><b>Tips Ventanilla</b></h2>
    </div>
    <ul class="tips">
      <li>
        <img src="" alt="">
        <p></p>
      </li>
    </ul>
  </div>
</div>

@endsection
