@extends('layouts.layout')

@section('content')


<h2 class="text-blue"><b>Registro y autorización de licencias de Rayos X</b></h2>

   <div class="line"></div>

   <!-- PRIMER PASO CREACION DEL TRAMITE Y ASIGNACION DEL PRIMER ESTADO -->
   <div class="row block w-100 newsletter">
      <div class="w-100">
         <div class="subtitle">
            <h3><b>Mis Trámites</b></h3>
         </div>

         <div class="box-body">
           <div class="row">
             <div class="col-md-12">
              @can('Nueva Solicitud RayosX')
               <a href="{{route('rayosx.crearTramiteRayosx')}}" class="btn blue w-100 py-2"> Crear Nuevo Trámite</a>
               <br/>
              @endcan
             </div>
               <div class="col-md-12">
                 <table class="table table-striped">
                   <tbody>
                     <tr>
                      <th> Id Trámite</th>
                      <th> Tipo Trámite </th>
                      
                      <th> Inicio Trámite </th>
                      <th> Actividad </th>
                      <th> Inicio Actividad </th>                      
                      <th> Observación </th>                      
                      <th><center> Ver Más</center></th>                      
                      <th><center> - </center></th>
                      <th><center> - </center></th>
                      <th><center> - </center></th>

                     </tr>
                   <!-- Conditional x  Variable Listado de Usuarios-->
                   @if(isset($tramitesrx))
                   @forelse($tramitesrx as $tramiterx)
                     <tr class="success">
                      @php
                        $dias_transcurridos=date_diff(new \DateTime($tramiterx->fecha_inicio), new \DateTime())->format("%d days");
                        $dias_maximos=$tramiterx->duracion_maxima_dias;
                        $porcentaje=(intval($dias_transcurridos)*100)/$dias_maximos;                        
                        if($porcentaje < 80){ $color="bg-success";}
                        if($porcentaje >= 80){ $color="bg-warning";}
                        if($porcentaje >= 80){ $color="bg-danger";}                           
                      @endphp
                       <td class="{{$color}}">{{ $tramiterx->id}}</td> 
                       <td>{{ $tramiterx->descripcion }}</td>
                       <td>{{ $tramiterx->created_at }}</td>                       
                       <td>{{ $tramiterx->actividad }}</td>
                       <td>{{ $tramiterx->fecha_inicio }}</td>                       
                       <td>{{ $tramiterx->observaciones}}</td>                       
                       <td align="center"><a href="{{url('/ventanilla/tramites/solicitud_rayosx/tramite/'.$tramiterx->id)}}"><i class="fa fa-search"></i></a></td>
                      @can('Ver Resoluciones')                       
                       <td align="center"><a href="{{url('/ventanilla/tramites/solicitud_rayosx/resolucion1/'.$tramiterx->id)}}" target="_blank"><i class="fa fa-file"></i></a></td>
                       <td align="center"><a href="{{url('/ventanilla/tramites/solicitud_rayosx/aprobacionProyectar/'.$tramiterx->id)}}" target="_blank"><i class="fa fa-file"></i></a></td>
                       <td align="center"><a href="{{url('/ventanilla/tramites/solicitud_rayosx/resolucion2/'.$tramiterx->id)}}" target="_blank"><i class="fa fa-file"></i></a></td>
                       <td align="center"><a href="{{url('/ventanilla/tramites/solicitud_rayosx/resolucion3/'.$tramiterx->id)}}" target="_blank"><i class="fa fa-file"></i></a></td>
                       @endcan
                     </tr>
                   <!-- Conditional Variable Empty Variable listado-->
                   @empty
                     <tr>
                       <td colspan="6" scope="col">No Existen Trámites Registrados</td>
                     </tr>
                   @endforelse

                   </tbody></table>
                   <!-- Conditional Pagination Bootstrap -->
                   <div class="box-footer clearfix">
                     @if(count($tramitesrx))
                       <div class="mt-2 mx-auto">
                         {{ $tramitesrx->links('pagination::bootstrap-4') }}
                       </div>
                     @endif
                   </div>
                   @endif

              </div>
            </div>
         </div>

       <div id="btnRegistrarSolicitud" class="col-md-12 pt-200 collapse">
         <p align="center">
            <br/>
            <!-- Primer Collapsible - Localizacion Entidad -->
            <button type="submit" class="btn yellow">
               Crear Solicitud
            </button>
         </p>
       </div>
   </div>
@endsection

@section ("scripts")

 <script languague="javascript">

  function colorRow(diasTrans) {
    return "bg-primary";   // The function returns the product of p1 and p2
  }

 </script>

@endsection
