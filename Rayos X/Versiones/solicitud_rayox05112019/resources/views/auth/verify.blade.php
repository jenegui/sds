@extends('layouts.layout')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Por favor verifique su correo electronico') }}</div>

                <div class="card-body">
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ __('Un enlace de verificación sera enviado al correo electronico.') }}
                        </div>
                    @endif

                    {{ __('Antes de proceder, por favor en su correo electronico el enlace de verificación que hemos enviado.') }}
                    {{ __('Si no recibiste el correo') }}, <a href="{{ route('verification.resend') }}">{{ __('Clic aca para enviar enlace de verificacion') }}</a>.
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
