@extends('layouts.layout')

@section('content')


<h2 class="text-blue"><b>Registrar Persona Natural</b></h2>

<form id="formSeccion1" action="{{route('registrar.personanatural')}}" method="post">
  {{ csrf_field() }}   
   <!-- PRIMERA PARTE DEL REGISTRO DE LA SOLICITUD - LOCALIZACION tipoTramite  -->
   <div id="paso1" class="modal-content">
        <div class="modal-body newsletter">
             <div class="subtitle modal-header w-100">
            <h3><b>Datos Básicos:</b></h3>
        </div>

        
        <div class="row">
          <div class="alert alert-info w-100">
            <strong><b>Importante!</b></strong> Por favor registre los datos exactamente como aparecen en su documento de identidad
          </div>
        </div>
        
        <div class="row">

           <div class="col">
              <span class="text-orange">•</span><label for="p_nombre">Primer Nombre</label>              
              <input id="p_nombre" name="p_nombre" placeholder="Ingresar Primer Nombre" class="form-control input-md validate[required, minSize[4], maxSize[30]]" required type="text" value="" onkeyup="javascript:this.value=this.value.toUpperCase();" pattern="{4,30}"  minlength="4" maxlength="30" title="Ingresar un Tamaño mínimo: 4 carácteres a un Tamaño máximo: 30 carácteres">
           </div>

           <div class="col">
              <span class="text-orange">•</span><label for="s_nombre">Segundo Nombre</label>
              <input id="s_nombre" name="s_nombre" placeholder="Ingresar Segundo Nombre" class="form-control input-md validate[minSize[4], maxSize[30]]"  type="text" value="" onkeyup="javascript:this.value=this.value.toUpperCase();" pattern="{4,30}"  minlength="4" maxlength="30" title="Ingresar un Tamaño mínimo: 4 carácteres a un Tamaño máximo: 30 carácteres">
           </div>

           <div class="col">
              <span class="text-orange">•</span><label for="p_apellido">Primer Apellido</label>
              <input id="p_apellido" name="p_apellido" placeholder="Ingresar Primer Apellido" class="form-control input-md validate[required, minSize[4], maxSize[30]]" required type="text" value="" onkeyup="javascript:this.value=this.value.toUpperCase();" pattern="{4,30}"  minlength="4" maxlength="30" title="Ingresar un Tamaño mínimo: 4 carácteres a un Tamaño máximo: 30 carácteres">
           </div>
           <div class="col">
              <span class="text-orange">•</span><label for="s_apellido">Segundo Apellido</label>
              <input id="s_apellido" name="s_apellido" placeholder="Ingresar Segundo Apellido" class="form-control input-md validate[minSize[4], maxSize[30]]" type="text" value="" onkeyup="javascript:this.value=this.value.toUpperCase();" pattern="{4,30}"  minlength="4" maxlength="30" title="Ingresar un Tamaño mínimo: 4 carácteres a un Tamaño máximo: 30 carácteres">
           </div>
        </div>

         <div class="row">
           <div class="col">
              <span class="text-orange">•</span><label for="tipo_identificacion">Tipo Documento</label>
              <select id="tipo_identificacion" name="tipo_identificacion" class="form-group form-group-lg validate[required]" required>
                 <option value=""> - Seleccione Tipo Documento -</option>
                 @foreach ($tipo_identificacion as $tipoIdentificacion => $value)
                 @if($value!=5)
                 <option value="{{ $value}}">
                    {{ $tipoIdentificacion}}</option>
                  @endif
                 @endforeach
              </select>
           </div>

           <div class="col">  
              <span class="text-orange">•</span><label for="nume_documento">Número Documento</label>
              <input id="nume_documento" name="nume_documento" placeholder="Ingresar Número Documento" class="form-control input-md validate[required, minSize[4], maxSize[15]]" required type="number" onKeyPress="if(this.value.length==15) return false;">
           </div>

           <div class="col">
               <span class="text-orange">•</span><label for="email">Correo Electrónico</label>
               <input id="email" name="email" placeholder="Ingresar Correo Electrónico" class="form-control validate[required, custom[email]]" type="email" value="" required>
            </div>

            <div class="col">
               <span class="text-orange">•</span><label for="email-confirma">Confirmar Correo Electrónico</label>
               <input id="email-confirma" name="email-confirma" placeholder="Ingresar Correo Electrónico" class="form-control validate[required, custom[email]]" type="email" value="" required>
            </div>
           
        </div>



        <div class="row">

            <div class="col">
               <span class="text-orange">•</span><label for="telefono_fijo">Teléfono fijo</label>
               <input id="telefono_fijo" name="telefono_fijo" placeholder="Ingresar Número telefonico de contacto en sede de la Instalación" class="form-control input-md validate[required, minSize[7], maxSize[15]]" type="number" value=""  pattern="[0-9]{7,15}" minlength="7" maxlength="15"  title="Ingresar un Tamaño mínimo: 7 carácteres a un Tamaño máximo: 15 carácteres" onKeyPress="if(this.value.length==15) return false;">
            </div>

            <div class="col">
               <span class="text-orange">•</span><label for="telefono_celular">Teléfono celular(*)</label>
               <input id="telefono_celular" name="telefono_celular" placeholder="Ingresar Número Celular de contacto en sede de la Instalación" class="form-control input-md validate[required, minSize[10], maxSize[15]]" type="number" value="" pattern="[0-9]{10,15}" minlength="10" maxlength="15"  title="Ingresar un Tamaño mínimo: 10 carácteres a un Tamaño máximo: 15 carácteres" onKeyPress="if(this.value.length==15) return false;">
            </div>      


        </div>

        
        <div class="subtitle modal-header w-100">
            <h3><b>Datos Geográficos:</b></h3>
        </div>


        <div class="row">

            <div class="col">
               <span class="text-orange">•</span><label for="nacionalidad">Nacionalidad(*)</label>               
               <select id="nacionalidad" name="nacionalidad" class="form-group form-group-lg validate[required]" required>
                  <option value=""> - Seleccione Pais -</option>
                  @foreach ($paises as $pais => $value)
                   <option value="{{ $value}}">
                      {{ $pais}}</option>
                   @endforeach
               </select>
            </div>

            <div class="col">
               <span class="text-orange">•</span><label for="departamento">Departamento Nacimiento(*)</label>               
               <select id="departamento" name="departamento" class="form-group form-group-lg validate[required]" required>
                  <option value=""> - Seleccione Departamento -</option>                  
               </select>
            </div>

            <div class="col">
               <span class="text-orange">•</span><label for="ciudad_nacimiento">Ciudad Nacimiento(*)</label>
               <select id="ciudad_nacimiento" name="ciudad_nacimiento" class="form-group form-group-lg validate[required]" required>
               <option value=""> - Seleccione Ciudad de Nacimiento -</option>                                   
               </select>
               <div class="col-md-2" style="visibility:hidden;"><span id="loader"><i class="fa fa-spinner fa-3x fa-spin"></i></span></div>
            </div>          

        </div>

        <div class="row">     

            <div class="col">
               <span class="text-orange">•</span><label for="depa_resi">Departamento de residencia(*)</label>              
               <select id="depa_resi" name="depa_resi" class="form-group form-group-lg validate[required]" required>
                  <option value=""> - Seleccione Departamento -</option>                      
                  @foreach ($departamentos as $departamento => $value)
                   <option value="{{ $value}}">{{ $departamento}}</option>
                   @endforeach               
               </select>
            </div>

            <div class="col">
               <span class="text-orange">•</span><label for="ciudad_resi">Ciudad Residencia(*)</label>
               <select id="ciudad_resi" name="ciudad_resi" class="form-group form-group-lg validate[required]" required>
               <option value=""> - Seleccione Ciudad de Nacimiento - </option>                                   
               </select>
               <div class="col-md-2" style="visibility:hidden;"><span id="loader"><i class="fa fa-spinner fa-3x fa-spin"></i></span></div>
            </div>          
        </div>

        <div class="row">
            <div class="col" id="divSubred"  style="visibility:hidden;">
               <span class="text-orange">•</span><label for="subred">Subred</label>
               <select id="subred" name="subred" class="form-group form-group-lg validate[required]">
                  <option value=""> -Seleccione Subred - </option>                  
               </select>
            </div>

            <div class="col" id="divLocalidad" style="visibility:hidden;">
               <span class="text-orange">•</span><label for="localidad">Localidad</label>
               <select id="localidad" name="localidad" class="form-group form-group-lg validate[required]">
                  <option value=""> - Seleccione Localidad - </option>                  
               </select>
            </div>
            <div class="col" id="divUpz" style="visibility:hidden;">
               <span class="text-orange">•</span><label for="upz">UPZ</label>
               <select id="upz" name="upz" class="form-group form-group-lg validate[required]">
                  <option value=""> - Seleccione UPZ - </option>                  
               </select>
            </div>

            <div class="col" id="divBarrio" style="visibility:hidden;">
               <span class="text-orange">•</span><label for="barrio">Barrio</label>
               <select id="barrio" name="barrio" class="form-group form-group-lg validate[required]">
                  <option value=""> - Seleccione Barrio - </option>                  
               </select>
            </div>
        </div>

        <div class="row">
             <div class="col w-100">
                <span class="text-orange">•</span><label for="dire_resi">Dirección de residencia / notificación(*)</label>
                <input id="dire_resi" name="dire_resi" class="form-control input-md validate[required, minSize[4], maxSize[100]]" type="text" required placeholder="Ingresar la Dirección Entidad" onkeyup="javascript:this.value=this.value.toUpperCase();" pattern="{4,100}"  minlength="4" maxlength="30" title="Ingresar un Tamaño mínimo: 4 carácteres a un Tamaño máximo: 100 carácteres">
             </div>
        </div>
            

        
        <div class="subtitle modal-header w-100">
            <h3><b>Datos Demográficos:</b></h3>
        </div>

        <div class="row">
            <div class="col">
                <span class="text-orange">•</span><label for="fecha_nacimiento">Fecha de Nacimiento</label>
                <input id="fecha_nacimiento" name="fecha_nacimiento" placeholder="Ingresar Fecha de nacimiento" class="form-control validate[required] input-md" type="date" required>
             </div>

             <div class="col">
                <span class="text-orange">•</span><label for="sexo">Sexo</label>
                <select id="sexo" name="sexo" class="form-group form-group-lg validate[required]">
                    <option value="">Seleccione...</option>
                    <option value="1">Hombre</option>
                    <option value="2">Mujer</option>
                    <option value="3">Intersexual</option>
                </select>
            </div>

            <div class="col">
                <span class="text-orange">•</span><label for="genero">Genero</label>
                <select id="genero" name="genero" class="form-group form-group-lg validate[required]">
                    <option value="">Seleccione...</option>
                    <option value="1">Masculino</option>
                    <option value="2">Femenino</option>
                    <option value="3">Transgenero</option>
                    <option value="4">No responde</option>
                </select>

            </div>

            <div class="col">
                <span class="text-orange">•</span><label for="orientacion">Orientación Sexual</label>
                <select id="orientacion" name="orientacion"  class="form-group form-group-lg validate[required]">
                    <option value="">Seleccione...</option>
                    <option value="1">Heterosexual</option>
                    <option value="2">Homosexual</option>
                    <option value="3">Bisexual</option>
                </select>                
            </div>            
        </div>

        <div class="row">
            <div class="col">
                <span class="text-orange">•</span><label for="etnia">Etnia</label>
                <select id="etnia" name="etnia" class="form-group form-group-lg validate[required]">
                    <option value="">Seleccione...</option>
                    <option value="1">Indigena</option>
                    <option value="2">Rom-Gitano</option>
                    <option value="3">Raizal</option>
                    <option value="4">Palenquero</option>
                    <option value="5">Afrocolombiano</option>
                    <option value="6">Ninguna</option>
                </select>

            </div>

            <div class="col">
                <span class="text-orange">•</span><label for="estado_civil">Estado Civil</label>
                <select id="estado_civil" name="estado_civil" class="form-group form-group-lg validate[required]">
                    <option value="">Seleccione...</option>
                    <option value="1">Soltero</option>
                    <option value="2">Casado</option>
                    <option value="3">Union marital de hecho</option>
                    <option value="4">Divorciado</option>
                    <option value="5">Viudo</option>
                </select>

            </div>

            <div class="col">
                <span class="text-orange">•</span><label for="nivel_educativo">Nivel Educativo</label>
                <select id="nivel_educativo" name="nivel_educativo" class="form-group form-group-lg validate[required]">
                    <option value="">Seleccione...</option>
                    <option value="1">Primaria</option>
                    <option value="2">Secundaria</option>
                    <option value="3">T&eacute;cnico</option>
                    <option value="4">Tecn&oacute;logo</option>
                    <option value="5">Profesional</option>
                    <option value="6">Especialista</option>
                    <option value="7">Maestria</option>
                    <option value="8">Doctorado</option>
                    <option value="9">Post-Doctorado</option>
                </select>

            </div>

        </div>

        <div class="row">
          <div class="form-group row">
              <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Contraseña') }}</label>

              <div class="col-md-6">
                  <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                  @error('password')
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $message }}</strong>
                      </span>
                  @enderror
              </div>
          </div>

          <div class="form-group row">
              <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirmar Contraseña') }}</label>

              <div class="col-md-6">
                  <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
              </div>
          </div>
        </div>
        

         <div id="btnRegistrar" class="col-md-12 pt-200">
            <p align="center">               
               <button type="submit" class="btn yellow">
                  Registrarse
               </button>            
               
            </p>
        </div>



        </div>
    </div>
</form>



@endsection


@section ("scripts")



<script languague="javascript">

  $(document).ready(function () {  


  //Paises

  $('select[name="nacionalidad"]').on('change', function(){    
  
    var paisId = $(this).val();
    if(paisId) {
        $.ajax({
            url: "{{url('/departamento/get')}}/"+paisId,
            type:"GET",
            dataType:"json",
            beforeSend: function(){
                $('#loader').css("visibility", "visible");
            },
            success:function(data) {
                $('select[name="departamento"]').empty();
                $('select[name="departamento"]').append('<option value=""> - Seleccione Departamento - </option>');
                $.each(data, function(key, value){
                    $('select[name="departamento"]').append('<option value="'+ value +'">' + key+ '</option>');
                });
            },
            complete: function(){
                $('#loader').css("visibility", "hidden");
            }
        });
    } else {
        $('select[name="departamento"]').empty();
    }

  });

  //Actualiza la lista de municipios de acuerdo al departamento seleccionada
      $('select[name="departamento"]').on('change', function(){          
        var departamentoId = $(this).val();
        if(departamentoId) {
            $.ajax({
                url: "{{url('/municipio/get')}}/"+departamentoId,
                type:"GET",
                dataType:"json",
                beforeSend: function(){
                    $('#loader').css("visibility", "visible");
                },
                success:function(data) {
                    $('select[name="ciudad_nacimiento"]').empty();
                    $('select[name="ciudad_nacimiento"]').append('<option value=""> - Seleccione Ciudad de Nacimiento - </option>');
                    $.each(data, function(key, value){
                        $('select[name="ciudad_nacimiento"]').append('<option value="'+ value +'">' + key+ '</option>');
                    });
                },
                complete: function(){
                    $('#loader').css("visibility", "hidden");
                }
            });
        } else {
            $('select[name="ciudad_nacimiento"]').empty();
        }

      });



    //Actualiza la lista de municipios de acuerdo al departamento seleccionada
      $('select[name="depa_resi"]').on('change', function(){          
        var departamentoId = $(this).val();
        if(departamentoId) {
            $.ajax({
                url: "{{url('/municipio/get')}}/"+departamentoId,
                type:"GET",
                dataType:"json",
                beforeSend: function(){
                    $('#loader').css("visibility", "visible");
                },
                success:function(data) {
                    $('select[name="ciudad_resi"]').empty();
                    $('select[name="ciudad_resi"]').append('<option value=""> - Seleccione Ciudad de Nacimiento - </option>');
                    $.each(data, function(key, value){
                        $('select[name="ciudad_resi"]').append('<option value="'+ value +'">' + key+ '</option>');
                    });
                },
                complete: function(){
                    $('#loader').css("visibility", "hidden");
                }
            });
        } else {
            $('select[name="ciudad_resi"]').empty();
        }
      });


      $('select[name="ciudad_resi"]').on('change', function(){          
        var ciudadResiId = $(this).val();
        if(ciudadResiId == 149) {            
            $.ajax({
                url: "{{url('/subred/get')}}",
                type:"GET",
                dataType:"json",
                beforeSend: function(){
                    $('#loader').css("visibility", "visible");
                    $('#divSubred').css("visibility", "visible");
                    $('#divLocalidad').css("visibility", "visible");
                    $('#divUpz').css("visibility", "visible");
                    $('#divBarrio').css("visibility", "visible");
                    
                },
                success:function(data) {
                    $('select[name="subred"]').empty();
                    $('select[name="subred"]').append('<option value=""> - Seleccione Subred - </option>');
                    $.each(data, function(key, value){
                        $('select[name="subred"]').append('<option value="'+ value +'">' + key+ '</option>');
                    });
                },
                complete: function(){
                    $('#loader').css("visibility", "hidden");
                }
            });
        } else {
            $('select[name="subred"]').empty();
            $('select[name="localidad"]').empty();
            $('select[name="upz"]').empty();
            $('select[name="barrio"]').empty();
            $('#divSubred').css("visibility", "hidden");
            $('#divLocalidad').css("visibility", "hidden");
            $('#divUpz').css("visibility", "hidden");
            $('#divBarrio').css("visibility", "hidden");
        }
      });

      //Actualiza la lista de municipios de acuerdo al departamento seleccionada
      $('select[name="subred"]').on('change', function(){          
        var subred = $(this).val();
        if(subred) {
            $.ajax({
                url: "{{url('/localidad/get')}}/"+subred,
                type:"GET",
                dataType:"json",
                beforeSend: function(){
                    $('#loader').css("visibility", "visible");
                },
                success:function(data) {
                    $('select[name="localidad"]').empty();
                    $('select[name="localidad"]').append('<option value=""> - Seleccione Localidad - </option>');
                    $.each(data, function(key, value){
                        $('select[name="localidad"]').append('<option value="'+ value +'">' + key+ '</option>');
                    });
                },
                complete: function(){
                    $('#loader').css("visibility", "hidden");
                }
            });
        } else {
            $('select[name="localidad"]').empty();
        }
      });

      //Actualiza la lista de municipios de acuerdo al departamento seleccionada
      $('select[name="localidad"]').on('change', function(){          
        var localidad = $(this).val();
        if(localidad) {
            $.ajax({
                url: "{{url('/upz/get')}}/"+localidad,
                type:"GET",
                dataType:"json",
                beforeSend: function(){
                    $('#loader').css("visibility", "visible");
                },
                success:function(data) {
                    $('select[name="upz"]').empty();
                    $('select[name="upz"]').append('<option value=""> - Seleccione UPZ - </option>');
                    $.each(data, function(key, value){
                        $('select[name="upz"]').append('<option value="'+ value +'">' + key+ '</option>');
                    });
                },
                complete: function(){
                    $('#loader').css("visibility", "hidden");
                }
            });
        } else {
            $('select[name="upz"]').empty();
        } 
      });

      $('select[name="upz"]').on('change', function(){          
        var upz = $(this).val();
        alert(upz);
        if(upz) {
            $.ajax({
                url: "{{url('/barrio/get')}}/"+upz,
                type:"GET",
                dataType:"json",
                beforeSend: function(){
                    $('#loader').css("visibility", "visible");
                },
                success:function(data) {
                    $('select[name="barrio"]').empty();
                    $('select[name="barrio"]').append('<option value=""> - Seleccione Barrio - </option>');
                    $.each(data, function(key, value){
                        $('select[name="barrio"]').append('<option value="'+ value +'">' + key+ '</option>');
                    });
                },
                complete: function(){
                    $('#loader').css("visibility", "hidden");
                }
            });
        } else {
            $('select[name="barrio"]').empty();
        } 
      });

      



});


</script>


@endsection

