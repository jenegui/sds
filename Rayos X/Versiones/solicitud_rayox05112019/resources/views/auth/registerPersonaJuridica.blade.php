@extends('layouts.layout')

@section('content')


<h2 class="text-blue"><b>Registrar Persona Juridica</b></h2>

<form id="formSeccion1" action="{{route('registrar.personajuridica')}}" method="post">
  {{ csrf_field() }}   
   <!-- PRIMERA PARTE DEL REGISTRO DE LA SOLICITUD - LOCALIZACION tipoTramite  -->
   <div id="paso1" class="modal-content">

        <div class="modal-body newsletter">
        
        <div class="subtitle modal-header w-100">
            <h3><b>Datos Básicos:</b></h3>
        </div>

        
        <div class="row">

           <div class="col-6">
              <span class="text-orange">•</span><label for="p_nombre">Razón Social</label>              
              <input id="p_nombre" name="p_nombre" placeholder="Ingresar Primer Nombre" class="form-control input-md validate[required, minSize[4], maxSize[30]]" required type="text" value="" onkeyup="javascript:this.value=this.value.toUpperCase();" pattern="{4,30}"  minlength="4" maxlength="30" title="Ingresar un Tamaño mínimo: 4 carácteres a un Tamaño máximo: 30 carácteres">
           </div>
           <div class="col">
              <span class="text-orange">•</span><label for="tipo_identificacion">Tipo Documento</label>
              <select id="tipo_identificacion" name="tipo_identificacion" class="form-group form-group-lg validate[required]" required>
                 <option value=""> - Seleccione Tipo Documento -</option>
                 @foreach ($tipo_identificacion as $tipoIdentificacion => $value)                 
                 <option value="{{ $value}}">{{ $tipoIdentificacion}}</option>                  
                 @endforeach
              </select>
           </div>

           <div class="col">  
              <span class="text-orange">•</span><label for="nume_documento">NIT</label>
              <input id="nume_documento" name="nume_documento" placeholder="Ingresar Número Documento" class="form-control input-md validate[required, minSize[4], maxSize[15]]" required type="number" onKeyPress="if(this.value.length==15) return false;">
           </div>

        </div>

        <div class="subtitle modal-header w-100">
            <h3><b>Representante Legal:</b></h3>
        </div>

        <div class="row">

           <div class="col">
              <span class="text-orange">•</span><label for="nombre_rp">Primer Nombre</label>              
              <input id="nombre_rp" name="nombre_rp" placeholder="Ingresar Primer Nombre" class="form-control input-md validate[required, minSize[4], maxSize[30]]" required type="text" value="" onkeyup="javascript:this.value=this.value.toUpperCase();" pattern="{4,30}"  minlength="4" maxlength="30" title="Ingresar un Tamaño mínimo: 4 carácteres a un Tamaño máximo: 30 carácteres">
           </div>

           <div class="col">
              <span class="text-orange">•</span><label for="s_nombre">Segundo Nombre</label>
              <input id="s_nombre" name="s_nombre" placeholder="Ingresar Segundo Nombre" class="form-control input-md validate[minSize[4], maxSize[30]]"  type="text" value="" onkeyup="javascript:this.value=this.value.toUpperCase();" pattern="{4,30}"  minlength="4" maxlength="30" title="Ingresar un Tamaño mínimo: 4 carácteres a un Tamaño máximo: 30 carácteres">
           </div>

           <div class="col">
              <span class="text-orange">•</span><label for="p_apellido">Primer Apellido</label>
              <input id="p_apellido" name="p_apellido" placeholder="Ingresar Primer Apellido" class="form-control input-md validate[required, minSize[4], maxSize[30]]" required type="text" value="" onkeyup="javascript:this.value=this.value.toUpperCase();" pattern="{4,30}"  minlength="4" maxlength="30" title="Ingresar un Tamaño mínimo: 4 carácteres a un Tamaño máximo: 30 carácteres">
           </div>
           <div class="col">
              <span class="text-orange">•</span><label for="s_apellido">Segundo Apellido</label>
              <input id="s_apellido" name="s_apellido" placeholder="Ingresar Segundo Apellido" class="form-control input-md validate[minSize[4], maxSize[30]]" type="text" value="" onkeyup="javascript:this.value=this.value.toUpperCase();" pattern="{4,30}"  minlength="4" maxlength="30" title="Ingresar un Tamaño mínimo: 4 carácteres a un Tamaño máximo: 30 carácteres">
           </div>
        </div>

         <div class="row">
           <div class="col">
              <span class="text-orange">•</span><label for="tipo_iden_rp">Tipo Documento</label>
              <select id="tipo_iden_rp" name="tipo_iden_rp" class="form-group form-group-lg validate[required]" required>
                 <option value=""> - Seleccione Tipo Documento -</option>
                 @foreach ($tipo_identificacion as $tipoIdentificacion => $value)
                 @if($value!=5)
                 <option value="{{ $value}}">
                    {{ $tipoIdentificacion}}</option>
                  @endif
                 @endforeach
              </select>
           </div>

           <div class="col">  
              <span class="text-orange">•</span><label for="nume_iden_rp">Número Documento</label>
              <input id="nume_iden_rp" name="nume_iden_rp" placeholder="Ingresar Número Documento" class="form-control input-md validate[required, minSize[4], maxSize[15]]" required type="number" onKeyPress="if(this.value.length==15) return false;">
           </div>

           <div class="col">
               <span class="text-orange">•</span><label for="email">Correo Electrónico</label>
               <input id="email" name="email" placeholder="Ingresar Correo Electrónico" class="form-control validate[required, custom[email]]" type="email" value="" required>
            </div>

            <div class="col">
               <span class="text-orange">•</span><label for="email-confirma">Confirmar Correo Electrónico</label>
               <input id="email-confirma" name="email-confirma" placeholder="Ingresar Correo Electrónico" class="form-control validate[required, custom[email]]" type="email" value="" required>
            </div>
           
        </div>



        <div class="row">

            <div class="col">
               <span class="text-orange">•</span><label for="telefono_fijo">Teléfono fijo</label>
               <input id="telefono_fijo" name="telefono_fijo" placeholder="Ingresar Número telefonico de contacto en sede de la Instalación" class="form-control input-md validate[required, minSize[7], maxSize[15]]" type="number" value=""  pattern="[0-9]{7,15}" minlength="7" maxlength="15"  title="Ingresar un Tamaño mínimo: 7 carácteres a un Tamaño máximo: 15 carácteres" onKeyPress="if(this.value.length==15) return false;">
            </div>

            <div class="col">
               <span class="text-orange">•</span><label for="telefono_celular">Teléfono celular(*)</label>
               <input id="telefono_celular" name="telefono_celular" placeholder="Ingresar Número Celular de contacto en sede de la Instalación" class="form-control input-md validate[required, minSize[10], maxSize[15]]" type="number" value="" pattern="[0-9]{10,15}" minlength="10" maxlength="15"  title="Ingresar un Tamaño mínimo: 10 carácteres a un Tamaño máximo: 15 carácteres" onKeyPress="if(this.value.length==15) return false;">
            </div>      


        </div>

        <div class="row">
          <div class="form-group row">
              <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Contraseña') }}</label>

              <div class="col-md-6">
                  <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                  @error('password')
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $message }}</strong>
                      </span>
                  @enderror
              </div>
          </div>

          <div class="form-group row">
              <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirmar Contraseña') }}</label>

              <div class="col-md-6">
                  <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
              </div>
          </div>
        </div>


         


        </div>


<div id="btnRegistrar" class="col-md-12 pt-200">
            <p align="center">               
               <button type="submit" class="btn yellow">
                  Registrarse
               </button>            
               
            </p>
        </div>
    </div>
</form>



@endsection


